package bmwModules;

import java.util.List;

import baseClass.AppDriver;
import constants.ElementConstants;

public class Payment extends AppDriver{

	static String propFileName = "src/elements/Locators.properties";	
	public Payment() 
	{
		super(propFileName);
		// TODO Auto-generated constructor stub
	}
	public String MakePayment(List<String> tmpAllModels) throws InterruptedException
	{
		String ret_Msg = "";
		//tmpAllModels.set(60, "Error in Make Payment");
		sleep(4000);
		scrollDown();
		scrollDown();
		sleep(5000);
		click(getElement(ElementConstants.BuyOnlineButton));
		sleep(30000);		
		click(getElement(ElementConstants.MakePaymentchkbox));
		sleep(5000);
		click(getElement(ElementConstants.MakePayment));
			
		sleep(9000);
		
		//sleep(2000);
		click(getElement(ElementConstants.NetbankingPopupClick));
		sleep(5000);
		switchwindow();
		//sleep(2000);
		//Going to Payment Screen. 
		maximizeScreen();		
		sleep(5000);
		//dropDownByIndex(getElement(ElementConstants.BMWNETBANKINGDEMOselectBank),2);
		click(getElement(ElementConstants.BMWNETBANKINGDEMOselectBank));
		sleep(4000);
		click(getElement(ElementConstants.BankTestBank));
		sleep(4000);
		click(getElement(ElementConstants.BMWNetBankingsubmit));
		sleep(5000);
		setValue(getElement(ElementConstants.CustomerID), "test");
		setValue(getElement(ElementConstants.TransactionPassword), "test");
		sleep(2000);
		click(getElement(ElementConstants.Submit));
		click(getElement(ElementConstants.SubmitAmount));
		sleep(2000);
		//back to main Screen
		switchbacktoMainWindow();
		sleep(2000);
		//String ScreenMessage = getText(getElement(ElementConstants.PurchaseComplete));
	//	click(getElement(ElementConstants.ViewMyBMW));
		//System.out.println("ScreenMessage :" + ScreenMessage);
		//ScreenMessage = ReplaceSplCharbyWhiteSpace(ScreenMessage);
		ret_Msg = "Payment Completed";
		/*
		if (ScreenMessage == "Congratulations!"){							  
			System.out.println("AM Payment Completed");
			ret_Msg = "Payment Completed";
		}else{
			ret_Msg = "Error during Payment Process, please verify";
			System.out.println("AM Error during Payment Process, please verify");
		}
		*/
		
		//tmpAllModels.set(60, ret_Msg);
		return ret_Msg;
	}
	
}
