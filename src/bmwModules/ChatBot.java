package bmwModules;

import java.io.IOException;
import java.util.List;

import baseClass.AppDriver;
import constants.ElementConstants;

public class ChatBot extends AppDriver {
	//static String propFileName = "D:\\sampledemo\\metadata recommenders and Automation\\Automation\\src\\elements\\Locators.properties";
			static String propFileName = "src/elements/Locators.properties";	
	public ChatBot() throws IOException, InterruptedException{
		super(propFileName);
	}
	
	public String InputChatBot(List<String> strTemp) throws IOException, InterruptedException{
		String retMessage = "";
		String Message = strTemp.get(5);
		String Response = strTemp.get(6);
		
		sleep(2000);
		System.out.println("check1");
		//hoveronElement(getElement(ElementConstants.BMWOnlineGenius));
		click(getElement(ElementConstants.BMWOnlineGenius));
		System.out.println("check2");
		setValue(getElement(ElementConstants.MessageTextArea), Message);
		click(getElement(ElementConstants.MessageSend));
		sleep(3000);
		//if(Response.length()>0){
		  //    System.out.println("Test Pass");
		//}
		click(getElement(ElementConstants.MessageRecive));
		switchwindow();
		String currentUrl=getcurrenturl();
		//System.out.println("the currenet URL="+currentUrl);
		
		if(strTemp.get(6).equals(currentUrl))
		{
			System.out.println("the currenet URL="+currentUrl);
			//retMessage =  getText(getElement(ElementConstants.ChatWindow));
			retMessage = currentUrl;
			retMessage = "Test Results As Expected :" + retMessage;
			strTemp.set(7,retMessage);
		}
		else 
		{
			System.out.println("URL is not same"+ currentUrl);
			retMessage = currentUrl;
			retMessage = "THE URL NOT CORRECT:" + retMessage;
			strTemp.set(7,retMessage);
		}
		
		switchbacktoMainWindow();
		
		click(getElement(ElementConstants.Chatwindowclose));
		return retMessage;
	}		
}