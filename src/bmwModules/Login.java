package bmwModules;

import java.io.IOException;
import java.util.List;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import baseClass.AppDriver;
import baseClass.ExecutionLog;
import constants.ElementConstants;

public class Login extends AppDriver {

	
	static String propFileName = "src/elements/Locators.properties";								
	     ExecutionLog Elog=new ExecutionLog();
			public Login() throws IOException, InterruptedException
			{
				super(propFileName);
			}
	
	public void BMWLogout() 
		{
		
		sleep(5000);
		scrollup();
		click(getElement(ElementConstants.LOGOUT));
		sleep(2000);
		}
	
	
	
	public String BMWLogin(List<String> strTemp) 
	
	{	
		try 
		{		
			Elog.createLog();
			Clearcookies();
			Elog.printLog("Start of BMW Login");
			//System.out.println("Start of BMW Login");
			String BMW_UserID	 = strTemp.get(7);
			String BMW_Password  = strTemp.get(8);	
			String Message  = strTemp.get(10);	
			String ret_Message = "";
			sleep(2000);
			
			click(getElement(ElementConstants.BMWLOGINCLICK));
			Elog.printLog("Login link clicked");
			sleep(2000);
			setValue(getElement(ElementConstants.USERNAME), BMW_UserID);
			Elog.printLog("User name entered ");
			sleep(2000);
			setValue(getElement(ElementConstants.PASSWORD), BMW_Password);
			sleep(2000);
			Elog.printLog("password entered");
			click(getElement(ElementConstants.LOGINBOTCHECK));
			sleep(2000);
			Elog.printLog("check box selected");
			click(getElement(ElementConstants.LOGINBUTTON));
			Elog.printLog("Login button selected");
			sleep(5000);
			
			String url = getcurrenturl();
			sleep(500);
			System.out.println("URL: " + url);
			Elog.printLog("login URL."+url);

			sleep(500);
//			if(url.equals("https://test.shop.bmw.in/#/home")||url.equals("https://test.shop.bmw.co.th/#/home"))
			if(url.equals("https://qa.shop.bmw.in/in/#/home")||url.equals("https://qa.shop.bmw.in/th/#/home")||url.equals("https://test.shop.bmw.in/#/home")||url.equals("https://test.shop.bmw.co.th/#/home"))
			{
				
				//System.out.println("Element :" + getElement(ElementConstants.LGNSUCCESSMSG));
				String test =getText(getElement(ElementConstants.LGNSUCCESSMSG));
				ret_Message = "Successfuly login to the system."+test;
				
				Elog.printLog("Successfuly login to the system.");
				Elog.printLog(" Sucess Message"+ret_Message);
				System.out.println(ret_Message);
				}//else if (url.equals("https://test.shop.bmw.in/#/login?returnUrl=%2Fhome")||url.equals("https://test.shop.bmw.co.th/#/login?returnUrl=%2Fhome"))
				else if (url.equals("https://qa.shop.bmw.in/in/#/login?returnUrl=%2Fhome")||url.equals("https://qa.shop.bmw.in/th/#/login?returnUrl=%2Fhome")||url.equals("https://test.shop.bmw.in/#/login?returnUrl=%2Fhome")||url.equals("https://test.shop.bmw.co.th/#/login?returnUrl=%2Fhome"))
				{ 
				ret_Message = getText(getElement(ElementConstants.LGNFAILUREMSG));
				Elog.printLog("Not able to login the system.");
				Elog.printLog(" Message"+ret_Message);
				System.out.println(ret_Message);
				}
				else 
				{
					ret_Message = "Error Please Verify";
				}
			
			//strTemp.set(7,ret_Message);
			System.out.println("Login ret_Message:" + ret_Message); 
			Elog.printLog("Login ret_Message:" + ret_Message); 
			
			
			return ret_Message;
			
			
		} 
		catch (Exception e) 
		{
			System.out.println("elemnt issue ");	
			//strTemp.set(7,"Exception Error");
			return "exception"+e;
			//Assert.assertEquals(true, false);
			// TODO: handle exception
		}
		
	}
	
}
