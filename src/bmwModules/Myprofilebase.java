package bmwModules;

import java.io.IOException;
import java.util.List;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import baseClass.AppDriver;
import baseClass.ExecutionLog;
import baseClass.ReadExcel;
import constants.ElementConstants;

public class Myprofilebase extends AppDriver  

{
	
	static String propFileName = "src/elements/Locators.properties";								
	
	public Myprofilebase() throws IOException, InterruptedException
	{
		super(propFileName);
	}

	public String logintoapplication(List<List<String>> tmpLogin) throws IOException, InterruptedException
	{
		String ret_Message = "";
		ret_Message=LaunchBrowserLoginAndGetLoginList(tmpLogin);
		return ret_Message; 
	}
	
	
	public String  myprofileselect()
	{
		sleep(2000);
		click(getElement(ElementConstants.MYPROFILE));
		sleep(5000);
		scrollDown();
		sleep(2000);
		scrollDown();
		scrollDown();
		sleep(2000);
		click(getElement(ElementConstants.MYPROFILEEDIT));
		sleep(2000);
		System.out.println("edit");
		scrollup();
		return "PASS";
		
	}
	
	public String  Title (List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String Title=strTemp.get(30);
		String profile_Title_FieldName = getElement(ElementConstants.TITLEFIELD);
        String profile_Title = getElement(ElementConstants.TITLE);
         ret_Message =selectValueProfileDropdownFields(profile_Title_FieldName,profile_Title,Title);
             ExecutionLog.printLog("titile" +ret_Message);
             sleep(1000);
             ExecutionLog.printLog("title finished ");
             return ret_Message;
         
         
	}
	
	
	public String  Firstname (List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		 String Firstname  = strTemp.get(31);
		 String profile_Firstname_FieldName = getElement(ElementConstants.FIRSTNAMEFIELD);
         String profile_Firstname = getElement(ElementConstants.FIRSTNAME);
             ret_Message =setValueProfileTextFields(profile_Firstname_FieldName,profile_Firstname,Firstname);
             ExecutionLog.printLog("firstname" +ret_Message);
             sleep(1000);
             ExecutionLog.printLog("firstname finished ");
             return ret_Message;
         
         
	}
	
	public String Lastname(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String Lastname  = strTemp.get(32);
		
		String profile_Lastname_Fieldname = getElement(ElementConstants.LASTNAMEFIELD);
        String profile_Lastname = getElement(ElementConstants.LASTNAME);
        ret_Message = setValueProfileTextFields(profile_Lastname_Fieldname,profile_Lastname,Lastname);
        sleep(1000);
        ExecutionLog.printLog("lastname" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("lastname finished ");
        return ret_Message;
        
	}
	
	public String Pincode(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String Zipcode  = strTemp.get(33);
		
		String profile_Pincode_Fieldname = getElement(ElementConstants.PINCODEFIELD);
        String profile_Pincode = getElement(ElementConstants.PINCODE);
        ret_Message = setValueProfileTextFields(profile_Pincode_Fieldname,profile_Pincode,Zipcode);
        sleep(1000);
        ExecutionLog.printLog("pincode" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("pincode finished ");
        return ret_Message;
        
	}
	 
	//India
	public String State(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String state= strTemp.get(34);
	          String profile_State_Fieldname = getElement(ElementConstants.STATEFIELD);
			  String profile_State = getElement(ElementConstants.STATE);
	         
			  if(existsElement(profile_State_Fieldname) && existsElement(profile_State)){
				  ret_Message ="State field is displayed";
		    	} else {
		    	ExecutionLog.printLog("Edit Profile-State field is not displayed");
		    	}	
	        return ret_Message;    
	    }
	    
	    public String City(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String city= strTemp.get(35);
	          String profile_City_Fieldname = getElement(ElementConstants.CITYFIELD);
			  String profile_City = getElement(ElementConstants.CITY);
	         
	           ret_Message = selectValueProfileDropdownFields(profile_City_Fieldname,profile_City,city);
	           ExecutionLog.printLog("City" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("City finished ");
	        return ret_Message;    
	    }
	    
	    public String Notification(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Notifications_Chosen= strTemp.get(36);
	          
			  String profile_Notificaionemail = getElement(ElementConstants.NOTIFICATIONEMAIL);
			  String profile_Notificaionsms = getElement(ElementConstants.NOTIFICATIONSMS);
	         
			  ret_Message =selectCheckBoxNotification(profile_Notificaionemail,profile_Notificaionsms,Notifications_Chosen);
	           ExecutionLog.printLog("Notifications_Chosen" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Notifications_Chosen finished");
	        return ret_Message;    
	    } 
		
	    
	    
	    
	
	    public String Addressline1(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Address_Line_1= strTemp.get(37);
	          
	          String profile_Address1_Fieldname = getElement(ElementConstants.ADDRESS1FIELD);
			  String profile_Address1 = getElement(ElementConstants.ADDRESS1);
	         
	           ret_Message = setValueProfileTextFields(profile_Address1_Fieldname,profile_Address1,Address_Line_1);
	           ExecutionLog.printLog("Address_Line_1" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Address_Line_1_Chosen finished");
	        return ret_Message;    
	    }
	    
	    public String Addressline2(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Address_Line_2= strTemp.get(38);
	          
	          String profile_Address2_Fieldname = getElement(ElementConstants.ADDRESS2FIELD);
			  String profile_Address2 = getElement(ElementConstants.ADDRESS2);
	           
			  scrollDown();
		    	sleep(500);
	           ret_Message = setValueProfileTextFields(profile_Address2_Fieldname,profile_Address2,Address_Line_2);
	           ExecutionLog.printLog("Address_Line_2" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Address_Line_2_Chosen finished");
	        return ret_Message;    
	    }
	    
	    public String Addressline3(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Address_Line_3= strTemp.get(39);
	          
	          String profile_Address3_Fieldname = getElement(ElementConstants.ADDRESS3FIELD);
			  String profile_Address3 = getElement(ElementConstants.ADDRESS3);
	         
	           ret_Message = setValueProfileTextFields(profile_Address3_Fieldname,profile_Address3,Address_Line_3);
	           ExecutionLog.printLog("Address_Line_3" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Address_Line_3_Chosen finished");
	        return ret_Message;    
	    }
	    
	    public String BirthdayIN(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String bdate=strTemp.get(40);
	          String bmonth=strTemp.get(41);
	          String Byear=strTemp.get(42);
	          
	          String profile_bdate = getElement(ElementConstants.BDATE);
	          String profile_bmonth = getElement(ElementConstants.BMONTH);
	          String profile_byear = getElement(ElementConstants.BYEAR);
	          click(getElement(ElementConstants.BIRTHDAY));
	          sleep(2000);

	          dropDownByIndex(profile_bmonth,3);
	          sleep(5000);
	          dropDownByIndex(profile_byear,4);
	          sleep(5000);
	          //selectByValue(profile_bdate,bdate);
	          //dropDownByValue(profile_byear,Byear);
	          //ClickDropdownValues(profile_byear,Byear);
	          //sleep(5000);
	          
	          click(profile_bdate);
	          //sleep(2000);
	          ret_Message="date added";
	          
	          return ret_Message;
	          
	    }
	    

	    
	    public String Gender(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Gender= strTemp.get(43);
	          
	          String profile_Gender_Fieldname = getElement(ElementConstants.GENDERFIELD);
			  String profile_Gender = getElement(ElementConstants.GENDER);
	         
			  ret_Message = selectValueProfileDropdownFields(profile_Gender_Fieldname,profile_Gender,Gender);
	           ExecutionLog.printLog("Gender" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Gender finished");
	        return ret_Message;    
	    }
	    
	   /* public String Weddingdate(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Wedding_Anniversary_Date= strTemp.get(44);
	          
	          String profile_Weddinganniversarydate = getElement(ElementConstants.WEDDINGANNIVERSARYDATE);
	         
	           ret_Message = selectDateCalender(profile_Weddinganniversarydate);
	           ExecutionLog.printLog("Weddinganniversarydate" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Weddinganniversarydate finished");
	        return ret_Message;    
	    }*/
	    
	    public String Occupation(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Occupation= strTemp.get(45);
	          
	          String profile_Occupation_Fieldname = getElement(ElementConstants.OCCUPATIONFIELD);
				String profile_Occupation = getElement(ElementConstants.OCCUPATION);
	         
				ret_Message = selectValueProfileDropdownFields(profile_Occupation_Fieldname,profile_Occupation,Occupation);
	           ExecutionLog.printLog("Gender" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Gender finished");
	        return ret_Message;    
	    }
	    
	    public String Profession(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Profession= strTemp.get(46);
	          
	          String profile_Profession_Fieldname = getElement(ElementConstants.PROFESSIONFIELD);
				String profile_Profession = getElement(ElementConstants.PROFESSION);
	         
				ret_Message = selectValueProfileDropdownFields(profile_Profession_Fieldname,profile_Profession,Profession);
	           ExecutionLog.printLog("Profession" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Profession finished");
	        return ret_Message;    
	    }
	    
	    public String Hobbies(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Your_Hobbies_Interests_values= strTemp.get(47);
	          
	          String profile_Your_Hobbies_Interest_Fieldname = getElement(ElementConstants.YOURHOBBIESINTERETSVALUESFIELD);
				String profile_Your_Hobbies_Interest = getElement(ElementConstants.YOURHOBBIESINTERETSVALUES);
	         
				ret_Message = setValueProfileTextFields(profile_Your_Hobbies_Interest_Fieldname,profile_Your_Hobbies_Interest,Your_Hobbies_Interests_values);
	           ExecutionLog.printLog("Your_Hobbies_Interests_values" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Your_Hobbies_Interests_values finished");
	        return ret_Message;    
	    }

	    public String Otherbrandscars(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String What_other_Brands_Cars_did_you_consider_value= strTemp.get(48);
	          
	          String profile_Brands_Cars_Fieldname = getElement(ElementConstants.BRANDSCARSFIELD);
				String profile_Brands_Cars = getElement(ElementConstants.BRANDSCARS);
	         
				ret_Message = setValueProfileTextFields(profile_Brands_Cars_Fieldname,profile_Brands_Cars,What_other_Brands_Cars_did_you_consider_value);
	           ExecutionLog.printLog("What_other_Brands_Cars_did_you_consider_value" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("What_other_Brands_Cars_did_you_consider_value finished");
	        return ret_Message;    
	    }
	    
	    public String Primarycar(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Will_this_be_your_Primary_Car= strTemp.get(49);
	          
	          String profile_Primarycar_Fieldname = getElement(ElementConstants.PRIMARYCARFIELD);
				String profile_Primarycar = getElement(ElementConstants.PRIMARYCAR);
				
				scrollDown();
				dropDownByIndex(profile_Primarycar,1);
//				ret_Message = selectValueProfileDropdownFields(profile_Primarycar_Fieldname,profile_Primarycar,Will_this_be_your_Primary_Car);
	           ExecutionLog.printLog("Will_this_be_your_Primary_Car" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Will_this_be_your_Primary_Car finished");
	        return ret_Message;    
	    }
		
	    public String Brand_1(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Brand_1= strTemp.get(50);
	          
	          String profile_Brand1_Fieldname = getElement(ElementConstants.BRAND1FIELD);
				String profile_Brand1 = getElement(ElementConstants.BRAND1);
	         
				ret_Message = setValueProfileTextFields(profile_Brand1_Fieldname,profile_Brand1,Brand_1);
	           ExecutionLog.printLog("Brand_1" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Brand_1 finished");
	        return ret_Message;    
	    }
	    
	    public String Model_1(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Model_1= strTemp.get(51);
	          
	          String profile_Model1_Fieldname = getElement(ElementConstants.MODEL1FIELD);
				String profile_Model1 = getElement(ElementConstants.MODEL1);
	         
				ret_Message = setValueProfileTextFields(profile_Model1_Fieldname,profile_Model1,Model_1);
	           ExecutionLog.printLog("Model_1" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Model_1 finished");
	        return ret_Message;    
	    }
	    
	    public String Brand_2(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Brand_2= strTemp.get(52);
	          
	          String profile_Brand2_Fieldname = getElement(ElementConstants.BRAND2FIELD);
				String profile_Brand2 = getElement(ElementConstants.BRAND2);
	         
				ret_Message = setValueProfileTextFields(profile_Brand2_Fieldname,profile_Brand2,Brand_2);
	           ExecutionLog.printLog("Brand_1" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Brand_1 finished");
	        return ret_Message;    
	    }
	    
	    public String Model_2(List<String> strTemp) throws IOException
	    {
	          String ret_Message = "";
	          String Model_2= strTemp.get(53);
	          
	          String profile_Model2_Fieldname = getElement(ElementConstants.MODEL2FIELD);
				String profile_Model2 = getElement(ElementConstants.MODEL2);
	         
			  ret_Message = setValueProfileTextFields(profile_Model2_Fieldname,profile_Model2,Model_2);
	           ExecutionLog.printLog("Model_2" +ret_Message);
	           sleep(1000);
	           ExecutionLog.printLog("Model_2 finished");
	        return ret_Message;    
	    }

	
	
	
	
	
	
	
	
	
	    
	    
	    
	    //Thailand 
	public String Amphur(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String Amphur= strTemp.get(54);

        String profile_AmphurField=getElement(ElementConstants.AMPHURFIELD);
		String profile_Amphur=getElement(ElementConstants.AMPHUR);
		
		ret_Message = selectValueProfileDropdownFields(profile_AmphurField,profile_Amphur,Amphur);
        sleep(1000);
        ExecutionLog.printLog("Amphur" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("Amphur finished ");
        return ret_Message;
        
	}
	
	public String District(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String District =strTemp.get(55);

		String profile_DistrictField= getElement(ElementConstants.DISTRICTFIELD);
		String profile_District=getElement(ElementConstants.DISTRICT);
		
		ret_Message = selectValueProfileDropdownFields(profile_DistrictField,profile_District,District);
        sleep(1000);
        ExecutionLog.printLog("District" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("District finished ");
        return ret_Message;
        
	}
	
	public String LineId(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String LineID=strTemp.get(56);

	    String profile_LineIDField=getElement(ElementConstants.LINEIDFIELD);
		String profile_LineID=getElement(ElementConstants.LINEID);
		
		ret_Message = setValueProfileTextFields(profile_LineIDField,profile_LineID,LineID);
        sleep(1000);
        ExecutionLog.printLog("LineId" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("Lineid finished ");
        return ret_Message;
        
	}
	
	public String Numberoffamilymembers(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String Numberoffamilymembers=strTemp.get(57);

		String profile_NumberoffamilymembersField=getElement(ElementConstants.NUMBEROFFAMILYMEMBERSFIELD);
		String profile_Numberoffamilymembers=getElement(ElementConstants.NUMBEROFFAMILYMEMBERS);
		
		ret_Message = setValueProfileTextFields(profile_NumberoffamilymembersField,profile_Numberoffamilymembers,Numberoffamilymembers);
        sleep(1000);
        ExecutionLog.printLog("Numberoffamilymembers" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("Numberoffamilymembers finished ");
        return ret_Message;
        
	}
	
	public String Maritalstatus(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String MaritalStatus=strTemp.get(58);

		String profile_MaritalStatusField=getElement(ElementConstants.MARITALSTATUSFIELD);
		String profile_MaritalStatus=getElement(ElementConstants.MARITALSTATUS);
		
		ret_Message = selectValueProfileDropdownFields(profile_MaritalStatusField,profile_MaritalStatus,MaritalStatus);
        sleep(1000);
        ExecutionLog.printLog("Maritalstatus" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("Maritalstatus finished ");
        return ret_Message;
        
	}
	
	public String MotiveOfPurchase(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		scrollDown();
		String MotiveOfPurchase=strTemp.get(59);
		
		String profile_MotiveOfPurchaseField=getElement(ElementConstants.MOTIVEOFPURCHASEFIELD);
		String profile_MotiveOfPurchase=getElement(ElementConstants.MOTIVEOFPURCHASE);
		
		ret_Message = setValueProfileTextFields(profile_MotiveOfPurchaseField,profile_MotiveOfPurchase,MotiveOfPurchase);
        sleep(1000);
        ExecutionLog.printLog("MotiveOfPurchase" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("MotiveOfPurchase finished ");
        return ret_Message;
        
	}
	
	
	public String PreviousBrand(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String PreviousBrand=strTemp.get(60);

		String profile_PreviousBrandField=getElement(ElementConstants.PREVIOUSBRANDFIELD);
		String profile_PreviousBrand=getElement(ElementConstants.PREVIOUSBRAND);
		
		ret_Message = setValueProfileTextFields(profile_PreviousBrandField,profile_PreviousBrand,PreviousBrand);
        sleep(1000);
        ExecutionLog.printLog("PreviousBrand" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("PreviousBrand finished ");
        return ret_Message;
        
	}
	
	public String YourHobbiesInterestsvalues(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String YourHobbiesInterestsvalues=strTemp.get(61);

        String profile_Your_Hobbies_Interest_Fieldname = getElement(ElementConstants.YOURHOBBIESINTERETSVALUESFIELD_TH);
        String profile_Your_Hobbies_Interest = getElement(ElementConstants.YOURHOBBIESINTERETSVALUES_TH);
		
        ret_Message = setValueProfileTextFields(profile_Your_Hobbies_Interest_Fieldname,profile_Your_Hobbies_Interest,YourHobbiesInterestsvalues);
        sleep(1000);
        ExecutionLog.printLog("YourHobbiesInterestsvalues" +ret_Message);
        sleep(1000);
        ExecutionLog.printLog("YourHobbiesInterestsvalues finished ");
        return ret_Message;
        
	}
	
	public void updateprfile()
	{
		sleep(2000);
		scrollDown();
		click(getElement(ElementConstants.UPDATE));
		sleep(5000);
		click(getElement(ElementConstants.UpdateOK));
	}
	
	/*public String Birthday(List<String> strTemp) throws IOException
	{
		String ret_Message = "";
		String bdate=strTemp.get(15);
		String bmonth=strTemp.get(16);
		String Byear=strTemp.get(17);
		scrollDown();
		
		String profile_bdate = getElement(ElementConstants.BDATE);
		String profile_bmonth = getElement(ElementConstants.BMONTH);
		String profile_byear = getElement(ElementConstants.BYEAR);
		click(getElement(ElementConstants.BIRTHDAY));
		sleep(2000);
		//click(getElement(ElementConstants.BIRTHDAY));
		//String bir=getElement(ElementConstants.BIRTHDAY);
		//click(profile_bmonth);
		//dropDownByValue(profile_bmonth,bmonth);
		//ClickDropdownValues(profile_bmonth,bmonth);
		//click(profile_byear);
		dropDownByIndex(profile_bmonth,3);
		sleep(5000);
		dropDownByIndex(profile_byear,3);
		sleep(5000);
		//selectByValue(profile_bdate,bdate);
		//dropDownByValue(profile_byear,Byear);
		//ClickDropdownValues(profile_byear,Byear);
		//sleep(5000);
		
		click(profile_bdate);
		//sleep(2000);
		ret_Message="date added";
		
		return ret_Message;
		
	}*/
	
	public String MyprofileEditIN(List<String> strTemp) throws IOException 
	{
		String rm ="";
		myprofileselect();
		rm = Title(strTemp);
		rm = Firstname(strTemp);
		rm = Lastname(strTemp);
		rm = Pincode(strTemp);
		rm = State(strTemp);
		rm = City(strTemp);
		rm = Notification(strTemp);
		rm = Addressline1(strTemp);
		rm = Addressline2(strTemp);
		rm = Addressline3(strTemp);
		//rm = Birthday_IN(strTemp);
		rm = Gender(strTemp);
		rm = Occupation(strTemp);
		rm = Profession(strTemp);
		//rm = Hobbies_IN(strTemp);
		rm = Otherbrandscars(strTemp);
		rm = Primarycar(strTemp);
		rm = Brand_1(strTemp);
		rm = Model_1(strTemp);
		rm = Brand_2(strTemp);
		rm = Model_2(strTemp);
		
		updateprfile();
		return "pass";
	}
	
	
	public String MyprofileEditTH(List<String> strTemp) throws IOException 
	{
		String rm ="";
		rm = myprofileselect();
				rm = Title(strTemp);
				rm = Firstname(strTemp);
				rm = Lastname(strTemp);
				rm = Pincode(strTemp);
				rm = Amphur(strTemp);
				rm = District(strTemp);
				rm = LineId(strTemp);
				rm = Notification(strTemp);
				rm = Numberoffamilymembers(strTemp);

				rm = Maritalstatus(strTemp);
				rm = MotiveOfPurchase(strTemp);
				rm = PreviousBrand(strTemp);
				rm = YourHobbiesInterestsvalues(strTemp);
				
				updateprfile();
				return "pass";
			}
			
	
 	/*public String MyprofileEdit(List<String> strTemp) 
    
    {     
          try 
          {
                Clearcookies();
                System.out.println("Start of Myprofile edit");
                String Title      = strTemp.get(5);
                String Firstname  = strTemp.get(6);
                String Lastname  = strTemp.get(7);  
                String pincode  = strTemp.get(8);
                String state= strTemp.get(9);
                String city= strTemp.get(10);
                String Notifications_Chosen= strTemp.get(11);
                String Address_Line_1= strTemp.get(12);
                String Address_Line_2= strTemp.get(13);
                String Address_Line_3= strTemp.get(14);
                String Birthday= strTemp.get(15);
                String Gender= strTemp.get(16);
                String Wedding_Anniversary_Date= strTemp.get(17);
                String Occupation= strTemp.get(18);
                String Profession= strTemp.get(19);
                String Your_Hobbies_Interests_values= strTemp.get(20);
                String What_other_Brands_Cars_did_you_consider_value= strTemp.get(21);
                String Will_this_be_your_Primary_Car= strTemp.get(22);
                String Brand_1= strTemp.get(23);
                String Model_1= strTemp.get(24);
                String Brand_2= strTemp.get(25);
                String Model_2= strTemp.get(26);
                
                String profile_Title_FieldName = getElement(ElementConstants.TITLEFIELD);
                String profile_Title = getElement(ElementConstants.TITLE);
       		 String profile_Firstname_FieldName = getElement(ElementConstants.FIRSTNAMEFIELD);
             String profile_Firstname = getElement(ElementConstants.FIRSTNAME);
                
                String profile_Lastname_Fieldname = getElement(ElementConstants.LASTNAMEFIELD);
                String profile_Lastname = getElement(ElementConstants.LASTNAME);
                String profile_Pincode_Fieldname = getElement(ElementConstants.PINCODEFIELD);
                String profile_Pincode = getElement(ElementConstants.PINCODE);
                String profile_Notificaionchosen_Fieldname = getElement(ElementConstants.NOTIFICATIONCHOSENFIELD);
                String profile_Notificaionemail_Fieldname = getElement(ElementConstants.NOTIFICATIONEMAIL);
                String profile_Notificaionsms_Fieldname = getElement(ElementConstants.NOTIFICATIONSMS); 
                
                //Indian 
                String profile_State_Fieldname = getElement(ElementConstants.STATEFIELD);
                String profile_State = getElement(ElementConstants.STATE);
                String profile_City_Fieldname = getElement(ElementConstants.CITYFIELD);
                String profile_City = getElement(ElementConstants.CITY);
                String profile_Address1_Fieldname = getElement(ElementConstants.ADDRESS1FIELD);
                String profile_Address1 = getElement(ElementConstants.ADDRESS1);
                String profile_Address2_Fieldname = getElement(ElementConstants.ADDRESS2FIELD);
                String profile_Address2 = getElement(ElementConstants.ADDRESS2);
                String profile_Address3_Fieldname = getElement(ElementConstants.ADDRESS3FIELD);
                String profile_Address3 = getElement(ElementConstants.ADDRESS3);
                String profile_Gender_Fieldname = getElement(ElementConstants.GENDERFIELD);
                String profile_Gender = getElement(ElementConstants.GENDER);
                String profile_Occupation_Fieldname = getElement(ElementConstants.OCCUPATIONFIELD);
                String profile_Occupation = getElement(ElementConstants.OCCUPATION);
                String profile_Profession_Fieldname = getElement(ElementConstants.PROFESSIONFIELD);
                String profile_Profession = getElement(ElementConstants.PROFESSION);
                String profile_Your_Hobbies_Interest_Fieldname = getElement(ElementConstants.YOURHOBBIESINTERETSVALUESFIELD);
                String profile_Your_Hobbies_Interest = getElement(ElementConstants.YOURHOBBIESINTERETSVALUES);
                String profile_Brands_Cars_Fieldname = getElement(ElementConstants.BRANDSCARSFIELD);
                String profile_Brands_Cars = getElement(ElementConstants.BRANDSCARS);
                String profile_Primarycar_Fieldname = getElement(ElementConstants.PRIMARYCARFIELD);
                String profile_Primarycar = getElement(ElementConstants.PRIMARYCAR);
                String profile_Brand1_Fieldname = getElement(ElementConstants.BRAND1FIELD);
                String profile_Brand1 = getElement(ElementConstants.BRAND1);
                String profile_Model1_Fieldname = getElement(ElementConstants.MODEL1FIELD);
                String profile_Model1 = getElement(ElementConstants.MODEL1);
                String profile_Brand2_Fieldname = getElement(ElementConstants.BRAND2FIELD);
                String profile_Brand2 = getElement(ElementConstants.BRAND2);
                String profile_Model2_Fieldname = getElement(ElementConstants.MODEL2FIELD);
                String profile_Model2 = getElement(ElementConstants.MODEL2);
                
                
                String ret_Message = "";
                sleep(2000);
                click(getElement(ElementConstants.MYPROFILE));
                sleep(5000);
                scrollDown();
                sleep(2000);
                scrollDown();
                scrollDown();
                sleep(2000);
                click(getElement(ElementConstants.MYPROFILEEDIT));
                sleep(2000);
                System.out.println("edit");
                scrollup();
                
                ret_Message =selectValueProfileDropdownFields(profile_Title_FieldName,profile_Title,Title);
                sleep(1000);
                if (ret_Message.equals("PASS")){
              ret_Message = setValueProfileTextFields(profile_Firstname_FieldName,profile_Firstname,Firstname);
              sleep(1000);
                }
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Lastname_Fieldname,profile_Lastname,Lastname);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Pincode_Fieldname,profile_Pincode,pincode);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                if(existsElement(profile_City_Fieldname) && existsElement(profile_City_Fieldname)){
                      System.out.println("City field is displayed");
                } else {
                ExecutionLog.printLog("Edit Profile-City field is not displayed");
                }           
                }
              if (ret_Message.equals("PASS")){
                ret_Message = selectValueProfileDropdownFields(profile_City_Fieldname,profile_City,city);
                sleep(1000);
              }
              
              
              
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Address1_Fieldname,profile_Address1,Address_Line_1);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                scrollDown();
                sleep(500);
                ret_Message = setValueProfileTextFields(profile_Address2_Fieldname,profile_Address2,Address_Line_2);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Address3_Fieldname,profile_Address3,Address_Line_3);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = selectValueProfileDropdownFields(profile_Gender_Fieldname,profile_Gender,Gender);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = selectValueProfileDropdownFields(profile_Occupation_Fieldname,profile_Occupation,Occupation);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = selectValueProfileDropdownFields(profile_Profession_Fieldname,profile_Profession,Profession);
                sleep(1000);
              }   
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Your_Hobbies_Interest_Fieldname,profile_Your_Hobbies_Interest,Your_Hobbies_Interests_values);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Brands_Cars_Fieldname,profile_Brands_Cars,What_other_Brands_Cars_did_you_consider_value);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                scrollDown();
                ret_Message = selectValueProfileDropdownFields(profile_Primarycar_Fieldname,profile_Primarycar,Will_this_be_your_Primary_Car);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Brand1_Fieldname,profile_Brand1,Brand_1);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Model1_Fieldname,profile_Model1,Model_1);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                ret_Message = setValueProfileTextFields(profile_Brand2_Fieldname,profile_Brand2,Brand_2);
                sleep(1000);
              }
              if (ret_Message.equals("PASS")){
                scrollDown();
                ret_Message = setValueProfileTextFields(profile_Model2_Fieldname,profile_Model2,Model_2);
                sleep(1000);
              }

                scrollDown();
                sleep(2000);
                click(getElement(ElementConstants.UPDATE));
                sleep(2000);
                click(getElement(ElementConstants.UpdateOK));
                
                
                
                
                strTemp.set(7,ret_Message);
                System.out.println("edit sucess " + ret_Message); 
                
                return ret_Message;
          
          }catch (Exception  e)
          {
                System.out.println("Erro"+e);
                return "exception"+e;
          }
    }
*/	

	
}

