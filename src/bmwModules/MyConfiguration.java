package bmwModules;

import java.io.IOException;
import java.util.List;

import baseClass.AppDriver;
import baseClass.ExecutionLog;
import constants.ElementConstants;

public class MyConfiguration extends AppDriver {


	static String propFileName = "src/elements/Locators.properties";	
	
	public MyConfiguration() 
	{
		super(propFileName);		
	}

	/*Method used to
     * Check and click on the Select a dealer button
     * Entering the zip code and select a city 
     * Select a dealer and Click on the continue button*/		
	public String selectDealer(List<String> tmpAllModels) throws InterruptedException
	{		
		String ret_Msg = "";
//		tmpAllModels.set(26, "Error in Dealer Selection");
		sleep(8000);
		scrollDown();
		sleep(8000);
		
		String xlInpDealerZip	= tmpAllModels.get(20);
		String xlInpDealerCity	= tmpAllModels.get(21);
		String xlDealerName 	= tmpAllModels.get(22);
		String xlDealerAddress	= tmpAllModels.get(23);
		String xlDealerCity		= tmpAllModels.get(24);
		String xlDealerZip		= tmpAllModels.get(25);
		
		if (existsElement(getElement(ElementConstants.SelectaDealerFirst))==true)
		{
		click(getElement(ElementConstants.SelectaDealerFirst));
		sleep(10000);
		setValue(getElement(ElementConstants.InputPincode), xlInpDealerZip);
		System.out.println("Zip Entered");
		sleep(1000);
		dropDownByIndex(getElement(ElementConstants.SelectCitydropdown),2);
		sleep(5000);
		click(getElement(ElementConstants.SelectDealer));
		sleep(3000);
		click(getElement(ElementConstants.ContinueButton));
		System.out.println("SelectDealer Completed");
		sleep(45000);
		ret_Msg = "My configuration: Dealer Selected";
		}else 
		{
		 ret_Msg = "My configuration: Select a dealer button is not displayed";
		}
		return ret_Msg;	
	}
	
	/*Method used to
     * Click on the view configuration button
     * Check and click the download summary button*/	
	public String downloadSummary()throws IOException, InterruptedException {
		String retMsg="";
		scrollDown();
        sleep(10000);
        explicitWaitByCSS(getElement(ElementConstants.ViewConfiguration));
		click(getElement(ElementConstants.ViewConfiguration));
		sleep(3000);
		if(existsElement(getElement(ElementConstants.DownloadSummary))==true)
		{
        click(getElement(ElementConstants.DownloadSummary));
		sleep(9000);
		retMsg="My configuration:Sucessfully download the summary";
        ExecutionLog.printLog("My configuration:Sucessfully download the summary"); 
		}else
		{
		 retMsg="My configuration:Download summary button is not displayed";
		}
        return retMsg;		
	}	
	
	/*Method used to
     * Check and click on the Change configuration link
     * Validate the navigated URL*/
	public String changeConfig()throws IOException, InterruptedException 
	{
		String retMsg="";
		sleep(2000);
		if(existsElement(getElement(ElementConstants.ChangeConfiguration))== true)
		{
		  click(getElement(ElementConstants.ChangeConfiguration));
		  sleep(6000);
	      switchwindow();
	        if (getcurrenturl().contains("www.bmw.in/en/ssl/configurator.html") || getcurrenturl().contains("www.bmw.co.th/en/ssl/configurator.html")){
	        	retMsg = "Change Config: Navigated to expected change configuration url";
	        	ExecutionLog.printLog("Change Config: Navigated to expected change configuration url");
	        } else{
	        	retMsg = "Change Config: Not Navigated to expected change configuration url";
	        	ExecutionLog.printLog("Change Config: Not Navigated to expected change configuration url");	      		
	        }
	        sleep(2000);
			closewindow();
			switchbacktoMainWindow();		
			sleep(4000); // To be deleted
		}else {
			retMsg = "Change Config: Change Configuration link is not displayed";
		}
			return retMsg;
	}
	
	/*Method used to
     * Check and click on the Remove configuration link
     * Click Yes in the confirmation window*/
	public String removeConfig()throws IOException, InterruptedException {
		String retMsg="";
		sleep(2000);
		System.out.println("Going to click");
		if (existsElement(getElement(ElementConstants.RemoveConfiguration)) == true)
		{
		click(getElement(ElementConstants.RemoveConfiguration));
		sleep(2000);
		click(getElement(ElementConstants.RemoveConfigurationYes));	
		//String retMessage = getText(getElement(ElementConstants.RemoveConfigYesMsg));
		sleep(2000);
		click(getElement(ElementConstants.RemoveConfigYesMsgOK));
		sleep(4000);
		retMsg = "Remove Config: Sucessfully removed the configuration";
    	ExecutionLog.printLog("Remove Config: Sucessfully removed the configuration");
		} else {
			retMsg = "Remove Config: Remove Configuration link is not displayed";
	    	ExecutionLog.printLog("Remove Config: Remove Configuration link is not displayed");
		}
		return retMsg;
	}
	
	/*Method used to
     * Click on the Book test drive button
     * Click Confirm  in the confirmation window
     * Captured the status message displayed*/
	public String bookTestDrive()throws IOException, InterruptedException {		
		String retMsg ="";
		scrollDown();
		sleep(6000);
		scrollDown();
		click(getElement(ElementConstants.BookTestDrive));
		System.out.println("All Models : BookTestDrive ");
		sleep(9000); 
		if (existsElement(getElement(ElementConstants.BookTestDriveConfirm)) == true){		
			click(getElement(ElementConstants.BookTestDriveConfirm));
			sleep(15000);
			click(getElement(ElementConstants.BookTestReconfirm));
			sleep(30000);
			explicitWaitByCSS(getElement(ElementConstants.BookTestDriveMsg));
			String testDriveStatusMsg = getText(getElement(ElementConstants.BookTestDriveMsg));
			if (testDriveStatusMsg.equalsIgnoreCase("Status: Test Drive Requested")){
				retMsg="Book Test Drive: Sucessfully Booked and the message captured is" +testDriveStatusMsg;
                ExecutionLog.printLog("Clicking Confirm button in Variants page"); 	
			} else {
                	retMsg="Book Test Drive: Status message is not displayed once after booked the car" ;
                    ExecutionLog.printLog("Book Test Drive: Status message is not displayed once after booked the car"); 	
                }			
		}else{
			retMsg = getText(getElement(ElementConstants.BookTestDriveMsg));
			System.out.println("retMessage2 :" + retMsg);
		}
		return retMsg;		
	}

	/*Method used to
     * Check and Click on the Request a quote button
     * Click skip on the confirmation window
     * Captured the status message displayed*/
	public String RequestaQuote()throws IOException, InterruptedException {		
		String retMsg = "";
		sleep(10000);
		explicitWaitByCSS(getElement(ElementConstants.RequestaQuote));
		sleep(5000);
		if (existsElement(getElement(ElementConstants.RequestaQuote)) == true){
			click(getElement(ElementConstants.RequestaQuote));
			System.out.println("All Models Visible: RequestaQuote ");
			sleep(12000);
			if(existsElement(getElement(ElementConstants.RequestaQuoteSkip))){
				click(getElement(ElementConstants.RequestaQuoteSkip));
				sleep(45000);
			}			
			String quoteMsg = getText(getElement(ElementConstants.RequestaQuoteMsg));
			if (quoteMsg.contains("is preparing your quote. You will receive it shortly.")){				
				retMsg="Request a Quote: Sucessfully Booked a request a quote and the message captured is -" + quoteMsg;
                ExecutionLog.printLog("Request a Quote: Sucessfully Booked a request a quote"); 	
			} else {
                	retMsg="Request a Quote: Unable to request a quote the car" ;
                    ExecutionLog.printLog("Request a Quote: Unable to request a quote the car"); 	
                }				
		}else {
			retMsg="My Configuration: Request a quote Button is not displayed" ;
		}
		return retMsg;
	}
	
	/*Method used to
     * Check and Click on the Calculate EMI Link
     * Validate the navigated URL*/
	public String calculateEMIMyConfig()throws IOException, InterruptedException {  		
	        String retMsg ="";       
	        scrollDown();
	        sleep(2000);
	        if (existsElement(getElement(ElementConstants.CalcualteEMIMyconfig))==true)
	        {
	           click(getElement(ElementConstants.CalcualteEMIMyconfig));
	           sleep(12000);
	           switchwindow();
	         if (getcurrenturl().contains("emicalculator.bmwindia.co") || getcurrenturl().contains("yourbmwleasingthailand.com/")){
	        	retMsg = "Calculate EMI: Navigated to expected EMI calculator url";
	        	ExecutionLog.printLog("Calculate EMI: Navigated to expected EMI calculator url");
	         } else{
	        	retMsg = "Calculate EMI: Not Navigated to expected EMI calculator url";
	        	ExecutionLog.printLog("Calculate EMI: Not navigated to expected EMI calculator url");	      		
	         }
	        sleep(2000);
			closewindow();
			switchbacktoMainWindow();		
			sleep(2000);
	        } else 
	        {
	        retMsg = "My Config : Calculate EMI Link is not displayed";
	        }	    
			return retMsg;
		} 
	
	/*Method used to
     * Verify the confirmed car details(Variants,Exterior,Interior,Engpower,price,offers....*/
	public String verifyConfirmedCar(List<String> tmpAllModels, String TestCase)
	{
		    sleep(5000);
		    System.out.println("Inside MyConfigurationsVerifyConfirmedCar");	
		    String ret_Msg 			= "";	
		    
		    String variants  	= tmpAllModels.get(15);
		    String sel_Extr  	= tmpAllModels.get(16);
		    String sel_Intr  	= tmpAllModels.get(17);	
		    String eng_Power 	= tmpAllModels.get(18);
		    String exShwrmPr 	= tmpAllModels.get(19); 	
		    String offers_1	    = tmpAllModels.get(14);
		    String offers_2	    = tmpAllModels.get(15);
		    String offers_3	    = tmpAllModels.get(16);
		    String offers_4	    = tmpAllModels.get(17);
		    String offers_5  	= tmpAllModels.get(18);

		if (variants == "") {
			ret_Msg = "Variant Missing in Excel";
		}
		if (sel_Extr == "") { 
			ret_Msg = ret_Msg + "Exterior Color Missing in Excel";
		}
		if (sel_Intr == "") { 
			ret_Msg = ret_Msg + "Upholstery details Missing in Excel";
		}
		if (eng_Power == ""){
			ret_Msg = ret_Msg + "Engine Power details Missing in Excel";
		}
		if ((variants == "") || (sel_Extr == "") || (sel_Intr == "") || (eng_Power == "")){
			ret_Msg = "Err:" + ret_Msg;
			System.out.println("ret_Msg :" + ret_Msg);
			return ret_Msg;
		}
		    sleep(4000);
		    String configCarName = getText(getElement(ElementConstants.ConfigCarName));
		    sleep(2000);
			scrollDown();	
			sleep(5000);
			click(getElement(ElementConstants.ViewConfiguration));
			sleep(4000);
		    String enginePower		=	getText(getElement(ElementConstants.EnginePower));
		    String exteriorColor	=	getText(getElement(ElementConstants.ExteriorColor));
		    String upholstery		=	getText(getElement(ElementConstants.Upholstery));
		    String exShowroomPrice  =	getText(getElement(ElementConstants.ExShowroomPrice));
		    String offersMessage1   =	getText(getElement(ElementConstants.OffersMessage1));
		    String offersMessage2	=	getText(getElement(ElementConstants.OffersMessage2));
		    String offersMessage3	=	getText(getElement(ElementConstants.OffersMessage3));
		    String offersMessage4	=	getText(getElement(ElementConstants.OffersMessage4));
		    String offersMessage5	=	getText(getElement(ElementConstants.OffersMessage5));
		
	     	if (!variants.equals(configCarName)){
			   ret_Msg = "Variant Not Matching";
		    }
		    if (!eng_Power.equals(enginePower)){
			    ret_Msg = ret_Msg + "Engine Power Statistics Not Matching ";
		     }
		    if (!sel_Extr.equals(exteriorColor)){
		     	ret_Msg = ret_Msg + "Exterior Color Not Matching. ";
		    }
		    if (!sel_Intr.equals(upholstery)){
			   ret_Msg = ret_Msg + "Upholstery Not Matching. ";
		    }		
			if (!exShwrmPr.equals(exShowroomPrice)){
				ret_Msg = ret_Msg + "ExShowroomPrice Not Matching. ";
			}		
			if (!offers_1.equals(offersMessage1)){
				ret_Msg = ret_Msg + "OffersMessage1 Not Matching. ";
			}
			if (!offers_2.equals(offersMessage2)){
				ret_Msg = ret_Msg + "OffersMessage2 Not Matching. ";
			}
			if (!offers_3.equals(offersMessage3)){
				ret_Msg = ret_Msg + "OffersMessage3 Not Matching. ";
			}
			if (!offers_4.equals(offersMessage4)){
				ret_Msg = ret_Msg + "OffersMessage4 Not Matching. ";
			}
			if (!offers_5.equals(offersMessage5)){
				ret_Msg = ret_Msg + "OffersMessage5 Not Matching. ";
			}		
		return ret_Msg;		
	}	
}
