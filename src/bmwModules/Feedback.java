package bmwModules;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.Alert;

import baseClass.AppDriver;
import constants.ElementConstants;

public class Feedback extends AppDriver 
{
	
	static String propFileName = "src/elements/Locators.properties";
	
	public Feedback() throws IOException, InterruptedException{
		super(propFileName);
	}
	
	/*Method used to
     * Hovering and clicking the feedback Icon
     * Clicking on each ratings and enter the suggestion
     * Clicking on submit button*/
	public String inputFeedback(List<String> strTemp) throws IOException, InterruptedException{
		
		Clearcookies();		
		String retMessage = "";
		String feedback = strTemp.get(63);
		String suggestions = strTemp.get(64);
		sleep(2000);
		System.out.println("check1");
		hoveronElement(getElement(ElementConstants.Feedback));
		sleep(1000);
		click(getElement(ElementConstants.Feedback));
		System.out.println("check2");
		sleep(3000);
		if (feedback.equals("1")){
			click(getElement(ElementConstants.Rating1));
			System.out.println("Rating is 1");			
		}
		if (feedback.equals("2")){
			click(getElement(ElementConstants.Rating2));
			System.out.println("Rating is 2");			
		}
		if (feedback.equals("3")){
			click(getElement(ElementConstants.Rating3));
			System.out.println("Rating is 3");			
		}
		if (feedback.equals("4")){
			click(getElement(ElementConstants.Rating4));
			System.out.println("Rating is 4");			
		}
		if (feedback.equals("5")){
			click(getElement(ElementConstants.Rating5));
			System.out.println("Rating is 5");			
		}
		if (feedback.equals("6")){
			click(getElement(ElementConstants.Rating6));
			System.out.println("Rating is 6");			
		}
		if (feedback.equals("7")){
			click(getElement(ElementConstants.Rating7));
			System.out.println("Rating is 7");			
		}
		if (feedback.equals("8")){
			click(getElement(ElementConstants.Rating8));
			System.out.println("Rating is 8");			
		}
		if (feedback.equals("9")){
			click(getElement(ElementConstants.Rating9));
			System.out.println("Rating is 9");			
		}
		if (feedback.equals("10")){
			click(getElement(ElementConstants.Rating10));
			System.out.println("Rating is 10");	
		}
		
		setValue(getElement(ElementConstants.Textbox), suggestions);
		System.out.println("check4");
		sleep(1000);
		click(getElement(ElementConstants.FeedbackSubmitButton));
		System.out.println("check5");
		sleep(6000);
		click(getElement(ElementConstants.FeedbackMessageAlertOkButton));
		System.out.println("check6");
		
		retMessage = "TEST Result as Expected";
		strTemp.set(7,retMessage);
		return retMessage;
	}
}
