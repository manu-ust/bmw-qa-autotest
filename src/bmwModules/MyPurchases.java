package bmwModules;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.AppDriver;
import constants.ElementConstants;

public class MyPurchases extends AppDriver{

	static String propFileName = "src/elements/Locators.properties";	
	
	public MyPurchases() {
		super(propFileName);
	}
	
	/*Method used to
     * Verify the confirmed car details(Variants,BookingID,Dealername in My purchase page.*/
	public String verifyVariantBookingIDAndDealer(List<String> tmpAllModels)
	{
		System.out.println("Inside MyPurchaseVerifyVariantBookingIDAndDealer");	
		String ret_Msg 			= "";
		String variants   		= "";
		String bookingID		= "";
		String dealerName		= "";
		
		sleep(5000);
		
		variants  	= tmpAllModels.get(15);
//		bookingID  	= tmpAllModels.get(6);
		dealerName 	= tmpAllModels.get(22);		

		if (variants == "") {
			ret_Msg = "Variant Missing in Excel";
		}
//		if (BookingID == "") { 
//			ret_Msg = ret_Msg + "Booking ID Missing in Excel";
//		}
		if (dealerName == "") { 
			ret_Msg = ret_Msg + "Dealer Details Missing in Excel";
		}
		if ((variants == "") || (dealerName == "")){
			ret_Msg = "Err:" + ret_Msg;
			System.out.println("ret_Msg :" + ret_Msg);			
			return ret_Msg;
		}
		
		String scrn_Variants	=	getText(getElement(ElementConstants.Scrn_Variants));
//		String Scrn_BookingID	=	getText(getElement(ElementConstants.Scrn_BookingID));
		
		if (variants.equals(scrn_Variants)){
			ret_Msg = "My purchases: Selected variant is matched";
		}else{
			System.out.println("Not Matching:" + variants + "|" + scrn_Variants);
			ret_Msg = "My purchases:Variant Not Matching ";
		}		
		List<WebElement> getDealerName = base.driver.findElements(By.cssSelector(getElement(ElementConstants.Scrn_DealerName)));
		for (int i =0; i<getDealerName.size(); i++){
			if (getDealerName.equals(dealerName)){
				ret_Msg = "My purchases: Selected Dealer name matched";
			}else{
				ret_Msg = "My purchases: Selected Dealer name not matched";
			}			
		}		
		if(existsElement(getElement(ElementConstants.CarbookingStatus)) == true){
			ret_Msg = "My purchases: Carbooking status displayed";			
		}else{
			ret_Msg = "My purchases: Carbooking status not displayed";
		}
		return ret_Msg;		
	}
}
