package bmwModules;

import java.io.IOException;
import java.sql.Driver;
import java.util.List;
import java.util.Map;

import org.apache.commons.exec.util.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.AppDriver;
import baseClass.BaseDriver;
import baseClass.ExecutionLog;
import constants.ElementConstants;

public class AllModels extends AppDriver{
						
	static String propFileName = "src/elements/Locators.properties";
	
	//Constructor used
	public AllModels() throws IOException, InterruptedException
	{
		super(propFileName);
	}
	
	/*Method used to
	      * Retrieve the text message after doen the payment
	      * check and click the ViewMyBMW button.*/
	public String viewBmw()
	{
		String ret_Msg = "";
		sleep(10000);
		String screenMessage = getText(getElement(ElementConstants.PurchaseComplete));
		System.out.println("ScreenMessage :" + screenMessage);
		screenMessage = ReplaceSplCharbyWhiteSpace(screenMessage);
		if(existsElement(getElement(ElementConstants.ViewMyBMW)) == true){
		   click(getElement(ElementConstants.ViewMyBMW));
		   sleep(30000);
		   ret_Msg="View BMW:Sucessfully clicked the view BMW button";
		}else{
			 ret_Msg="View BMW: View BMW button is not displayed to click";
		}
		return ret_Msg;
	}
	
	    /*Method used to
         * Clicking the right tab -MyPurchases in my configuration
         * check and click the ViewMyBMW button.*/
		public String navigateClickRight()throws IOException, InterruptedException {		
		String strTotConfig ="";
		String retMessage = getText(getElement(ElementConstants.Config1ofn));
		click(getElement(ElementConstants.clickRight));
		sleep(2000);
		System.out.println("NavigateClickRight retMessage :" + retMessage.substring(0,8) + "|");
		String xx = retMessage.substring(0,8);
		if (xx.equals("Purchase")){
			strTotConfig = retMessage.substring(14);
		}else{
			strTotConfig = retMessage.substring(18);
		}		
		strTotConfig = strTotConfig.replaceAll("f", "");
		strTotConfig = strTotConfig.replaceAll("o", "");
		strTotConfig = strTotConfig.trim();		
		int intTotCnt = Integer.parseInt(strTotConfig);		
		String strCurConfig = getText(getElement(ElementConstants.ConfigCurCnt));	
		System.out.println("from nex xpath Config :" + strCurConfig);		
		int intCurCnt = Integer.parseInt(strCurConfig);
		retMessage = strCurConfig + "-" + strTotConfig;		
		System.out.println("NavigateClickRight StrCurConfig - StrTotConfig :" + retMessage);
		if (strTotConfig.equals(strCurConfig)){	
			System.out.println("NavigateClickRight READY to Break");
			return "BREAK";
		}
		System.out.println("NavigateClickRight Continue");
		return "CONTINUE"; 	
	}
	
		/*Method used to
         * Check and click the calculate EMI button
         * Validate the navigated URL.*/
    public String calculateEMI()throws IOException, InterruptedException {  
        String retMsg =""; 
        scrollDown();
        sleep(2000);
        if(existsElement(getElement(ElementConstants.CaclulateEMI)))
        {
          click(getElement(ElementConstants.CaclulateEMI));
          sleep(12000);
          switchwindow();
        if (getcurrenturl().contains("emicalculator.bmwindia.co") || getcurrenturl().contains("yourbmwleasingthailand.com/"))
        {
        	retMsg = "Calculate EMI: Navigated to expected EMI calculator url";
        	ExecutionLog.printLog("Calculate EMI: Navigated to expected EMI calculator url");
        } else{
        	retMsg = "Calculate EMI: Not Navigated to expected EMI calculator url";
        	ExecutionLog.printLog("Calculate EMI: Not navigated to expected EMI calculator url");	      		
        }
        sleep(2000);
		closewindow();
		switchbacktoMainWindow();		
		sleep(2000); 
        }else{
        	retMsg = "Calculate EMI button is not displayed in All models page";
        }
		return retMsg;
	} 
    
    /*Method used to
     * Check and click the Check availability button
     * Validate the Exterior and Interior name displayed
     * Click on the Check availability window OK  button 
     * Click on the Confirm button.*/
    public String checkavailablity( List<String> tmpAllModels) throws IOException
	{
		String retMsg="";
		
		String sel_Extr   = tmpAllModels.get(16);
        String sel_Intr   = tmpAllModels.get(17);
        
        String check_Availability_Exterior = getElement(ElementConstants.Checkavailabilitywindowexterior);
        String check_Availability_Interior = getElement(ElementConstants.Checkavailabilitywindowinterior);
        String confirm_Button =getElement(ElementConstants.CheckAvailabilityconfirmButton);
        
		sleep(1000);
		if(existsElement(getElement(ElementConstants.CheckAvailabilityButton)))
		{
		   click(getElement(ElementConstants.CheckAvailabilityButton));
		   sleep(5000);
		   String statusStockAvailable=getText(getElement(ElementConstants.CheckAvailabilityButtonMessage));
		   System.out.println("Message"+ statusStockAvailable);
		if (statusStockAvailable.equals("Stock is available for the selected configuration")){
			retMsg="Check availability window: Stock available for the selected configuration";
            ExecutionLog.printLog("Check availability window: Stock available for the selected configuration");
		}else{
			retMsg="Check availability window: Stock not available for the selected configuration";
            ExecutionLog.printLog("Check availability window: Stock not available for the selected configuration"); 
		   } 
		try {
		 if(existsElement(check_Availability_Exterior)== true){
			String actualExteriorName = getText(check_Availability_Exterior);
			if (actualExteriorName.equals(sel_Extr)) {
				retMsg="Check availability window: Exterior name - " + sel_Extr + " is matched";
                ExecutionLog.printLog("Check availability window: Exterior name - " + sel_Extr + " is matched"); 			
			}
		}else{
			retMsg="Check availability window: Exterior name - " + sel_Extr + " is not matched";
            ExecutionLog.printLog("Check availability window: Exterior name - " + sel_Extr + " is not matched"); 
		   } 
		 }catch (Exception e){
			 retMsg="Check availability window: Exterior name - " + sel_Extr + " is not displayed" + e;
             ExecutionLog.printLog("Check availability window: Exterior name - " + sel_Extr + " is not displayed" + e);
		  }
		
		try {
			 if(existsElement(check_Availability_Interior)== true){
				String actualInteriorName = getText(check_Availability_Interior);
				if (actualInteriorName.equals(sel_Intr)){
					retMsg="Check availability window: Interior name - " + sel_Intr + " is matched";
	                ExecutionLog.printLog("Check availability window: Interior name - " + sel_Intr + " is matched"); 			
				}
			}else{
				retMsg="Check availability window: Interior name - " + sel_Intr + " is not matched";
                ExecutionLog.printLog("Check availability window: Interior name - " + sel_Intr + " is not matched"); 			
			   } 
			 }catch (Exception e){
				 retMsg="Check availability window: Interior name - " + sel_Intr+ " is not displayed" + e;
	             ExecutionLog.printLog("Check availability window: Interior name - " + sel_Intr + " is not displayed" + e);
			  }	
		
		click(getElement(ElementConstants.CheckAvailabilityButtonMessageOK));
		sleep(3000);
		retMsg="Check availability window: Closed";
		try {
			if (existsElement(confirm_Button) == true){
				click(confirm_Button);
				retMsg="All models:Clicking Confirm button in Variants page";
                ExecutionLog.printLog("All models:Clicking Confirm button in Variants page"); 			
			}				
		}catch (Exception e){
			 retMsg="All models:Confirm button is not displayed/enabled to click" + e;
             ExecutionLog.printLog("All models:Confirm button is not displayed/enabled to click" + e);
		  }	
		sleep(45000);
		}else{
			retMsg ="All models: Check availability button is not displayed to click";
		}
		return retMsg;
	} 
	
	
    /*Method used to
     * Launching the browser and login to the site
     * Navigating to My BMW/My purchase page.*/	
	public String navigateto(List<List<String>> tmpLogin, String navPage) throws IOException, InterruptedException {
		String retMessage;
		String ret_Msg = "";
		
		retMessage = LaunchBrowserLoginAndGetLoginList(tmpLogin);	    
	    if (retMessage.equals("PASS")){	    	
	    	sleep(500);
	    	if (navPage.equals("myBMW_Config")){
	    		click(getElement(ElementConstants.MyBMW));
	    		ret_Msg= "Sucessfully navigated to MYBMW page";
	    	}	
	    	if (navPage.equals("myBMW_MyPurchase")){
	    		click(getElement(ElementConstants.MyBMW));
	    		sleep(3000);
	    		click(getElement(ElementConstants.MyPurchasesTab));
	    		ret_Msg= "Sucessfully navigated to MYPurchases page";
	    	}	
	    	sleep(4000);   	
	    }
		return ret_Msg;	
	}
		
       
	    /*Method used to
         * Check and select price range 
         * Select a car series and click either by image/text/buy-now button
         * Select fuel type or variant 
         * Select Exterior and Interior colour*/	
		public String carSelection(List<String> tmpAllModels) throws IOException, InterruptedException 
	    {
	                 String retMessage;
	                 String ret_Msg = "";
	          
	                 System.out.println("AllModelsConfigureCarAndConfirm check 1");  
	                
	                      scrollDown(); 
	                      System.out.println("AllModelsConfigureCarAndConfirm check 1");  
	                      String all_Models = tmpAllModels.get(11);
	                      String select_Car = tmpAllModels.get(12);
	                      String selCar_Act = tmpAllModels.get(13); 
	                      String fuel_Type  = tmpAllModels.get(14);
	                      String variants   = tmpAllModels.get(15);
	                      String sel_Extr   = tmpAllModels.get(16);
	                      String sel_Intr   = tmpAllModels.get(17);
	                      	                  	                      
	                      System.out.println("***************************");
	                      	                  	                      
	                      String price_Range_Options = getElement(ElementConstants.Pricerangedropdown);
	                      String fuel_Type_Options = getElement(ElementConstants.Fueltypeoption);
	                      String variant_Type_Options = getElement(ElementConstants.Varianttypeoption);
	                      String exterior_Type_Select =getElement(ElementConstants.Exteriortype);
	                      String interior_Type_Select =getElement(ElementConstants.Interiortype);
	                      String car_Series_List = getElement(ElementConstants.Carserieslist);
	                      String car_Click_Image = getElement(ElementConstants.Carseriesclickimage);
	                      String car_Click_Button = getElement(ElementConstants.Carseriesclickbutton);
	                      String car_Click_Text = getElement(ElementConstants.Carseriesclicktext); 
	                      String zoom_Click_Image =getElement(ElementConstants.ZoomiconImage); 
	                      String car_Variants_List=getElement(ElementConstants.Carvariantslist); 
	                      String car_Zoom_List=getElement(ElementConstants.Carzoomlist);
	                      String car_Zoom_close=getElement(ElementConstants.Zoomwindowclose);
	                      
	
	                      if(existsElement(price_Range_Options))
	                      {
	                        click(price_Range_Options);
	                        sleep(500);
	                        ret_Msg =  selectPriceRange(price_Range_Options,all_Models);	                      
	                      if (ret_Msg.contains("Sucessfully selected the price range")){	                         
	                        if (selCar_Act.equals("Click Image")){
	                        	ret_Msg = selectCarSeries(car_Click_Image,car_Series_List,select_Car);                                            
	                    	   if(ret_Msg.contains("Unable to click the car series")){
	                    		   ret_Msg ="Unable to click the car series -by Image"; 
	                    	   }
	                         }else if (selCar_Act.equals("Click BuyNow")){
	                        	 ret_Msg = selectCarSeries(car_Click_Button,car_Series_List,select_Car);
	                        	if(ret_Msg.contains("Unable to click the car series")){
		                    		   ret_Msg ="Unable to click the car series -by Buynow button"; 
		                    	   }
	                        }else if (selCar_Act.equals("Click Text")){
	                        	ret_Msg = selectCarSeries(car_Click_Text,car_Series_List,select_Car);
	                    	   if(ret_Msg.contains("Unable to click the car series")){
	                    		   ret_Msg ="Unable to click the car series -by Text"; 
	                    	   }	                        	                          
	                         }else{
	                         ret_Msg = "Please verify car not able to select ";
	                        }
	                      }
	                    } else{
	                    	ret_Msg ="All Models:Price range option is not displayed";
	                    }
	                      sleep(8000);
	                      scrollDown(); 
	                      sleep(4000);	                      
	                      if (ret_Msg.contains("Successfully clicked the car series"))
	                      {    
	                    	  click(fuel_Type_Options);
	                    	  sleep(2000);
	                    	  ret_Msg =selectFuelType(fuel_Type_Options,fuel_Type);
	                      }                                   	                     	                      	                      
		                  if (ret_Msg.contains("Sucessfully selected the Fuel type"))
		                  {
	                            click(variant_Type_Options);
	                            sleep(2000);
	                            ret_Msg = selectVariantType(variant_Type_Options, variants);                                	                            
	                      }	                      
	                      scrollDown();
	                      sleep(2000);
	                      
	                      if (ret_Msg.contains("Sucessfully selected the Variant type"))
	                      {
	                            sleep(4000);
	                            ret_Msg =  clickExteriorColour(exterior_Type_Select,sel_Extr);
	                            sleep(4000);
	                      }
	                     
	                      if (ret_Msg.contains("Sucessfully selected the Exterior colour"))
	                      {
	                            sleep(6000);
	                            ret_Msg = clickInteriorColour(interior_Type_Select,sel_Intr) ;
	                            sleep(5000);
	                      }	        
	        return ret_Msg;     
	    
	        }
		
		 /*Method used to
         * Click the zoom Icon for the selected variant 
         * Click on each icon images*/
		public String clickZoomImage(List<String> tmpAllModels) throws IOException{	
	
        String retMsg ="";
        String variant   = tmpAllModels.get(15);
        
        String zoom_Click_Icon =getElement(ElementConstants.ZoomiconImage); 
        String car_Variants_List=getElement(ElementConstants.Carvariantslist); 
        String car_Zoom_List=getElement(ElementConstants.Carzoomlist);
        String car_Zoom_close=getElement(ElementConstants.Zoomwindowclose);
        
        scrollup();
        sleep(2000);
        
		List<WebElement> zoomiconclick = base.driver.findElements(By.cssSelector(zoom_Click_Icon));
		List<WebElement> carvariants = base.driver.findElements(By.cssSelector(car_Variants_List));
			try{
				for(int i=0; i<carvariants.size();i++){
					 if(carvariants.get(i).getText().contains(variant)){
					 zoomiconclick.get(i).click();
	                 sleep(5000);
	                 retMsg = "Car variants page: Cicking the zoom icon button";
					 ExecutionLog.printLog("Car variants page: Cicking the zoom icon button"); 
	                 break;                 
			   }
			} 
		}catch (Exception e) {
			     retMsg="Car variants page: Unable to click the zoom icon button" +e;
				 ExecutionLog.printLog("Unable to click the zoom icon button" +e); 
			 }
		
		 List<WebElement> zoomelementimages = base.driver.findElements(By.cssSelector(car_Zoom_List));
		 try{
		   for(int j=0; j<zoomelementimages.size();j++){
			     zoomelementimages.get(j).click();
                 sleep(3000);
                 retMsg = "Zoom Window: Clicked on each images" ;
				 ExecutionLog.printLog("Zoom Window: Clicked on each images"); 
		   }
		 }catch (Exception e) {
			 retMsg="Zoom Window: Unable to click the zoom window images" +e;
			 ExecutionLog.printLog("Zoom Window: Unable to click the zoom window images" +e); 
		 }
		 finally {
			 click(car_Zoom_close);
			 sleep(2000);
		 }
		 return retMsg;
   }	   		
	}