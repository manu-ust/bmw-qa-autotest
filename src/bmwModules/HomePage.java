package bmwModules;


import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseClass.AppDriver;
import constants.ElementConstants;


public class HomePage  extends AppDriver {
	
	static String propFileName = "src/elements/Locators.properties";

	public HomePage() 
	{
		super(propFileName);		
	}
	
	/*Method used to
     * Click on the header link 
     * Validate the navigating URL*/
	public String verifyHeaders_GU() throws IOException, InterruptedException
	{
		    String retMessage="";
		    if(existsElement(getElement(ElementConstants.headerBMWLink_GU)))
		    {
		      click(getElement(ElementConstants.headerBMWLink_GU));
			  sleep(2000);
		      switchwindow();
		      sleep(3000);
		        if (getcurrenturl().contains("https://www.bmw.in/en/index.html") || getcurrenturl().contains("https://www.bmw.co.th/th/index.html")){
		        	retMessage = "Home page: Sucessfully navigated to expected BMW home site";
		        } else{
		        	retMessage = "Home page: Not navigated to expected BMW home site";      		
		        }
				closewindow();
				switchbacktoMainWindow();		
				sleep(4000);
		    }else {
		    	retMessage = "Home page: Header link is not displayed";
		    }
				return retMessage;
	}
	
	/*Method used to
     * Click on each carousel banner images*/
	public String clickCarouselImages() throws IOException
	{		
		String retMessage="";
		try{			
		List<WebElement> carouselImages = base.driver.findElements(By.cssSelector(getElement(ElementConstants.carouselImage_GU))); 		
		for (int i=0; i<carouselImages.size(); i++){			
			carouselImages.get(i).click();	
		}
		    retMessage = "Home page: Sucessfully clicked all carousel image banners";
		}catch (Exception e){
			retMessage = "Home page: Unabled to click the carousel image banners" +e ;			
		}
		return retMessage;
	}
	
	/*Method used to
     * Launch the browser and application*/
	public String browser_login(List<List<String>> listLogin, List<String> strTemp) 
	{
		String retMessage="";
		retMessage=BrowserLaunch(strTemp);		
		return retMessage;
		
	}
	
}
	

	
