package co.in.bmw.ci.autotest.api.rest;

public class Groups {
	private String displayGroupName;

	private Commands[] commands;

	public String getDisplayGroupName ()
	{
	return displayGroupName;
	}

	public void setDisplayGroupName (String displayGroupName)
	{
	this.displayGroupName = displayGroupName;
	}

	public Commands[] getCommands ()
	{
	return commands;
	}

	public void setCommands (Commands[] commands)
	{
	this.commands = commands;
	}

	}
