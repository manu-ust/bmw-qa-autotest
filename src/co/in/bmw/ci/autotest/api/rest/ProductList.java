package co.in.bmw.ci.autotest.api.rest;

public class ProductList {
	private String connectionState;

    private String OSVersion;

    private String Batch;

    private String StatusId;

    private String HwName;

    private String RMA;

    private String ProductName;

    private String FirwareName;

    private String RmaDate;

    private String AgentVersion;

    private String Builddate;

    private String DeviceId;

    private String lastActiveTime;

    private String OS;

    private String AgentType;

    private String SN;

    private String SwVersion;

    private String ProductId;

    private String PlatformArch;

    private String ApiVersion;

    private String HwVersion;

    private String SwName;

    private String Status;

    private String FirmwareId;

    private String[] VirtualProductId;

    private String MAC;

    public String getConnectionState ()
    {
        return connectionState;
    }

    public void setConnectionState (String connectionState)
    {
        this.connectionState = connectionState;
    }

    public String getOSVersion ()
    {
        return OSVersion;
    }

    public void setOSVersion (String OSVersion)
    {
        this.OSVersion = OSVersion;
    }

    public String getBatch ()
    {
        return Batch;
    }

    public void setBatch (String Batch)
    {
        this.Batch = Batch;
    }

    public String getStatusId ()
    {
        return StatusId;
    }

    public void setStatusId (String StatusId)
    {
        this.StatusId = StatusId;
    }

    public String getHwName ()
    {
        return HwName;
    }

    public void setHwName (String HwName)
    {
        this.HwName = HwName;
    }

    public String getRMA ()
    {
        return RMA;
    }

    public void setRMA (String RMA)
    {
        this.RMA = RMA;
    }

    public String getProductName ()
    {
        return ProductName;
    }

    public void setProductName (String ProductName)
    {
        this.ProductName = ProductName;
    }

    public String getFirwareName ()
    {
        return FirwareName;
    }

    public void setFirwareName (String FirwareName)
    {
        this.FirwareName = FirwareName;
    }

    public String getRmaDate ()
    {
        return RmaDate;
    }

    public void setRmaDate (String RmaDate)
    {
        this.RmaDate = RmaDate;
    }

    public String getAgentVersion ()
    {
        return AgentVersion;
    }

    public void setAgentVersion (String AgentVersion)
    {
        this.AgentVersion = AgentVersion;
    }

    public String getBuilddate ()
    {
        return Builddate;
    }

    public void setBuilddate (String Builddate)
    {
        this.Builddate = Builddate;
    }

    public String getDeviceId ()
    {
        return DeviceId;
    }

    public void setDeviceId (String DeviceId)
    {
        this.DeviceId = DeviceId;
    }

    public String getLastActiveTime ()
    {
        return lastActiveTime;
    }

    public void setLastActiveTime (String lastActiveTime)
    {
        this.lastActiveTime = lastActiveTime;
    }

    public String getOS ()
    {
        return OS;
    }

    public void setOS (String OS)
    {
        this.OS = OS;
    }

    public String getAgentType ()
    {
        return AgentType;
    }

    public void setAgentType (String AgentType)
    {
        this.AgentType = AgentType;
    }

    public String getSN ()
    {
        return SN;
    }

    public void setSN (String SN)
    {
        this.SN = SN;
    }

    public String getSwVersion ()
    {
        return SwVersion;
    }

    public void setSwVersion (String SwVersion)
    {
        this.SwVersion = SwVersion;
    }

    public String getProductId ()
    {
        return ProductId;
    }

    public void setProductId (String ProductId)
    {
        this.ProductId = ProductId;
    }

    public String getPlatformArch ()
    {
        return PlatformArch;
    }

    public void setPlatformArch (String PlatformArch)
    {
        this.PlatformArch = PlatformArch;
    }

    public String getApiVersion ()
    {
        return ApiVersion;
    }

    public void setApiVersion (String ApiVersion)
    {
        this.ApiVersion = ApiVersion;
    }

    public String getHwVersion ()
    {
        return HwVersion;
    }

    public void setHwVersion (String HwVersion)
    {
        this.HwVersion = HwVersion;
    }

    public String getSwName ()
    {
        return SwName;
    }

    public void setSwName (String SwName)
    {
        this.SwName = SwName;
    }

    public String getStatus ()
    {
        return Status;
    }

    public void setStatus (String Status)
    {
        this.Status = Status;
    }

    public String getFirmwareId ()
    {
        return FirmwareId;
    }

    public void setFirmwareId (String FirmwareId)
    {
        this.FirmwareId = FirmwareId;
    }

    public String[] getVirtualProductId ()
    {
        return VirtualProductId;
    }

    public void setVirtualProductId (String[] VirtualProductId)
    {
        this.VirtualProductId = VirtualProductId;
    }

    public String getMAC ()
    {
        return MAC;
    }

    public void setMAC (String MAC)
    {
        this.MAC = MAC;
    }

}
