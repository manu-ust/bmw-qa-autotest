package co.in.bmw.ci.autotest.api.rest;

public class Productline
{
    private String count;

    private Data[] data;

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public Data[] getData ()
    {
        return data;
    }

    public void setData (Data[] data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [count = "+count+", data = "+data+"]";
    }
}
    