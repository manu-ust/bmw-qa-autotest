package co.in.bmw.ci.autotest.api.rest;

public class ActivateProductJsonResponse {
	private Eps[] eps;

	private String pId;

	private String cpId;

	private Transport transport;

	private String cfgTmplId;

	private String vpId;

	private String prodTmplId;

	private String plId;

	private String apiKey;

	public Eps[] getEps() {
		return eps;
	}

	public void setEps(Eps[] eps) {
		this.eps = eps;
	}

	public String getPId() {
		return pId;
	}

	public void setPId(String pId) {
		this.pId = pId;
	}

	public String getCpId() {
		return cpId;
	}

	public void setCpId(String cpId) {
		this.cpId = cpId;
	}

	public Transport getTransport() {
		return transport;
	}

	public void setTransport(Transport transport) {
		this.transport = transport;
	}

	public String getCfgTmplId() {
		return cfgTmplId;
	}

	public void setCfgTmplId(String cfgTmplId) {
		this.cfgTmplId = cfgTmplId;
	}

	public String getVpId() {
		return vpId;
	}

	public void setVpId(String vpId) {
		this.vpId = vpId;
	}

	public String getProdTmplId() {
		return prodTmplId;
	}

	public void setProdTmplId(String prodTmplId) {
		this.prodTmplId = prodTmplId;
	}

	public String getPlId() {
		return plId;
	}

	public void setPlId(String plId) {
		this.plId = plId;
	}

	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	@Override
	public String toString() {
		return "ClassPojo [eps = " + eps + ", pId = " + pId + ", cpId = " + cpId + ", transport = " + transport
				+ ", cfgTmplId = " + cfgTmplId + ", vpId = " + vpId + ", prodTmplId = " + prodTmplId + ", plId = "
				+ plId + ", apiKey = " + apiKey + "]";
	}
}
