package co.in.bmw.ci.autotest.api.rest;

public class ChannelQueue
{
    private String port;

    private String sasToken;

    private String connectionURI;

    public String getPort ()
    {
        return port;
    }

    public void setPort (String port)
    {
        this.port = port;
    }

    public String getSasToken ()
    {
        return sasToken;
    }

    public void setSasToken (String sasToken)
    {
        this.sasToken = sasToken;
    }

    public String getConnectionURI ()
    {
        return connectionURI;
    }

    public void setConnectionURI (String connectionURI)
    {
        this.connectionURI = connectionURI;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [port = "+port+", sasToken = "+sasToken+", connectionURI = "+connectionURI+"]";
    }
}