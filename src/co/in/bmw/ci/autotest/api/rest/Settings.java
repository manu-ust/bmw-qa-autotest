package co.in.bmw.ci.autotest.api.rest;


public class Settings
{
private String fid;

private String[] listOfPossibleValues;

private String formType;

private String name;

private String[] constraints;

private String groupname;

private String datatype;

private String defaultvalue;

private String datatypename;

public String getFid ()
{
return fid;
}

public void setFid (String fid)
{
this.fid = fid;
}

public String[] getListOfPossibleValues ()
{
return listOfPossibleValues;
}

public void setListOfPossibleValues (String[] listOfPossibleValues)
{
this.listOfPossibleValues = listOfPossibleValues;
}

public String getFormType ()
{
return formType;
}

public void setFormType (String formType)
{
this.formType = formType;
}

public String getName ()
{
return name;
}

public void setName (String name)
{
this.name = name;
}

public String[] getConstraints ()
{
return constraints;
}

public void setConstraints (String[] constraints)
{
this.constraints = constraints;
}

public String getGroupname ()
{
return groupname;
}

public void setGroupname (String groupname)
{
this.groupname = groupname;
}

public String getDatatype ()
{
return datatype;
}

public void setDatatype (String datatype)
{
this.datatype = datatype;
}

public String getDefaultvalue ()
{
return defaultvalue;
}

public void setDefaultvalue (String defaultvalue)
{
this.defaultvalue = defaultvalue;
}

public String getDatatypename ()
{
return datatypename;
}

public void setDatatypename (String datatypename)
{
this.datatypename = datatypename;
}


}

