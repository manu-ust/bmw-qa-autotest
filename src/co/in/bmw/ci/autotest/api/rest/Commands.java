package co.in.bmw.ci.autotest.api.rest;
public class Commands
{
    private String id;

    private String onScreenDisplay;

    private String deviceCommand;

    private String rr;

    private String addToProductPage;

  //  private Parameters[] parameters;

    private String addToOTAGroupProfilePage;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getOnScreenDisplay ()
    {
        return onScreenDisplay;
    }

    public void setOnScreenDisplay (String onScreenDisplay)
    {
        this.onScreenDisplay = onScreenDisplay;
    }

    public String getDeviceCommand ()
    {
        return deviceCommand;
    }

    public void setDeviceCommand (String deviceCommand)
    {
        this.deviceCommand = deviceCommand;
    }

    public String getRr ()
    {
        return rr;
    }

    public void setRr (String rr)
    {
        this.rr = rr;
    }

    public String getAddToProductPage ()
    {
        return addToProductPage;
    }

    public void setAddToProductPage (String addToProductPage)
    {
        this.addToProductPage = addToProductPage;
    }
    public String getAddToOTAGroupProfilePage ()
    {
        return addToOTAGroupProfilePage;
    }

    public void setAddToOTAGroupProfilePage (String addToOTAGroupProfilePage)
    {
        this.addToOTAGroupProfilePage = addToOTAGroupProfilePage;
    }

   
}
			