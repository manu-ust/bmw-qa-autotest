package co.in.bmw.ci.autotest.api.rest;

public class NotificationResponse {
  private String message;

  private String timeStamp;

  private String priority;

  private String cuId;

  private NotificationData data;

  private String messageType;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getTimeStamp() {
    return timeStamp;
  }

  public void setTimeStamp(String timeStamp) {
    this.timeStamp = timeStamp;
  }

  public String getPriority() {
    return priority;
  }

  public void setPriority(String priority) {
    this.priority = priority;
  }

  public String getCuId() {
    return cuId;
  }

  public void setCuId(String cuId) {
    this.cuId = cuId;
  }

  
  public String getMessageType() {
    return messageType;
  }

  public void setMessageType(String messageType) {
    this.messageType = messageType;
  }
  

  public NotificationData getData() {
    return data;
  }

  public void setData(NotificationData data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "NotificationResponse [message = " + message + ", timeStamp = " + timeStamp + ", priority = " + priority + ", cuId = " + cuId + ", data = " + data
        + ", messageType = " + messageType + "]";
  }
}
