package co.in.bmw.ci.autotest.api.rest;

public class Attributes
{
private String dataType;

private String description;

private String name;

private String[] constraints;

private String localId;

private String dataTypeName;

public String getDataType ()
{
return dataType;
}

public void setDataType (String dataType)
{
this.dataType = dataType;
}

public String getDescription ()
{
return description;
}

public void setDescription (String description)
{
this.description = description;
}

public String getName ()
{
return name;
}

public void setName (String name)
{
this.name = name;
}

public String[] getConstraints ()
{
return constraints;
}

public void setConstraints (String[] constraints)
{
this.constraints = constraints;
}

public String getLocalId ()
{
return localId;
}

public void setLocalId (String localId)
{
this.localId = localId;
}

public String getDataTypeName ()
{
return dataTypeName;
}

public void setDataTypeName (String dataTypeName)
{
this.dataTypeName = dataTypeName;
}

}
