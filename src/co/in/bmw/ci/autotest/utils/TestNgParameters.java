package co.in.bmw.ci.autotest.utils;

import java.util.Map;

public class TestNgParameters {

    private String browser;
    private String deviceSerialNo;
    private String url;
    private String restUrl;
    private String jiraUrl;
    private Map map;
    private String deviceSerialNo1;
    private String deviceSerialNo2;
    private String deviceSerialNo3;
    private String deviceMac;
    private String queueType;
    private String brokerType;
    private String authenticationType;
    private String systemUserName;
    private String systemUserPassword;
    private String azureUserName;
    private String azurePassword;

    public TestNgParameters() {
        GetValuesFromTestNg obj = new GetValuesFromTestNg();
        map = obj.getValuesFromXml();
    }

    public String getBrowser() {
        browser = (String) map.get("browser");
        return browser;
    }
    
    public String getUserName(){
    	systemUserName = (String) map.get("userName");

         return systemUserName;
    	
    }
    public String azureUserName(){
    	azureUserName = (String) map.get("azureUserName");
         return azureUserName;
    	
    }
    
    public String azurePassword(){
    	azurePassword = (String) map.get("azurePassword");

         return azurePassword;
    	
    }
    public String getPassword(){
    	systemUserPassword = (String) map.get("password");
         return systemUserPassword;
    	
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    //THIS METHOD WILL BE DEPRECATED
    public String getDeviceMac() {
        deviceMac = (String) map.get("deviceMAC");
        return deviceMac;
    }

    public String getQueueType() {
      queueType = (String) map.get("queueType");
        return queueType;
    }

    public String getBrokerType() {
      brokerType = (String) map.get("brokerType");
        return brokerType;
    }

    public String getAuthenticationType() {
      authenticationType =  (String) map.get("authenticationType");//System.getProperty("authenticationType");
        return authenticationType;
    }

    public String getDeviceSerialNo() {
        deviceSerialNo = (String) map.get("deviceSerialNo");
        return deviceSerialNo;
    }

    public String getDeviceSerialNo1() {
        deviceSerialNo1 = (String) map.get("deviceSerialNo1");
        return deviceSerialNo1;
    }

    public String getDeviceSerialNo2() {
        deviceSerialNo2 = (String) map.get("deviceSerialNo2");
        return deviceSerialNo2;
    }

    public String getDeviceSerialNo3() {
        deviceSerialNo3 = (String) map.get("deviceSerialNo3");
        return deviceSerialNo3;
    }

    public String getUrl() {
        url = (String) map.get("url");
        return url;
    }
    
    public String getRestUrl() {
      restUrl = (String) map.get("restUrl");
      return restUrl;
  }

	public String getJiraUrl() {
		jiraUrl = (String) map.get("jiraUrl");
		return jiraUrl;
	}

	

}
