package co.in.bmw.ci.autotest.utils;

import org.openqa.selenium.WebDriver;

public class Constants {
    static WebDriver driver;
    static WebInterfaceWrapper w = new WebInterfaceWrapper(driver);
    static TestNgParameters testNg = new TestNgParameters();
    static CommonUtils cu = new CommonUtils();
    //Login page
    public static final String CHROME_SERVER_DRIVER =System.getProperty("user.dir")+"//fileStore//drivers//chromedriver.exe";
    public static final String IE_SERVER_DRIVER = "C:\\Selenium\\driver\\IEDriverServer_32.exe";
    public static final long IMPLICIT_WAIT = 30;
    public static final long EXPLICIT_WAIT = 50;
    public static final long DEVICE_SAVE_EXPLICIT_WAIT = 25;
    public static final String LOGIN_USERNAME = "autouser";
    public static final String MFGID="mfg0E31";
    public static final String HWversion="1.0.1";
    public static final String SWversion="1.1.1";
    
    
    //Application specific constants
    public static final String COMPANY_CUID = ""+cu.generateRandomNumber()+cu.generateRandomNumber();
    public static final String COMPANY_NAME = "Ui_Test-" + cu.randomName();
    public static final String PRODUCTLINE_NAME = "PM_" + cu.randomName();
    public static final String PACKAGEGROUP_NAME = "package_" + cu.randomName();
    public static final String GROUP_NAME="reportinggrp_"+cu.randomName();
    
    public static final String DEVICE_ID = "3e68debbd53b45";
    //Provision page Control panel
    public static final String CP_USERNAME = "autosubscriber" + cu.randomName();
    public static final String CP_PASSWORD = "autosubscriber" + cu.randomName();
    public static final int IN_RANDOM_NUMBER_LENGTH = 4;
    public static final String ROOT_USERNAME = testNg.getUserName();
    public static final String ROOT_PASSWORD = testNg.getPassword();
    public static final String PHONE_NUMBER = "18000"+cu.generateRandomNumber() + cu.generateRandomNumber();
    public static final String SUBSCRIBER_NAME = "AutoSubscriber" + cu.randomName();
    public static final String SUBSCRIBER_EMAIL = cu.randomName() + "@smartrg.com";
    //REST API url
    public static final String DEVICE_API_URL = testNg.getUrl() + "/api/v1/devices/sn:" + testNg.getDeviceSerialNo() + "/data";
    public static final String DELETE_USER_API = testNg.getUrl() + "/portal/users/";
    //json path
    public static final String WIFI_JSON_PATH = "applications~WIFI~dto~Settings~WIFI~WLANConfigurations~1||applications~WIFI~dto~Settings~WIFI~WLANConfigurations~2||applications~WIFI~dto~Settings~WIFI~WLANConfigurations~3||applications~WIFI~dto~Settings~WIFI~WLANConfigurations~4||applications~WIFI~dto~Settings~WIFI~WLANConfigurations~5";
    public static final String PF_JSON_PATH = "applications~PF~dto~Settings~PortForwards~1";
    public static final String CF_WHITELIST_JSON_PATH = "applications~CF~dto~Settings~ContentFiltering~Whitelist~0";
    public static final String CF_DEFAULT_JSON_PATH = "applications~CF~dto~Settings~ContentFiltering~Default";
    public static final String CF_HOST1_JSON_PATH = "applications~CF~dto~Settings~ContentFiltering~Hosts~1";
    public static final String TB_LOCAL_TIMEZONE_JSON_PATH="applications~TZ~dto~Settings~Time";
    public static final String TB_WINDOWS1_JSON_PATH="applications~TZ~dto~Settings~TimeBlocking~Configuration~Default~Windows~1";
    public static final String TB_WINDOWS2_JSON_PATH="applications~TZ~dto~Settings~TimeBlocking~Configuration~Default~Windows~2";
    public static final String TB_HOST1_JSON_PATH="applications~TZ~dto~Settings~TimeBlocking~Configuration~Hosts~1~Windows~1";
    public static final String TB_HOST2_JSON_PATH="applications~TZ~dto~Settings~TimeBlocking~Configuration~Hosts~1~Windows~2";
    //Subscriber CSV functionality
    public static final String SUBSCRIBER_MANAGEMENT_URL = "http://test.t.com";
    
    //Dynamic end POINT
    public static final String DYNAMICENDPOINT_NAME="DEP"+cu.randomName();
    //Product line page
    public static final String FIRMWARE_NAME = "fw_" + cu.randomName();
    
    //new window swith fail
    public static final String KEYWORD_FAIL="Sorry!";
    public static final String KEYWORD_PASS = "Yes!";
    
 // Locationmanager  settings Localid
    public static final int  dataEnabled = 201;
    public static final int indoorLocation = 202;
    public static final int dataDeliveryInterval = 203;
    public static final int isOnline = 204;
    public static final int isOffline = 205;
    
  
}

