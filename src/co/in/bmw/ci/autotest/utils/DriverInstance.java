package co.in.bmw.ci.autotest.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;


public class DriverInstance {
  private WebDriver driver;

  public WebDriver getDriver() {
    return driver;
  }

  public void setDriver(WebDriver driver) {
    this.driver = driver;
  }

  /**
     * Open browser using browser type constant
     *
     * @return
 * @throws IOException 
     */
  public WebDriver openBrowser(String browserType) {

		if (browserType.equals("Firefox")) {
			/*String Xport = System.getProperty("lmportal.xvfb.id", ":1");
			final File firefoxPath = new File(System.getProperty("path", "/home/agent/Downloads/firefox/firefox"));
			FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);
			firefoxBinary.setEnvironmentProperty("DISPLAY", Xport);*/
			WebDriver driver = new FirefoxDriver();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			
			
			
			
		/*	ChromeDriverService chromeDriverService = new ChromeDriverService.Builder()
			        .usingDriverExecutable(new File("/usr/bin/google-chrome"))
			        .usingAnyFreePort().withEnvironment(ImmutableMap.of("DISPLAY", ":99")).build();
			chromeDriverService.start();
			WebDriver driver = new ChromeDriver(chromeDriverService);
			driver.manage().window().maximize();
		*/	
			return driver;
    } else if (browserType.equals("Chrome")) {
      System.setProperty("webdriver.chrome.driver", Constants.CHROME_SERVER_DRIVER);
      ChromeOptions options = new ChromeOptions();
     options.addArguments("--start-maximized");
    // Headless chrome   
   // options.addArguments("headless");
  //  options.addArguments("window-size=1400x850");
      Map<String, Object> prefs = new HashMap<String, Object>();
      prefs.put("credentials_enable_service", false);
      prefs.put("profile.password_manager_enabled", false);
      options.setExperimentalOption("prefs", prefs);

      DesiredCapabilities capabilities = DesiredCapabilities.chrome();
      capabilities.setCapability(ChromeOptions.CAPABILITY, options);
      driver = new ChromeDriver(capabilities);
      driver.manage().window().maximize();
    } else if (browserType.equals("IE")) {
      System.setProperty("webdriver.ie.driver", Constants.IE_SERVER_DRIVER);
      driver = new InternetExplorerDriver();
    } else if (browserType.equals("Phantomjs")) {
      File file = new File("//home//autotest//e2e//phantomjs//bin//phantomjs");
      System.setProperty("phantomjs.binary.path", file.getAbsolutePath());
      //driver = new PhantomJSDriver();
      //driver.manage().window().maximize();
    } else if (browserType.equals("Htmlunit")) {  
      DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
      capabilities.setBrowserName("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20160101 Firefox/66.0");
      HtmlUnitDriver driver = new HtmlUnitDriver(capabilities);
      driver.setJavascriptEnabled(true);
      //driver.manage().window().maximize();
    }
    return driver;
  }

  /**
   * Open browser using browser type constant
   *
   * @return
   */
  public WebDriver openFirefoxProfile() {
    FirefoxProfile fprofile = new FirefoxProfile();
    // Set Location to store files after downloading.
    fprofile.setPreference("browser.download.dir", System.getProperty("user.home") + "\\CSVReports\\");
    fprofile.setPreference("browser.download.folderList", 2);
    // Set Preference to not show file download confirmation dialogue using
    // MIME types Of different file extension types.
    fprofile.setPreference("browser.helperApps.neverAsk.saveToDisk",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;"// MIME
            // types Of MS Excel File.
            + "application/pdf;" // MIME types Of PDF File.
            + "application/vnd.openxmlformats-officedocument.wordprocessingml.document;" // MIME
            // types Of MS Doc File.
            + "text/plain;" // MIME types Of text File.
            + "text/csv"); // MIME types Of CSV File.
    fprofile.setPreference("browser.download.manager.showWhenStarting", false);
    fprofile.setPreference("pdfjs.disabled", true);
    // Pass fprofile parameter In webdriver to use preferences to download
    // file.
    driver = new FirefoxDriver(fprofile);
    return driver;
  }

}
