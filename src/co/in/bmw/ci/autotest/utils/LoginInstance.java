package co.in.bmw.ci.autotest.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class LoginInstance {
  
      private static LoginInstance appWebInterfaceWrapperaz;
      private TestNgParameters testNg = new TestNgParameters();
      private DriverInstance driverInstance = new DriverInstance();
      private WebDriver driver = driverInstance.openBrowser(testNg.getBrowser());
      private AppWebInterfaceWrapper aw = new AppWebInterfaceWrapper(driver);
      private WebInterfaceWrapper w;
      private String path;
      // private Rest rest = new Rest(Constants.ROOT_USERNAME,
      // Constants.ROOT_PASSWORD);
      // private NorthBoundInterface nbi = new NorthBoundInterface();
      // private Map<String, String> m;
      // private CommonUtils cu;

      /**
       * Constructor encapsulation
       */
      public LoginInstance() {

      }

      public LoginInstance(WebDriver driver) {
        w = new WebInterfaceWrapper(driver);
      }

      private static final Logger log = Logger.getLogger(LoginInstance.class.getName());

      
      public void login(String authenticationType,String userName, String password) throws InterruptedException{
        String token = null;
        if(authenticationType.equals("Okta")){
          loginAsOkta(userName, password);
        }else{
          loginAsAzure("chandran.peethambaram@flex.com", userName, password);
        }
        
      }
      
      /**
       * This method used to login as given user id and password
       *
       * @param userName
       * @param password
       * @return
       * @throws InterruptedException
       */
      public void loginAsOkta(String userName, String password) throws InterruptedException {
        log.debug("loginAsOkta " + userName + " method invoked...");
        //w.openBrowser(testNg.getBrowser());
        w.navigate(testNg.getUrl());
        w.click(CssLocators.loginLinkKey);
        w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
        w.sendKeys(CssLocators.passwordKey, password);
        w.click(CssLocators.signinButtonKey);
        w.waitForTitle("Select Company - Flex Connected Intelligence");

      }
      /**
       * This method used to login as given user id and password
       *
       * @param userName
       * @param password
       * @return
       * @throws InterruptedException
       */
      public void loginAsAzure(String userName,String  azureUserName,String azurePassword) throws InterruptedException {
        log.debug("loginAsAzure " + userName + " method invoked...");
        //w.openBrowser(testNg.getBrowser());
        w.navigate(testNg.getUrl());
        w.click(CssLocators.azureLoginLinkKey);
        w.sendKeys(CssLocators.inputUserIdKey, userName);
        w.click(CssLocators. signInButtonKey);
        w.sendKeys(CssLocators.inputUsernameFieldKey,azureUserName);
        w.sendKeys(CssLocators.inputPasswordFieldKey, azurePassword);
        w.click(CssLocators.loginButtonKey);
        w.waitForTitle("Select Company - Flex Connected Intelligence");

      }
}
