package co.in.bmw.ci.autotest.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Rest {
	public Logger APP_LOGS = Logger.getLogger(WebInterfaceWrapper.class.getName());
	HttpClient client = new DefaultHttpClient();
	private String userName;
	private String passWord;
	private String responseData;
	private int statusCode = 0;
	private JSONObject jsonObj;
	private static final Logger log = Logger.getLogger(Rest.class.getName());

	public Rest(String userName, String passWord) {
		this.userName = userName;
		this.passWord = passWord;
	}

	/**
	 * Setting the Basic Authentication for the Application
	 * 
	 * @return basicAuth
	 */
	public String buildAuthHeader() {
		String userpass = userName + ":" + passWord;
		String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
		return basicAuth;

	}

	/**
	 * Get the API parameters for the given URL
	 * 
	 * @param url
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public String getJson(String url, String token) throws IllegalStateException, IOException {
		try {
			HttpGet request = new HttpGet(url);
			request.setHeader("Accept", "application/json");
			request.setHeader("Authorization", "Bearer " + token);
			HttpResponse response = client.execute(request);
			responseData = this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to get the request", e);
		}
		return responseData;

	}

	public String getJsonWithToken(String url, String token) throws IllegalStateException, IOException {
		try {
			HttpGet request = new HttpGet(url);
			request.setHeader("Accept", "application/json");
			request.setHeader("Authorization", "Bearer " + token);
			HttpResponse response = client.execute(request);
			responseData = this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to get the request", e);
		}
		return responseData;

	}

	public String postJsonWithToken(String url, JSONObject jsonObject, String token)
			throws IllegalStateException, IOException {
		try {
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", buildAuthHeader());
			// post.addHeader("Accept", "application/json");
			post.setHeader("Authorization", "Bearer " + token);
			StringEntity se = new StringEntity(jsonObject.toString());
			post.setEntity(se);
			se.setContentType(ContentType.APPLICATION_JSON.toString());
			HttpResponse response = client.execute(post);
			responseData = this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}
		return responseData;

	}

	public void delete(String url, String token) throws IllegalStateException, IOException {
		try {
			HttpDelete delete = new HttpDelete(url);
			delete.setHeader("Authorization", buildAuthHeader());
			delete.setHeader("Authorization", "Bearer " + token);
			HttpResponse response = client.execute(delete);
			this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}

	}

	/**
	 * Put the API parameters
	 * 
	 * @param url
	 * @param json
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public String putJson(String url, JSONObject json) throws IllegalStateException, IOException {
		try {
			HttpPut put = new HttpPut(url);
			put.setHeader("Authorization", buildAuthHeader());
			StringEntity se = new StringEntity(json.toString());
			put.setEntity(se);
			// se.setContentType(ContentType.APPLICATION_JSON.toString());
			HttpResponse response = client.execute(put);
			responseData = this.readResponseOutput(response);

		} catch (Exception e) {
			log.error("Unable to put the request", e);
		}
		return responseData;
	}

	/**
	 * Post the API parameters
	 * 
	 * @param url
	 * @param jsonArrayData
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public void postJson(String url, JSONArray jsonArrayData) throws IllegalStateException, IOException {
		try {
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", buildAuthHeader());
			StringEntity se = new StringEntity(jsonArrayData.toString());
			post.setEntity(se);
			// se.setContentType(ContentType.APPLICATION_JSON.toString());
			HttpResponse response = client.execute(post);
			this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}

	}

	/**
	 * Post the API parameters
	 */
	public String postJson(String url, JSONObject jsonObject) throws IllegalStateException, IOException {
		try {
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", buildAuthHeader());
			StringEntity se = new StringEntity(jsonObject.toString());
			post.setEntity(se);
			// se.setContentType(ContentType.APPLICATION_JSON.toString());
			HttpResponse response = client.execute(post);
			responseData = this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}
		return responseData;

	}

	/**
	 * Post the API parameters
	 */
	public JSONObject postJsonAndGetObject(String url, JSONObject jsonObject)
			throws IllegalStateException, IOException {
		try {
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", buildAuthHeader());
			StringEntity se = new StringEntity(jsonObject.toString());
			post.setEntity(se);
			// se.setContentType(ContentType.APPLICATION_JSON.toString());
			HttpResponse response = client.execute(post);
			responseData = this.readResponseOutput(response);
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(responseData);
			jsonObj = (JSONObject) obj;
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}
		return jsonObj;

	}

	/**
	 * Post the API parameters
	 */
	public void deleteJson(String url) throws IllegalStateException, IOException {
		try {
			HttpDelete delete = new HttpDelete(url);
			delete.setHeader("Authorization", buildAuthHeader());
			HttpResponse response = client.execute(delete);
			this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}

	}

	public String readResponseOutput(HttpResponse response) throws IllegalStateException, IOException {
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		String lineResponse = "";
		while ((line = rd.readLine()) != null) {
			lineResponse = lineResponse + line;
			log.debug(line);
		}
		return lineResponse;

	}

	/**
	 * Get the API parameters for the given URL
	 */
	public int getJsonWithStatusCode(String url) throws IllegalStateException, IOException {
		try {

			HttpGet request = new HttpGet(url);
			request.setHeader("Authorization", buildAuthHeader());
			HttpResponse response = client.execute(request);
			statusCode = response.getStatusLine().getStatusCode();
			responseData = this.readResponseOutput(response);
		} catch (Exception e) {

			log.error("Unable to get the request", e);
		}
		return statusCode;
	}

	/**
	 * Put JSON and return the object
	 */
	public JSONObject putJsonAndGetObject(String url, JSONObject jsonObject) throws IllegalStateException, IOException {
		try {
			HttpPut put = new HttpPut(url);
			put.setHeader("Authorization", buildAuthHeader());
			StringEntity se = new StringEntity(jsonObject.toString());
			put.setEntity(se);
			// se.setContentType(ContentType.APPLICATION_JSON.toString());
			HttpResponse response = client.execute(put);
			responseData = this.readResponseOutput(response);
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(responseData);
			jsonObj = (JSONObject) obj;
		} catch (Exception e) {
			log.error("Unable to put the request", e);
		}
		return jsonObj;
	}

	/**
	 * Post the API parameters as a Array
	 */
	public String postJsonArray(String url, JSONArray jsonArray) throws IllegalStateException, IOException {
		try {
			HttpPost post = new HttpPost(url);
			post.setHeader("Authorization", buildAuthHeader());
			StringEntity se = new StringEntity(jsonArray.toString());
			post.setEntity(se);
			HttpResponse response = client.execute(post);
			responseData = this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}
		return responseData;
	}
	
	
	public String postJiraApi(String url,JSONObject jsonObject)
			throws IllegalStateException, IOException {
		try {
			HttpPost post = new HttpPost(url);
			post.addHeader("Accept", "application/json");
			post.setHeader("Authorization", "Basic ZGowMDQ4Mjk0OUB0ZWNobWFoaW5kcmEuY29tOkBXZWxjb21lMTIz ");
			StringEntity stringEntity = new StringEntity(jsonObject.toString());
			post.setEntity(stringEntity);
			stringEntity.setContentType(ContentType.APPLICATION_JSON.toString());
			HttpResponse response = client.execute(post);
			responseData = response.getStatusLine().toString();
		} catch (Exception e) {
			log.error("Unable to post the request", e);
		}
		return responseData;

	}
	public String getJiraApi(String url) throws IllegalStateException, IOException {
		try {
			HttpGet request = new HttpGet(url);
			request.setHeader("Accept", "application/json");
			request.setHeader("Authorization", "Basic ZGV2ZW5kcmEuamhhQGZsZXguY29tOkBXZWxjb21lMTIz");
			HttpResponse response = client.execute(request);
			responseData = this.readResponseOutput(response);
		} catch (Exception e) {
			log.error("Unable to get the request", e);
		}
		return responseData;

	}

}
