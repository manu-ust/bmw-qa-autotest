package co.in.bmw.ci.autotest.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class AppWebInterfaceAzure {
	
		  private static AppWebInterfaceAzure appWebInterfaceWrapperaz;
		  private WebInterfaceWrapper w;
		  private TestNgParameters testNg = new TestNgParameters();
		  private String path;
		  // private Rest rest = new Rest(Constants.ROOT_USERNAME,
		  // Constants.ROOT_PASSWORD);
		  // private NorthBoundInterface nbi = new NorthBoundInterface();
		  // private Map<String, String> m;
		  // private CommonUtils cu;

		  /**
		   * Constructor encapsulation
		   */
		  private AppWebInterfaceAzure() {

		  }

		  public AppWebInterfaceAzure(WebDriver driver) {
		    w = new WebInterfaceWrapper(driver);
		  }

		  private static final Logger log = Logger.getLogger(AppWebInterfaceAzure.class.getName());

		  /**
		   * This method used to login as given user id and password
		   *
		   * @param userName
		   * @param password
		   * @return
		   * @throws InterruptedException
		   */
		  public String loginAzure(String  azureUserName,String azurePassword) throws InterruptedException {
		    log.debug("loginAzure As " + azureUserName + " method invoked...");
		    w.navigate(testNg.getUrl());
		    w.click(CssLocators.azureLoginLinkKey);
		    w.sendKeys(CssLocators.inputUserIdKey,azureUserName);
		    w.sendKeys(CssLocators.inputPasswordKey,azurePassword);
		    w.click(CssLocators. signInButtonKey);
		    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
		    w.waitUntilCSSElementVisible(CssLocators.signinCompanySelectKey);
		    w.selectValue(CssLocators.signinCompanySelectKey, "Softweb Ad");
		    w.click(CssLocators.signinCompanySelectKey);
		    w.click(CssLocators.submitCompanyButtonKey);
		    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
		    w.waitUntilElementVisible(CssLocators.productLineSearchboxKey);

		    Object token = w.javaScriptGetText();
		    if (w.isDisplayed(CssLocators.getDisplayedUserNameWelcomeMessage)) {
		        log.debug("Signin successful...");
		      } else {
		        log.debug("Signin failed...");
		      }
		      return token.toString();
		  }
}
