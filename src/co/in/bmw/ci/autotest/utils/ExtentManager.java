package co.in.bmw.ci.autotest.utils;

import java.io.File;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentManager {
	private static ExtentReports extent;
	public static ExtentReports getInstance() {
		if (extent == null) {

			extent = new ExtentReports(System.getProperty("user.dir") + File.separator + "AdvanceReport.html", true);
			System.out.println(System.getProperty("user.dir") + File.separator + "AdvanceReport.html");

		}
		return extent;
	}

}
