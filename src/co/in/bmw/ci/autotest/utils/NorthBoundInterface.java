package co.in.bmw.ci.autotest.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import co.in.bmw.ci.autotest.api.jira.TransitionDetails;
import co.in.bmw.ci.autotest.api.jira.Transitions;
import co.in.bmw.ci.autotest.api.rest.Attributes;
import co.in.bmw.ci.autotest.api.rest.Command;
import co.in.bmw.ci.autotest.api.rest.Commands;
import co.in.bmw.ci.autotest.api.rest.Data;
import co.in.bmw.ci.autotest.api.rest.Groups;
import co.in.bmw.ci.autotest.api.rest.Product;
import co.in.bmw.ci.autotest.api.rest.ProductLineAttribute;
import co.in.bmw.ci.autotest.api.rest.ProductLineSetting;
import co.in.bmw.ci.autotest.api.rest.ProductList;
import co.in.bmw.ci.autotest.api.rest.Productline;
import co.in.bmw.ci.autotest.api.rest.Settings;

/*
NorthBoundInterface
NorthBoundInterface
*/
public class NorthBoundInterface {
  private TestNgParameters testNg = new TestNgParameters();
  private Rest rest = new Rest(Constants.ROOT_USERNAME, Constants.ROOT_PASSWORD);
  private final String companyId = "d0eb9701-6e40-49a4-8c07-efd7e995deb3";

  public NorthBoundInterface() {
  // TODO Auto-generated constructor stub
  }
  /**
   * Method to get list of Products under given company ID
   * 
   *
   * @param serialNo
   * @throws IOException
   */
  public void getProductList(String companyId, String token) throws IOException {
    String getProductsUrl = testNg.getRestUrl() + "/api/v1/company/" + companyId + "/products";
    rest.getJson(getProductsUrl, token);
  }

  public String getproductLineId(String productLineName, String token) throws IOException, ParseException {
    Map<String, String> map = new HashMap<String, String>();
    String productLineId = null;
    String getEndPointUrl = testNg.getRestUrl() + "/api/v1/company/" + companyId + "/productlines";
    Gson gson = new Gson();
    Productline productline = gson.fromJson(rest.getJsonWithToken(getEndPointUrl, token), Productline.class);
    Data[] data = productline.getData();
    for (Data s : data) {
      map.put(s.getName(), s.getProductLineId());
      if (s.getName().equals(productLineName)) {
        productLineId = s.getProductLineId();
      }
    }

    for (Map.Entry<String, String> entry : map.entrySet()) {
      if (entry.getKey().equals(productLineName)) {
        break;
      }
    }
    return productLineId;
  }

  public String getvirtualProductID(String productLineId, String token) throws IOException, ParseException {
    String virtualProdId = null;
    String url = testNg.getRestUrl() + "/api/v1/company/" + companyId + "/productlines/" + productLineId + "/products";
    Gson gson = new Gson();
    Product product = gson.fromJson(rest.getJsonWithToken(url, token), Product.class);
    ProductList[] productList = product.getData();
    for (ProductList p : productList) {
      String[] vId = p.getVirtualProductId();
      for (String virtual : vId) {
        virtualProdId = virtual;
      }
    }
    return virtualProdId;
  }

  public String getProductLineCommandData(String productLineId, String token) throws IOException, ParseException {
    String name = null;
    String url = testNg.getRestUrl() + "/api/v1/company/" + companyId + "/productlines/" + productLineId + "/commands";
    Gson gson = new Gson();
    Command product = gson.fromJson(rest.getJsonWithToken(url, token), Command.class);
    Groups[] groups = product.getGroups();
    for (Groups p : groups) {
      Commands[] cmd = p.getCommands();
      for (Commands c : cmd) {
        name = c.getOnScreenDisplay();
      }
    }
    return name;
  }

  public List<String> getProductLineAttributeData(String productLineId, String token) {
    List<String> list = new ArrayList<>();
    String url = testNg.getRestUrl() + "/api/v1/company/" + companyId + "/productlines/" + productLineId + "/attributes";
    Gson gson = new Gson();
    ProductLineAttribute product = null;
    try {
      product = gson.fromJson(rest.getJsonWithToken(url, token), ProductLineAttribute.class);
    } catch (JsonSyntaxException | IllegalStateException | IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    Attributes[] attr = product.getAttributes();
    for (Attributes p : attr) {
      list.add(p.getName());
    }
    return list;
  }

  public List<String> getProductLineSettingData(String productLineId, String token) {
    List<String> list = new ArrayList<>();
    String url = testNg.getRestUrl() + "/api/v1/company/" + companyId + "/productlines/" + productLineId + "/settings";
    Gson gson = new Gson();
    ProductLineSetting product = null;
    try {
      product = gson.fromJson(rest.getJsonWithToken(url, token), ProductLineSetting.class);
    } catch (JsonSyntaxException | IllegalStateException | IOException e) {
      e.printStackTrace();
    }

    Settings[] attr = product.getSettings();
    for (Settings sett : attr) {
      list.add(sett.getName());
    }

    return list;
  }

  public List<String> getDynamicEPData(String productLineId, String token, String mfgId) {
    List<String> list = new ArrayList<>();
    String url = testNg.getRestUrl() + "/api/v1/company/" + companyId + "/productlines/" + productLineId + "/endpoints/" + mfgId + "/attributes";
    Gson gson = new Gson();
    ProductLineAttribute product = null;
    try {
      product = gson.fromJson(rest.getJsonWithToken(url, token), ProductLineAttribute.class);
    } catch (JsonSyntaxException | IllegalStateException | IOException e) {
      // TODO Auto-generated catch block
      e.getMessage();
    }
    Attributes[] attr = product.getAttributes();
    for (Attributes p : attr) {
      list.add(p.getName());
    }
    return list;
  }

  public void updatedJiraStatus(String ticketNo, String transitionId) throws ParseException, IllegalStateException, IOException {
    JSONParser parser = new JSONParser();
    String jiraUrl = testNg.getJiraUrl() + "/rest/api/latest/issue/" + ticketNo + "/transitions?expand=transitions.fields";
    String addEndPoint = "{\"transition\":{\"id\":\"" + transitionId + "\"}}";
    Object obj = parser.parse(addEndPoint);
    JSONObject jsonObject = (JSONObject) obj;
    rest.postJiraApi(jiraUrl, jsonObject);
  }

  public Map<String, String> getJiraTransitionId(String ticketNo) throws ParseException, IllegalStateException, IOException {
    String jiraUrl = testNg.getJiraUrl() + "/rest/api/latest/issue/" + ticketNo + "/transitions";
    Gson gson = new Gson();
    TransitionDetails transitionDetails = gson.fromJson(rest.getJiraApi(jiraUrl), TransitionDetails.class);
    Transitions[] trans = transitionDetails.getTransitions();
    Map<String, String> map = new HashMap<>();
    for (Transitions id : trans) {
      map.put(id.getName(), id.getId());
    }
    return map;
  }

  public void changeJiraStatus(String methodName, String testCaseStatus) throws ParseException, IllegalStateException, IOException {
    String[] name = methodName.split("_");
    String ticketNo = null;
    /*if (name!=null && name.length>1) {
    	 ticketNo=name[0]+"-"+name[1];
    
    Map<String, String>	map=getJiraTransitionId(ticketNo);
    Iterator iter = (Iterator) map.entrySet().iterator();
    while(iter.hasNext()) {
        Map.Entry entry = (Map.Entry) iter.next();
        if (entry.getKey().equals(testCaseStatus)) {
        	updatedJiraStatus(ticketNo, entry.getValue().toString());
    	}
    }
    }*/
  }

}
