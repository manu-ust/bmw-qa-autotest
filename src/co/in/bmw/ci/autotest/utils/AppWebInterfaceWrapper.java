package co.in.bmw.ci.autotest.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.google.gson.Gson;

import co.in.bmw.ci.autotest.api.rest.NotificationResponse;

public class AppWebInterfaceWrapper {
  private static AppWebInterfaceWrapper appWebInterfaceWrapper;

  private WebInterfaceWrapper w;
  private TestNgParameters testNg = new TestNgParameters();
  String path;
  final SimpleDateFormat dateformat = new SimpleDateFormat("MMM d, yyyy");
  final SimpleDateFormat dateformat1 = new SimpleDateFormat("ddMM");

  /**
   * Constructor encapsulation
   */
  private AppWebInterfaceWrapper() {

  }

  public AppWebInterfaceWrapper(WebDriver driver) {
    w = new WebInterfaceWrapper(driver);
  }

  private static final Logger log = Logger.getLogger(AppWebInterfaceWrapper.class.getName());

  public void login(String authenticationType, String userName, String password) throws InterruptedException {
    String token = null;
    if (authenticationType.equals("Okta")) {
      loginAsOkta(userName, password);
    } else if (authenticationType.equals("Azure")) {
      loginAsAzure(userName, password);
    } else {
      loginAsKeyClock(userName, password);
    }

  }

  /**
   * This method used to login to Keyclock as given user id and password
   *
   * @param userName
   * @param password
   * @return
   * @throws InterruptedException
   */
  public void loginAsKeyClock(String userName, String password) throws InterruptedException {
    log.debug("loginAsKeyClock " + userName + " method invoked...");
    // w.openBrowser(testNg.getBrowser());
    w.navigate(testNg.getUrl());
    w.click(CssLocators.keyClockLinkKey);
    Thread.sleep(3500);
    w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    // w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(3000);

  }

  /**
   * This method used to login as given user id and password
   *
   * @param userName
   * @param password
   * @return
   * @throws InterruptedException
   */
  public void loginAsOkta(String userName, String password) throws InterruptedException {
    log.debug("loginAsOkta " + userName + " method invoked...");
    // w.openBrowser(testNg.getBrowser());
    w.navigate(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);
    w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(3000);

  }

  /**
   * This method used to login as given user id and password
   *
   * @param userName
   * @param password
   * @return
   * @throws InterruptedException
   */
  public void loginAsAzure(String azureUserName, String azurePassword) throws InterruptedException {
    log.debug("loginAsAzure " + azureUserName + " method invoked...");
    // w.openBrowser(testNg.getBrowser());
    w.navigate(testNg.getUrl());
    w.click(CssLocators.azureLoginLinkKey);
    w.sendKeys(CssLocators.inputUserIdKey, "Chandran.Peethambaram@Flexiot.onmicrosoft.com");
    w.sendKeys(CssLocators.inputPasswordKey, "Rose$123");
    w.click(CssLocators.signInButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.waitUntilCSSElementVisible(CssLocators.signinCompanySelectKey);

  }

  public void selectCompany(String companyName) throws Exception {
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.sendKeys(CssLocators.signinCompanySelectKey, companyName);
    w.click(CssLocators.submitCompanyButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    // Object token = w.javaScriptGetText();
    /*
     * if (w.isDisplayed(CssLocators.productLineSearchboxKey)) { if (w.isDisplayed(CssLocators.productLineSearchboxKey)) { log.debug("Signin successful..."); }
     * else { log.debug("Signin failed..."); }
     */
    // return token.toString();
  }

  /**
   * This method used to login as given user id and password
   *
   * @param userName
   * @param password
   * @return
   * @throws InterruptedException
   */
  public String loginAs(String userName, String password) throws InterruptedException {
    log.debug("loginAs " + userName + " method invoked...");
    // w.openBrowser(testNg.getBrowser());
    String login = "Success";
    w.navigate(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);
    w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.waitUntilElementVisible(CssLocators.signinCompanySelectKey);
    w.waitForElementToBeClickable(CssLocators.signinCompanySelectKey);
    Thread.sleep(5000);
    w.sendKeys(CssLocators.signinCompanySelectKey, "e2e-okta-RabbitMQ");
    w.click(CssLocators.submitCompanyButtonKey);
    // Object token = w.javaScriptGetText();
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    if (w.isDisplayed(CssLocators.getDisplayedUserNameWelcomeMessage)) {
      log.debug("Signin successful...");
    } else {
      log.debug("Signin failed...");
    }
    return login;

  }

  /**
   * This method used to login as given user id and password
   *
   * @param userName
   * @param password
   * @throws InterruptedException
   */
  public void justLogin(String userName, String password) throws InterruptedException {
    log.debug("loginAs " + userName + " method invoked...");
    w.openBrowser(testNg.getBrowser());
    w.navigate(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);
    w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.waitForTitle("Select Company - Flex Connected Intelligence");
    if (w.getTitle().equals("Select Company - Flex Connected Intelligence")) {
      log.debug("Signin successful...");
    } else {
      log.debug("Signin failed...");
    }
  }

  /**
   * This method used to login as given user id and password
   *
   * @param userName
   * @param password
   * @return
   * @throws InterruptedException
   */
  public String loginAs1(String userName, String password, int additionalComanyId) throws InterruptedException {
    log.debug("loginAs " + userName + " method invoked...");
    w.openBrowser(testNg.getBrowser());
    w.navigate(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);
    w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.waitForTitle("Select Company - Flex Connected Intelligence");
    w.selectValueByIndex(CssLocators.signinCompanySelectKey, additionalComanyId);
    w.click(CssLocators.submitCompanyButtonKey);
    w.waitForTitle("Product Line - Flex Connected Intelligence");
    w.waitUntilElementVisible(CssLocators.productLineSearchboxKey);
    Object token = w.javaScriptGetText();
    if (w.getTitle().equals("Product Line - Flex Connected Intelligence")) {
      log.debug("Signin successful...");
    } else {
      log.debug("Signin failed...");
    }
    return token.toString();
  }

  public int verifyUserCompany(String userName, String password, int additionalComanyId) throws InterruptedException {
    log.debug("loginAs " + userName + " method invoked...");
    w.openBrowser(testNg.getBrowser());
    w.navigate(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);
    w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.selectValueByIndex(CssLocators.signinCompanySelectKey, additionalComanyId);
    w.click(CssLocators.submitCompanyButtonKey);
    w.waitUntilElementVisible(CssLocators.productLineSearchboxKey);
    Object token = w.javaScriptGetText();
    if (w.isDisplayed(CssLocators.productLineSearchboxKey)) {
      log.debug("Signin successful...");
    } else {
      log.debug("Signin failed...");
    }
    return additionalComanyId;
  }

  /**
   * This method used verify to User login
   * 
   * @throws InterruptedException
   */

  public String verifyLogin(String userName, String password, String company) throws InterruptedException {

    w.navigate(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);
    // sign in without UserID and Password
    w.click(CssLocators.signinButtonKey);
    String errorMessage = w.getText(CssLocators.loginFailedMessage);
    log.debug(errorMessage + "--Error Message Displayed");

    Assert.assertTrue(errorMessage.contains("We found some errors"));

    // signin without Password
    w.sendKeys(CssLocators.loginuserNameFieldKey, userName);
    w.click(CssLocators.signinButtonKey);
    String errorMessage1 = w.getText(CssLocators.getLoginErrorForPassword);
    log.debug(errorMessage1 + "--Error Message Displayed");
    Assert.assertEquals("The field cannot be left blank", errorMessage1);

    // Sign in with Invalid Password
    w.sendKeys(CssLocators.passwordKey, "Null");
    w.click(CssLocators.signinButtonKey);
    Thread.sleep(3000);
    String signInErrorMessage = w.getText(CssLocators.loginFailedMessage);
    log.debug(signInErrorMessage + "--Error Message Displayed for Invalid UserID/Password");
    Assert.assertEquals("Sign in failed!", signInErrorMessage);

    w.clear(CssLocators.passwordKey);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.waitUntilElementVisible(CssLocators.signinCompanySelectKey);
    w.waitForElementToBeClickable(CssLocators.signinCompanySelectKey);
    Thread.sleep(5000);
    w.sendKeys(CssLocators.signinCompanySelectKey, company);
    w.click(CssLocators.submitCompanyButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.waitUntilElementVisible(CssLocators.productLineSearchboxKey);
    Thread.sleep(5000);
    Object token = w.javaScriptGetText();

    if (w.isDisplayed(CssLocators.productLineSearchboxKey)) {
      log.debug("Signin successful...");
    } else {
      log.debug("Signin failed...");
    }
    return token.toString();

  }

  public boolean selectProductLine(String name) throws InterruptedException {
    log.debug("searchProductLine method invoked...");
    try {
      w.click(CssLocators.productMaster);
      Thread.sleep(1000);
      w.clear(CssLocators.searchboxKey);
      w.sendKeys(CssLocators.searchboxKey, name);
      Thread.sleep(1000);
      // getElementFromList(name,
      // CssLocators.searchProductLineResultKey).click();
      Assert.assertTrue(w.getDriver().findElement(By.xpath("//td[contains(text(),'" + name + "')]")).isDisplayed());
      return true;
    } catch (Exception e) {
      return false;
    }

  }

  /**
   * Method to get a particular webelement from a list based on name
   * 
   * @param name
   * @param csslocator
   * @return WebElement
   */
  public WebElement getElementFromList(String name, String csslocator) {
    log.debug("getElementFromList started");
    List<WebElement> options = w.getWebElements(csslocator);
    for (int i = 0; i <= options.size(); i++) {
      String str = options.get(i).getText();
      if (str.equalsIgnoreCase(name)) {
        log.debug("match found.." + options.get(i).getText());
        return options.get(i);
      }
    }
    return null;
  }

  /**
   * Method to Search Product
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public boolean searchProduct(String id) throws InterruptedException {
    log.debug("searchProduct method invoked...");
    try {
      w.waitUntilElementVisible(CssLocators.searchboxKey);
      Thread.sleep(2000);
      w.clear(CssLocators.searchboxKey);
      Thread.sleep(2000);
      w.sendKeys(CssLocators.searchboxKey, id);
      Thread.sleep(2000);
      Assert.assertEquals(w.getText(CssLocators.searchProductDeviceIDResultKey), id);
      return true;
    } catch (Exception e) {
      return false;
    }

  }

  /**
   * Method to get Product status
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public String getFirstProductStatus(String productLineName, String id) throws InterruptedException {
    log.debug("getProductStatus method invoked...");
    selectProductLine(productLineName);
    searchProduct(id);
    w.click(CssLocators.clickOnView);
    String status = w.getText(CssLocators.productStatusContainerKey);
    return status;
  }

  /**
   * Method to Add Product Line
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public String addProductLine(String productLineName) throws InterruptedException {
    log.debug("addProductLine method invoked...");
    String productLineSku = productLineName + "_SKU";
    String productLineDescription = "Product Line definition used for e2e autotest";
    // w.getDriver().findElement(By.xpath("//*[@id=\"btnHeaderAdd\"]")).click();
    w.waitUntilCssElementVisible(CssLocators.productMaster);
    w.click(CssLocators.productMaster);
    if (w.isDisplayed(CssLocators.addProductLineLinkKey)) {
      w.click(CssLocators.addProductLineLinkKey);
    }
    w.sendKeys(CssLocators.addProductLineNameFieldKey, productLineName);
    w.sendKeys(CssLocators.addProductLineSkuFieldKey, productLineSku);
    w.sendKeys(CssLocators.addProductLineDescriptionFieldKey, productLineDescription);
    w.click(CssLocators.addProductLineSaveButtonKey);
    return productLineName;
  }

  /**
   * Method to Add Product Line
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public boolean configureProductLine(String productLineName) throws InterruptedException {
    log.debug("addProductLine method invoked...");
    // w.click(CssLocators.configureProductLineButtonKey);
    if (w.isEnabled(CssLocators.addProductLineLinkKey)) {
      return true;
    }
    return false;
  }

  /**
   * Method to update Product Line SKU
   * 
   * @return
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */

  public String updateProductLine(String productLineName) throws InterruptedException {
    log.debug("updateProductLineSku method invoked...");
    String productline_desc = w.generateRandomNumber() + "DESC";
    selectProductLine(productLineName);
    w.click(CssLocators.editProductLineButtonKey);
    Thread.sleep(5000);
    w.clear(CssLocators.addProductLineDescriptionFieldKey);
    w.sendKeys(CssLocators.addProductLineDescriptionFieldKey, productline_desc);
    w.click(CssLocators.clickSaveButtonKey);
    return productline_desc;
  }

  /**
   * Method to Delete Product Line
   *
   * @throws java.lang.InterruptedException
   */
  public void deleteProductLine(String productLineName) throws InterruptedException {
    log.debug("deleteProductLine method invoked...");
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Assert.assertTrue(selectProductLine(productLineName));
    w.click(CssLocators.deleteProductLineButtonKey);
    w.click(CssLocators.deleteConfirmationNotificationKey);
  }

  /**
   * Method to Configure Product Line Attributes
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public String configureProductLineAttributes(String productLineName) throws InterruptedException {
    log.debug("configureProductLineAttributes method invoked...");
    String attributeName = "Attribute-1_" + w.randomName();
    String attributeDescription = "Test attribute-1 used for e2e autotest";
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.attributesLinkKey);
    w.click(CssLocators.addAttributesButtonKey);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.sendKeys(CssLocators.addAttributeDescriptionFieldKey, attributeDescription);
    w.selectValue(CssLocators.addAttributeDataTypeFieldKey, "Float");
    w.click(CssLocators.addAttributeSaveButtonKey);
    w.waitUntilElementVisible(CssLocators.attributesTextKey);
    Assert.assertEquals(w.getText(CssLocators.attributesTextKey), attributeName);
    return attributeName;
  }

  /**
   * Method to Configure Product Line Settings
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public String configureProductLineSettings(String productLineName) throws InterruptedException {
    log.debug("configureProductLineSettings method invoked...");
    String settingName = "Settings-1_" + w.randomName();
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.settingsLinkKey);
    w.click(CssLocators.addSettingsButtonKey);
    w.selectValue(CssLocators.addSettingsTypeSelectKey, "Text Field");
    w.click(CssLocators.addSettingTypeNextKey);
    w.sendKeys(CssLocators.addSettingNameFieldKey, settingName);
    w.sendKeys(CssLocators.addSettingGroupNameFieldKey, settingName);
    w.selectValue(CssLocators.addSettingDataTypeFieldKey, "Float");
    w.sendKeys(CssLocators.addSettingDefaultValueFieldKey, "65");
    w.click(CssLocators.addAttributeSaveButtonKey);
    w.waitUntilElementVisible(CssLocators.settingsTextKey);
    Assert.assertEquals(w.getText(CssLocators.settingsTextKey), settingName);
    return settingName;
  }

  /**
   * Method to Configure Product Line Settings
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public String configureProductLineSettingsCB(String productLineName) throws InterruptedException {
    log.debug("configureProductLineSettings method invoked...");
    String settingName = "Settings-1_" + w.randomName();
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.settingsLinkKey);
    w.click(CssLocators.addSettingsButtonKey);
    w.selectValue(CssLocators.addSettingsTypeSelectKey, "Text Field");
    w.click(CssLocators.addSettingTypeNextKey);
    w.sendKeys(CssLocators.addSettingNameFieldKey, settingName);
    w.sendKeys(CssLocators.addSettingGroupNameFieldKey, settingName);
    w.selectValue(CssLocators.addSettingDataTypeFieldKey, "Float");
    w.click(CssLocators.addAttributeSaveButtonKey);
    w.waitUntilElementVisible(CssLocators.settingsTextKey);
    Assert.assertEquals(w.getText(CssLocators.settingsTextKey), settingName);
    return settingName;
  }

  /**
   * Method to Configure Product Line Commands
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public String configureProductLineCommands(String productLineName) throws InterruptedException {
    log.debug("configureProductLineCommands method invoked...");
    String commandName = "Command-1_" + w.randomName();
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.commandsLinkKey);
    w.click(CssLocators.addCommandsButtonKey);
    w.selectValue(CssLocators.addCommandTypeSelectKey, "Dropdown");
    w.sendKeys(CssLocators.addCommandTypeNameFieldKey, commandName);
    w.click(CssLocators.addCommandTypeNextKey);
    w.sendKeys(CssLocators.addCommandNameFieldKey, commandName);
    w.sendKeys(CssLocators.addCommandGroupNameFieldKey, commandName);
    w.selectValue(CssLocators.addCommandDataTypeFieldKey, "Float");
    w.sendKeys(CssLocators.addCommandDeviceValueFieldKey, "0");
    w.sendKeys(CssLocators.addCommandPossibleValueFieldKey, "0,1");
    w.sendKeys(CssLocators.addCommandValueTypeFieldKey, "integer");
    w.click(CssLocators.addAttributeSaveButtonKey);
    w.waitUntilElementVisible(CssLocators.commandsTextKey);
    Assert.assertEquals(w.getText(CssLocators.commandsTextKey), commandName);
    return commandName;
  }

  /**
   * Method to write Json file
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  @SuppressWarnings("unchecked")
  public void writeJsonFile(String deviceId, String serialNumber) throws java.text.ParseException, IOException, InterruptedException {
    log.debug("writeJsonFile method invoked...");

    JSONObject obj = new JSONObject();
    obj.put("RUNDATE", "2014-05-05T06:00:00.000Z");
    obj.put("SITE", "ATL");

    JSONObject productPropetiesObj = new JSONObject();
    productPropetiesObj.put("Status", "0X10");
    productPropetiesObj.put("MAC", "00:00:00:00:00:00");
    productPropetiesObj.put("OS", "LINUX");
    productPropetiesObj.put("OSVersion", "1.0.0");
    productPropetiesObj.put("ApiVersion", "1.0");
    productPropetiesObj.put("HwVersion", "1.0.0");
    productPropetiesObj.put("HwName", "Amsterdam");
    productPropetiesObj.put("Builddate", "2014-05-05T06:00:00.000Z");
    productPropetiesObj.put("Batch", "100");
    productPropetiesObj.put("RMA", "123");
    productPropetiesObj.put("RmaDate", "2016-05-05T06:00:00.000Z");
    productPropetiesObj.put("SN", serialNumber);
    productPropetiesObj.put("SwVersion", "1.0.0");
    productPropetiesObj.put("SwName", "FocusLife" + w.randomName());
    productPropetiesObj.put("DeviceId", deviceId);

    JSONArray list = new JSONArray();
    list.add(productPropetiesObj);

    obj.put("PRODUCT", list);
    try {

      FileWriter file = new FileWriter(absolutePath());
      file.write(obj.toJSONString());
      file.flush();
      file.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.print(obj);
  }

  /**
   * Method to write Json file with specified HW version And SW version
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  @SuppressWarnings("unchecked")
  public void writeProductJsonFile(String deviceId, String serialNumber, String swVersion, String hwVersion)
      throws java.text.ParseException, IOException, InterruptedException {
    log.debug("writeJsonFile method invoked...");

    JSONObject obj = new JSONObject();
    obj.put("RUNDATE", "2014-05-05T06:00:00.000Z");
    obj.put("SITE", "ATL");

    JSONObject productPropetiesObj = new JSONObject();
    productPropetiesObj.put("Status", "0X10");
    productPropetiesObj.put("MAC", "00:00:00:00:00:00");
    productPropetiesObj.put("OS", "LINUX");
    productPropetiesObj.put("OSVersion", "1.0.0");
    productPropetiesObj.put("ApiVersion", "1.0");
    productPropetiesObj.put("HwVersion", hwVersion);
    productPropetiesObj.put("HwName", "Amsterdam");
    productPropetiesObj.put("Builddate", "2014-05-05T06:00:00.000Z");
    productPropetiesObj.put("Batch", "100");
    productPropetiesObj.put("RMA", "123");
    productPropetiesObj.put("RmaDate", "2016-05-05T06:00:00.000Z");
    productPropetiesObj.put("SN", serialNumber);
    productPropetiesObj.put("SwVersion", swVersion);
    productPropetiesObj.put("SwName", "FocusLife" + w.randomName());
    productPropetiesObj.put("DeviceId", deviceId);

    JSONArray list = new JSONArray();
    list.add(productPropetiesObj);

    obj.put("PRODUCT", list);
    try {

      FileWriter file = new FileWriter(absolutePath());
      file.write(obj.toJSONString());
      file.flush();
      file.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.print(obj);
  }

  /**
   * Method to Create Product using Json file
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  public void addProductUsingJsonFile(String productLineName, String deviceId, String serialNumber)
      throws java.text.ParseException, IOException, InterruptedException {
    log.debug("addProductUsingJsonFile method invoked...");
    Thread.sleep(4000);
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    w.waitUntilCSSElementVisible(CssLocators.addProductButtonKey);
    Thread.sleep(2000);
    w.click(CssLocators.addProductButtonKey);
    writeJsonFile(deviceId, serialNumber);
    Thread.sleep(2000);
    w.waitUntilCSSElementVisible(CssLocators.fileInputStringKey);
    w.sendKeys(CssLocators.fileInputStringKey, absolutePath());
    w.click(CssLocators.fileUploadSubmitButtonKey);
    w.waitUntilCSSElementVisible(CssLocators.fileUploadConfirmButtonKey);
    w.click(CssLocators.fileUploadConfirmButtonKey);
    Thread.sleep(4000);

  }

  /**
   * Method to add Filter for Product Master on Dash board
   */

  public void filterProductMasterOnDashboard(String productMaster) throws InterruptedException {
    w.click(CssLocators.dashbordLinkKey);
    Thread.sleep(2500);
    w.selectValue(CssLocators.selectProductMasterInDashboard, productMaster);

  }

  /**
   * Method to get total count of Products on Dashboard
   * 
   * @return
   * @throws InterruptedException
   */
  public int getDashboardProductCount() throws InterruptedException {
    w.click(CssLocators.dashbordLinkKey);
    Thread.sleep(2500);
    String count = w.getText(CssLocators.getTotalProductCount);
    return Integer.valueOf(count);
  }

  /**
   * Method to get total count of Not active Products on Dashboard
   * 
   * @return
   * @throws InterruptedException
   */
  public int getDashboardInactiveProductCount() throws InterruptedException {
    w.click(CssLocators.dashbordLinkKey);
    Thread.sleep(2500);
    String count = w.getText(CssLocators.getTotalProductCount);
    return Integer.valueOf(count);
  }

  public boolean notYetActiveProductOnDashboard(String productMaster, String diviceId) throws InterruptedException {
    w.click(CssLocators.dashbordLinkKey);
    Thread.sleep(2500);
    filterProductMasterOnDashboard(productMaster);
    String count = w.getDriver().findElement(By.xpath("//label[text()=\"Not yet active\"]/preceding-sibling::p")).getText();
    Assert.assertEquals(count, "1");
    w.click(CssLocators.dashboardProductDetails);
    searchProduct(diviceId);
    String status = w.getText(CssLocators.productStatus);
    Assert.assertTrue(status.contains("Not Yet Active"));
    if (status.contains("Not Yet Active")) {
      return true;
    }
    return false;
  }

  public boolean activedProductsOnDashboard(String productMaster, String diviceId) throws InterruptedException {
    w.click(CssLocators.dashbordLinkKey);
    Thread.sleep(2500);
    filterProductMasterOnDashboard(productMaster);
    String count = w.getDriver().findElement(By.xpath("//label[text()=\"Active and not connected\"]/preceding-sibling::p[1]")).getText();
    Assert.assertEquals(count, "1");
    w.click(CssLocators.dashboardProductDetails);
    searchProduct(diviceId);
    String status = w.getText(CssLocators.productStatus);
    Assert.assertTrue(status.contains("Active and Not Connected"));
    if (status.contains("Active and Not Connected")) {
      return true;
    }
    return false;
  }

  public boolean connectedProductsOnDashboard(String productMaster, String diviceId) throws InterruptedException {
    w.click(CssLocators.dashbordLinkKey);
    Thread.sleep(2500);
    filterProductMasterOnDashboard(productMaster);
    String count = w.getDriver().findElement(By.xpath("//label[text()=\"Active and connected\"]/preceding-sibling::p[1]")).getText();
    Assert.assertEquals(count, "1");
    w.click(CssLocators.dashboardProductDetails);
    searchProduct(diviceId);
    String status = w.getText(CssLocators.productStatus);
    Assert.assertTrue(status.contains("Active and Connected"));
    if (status.contains("Active and Connected")) {
      return true;
    }
    return false;
  }

  /**
   * Method to Activate a Product by interacting with Agent
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  public void publishDeactivateCommand(String productLineName, String deviceID) throws java.text.ParseException, IOException, InterruptedException {
    log.debug("publishDeactivateCommand method invoked...");
    w.click(CssLocators.clickOnProductLink);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Assert.assertEquals(getFirstProductStatus(productLineName, deviceID), "Active");
    w.click(CssLocators.commandsLinkKey);
    w.click(CssLocators.productDeactivateButtonKey);
    w.click(CssLocators.successOkButtonKey);
    Assert.assertEquals(getFirstProductStatus(productLineName, deviceID), "Inactive");
  }

  /**
   * This method used to generate current date,day,month and year
   *
   * @return current time in hh:mm:ss
   * @throws InterruptedException
   */
  public String generateUserLoginTime() throws InterruptedException {
    log.debug("generateUserLoginTime started");
    Calendar now = Calendar.getInstance();
    String friendlyDate = getFriendlyDate(now);
    log.debug(friendlyDate);
    log.debug("generateUserLoginTime completed");
    return friendlyDate;
  }

  /**
   * This method used to get Date and Time
   *
   * @param theDate
   * @return current time in hh:mm:ss
   * @throws InterruptedException
   */
  public static String getFriendlyDate(Calendar theDate) throws InterruptedException {
    return getFriendlyDate(theDate, false);
  }

  /**
   * This method used to get Date
   *
   * @param theDate
   * @param verbose
   * @return current date in the corresponding format Eg.26th Dec 2015
   * @throws InterruptedException
   */
  public static String getFriendlyDate(Calendar theDate, boolean verbose) throws InterruptedException {
    log.debug("getFriendlyDate started");
    int month = theDate.get(Calendar.MONTH);
    int year = theDate.get(Calendar.YEAR);
    int dayOfMonth = theDate.get(Calendar.DAY_OF_MONTH);
    int dayOfWeek = theDate.get(Calendar.DAY_OF_WEEK);
    // Get the day of the week as a String.
    // Note: The Calendar DAY_OF_WEEK property is NOT zero-based, and Sunday
    // is the first day of week.
    String friendly = "";
    switch (dayOfWeek) {
    case 1:
      friendly = "Sun";
      break;
    case 2:
      friendly = "Mon";
      break;
    case 3:
      friendly = "Tue";
      break;
    case 4:
      friendly = "Wed";
      break;
    case 5:
      friendly = "Thu";
      break;
    case 6:
      friendly = "Fri";
      break;
    case 7:
      friendly = "Sat";
      break;
    default:
      friendly = "BadDayValue";
      break;
    }
    friendly += " ";
    // Get a friendly version of the month.
    // Note: The Calendar MONTH property is zero-based to increase the
    // chance of developers making mistakes.
    switch (month) {
    case 0:
      friendly += "Jan";
      break;
    case 1:
      friendly += "Feb";
      break;
    case 2:
      friendly += "Mar";
      break;
    case 3:
      friendly += "Apr";
      break;
    case 4:
      friendly += "May";
      break;
    case 5:
      friendly += "Jun";
      break;
    case 6:
      friendly += "Jul";
      break;
    case 7:
      friendly += "Aug";
      break;
    case 8:
      friendly += "Sep";
      break;
    case 9:
      friendly += "Oct";
      break;
    case 10:
      friendly += "Nov";
      break;
    case 11:
      friendly += "Dec";
      break;
    default:
      friendly += "BadMonthValue";
      break;
    }
    // Add padding and the prefix to the day of month
    if (verbose == true) {
      friendly += " the " + dayOfMonth;
    } else {
      friendly += " " + dayOfMonth;
    }
    String dayString = String.valueOf(dayOfMonth); // Convert dayOfMonth to
    // String using valueOf
    // Suffix is "th" for day of day of month values ending in 0, 4, 5, 6,
    // 7, 8, and 9
    if (dayString.endsWith("0") || dayString.endsWith("4") || dayString.endsWith("5") || dayString.endsWith("6") || dayString.endsWith("7")
        || dayString.endsWith("8") || dayString.endsWith("9")) {
      friendly += "th ";
    }
    // Suffix is "st" for day of month values ending in 1
    if (dayString.endsWith("1")) {
      friendly += "st ";
    }
    // Suffix is "nd" for day of month values ending in 2
    if (dayString.endsWith("2")) {
      friendly += "nd ";
    }
    // Suffix is "rd" for day of month values ending in 3
    if (dayString.endsWith("3")) {
      friendly += "rd ";
    }
    // Add more padding if we've been asked to be verbose
    if (verbose == true) {
      friendly += "of ";
    }
    friendly += "" + year;
    log.debug("getFriendlyDate completed");
    return friendly;
  }

  /**
   * This method used to get the current Calendar Time in a Calendar object
   *
   * @return current time in hh:mm:ss
   * @throws InterruptedException
   */
  public String currentCalenderTime() throws InterruptedException {
    log.debug("currentCalenderTime started");
    DateFormat df = new SimpleDateFormat("HH:mm:ss");
    Date dateObj = new Date();
    log.debug(df.format(dateObj));
    log.debug("currentCalenderTime completed");
    return (df.format(dateObj));
  }

  /**
   * This method used to calculate the time difference
   *
   * @param time1-
   *          hh:mm:ss
   * @param time2-
   *          hh:mm:ss
   * @return time difference between time1 and time2
   * @throws InterruptedException
   * @throws ParseException
   */
  public boolean getTimeDifference(String time1, String time2) throws InterruptedException, ParseException {
    log.debug("getTimeDifference started");
    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
    Date date1 = format.parse(time1);
    Date date2 = format.parse(time2);
    long difference = date2.getTime() - date1.getTime();
    double seconds = difference / 1000.0;
    log.debug(seconds + " Difference in Seconds");
    boolean timeDifference = false;
    if (seconds <= 150) {
      log.debug("User Login Time Verified successfully");
      timeDifference = true;
    }
    log.debug("getTimeDifference completed");
    return timeDifference;
  }

  /**
   * This method used to modify the key value pair node in JSON Editor
   *
   * @param key
   *          - key field in the json editor
   * @param index
   *          - length of the key text
   * @param value
   *          - value field in the json editor
   */

  public void modifyKeyValuePair(String key, String index, String value) {
    log.debug("modifyKeyValuePair started");
    WebElement element = w.getDriver().findElement(By.partialLinkText(key));
    Actions actions = new Actions(w.getDriver());
    actions.contextClick(element).build().perform();
    actions.sendKeys(Keys.RETURN).build().perform();
    if (System.getProperty("os.name").contains("Mac")) {
      log.debug("MAC");
      actions.sendKeys(Keys.COMMAND).sendKeys("a").build().perform();
    } else {
      log.debug("NON-MAC");
      actions.keyDown(Keys.CONTROL).sendKeys("a").build().perform();
      actions.keyUp(Keys.CONTROL);
    }
    w.sendKeys("//input[@value='" + key + "']", key);
    actions.sendKeys(Keys.TAB).perform();
    // actions = clearAndEnterText(index.length(), value);
    log.debug("modifyKeyValuePair completed");
  }

  /**
   * This method method used to Logout of Application
   *
   * @throws InterruptedException
   */
  public void logOut() throws Exception {
    log.debug("logOut started");
    // w.waitUntilElementVisible(XpathLocators.customerSupportTabLinkKey);
    // w.click(XpathLocators.customerSupportTabLinkKey);
    // clickOnArrowDownKey();
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    // w.click(CssLocators.logOutLinkKey);
    w.clickOnLink("Logout");
    // w.waitForTitle("SmartRG Device Manager");
    // Assert.assertEquals(w.getTitle(), "SmartRG Device Manager");
    log.debug("logOut completed");
  }

  /**
   * This is a common method used to logIn Application
   *
   * @param login
   *          - login
   * @param password
   *          - password
   * @throws InterruptedException
   */
  public void logIn(String login, String password) {
    log.debug("logIn started");
    w.sendKeys(CssLocators.loginuserNameFieldKey, login);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.waitForTitle("Home");
    Assert.assertEquals(w.getTitle(), "Home");
    log.debug("logIn completed");
  }

  /**
   * This method used to generate random value using given max and min value
   *
   * @param maxValue
   * @param minValue
   * @return randomValue
   * @throws InterruptedException
   */
  public int randomValueGenerate(int maxValue, int minValue) {
    log.debug("randomValueGenerate started");
    Random rand = new Random();
    int randomValue = rand.nextInt(maxValue) + minValue;
    log.debug("randomValueGenerate completed");
    return randomValue;
  }

  /**
   * This method method used to split value and return the index location
   *
   * @param inputValue
   *          - Input String to be checked
   * @param splitFormat
   *          - comma or backspace
   * @param index
   *          - position of the string after splitting
   * @return splitValue - returns the string value for the index given
   * @throws InterruptedException
   */
  public String splitValue(String inputValue, String splitFormat, int index) {
    log.debug("splitValue started");
    String[] splitValue;
    splitValue = inputValue.split(splitFormat);
    log.debug(splitValue[index] + "Actual Value");
    log.debug("splitValue completed");
    return splitValue[index];
  }

  /**
   * This method used to Get,Shuffle and Return Events
   *
   * @return Random Events Name
   * @throws InterruptedException
   */
  public String getRandomEvents() throws InterruptedException {
    log.debug("getRandomEvents started");
    String getRandomEvents;
    List<String> list = new ArrayList<String>();
    list.add("Subscriber associated");
    list.add("Reboot");
    list.add("Service enable");
    list.add("Service disable");
    list.add("Firmware upgraded");
    Collections.shuffle(list);
    getRandomEvents = list.get(0);
    log.debug("getRandomEvents completed");
    return getRandomEvents;
  }

  /**
   * THIS METHOD WILL BE DEPRECATED
   */

  public String getCalenderDate() throws InterruptedException {
    log.debug("getCalenderDate started");
    // Get the current date and time in a Calendar object
    Calendar now = Calendar.getInstance();
    String friendlyDate = getFriendlyDate(now);
    log.debug(friendlyDate);
    log.debug("getCalenderDate completed");
    return friendlyDate;
  }

  /**
   * Method to Add Attributes reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addAttribute(String productlineName, String dataType, String id) throws InterruptedException {
    log.debug("addAttribute method invoked...");
    String attributeName = dataType + "-Attr" + w.randomName();
    w.click(CssLocators.addAttributesButtonKey);
    Thread.sleep(1000);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.sendKeys(CssLocators.addAttributeGroupName, attributeName);
    w.sendKeys(CssLocators.addAttributeDescriptionFieldKey, "Test attribute-1 used for e2e autotest");
    w.selectValue(CssLocators.addAttributeDataTypeFieldKey, dataType);
    w.clear(CssLocators.addAttributeLocalIDFieldKey);
    w.sendKeys(CssLocators.addAttributeLocalIDFieldKey, id);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(1000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.mouseOverOnElement(CssLocators.addAttributesButtonKey);
    return attributeName;
  }

  /**
   * Method to Add Attributes reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addAttributeMinMax(String productlineName, String dataType, String id) throws InterruptedException {
    log.debug("addAttribute method invoked...");
    String attributeName = dataType + "-Attr" + w.randomName();
    Thread.sleep(1000);
    w.click(CssLocators.addAttributesButtonKey);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.sendKeys(CssLocators.addAttributeGroupName, attributeName);
    w.sendKeys(CssLocators.addAttributeDescriptionFieldKey, "Test attribute-1 used for e2e autotest");
    w.selectValue(CssLocators.addAttributeDataTypeFieldKey, dataType);

    w.selectValue(CssLocators.addAttributeconstraintsTypeFieldKey, "Min");
    w.sendKeys(CssLocators.addAttributeconstraintsTextFieldKey, "0");
    w.click(CssLocators.addAttributeconstraintsAddButtonFieldKey);
    w.selectValue(CssLocators.addAttributeconstraintsTypeFieldKey, "Max");

    if (dataType == "Byte") {
      w.sendKeys(CssLocators.addAttributeconstraintsTextFieldKey, "1");
    } else {
      w.sendKeys(CssLocators.addAttributeconstraintsTextFieldKey, "1000");
    }
    w.click(CssLocators.addAttributeconstraintsAddButtonFieldKey);
    w.clear(CssLocators.addAttributeLocalIDFieldKey);
    w.sendKeys(CssLocators.addAttributeLocalIDFieldKey, id);

    w.click(CssLocators.clickSaveButtonKey);

    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(1000);
    w.mouseOverOnElement(CssLocators.addAttributesButtonKey);
    return attributeName;
  }

  /**
   * Method to Add Attributes reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addAttributeWithAllConstraints(String productlineName, String dataType, String id) throws InterruptedException {
    log.debug("addAttribute method invoked...");
    String attributeName = dataType + "-Attr" + w.randomName();
    w.click(CssLocators.addAttributesButtonKey);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.sendKeys(CssLocators.addAttributeGroupName, attributeName);
    w.sendKeys(CssLocators.addAttributeDescriptionFieldKey, "Test attribute-1 used for e2e autotest");
    w.selectValue(CssLocators.addAttributeDataTypeFieldKey, dataType);
    w.clear(CssLocators.addAttributeLocalIDFieldKey);
    w.sendKeys(CssLocators.addAttributeLocalIDFieldKey, id);
    w.selectValue(CssLocators.addAttributeconstraintsTypeFieldKey, "Positive");
    w.click(CssLocators.addAttributeconstraintsAddButtonFieldKey);
    w.selectValue(CssLocators.addAttributeconstraintsTypeFieldKey, "Min");
    w.sendKeys(CssLocators.addAttributeconstraintsTextFieldKey, "0");
    w.click(CssLocators.addAttributeconstraintsAddButtonFieldKey);
    w.selectValue(CssLocators.addAttributeconstraintsTypeFieldKey, "Max");
    w.sendKeys(CssLocators.addAttributeconstraintsTextFieldKey, "1000");
    w.click(CssLocators.addAttributeconstraintsAddButtonFieldKey);

    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return attributeName;
  }

  /**
   * Method to Add Attributes with constraints reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addAttributeStringConstraint(String productlineName, String dataType) throws InterruptedException {
    log.debug("addAttribute method invoked...");
    String attributeName = "Attribute-1_" + w.randomName();
    w.click(CssLocators.addAttributesButtonKey);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.sendKeys(CssLocators.addAttributeGroupName, attributeName);
    w.sendKeys(CssLocators.addAttributeDescriptionFieldKey, "Test attribute-1 used for e2e autotest");
    w.selectValue(CssLocators.addAttributeDataTypeFieldKey, dataType);

    w.selectValue(CssLocators.addAttributeconstraintsTypeFieldKey, "MaxLength");
    w.sendKeys(CssLocators.addAttributeconstraintsTextFieldKey, "655");
    w.click(CssLocators.addAttributeconstraintsAddButtonFieldKey);

    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.addAttributesButtonKey);
    return attributeName;
  }

  /**
   * Method to Add Attributes with Local Id
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public void addAttributeWithlocalId(String productlineName, String dataType, String localId) throws InterruptedException {
    log.debug("addAttribute method invoked...");
    String attributeName = "Attribute-1_" + w.randomName();
    w.click(CssLocators.addAttributesButtonKey);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.sendKeys(CssLocators.addAttributeGroupName, attributeName);
    w.sendKeys(CssLocators.addAttributeDescriptionFieldKey, "Test attribute-1 used for e2e autotest");
    w.selectValue(CssLocators.addAttributeDataTypeFieldKey, dataType);
    w.clear(CssLocators.locaiIdkey);
    w.sendKeys(CssLocators.locaiIdkey, localId);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.addAttributesButtonKey);

  }

  /**
   * Method to Add Settings reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addSetting(String productlineName, String type, String dataType) throws InterruptedException {
    log.debug("addSetting method invoked...");
    String settingName = "Setting-1_" + w.randomName();
    w.click(CssLocators.addSettingsButtonKey);
    w.selectValue(CssLocators.addSettingsTypeSelectKey, type);
    w.click(CssLocators.addSettingTypeNextKey);
    w.sendKeys(CssLocators.addSettingNameFieldKey, settingName);
    w.sendKeys(CssLocators.addSettingGroupNameFieldKey, settingName);
    w.selectValue(CssLocators.addSettingDataTypeFieldKey, dataType);
    w.sendKeys(CssLocators.addSettingDefaultValueFieldKey, "1");
    w.click(CssLocators.addSettingsSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.addAttributesButtonKey);

    return settingName;
  }

  /**
   * Method to Add Settings reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addSettingDropDown(String productlineName, String type, String dataType) throws InterruptedException {
    log.debug("addSetting method invoked...");
    String settingName = "Setting-1_" + w.randomName();
    // w.click(CssLocators.attributesLinkKey);
    w.click(CssLocators.addSettingsButtonKey);
    w.selectValue(CssLocators.addSettingsTypeSelectKey, type);
    w.click(CssLocators.addSettingTypeNextKey);
    w.sendKeys(CssLocators.addSettingNameFieldKey, settingName);
    w.sendKeys(CssLocators.addSettingGroupNameFieldKey, settingName);
    w.selectValue(CssLocators.addSettingDataTypeFieldKey, dataType);
    w.sendKeys(CssLocators.addSettingDefaultValueFieldKey, "1");
    w.sendKeys(CssLocators.addSettingPossibleValueFieldKey, "1");
    w.click(CssLocators.addSettingsSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.addAttributesButtonKey);
    return settingName;
  }

  /**
   * Method to Add Settings for Checkbox reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addSettingCB(String type, String dataType) throws InterruptedException {
    log.debug("addSettingCB method invoked...");
    String settingName = "Setting-1_" + w.randomName();
    // w.click(CssLocators.attributesLinkKey);
    w.click(CssLocators.addSettingsButtonKey);
    w.selectValue(CssLocators.addSettingsTypeSelectKey, type);
    w.click(CssLocators.addSettingTypeNextKey);
    w.sendKeys(CssLocators.addSettingNameFieldKey, settingName);
    w.sendKeys(CssLocators.addSettingGroupNameFieldKey, settingName);
    w.click(CssLocators.addSettingsSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.addAttributesButtonKey);
    return settingName;

  }

  /**
   * Method to Configure Product Line Attributes
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public String configureStaticEpAttributes(String productLineName, String dataType, String id) throws InterruptedException {
    log.debug("configureStaticEpAttributes method invoked...");
    // String attributeName = "Attribute-1_" + w.randomName();
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    String attributeName = addAttribute(productLineName, dataType, id);
    // w.waitUntilElementVisible(CssLocators.attributesTextKey);
    return attributeName;
  }

  /**
   * Method to Configure Dynamic endpoint Attributes
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   */
  public String configureDynamicEpAttributes(String productLineName) throws InterruptedException {
    log.debug("configureProductLineAttributes method invoked...");
    String endPointName = "ep_" + w.randomName();
    String mfgId = Constants.MFGID;
    String epAttributeName = "epAttribute-1_" + w.randomName();
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.addEndpointButtonKey);
    w.sendKeys(CssLocators.addEndpointNameFieldKey, endPointName);
    w.sendKeys(CssLocators.addEndpointMfgIdFieldKey, mfgId);
    w.sendKeys(CssLocators.addEndpointDescriptionFieldKey, "Test ep used for e2e autotest");
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(6000);
    w.waitUntilElementVisible(CssLocators.attributesLinkKey);
    addAttribute(epAttributeName, "Boolean", "1");
    w.waitUntilElementVisible(CssLocators.attributesTextKey);
    // w.click(CssLocators.configureProductLineBackButtonKey);
    selectProductLine(productLineName);
    // w.click(CssLocators.searchProductLineResultKey);
    return epAttributeName;
  }

  public String absolutePath() {
    File f = new File("");
    String path = "";
    path = f.getAbsolutePath();
    System.out.println("Path : " + path);
    return path + "_e2eAddProduct.json";
  }

  public String productSync(String productLineName) throws InterruptedException {
    log.debug("productSync method invoked...");
    w.click(CssLocators.pickFirstProductFromTheList);
    w.click(CssLocators.firmwareDetails);
    String osVersion = w.getText(CssLocators.getOSVersion);
    return osVersion;
  }

  // To Test Inheritated all static endpoint Productline attributes

  /*
   * public String getProductLineProductAttributeName(String productLineName) throws InterruptedException {
   * log.debug("getProductLineProductAttributeName method invoked..."); w.click(CssLocators.configureProductLineButtonKey); String attrName =
   * w.getText(CssLocators.prodLineAttributesName); w.click(CssLocators.backButton); return attrName; }
   */

  public void productAttributeName(String productLineName, String deviceID) throws InterruptedException {
    log.debug("productAttributeValue method invoked...");
    Thread.sleep(2000);
    selectProductLine(productLineName);
    Thread.sleep(1000);
    w.click(CssLocators.openProductLine);
    // w.mouseOverOnElement(CssLocators.clickOnProductLink);
    searchProduct(deviceID);
    w.click(CssLocators.viewButtonOfProductKey);
    Thread.sleep(2000);

  }

  public String getProdLineAttribute(String productLineName) throws InterruptedException {

    log.debug("getProdLineAttribute method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.addEndpointButtonKey);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
    String attributeName = w.getDriver().findElement(By.xpath(".//*[@id=\"tblAttributes\"]/tbody/tr[1]/td[2]")).getText();
    System.out.println(attributeName);
    return attributeName;
  }

  /*
   * public String productAttributeValue(String productLineName) throws InterruptedException { log.debug("productAttributeValue method invoked...");
   * w.click(CssLocators.pickFirstProductFromTheList); w.click(CssLocators.attributesButton); String attrValue = w.getText(CssLocators.attributesValue); return
   * attrValue; }
   */

  /*
   * public String productSettingValue(String productLineName) throws InterruptedException { log.debug("productSync method invoked...");
   * w.click(CssLocators.pickFirstProductFromTheList); w.click(CssLocators.productSetting); w.click(CssLocators.productSettingExpandButton);
   * w.click(CssLocators.pickFirstGroupFromTheGroupList); String gropuValue = w.getAttributeValue(CssLocators.firstGroupValueFromTheGroupList); return
   * gropuValue; } public String productSettingsName(String productLineName) throws InterruptedException { log.debug("productSync method invoked...");
   * w.click(CssLocators.pickFirstProductFromTheList); w.click(CssLocators.productSetting); w.click(CssLocators.productSettingExpandButton);
   * w.click(CssLocators.pickFirstGroupFromTheGroupList); String gropuValue = w.getText(CssLocators.firstGroupNameFromTheGroupList); return gropuValue; }
   */

  public String getProdLineSetting(String productLineName) throws InterruptedException {
    log.debug("productSync method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(3000);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
    w.click(CssLocators.settingsLinkKey);
    Thread.sleep(3000);
    String settingValue = w.getText(CssLocators.getSettingName);
    return settingValue;
  }

  public String productSettingName(String productLineName, String deviceID) throws InterruptedException {
    log.debug("productSync method invoked...");
    connectedProduct(productLineName, deviceID);
    Thread.sleep(2000);
    w.click(CssLocators.productSettingLinkKey);

    String settingValue = w.getText(CssLocators.getSettingNameProduct);
    return settingValue;
  }

  /**
   * Method to Add Product Line
   *
   * @return Product Line Name
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public void addFirmware(String firmwareName) throws InterruptedException, ParseException, IOException {
    log.debug("addFirmware method invoked...");
    String firmwareDescription = "firmware definition used for e2e autotest";
    String deviceId = "e2e" + w.generateRandomNumber() + "d" + w.generateRandomNumber();
    String serialNumber = "30042_" + w.generateRandomNumber();
    Thread.sleep(2000);
    w.click(CssLocators.addPackageButtonKey);
    w.sendKeys(CssLocators.addPackageNameFieldKey, firmwareName);
    w.sendKeys(CssLocators.addPackageGroupDescriptionFieldKey, firmwareDescription);
    w.sendKeys(CssLocators.addSoftwareVersionFieldKey, "2.0.1");
    w.sendKeys(CssLocators.addHardwareVersionFieldKey, "1.0.2");

    writeJsonFile(deviceId, serialNumber);
    Thread.sleep(5000);
    w.sendKeys(CssLocators.adduploadButtonFieldKey, absolutePath());
    w.click(CssLocators.addPackageSaveButtonFieldKey);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(6000);
    w.mouseOverOnElement(CssLocators.fwProdLineSearchboxKey);
  }

  /**
   * Method to Add User
   *
   * @return userId
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public String addUser(String userId, String emailId, String userRole) throws InterruptedException {

    log.debug("addUser method invoked..." + userId);
    w.click(CssLocators.addUserslinkFieldKey);
    w.sendKeys(CssLocators.userId, userId);
    w.sendKeys(CssLocators.secondaryEmail, emailId);
    w.sendKeys(CssLocators.firstName, "Auto-" + w.randomName());
    w.sendKeys(CssLocators.lastName, "test User");
    w.sendKeys(CssLocators.telephone, "1234567890");
    w.selectValue(CssLocators.userRole, userRole);
    // Thread.sleep(5000);
    w.click(CssLocators.saveUser);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(3000);
    return userId;
  }

  public String editUser(String userId, String emailId, String userRole) throws InterruptedException {

    log.debug("editUser method invoked..." + userId);
    String firstName = "Automated" + w.generateRandomNumber();
    selectUser(userId, userRole);
    Thread.sleep(2000);
    w.click(CssLocators.editUserKey);
    Thread.sleep(2000);
    w.clear(CssLocators.firstName);
    w.sendKeys(CssLocators.firstName, firstName);
    w.clear(CssLocators.lastName);
    w.sendKeys(CssLocators.lastName, "testuser");
    w.clear(CssLocators.telephone);
    w.sendKeys(CssLocators.telephone, "1234567890");

    // Thread.sleep(5000);
    w.click(CssLocators.userIdUpdatebuttonFieldKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);

    return firstName;
  }

  /**
   * Method to Search Product Line
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public boolean selectUser(String name, String userRole) throws InterruptedException {
    log.debug("searchUser method invoked..." + name);
    try {
      Thread.sleep(4000);
      w.clickOnLink(userRole);
      Thread.sleep(2000);
      w.waitUntilElementVisible(CssLocators.userIdSearchboxKey);
      w.clear(CssLocators.userIdSearchboxKey);
      w.sendKeys(CssLocators.userIdSearchboxKey, name);
      w.click(CssLocators.userIdSearchbuttonFieldKey);
      w.getUser(name).click();

    } catch (Exception e) {
      return false;
    }
    return true;
  }

  /**
   * Method to Search Product Line
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public void deleteUser(String name, String userRole) throws InterruptedException {
    log.debug("deleteUser method invoked..." + name);
    selectUser(name, userRole);
    w.click(CssLocators.deleteUserKey);
    w.click(CssLocators.userdeleteConfirmationOkKey);
    Thread.sleep(3500);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    // w.waitUntilElementVisible(CssLocators.userIdSearchboxKey);
    // Assert.assertFalse(selectUser(name));

  }

  /**
   * Method get Account Status
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public String getAccountStatus(String userId) throws InterruptedException {
    log.debug("getAccountStatus method invoked...");
    String status;
    status = w.getDriver().findElement(By.xpath("//td[text()='" + userId + "']/following-sibling::td/span")).getText();

    return status;

  }

  /**
   * Method to edit user profile
   *
   * @return
   * @throws Exception
   */
  public String editUserProfile() throws Exception {
    log.debug("editUserProfile method invoked...");
    String phoneNumber = "1234561" + w.generateRandomNumber();
    w.click(CssLocators.editUserProfileLinkKey);

    Thread.sleep(3000);
    w.clear(CssLocators.editPhoneNum);
    w.sendKeys(CssLocators.editPhoneNum, phoneNumber);
    Thread.sleep(2000);
    w.click(CssLocators.editUserUpdatebuttonFieldKey);
    Thread.sleep(2000);
    return phoneNumber;
  }

  /**
   * Method to edit user profile
   *
   * @return
   * @throws Exception
   */
  public void editUserPreference(String value) throws Exception {
    log.debug("editUserPreference method invoked...");
    w.click(CssLocators.preferenceeditFieldKey);
    w.clear(CssLocators.preferenceValueFieldKey);
    w.sendKeys(CssLocators.preferenceValueFieldKey, value);
    w.click(CssLocators.preferencesavebuttonFieldKey);

  }

  /**
   * Method to check about details of project
   *
   * @return
   * @throws Exception
   */
  public void checkAboutDetails() throws InterruptedException {
    log.debug("checkAboutDetails method invoked...");
    Thread.sleep(2500);
    log.info("Portal Version:" + w.getDriver().findElement(By.xpath("//b[contains(text(),\"Portal Version\")]/..")).getText().trim());
    log.info("API Version:" + w.getDriver().findElement(By.xpath("//b[contains(text(),\"API  Version\")]/..")).getText().trim());
    w.click(CssLocators.aboutOKButton);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
  }

  /**
   * Method to change company
   *
   * @return
   * @throws Exception
   */

  /*
   * public void changeCompany(String companyName) throws InterruptedException { log.debug("changecompany method invoked...");
   * w.selectValue(CssLocators.changeCompanyDropDownFieldKey, companyName); w.click(CssLocators.changeCompanySubmitFieldKey); Thread.sleep(2000); }
   */

  /**
   * Method to edit user profile
   *
   * @return
   * @throws Exception
   */
  public void addUserPreference(String prefernceValue) throws Exception {
    log.debug("addUserPreference method invoked...");
    w.click(CssLocators.preferenceAddbuttonFieldKey);
    Thread.sleep(2000);
    w.selectValue(CssLocators.preferencedelivaryMethodFieldKey, "WebHook");
    Thread.sleep(3000);
    w.selectValue(CssLocators.preferencedelivaryattributeKey, "WebUrl");
    w.sendKeys(CssLocators.preferenceValueFieldKey, prefernceValue);
    Thread.sleep(2000);
    w.click(CssLocators.preferencesavebuttonFieldKey);
  }

  /**
   * Method to edit user profile
   *
   * @return
   * @throws Exception
   */
  public void addUserPreferences() throws Exception {
    log.debug("addUserPreference method invoked...");
    w.click(CssLocators.preferenceAddbuttonFieldKey);
    w.selectValue(CssLocators.preferencedelivaryMethodFieldKey, "WebHook");
    w.selectValue(CssLocators.preferencedelivaryattributeKey, "WebUrl");
    w.sendKeys(CssLocators.preferenceValueFieldKey, "https://dev-2.portal.us.flex-ci.com/#/notification/webHook");
    Thread.sleep(2000);
    w.click(CssLocators.preferencesavebuttonFieldKey);
  }

  public String addOTAGroup(String productLine, String otaGroupName, String hardwareVersion, String packageVersion) throws InterruptedException {
    log.debug("addOTAGroup method invoked...");
    w.click(CssLocators.addOTAGroupLinkKey);
    w.sendKeys(CssLocators.addOTAGroupNameFieldKey, otaGroupName);
    w.selectValue(CssLocators.addOTAGroupProductLineFieldKey, productLine);
    Thread.sleep(4000);
    w.selectValue(CssLocators.addOTAGroupHardwareVersionFieldKey, hardwareVersion);
    Thread.sleep(2000);
    w.selectValue(CssLocators.addOTAGroupSoftwareVersionFieldKey, packageVersion);

    w.click(CssLocators.clickSaveButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    return otaGroupName;
  }

  /**
   * Method to Search OTA Product Group
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public boolean selectOTAGroup(String name) throws InterruptedException {
    log.debug("searchOTAGroup method invoked..." + name);
    w.click(CssLocators.groupLinkKey);
    Thread.sleep(1500);
    w.clear(CssLocators.OTAGroupSearchboxKey);
    w.sendKeys(CssLocators.OTAGroupSearchboxKey, name);
    if (w.getText(CssLocators.searchOTAGroupResultKey).equals(name)) {
      return true;
    }
    return false;
  }

  /**
   * Method to Add Product From Current Product List And Add to Available Product List
   *
   * @return OTA Group Name
   * @throws java.lang.InterruptedException
   */
  public String addToCurrentProdList(String otaGroupName) throws InterruptedException {
    log.debug("AddToCurrentProdList method invoked...");
    selectOTAGroup(otaGroupName);
    w.click(CssLocators.searchOTAGroupResultKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.click(CssLocators.addProductToOTA);
    Thread.sleep(2500);
    w.click(CssLocators.selectProductFromList);
    w.click(CssLocators.addProductsToGroupButtonKey);
    return otaGroupName;
  }

  /**
   * Method to Remove Product From Current Product List And Add to Available Product List
   *
   * @return OTA Group Name
   * @throws java.lang.InterruptedException
   */
  public void removeFromCurrentProdList(String otaGroupName) throws InterruptedException {
    log.debug("RemoveFromCurrentProdList method invoked...");
    selectOTAGroup(otaGroupName);
    w.click(CssLocators.searchOTAGroupResultKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.click(CssLocators.removeOTAProductLinkKey);
    Thread.sleep(2000);
  }

  /**
   * Method to Update OTA Group Details
   *
   * @return OTA Group Name
   * @throws java.lang.InterruptedException
   */
  public String editOTAGroup(String otaGroupName) throws InterruptedException {
    log.debug("editOTAGroup method invoked...");
    String newOTAgroupName = otaGroupName + "_edited";
    selectOTAGroup(otaGroupName);
    w.click(CssLocators.editOTAGroupButtonKey);
    Thread.sleep(3000);
    w.clear(CssLocators.addOTAGroupNameFieldKey);
    w.sendKeys(CssLocators.addOTAGroupNameFieldKey, newOTAgroupName);
    w.click(CssLocators.updateButtonKey);
    return otaGroupName;
  }

  /**
   * Method to Delete OTA Product Group
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  /**
   * Method to Update OTA Group Details
   *
   * @return OTA Group Name
   * @throws java.lang.InterruptedException
   */
  public boolean verifyHwVersionitOTAGroup(String otaGroupName, String hw) throws InterruptedException {
    log.debug("verifyHwVersionitOTAGroup method invoked...");
    Thread.sleep(3000);
    // w.click(CssLocators.oTATabLinkKey);
    selectOTAGroup(otaGroupName);
    // String name =
    // w.getText(CssLocators.addOTAGroupHardwareVersionFieldKey);
    // System.out.println("NAME:"+name);
    w.click(CssLocators.editOTAGroupButtonKey);
    Thread.sleep(3000);
    boolean searchIconEnabled = w.getDriver().findElement(By.id("ddlHardwareVersion")).isEnabled();
    if (searchIconEnabled == true) {
      w.sendKeys(CssLocators.addOTAGroupHardwareVersionFieldKey, hw);
      return true;
    }
    w.click(CssLocators.updateButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    return searchIconEnabled;
  }

  /**
   * Method to filter Product master on OTA group
   * 
   * @throws InterruptedException
   */
  public String filterProductMasterOnOTApage(String productMaster) throws InterruptedException {
    log.debug(" filterProductMasterOnOTApage method invoked...");
    w.click(CssLocators.groupLinkKey);
    Thread.sleep(1500);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//select[@id=\"packageName\"]")).sendKeys(productMaster);
    String text = w.getText(CssLocators.getProductMasterNameinOTA);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//a[text()=\"Clear all\"]")).click();
    return text;
  }

  /** Method to Verify Filter by package software version */
  public String filterPackageVersionOnOTApage(String version) throws InterruptedException {
    log.debug(" filterPackageVersionOnOTApage method invoked...");
    Thread.sleep(1500);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//select[@id=\"softwareVersion\"]")).sendKeys(version);
    String text = w.getText(CssLocators.getPackageVersionFromSearchResult);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//a[text()=\"Clear all\"]")).click();
    return text;

  }

  /** Method to Verify Filter by package Group Name */
  public String filterPackageGroupNameOnOTApage(String packageGroupName) throws InterruptedException {
    log.debug(" filterPackageGroupName method invoked...");
    Thread.sleep(2500);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//select[@id=\"packageGroup\"]")).sendKeys(packageGroupName);
    String text = w.getText(CssLocators.getPackageGroupNameFromSearchResult);
    Thread.sleep(2500);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//a[text()=\"Clear all\"]")).click();
    return text;

  }

  /** Method to Verify Filter by package Group Name */
  public String filterPackageHWversionOTApage(String packageHWversion) throws InterruptedException {
    log.debug(" filterPackageHWversionOTApage method invoked...");
    Thread.sleep(1500);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//select[@id=\"hardwareVersion\"]")).sendKeys(packageHWversion);
    String text = w.getText(CssLocators.getPackageHWversionFromSearchReasult);
    w.getDriver().findElement(By.xpath("//div[@id=\"otaGroupTab\"]/div/div[2]//a[text()=\"Clear all\"]")).click();
    return text;

  }

  /**
   * Method to Update OTA Group Details
   *
   * @return OTA Group Name
   * @throws java.lang.InterruptedException
   */
  public boolean verifyUserCanDeleteOTAGroup(String otaGroupName, String hw) throws InterruptedException {
    log.debug("verifyUserCanDeleteOTAGroup   method invoked...");
    Thread.sleep(3000);
    selectOTAGroup(otaGroupName);
    w.click(CssLocators.editOTAGroupButtonKey);
    Thread.sleep(3000);
    boolean searchIconEnabled = w.getDriver().findElement(By.id("ddlHardwareVersion")).isEnabled();
    System.out.println("##############" + searchIconEnabled);
    if (searchIconEnabled == true) {
      w.sendKeys(CssLocators.addOTAGroupHardwareVersionFieldKey, hw);
      return true;
    }
    w.click(CssLocators.updateButtonKey);
    // String name =
    // w.getText(CssLocators.addOTAGroupHardwareVersionFieldKey);
    return searchIconEnabled;
  }

  public void deleteOTAGroup(String otaGroupName) throws InterruptedException {
    log.debug("deleteOTAGroup method invoked...");
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(2000);
    selectOTAGroup(otaGroupName);
    Thread.sleep(2000);
    w.click(CssLocators.deleteOTAGroupButtonKey);
    w.click(CssLocators.deleteConfirmationNotificationKeyOTA);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.oTATabLinkKey);
    Thread.sleep(4000);
  }

  /*
   * Method to Search Reporting Group
   * @return
   * @throws java.lang.InterruptedException
   */
  public boolean selectReportingGroup(String name) throws InterruptedException {
    log.debug("searchReportingGroup method invoked...");
    try {
      w.click(CssLocators.groupLinkKey);
      Thread.sleep(3500);
      w.click(CssLocators.reportingGroupKey);
      Thread.sleep(1500);
      w.clear(CssLocators.reportingGroupSearchBoxKey);
      w.sendKeys(CssLocators.reportingGroupSearchBoxKey, name);
      Thread.sleep(3500);
      // getElementFromList(name,
      // CssLocators.reportingGroupSearchResultsKey).click();
      w.click(CssLocators.reportingGroupSearchResultsKey);
      Thread.sleep(3000);
    } catch (Exception e) {
      return false;
    }
    return true;
  }

  /**
   * Method to Add ReportingGroup
   *
   * @return Reporting Group Name
   * @throws java.lang.InterruptedException
   */

  public String addReportingGroup(String GroupName) throws InterruptedException {

    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.click(CssLocators.addReportingGrouplinkKey);
    w.sendKeys(CssLocators.addReportingGroupNameKey, GroupName);
    w.click(CssLocators.addReportingGroupSaveKey);
    w.mouseOverOnElement(CssLocators.addReportingGroupSaveKey);
    w.waitForElementToBeClickable(CssLocators.addReportingGrouplinkKey);
    Thread.sleep(3000);
    return GroupName;

  }

  /**
   * Method to Edit ReportingGroup
   *
   * @return Reporting Group Name
   * @throws java.lang.InterruptedException
   */
  public String editReportingGroup(String ReportingGroup) throws Exception {
    String GroupName = "Edited_GroupName_New--" + w.randomName();
    selectReportingGroup(ReportingGroup);
    w.click(CssLocators.editReportingGroupKey);
    Thread.sleep(3000);
    w.clear(CssLocators.addReportingGroupNameKey);
    w.sendKeys(CssLocators.addReportingGroupNameKey, GroupName);
    w.click(CssLocators.clickSaveButtonKey);
    return GroupName;
  }

  /**
   * Method to Delete ReportingGroup
   *
   * @throws java.lang.InterruptedException
   */

  public void deleteReportingGroup(String reportingGroup) throws Exception {
    Thread.sleep(3000);
    selectReportingGroup(reportingGroup);
    w.click(CssLocators.deleteReportingGroupButtonKey);
    w.click(CssLocators.deleteReportingGroupcConfirmButtonKey);
    Thread.sleep(4000);

  }

  public String addToCurrentList(String reportingGroup) throws Exception {
    selectReportingGroup(reportingGroup);
    log.debug("AddToCurrentProdList method invoked...");
    Assert.assertEquals(selectReportingGroup(reportingGroup), true);
    w.click(CssLocators.selectAvailableVirtualProductKey);
    w.click(CssLocators.addButtonFieldKey);
    return reportingGroup;

  }

  /**
   * Method to Add Users to ReportingGroup
   * 
   * @throws java.lang.InterruptedException
   */

  public String removeFromCurrentList(String reportingGroup) throws Exception {
    selectReportingGroup(reportingGroup);
    log.debug("removeFromCurrentList method invoked...");
    Assert.assertEquals(selectReportingGroup(reportingGroup), true);
    w.click(CssLocators.selectCurrentVirtualProductKey);
    w.click(CssLocators.removeButtonFieldKey);
    return reportingGroup;

  }

  /**
   * Method to Add Users to ReportingGroup
   * 
   * @throws java.lang.InterruptedException
   */

  public String updateUserField(String reportingGroup, String value) throws Exception {
    selectReportingGroup(reportingGroup);
    log.debug("removeFromCurrentList method invoked...");
    Assert.assertEquals(selectReportingGroup(reportingGroup), true);
    w.click(CssLocators.selectUserFieldTabKey);
    w.getDriver().findElement(By.xpath("//*[@id=\"lnkEdit\"]")).click();
    Thread.sleep(3000);
    w.sendKeys(CssLocators.inputUserFieldValueKey, value);
    return value;

  }

  /**
   * Method to Add Users to ReportingGroup
   * 
   * @throws java.lang.InterruptedException
   */

  public String addUserToCurrentList(String reportingGroup) throws Exception {
    selectReportingGroup(reportingGroup);
    log.debug("addUserToCurrentList method invoked...");
    Assert.assertEquals(selectReportingGroup(reportingGroup), true);
    w.click(CssLocators.selectUsersTabKey);
    w.click(CssLocators.selectAvailableUserKey);
    w.click(CssLocators.addButtonUserKey);
    return reportingGroup;

  }

  /**
   * Method to Add Users to ReportingGroup
   * 
   * @throws java.lang.InterruptedException
   */

  public String addReportingGroupUser(String reportingGroup) throws Exception {
    selectReportingGroup(reportingGroup);
    w.click(CssLocators.reportingGroupUsersLinkKey);
    w.click(CssLocators.reportingGroupAddUserKey);

    String user = w.getText(CssLocators.reportingGrpUserName);
    // table[@id="tblAssignedUser"]//td[text()="installer174@flextronics.com"]
    w.click(CssLocators.reportingGroupAvailableUserKey);
    w.click(CssLocators.reportingGroupUserAddButtonKey);
    Thread.sleep(2000);
    w.getDriver().navigate().back();
    w.click(CssLocators.reportingGroupUsersLinkKey);
    return user;

  }

  /**
   * Method to Remove Users from ReportingGroup
   * 
   * @throws java.lang.InterruptedException
   */

  public void removeReportingGroupUser(String reportingGroup, String userName) throws Exception {

    w.click(CssLocators.reportingGroupUsersLinkKey);
    w.getDriver().findElement(By.xpath("//td[text()='" + userName + "']/following-sibling::td/a[@id=\"lnkDeleteUser\"]")).click();
    w.click(CssLocators.reportingGroupAddUserFieldsSaveKey);
  }

  /**
   * Method to Edit value in User Fields of ReportingGroup
   * 
   * @throws java.lang.InterruptedException
   */
  public String ValueEditReportingGroupUserFields(String reportingGroup, String userFieldName) throws InterruptedException {

    String Value = "12_" + w.generateRandomNumber();
    selectReportingGroup(reportingGroup);
    Thread.sleep(3000);
    w.click(CssLocators.reportingGroupUserFieldsLinkKey);
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//td[text()='" + userFieldName + "']//following-sibling::td/a")).click();
    Thread.sleep(3000);
    w.clear(CssLocators.reportingGroupUserFieldsValueInputKey);
    w.sendKeys(CssLocators.reportingGroupUserFieldsValueInputKey, Value);
    w.click(CssLocators.reportingGroupUserFieldsSaveKey);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return Value;

  }

  /**
   * Method to Edit User Fields of ReportingGroup
   * 
   * @return User Fields Name
   * @throws java.lang.InterruptedException
   */
  public String editReportingGroupUserFields() throws Exception {

    String UserFieldDesc = "aaa" + w.randomName();

    // w.click(CssLocators.reportingGroupUserFieldsSettingKey);

    Thread.sleep(3000);
    w.click(CssLocators.reportingGroupUserFieldsEditLinkKey);
    Thread.sleep(4000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.clear(CssLocators.reportingGroupUserFieldsDescriptionKey);
    w.sendKeys(CssLocators.reportingGroupUserFieldsDescriptionKey, UserFieldDesc);
    w.click(CssLocators.clickSaveButtonKey);
    return UserFieldDesc;

  }

  /**
   * Method to Add User Fields Of ReportingGroup
   * 
   * @return User Fields Name
   * @throws java.lang.InterruptedException
   */

  public String addReportingGroupUserFields(String OrderVal) throws Exception {
    String UserFieldsName = "UserFielsName__" + w.randomName();
    String Description = UserFieldsName + w.randomName();
    String APIkey = "1234_" + w.randomName();
    w.waitUntilCSSElementVisible(CssLocators.configureReportingGroupKey);
    w.mouseOverOnElement(CssLocators.configureReportingGroupKey);
    w.click(CssLocators.configureReportingGroupMoverKey);

    Thread.sleep(1000);
    w.click(CssLocators.reportingGroupAddUserFieldsKey);
    w.sendKeys(CssLocators.reportingGroupUserFieldsNameKey, UserFieldsName);
    w.sendKeys(CssLocators.reportingGroupUserFieldsDescriptionKey, Description);
    w.sendKeys(CssLocators.reportingGroupUserFieldsDataTypeKey, "String");
    w.sendKeys(CssLocators.reportingGroupUserFieldsAPIKey, APIkey);
    w.sendKeys(CssLocators.reportingGroupUserFieldsOrderKey, "1");
    w.click(CssLocators.clickSaveButtonKey);
    return UserFieldsName;

  }

  /**
   * Method to Delete User Fields Of ReportingGroup
   * 
   * @throws java.lang.InterruptedException
   */
  public void deleteReportingGroupUserFields(String userFieldName) throws Exception {
    w.click(CssLocators.groupLinkKey);
    w.click(CssLocators.reportingGroupKey);

    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.configureReportingGroupKey);
    w.click(CssLocators.configureReportingGroupMoverKey);
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//td[text()='" + userFieldName + "']//following-sibling::td/a[@id='lnkDelete']")).click();
    w.click(CssLocators.reportingGroupDeleteUserFieldsConfirmButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
  }

  /**
   * Method to test login failure due to wrong credentials
   * 
   * @param login
   * @param password
   */
  public void logInWithWrongCredentials(String login, String password) {
    log.debug("logInWithWrongCredentials started");
    w.clear(CssLocators.loginuserNameFieldKey);
    w.sendKeys(CssLocators.loginuserNameFieldKey, login);
    w.clear(CssLocators.passwordKey);
    w.sendKeys(CssLocators.passwordKey, password);
    w.click(CssLocators.signinButtonKey);
    w.waitUntilElementVisible(CssLocators.loginFailedMessage);
    log.debug("logInWithWrongCredentials completed");
  }

  /**
   * Method to check the forgotPassword
   * 
   * @param userName
   */
  public void forgotPassword(String userName) {
    log.debug("forgotPassword started");

    // w.click(CssLocators.loginLinkKey);
    w.click(CssLocators.needHelpLink);
    w.click(CssLocators.forgotPasswordLink);

    w.sendKeys(CssLocators.forgotPasswordEmailKey, userName);
    w.click(CssLocators.forgotPasswordSendEmailButtonKey);
    w.waitUntilElementVisible(CssLocators.emailSentContainerKey);
    log.debug("forgotPassword completed");
  }

  /**
   * Method to check the availability of flex logo
   * 
   * @return
   */
  public boolean isFlexLogoAvailable() {
    log.debug("isFlexLogoAvailable started");

    w.navigate(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);
    return w.isDisplayed(CssLocators.homePageFlexLogoKey);

  }

  /**
   * Method to Configure Dynamic endpoint
   *
   * @throws java.lang.InterruptedException
   */
  public void selectEpConfiguration(String productLineName, String dynamicEndPoint) throws InterruptedException {
    log.debug("add configureProductLineDynamic EP Setting method invoked...");
    selectProductLine(productLineName);
    Thread.sleep(2000);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(2000);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName + " (Product)");
    w.sendKeys(CssLocators.selectDynamicEndPoint, dynamicEndPoint + " (Peripheral)");

  }

  /**
   * Method to Configure Dynamic endpoint
   *
   * @throws java.lang.InterruptedExceptionyyyyy
   */
  public void selectConfigurationsForDynamicEp(String productLineName, String DynamicEndPoint) throws InterruptedException {
    log.debug("add configureProductLineDynamic EP Setting method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(3000);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
    w.sendKeys(CssLocators.selectDynamicEndPoint, DynamicEndPoint);
    Thread.sleep(6000);

    // String SettingName=addSetting(productLineName,"Text Field", "Float");

  }

  /**
   * Method to Add User Details in configuration
   * 
   * @throws java.lang.InterruptedException
   */

  public String configureProductLineUserFields(String productLineName, String dataType, String OrderValue) throws InterruptedException {
    log.debug("configureProductLine UserFields method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.userFieldLinkKey);
    String userFieldName = addUserFieldsValues(dataType, OrderValue);
    return userFieldName;
  }

  /**
   * Method to Edit Product Line UserFieldsValues
   * 
   * @throws java.lang.InterruptedException
   */

  public String editUserFieldsValues(String productLineName) throws Exception {
    String userFieldDescription = "User Field Descrption for " + productLineName;
    Thread.sleep(2500);
    w.click(CssLocators.addUserFieldEditButtonKey);
    Thread.sleep(4000);
    w.clear(CssLocators.addUserFieldDescriptionFeildKey);
    w.sendKeys(CssLocators.addUserFieldDescriptionFeildKey, userFieldDescription);
    w.click(CssLocators.addUserFieldSaveButtonKey);
    Thread.sleep(2000);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.addUserFieldButtonKey);
    // String UserDetailsName=
    // w.getText(CssLocators.addUserFieldNameFieldKey);
    return userFieldDescription;

  }

  /**
   * Method to Add Product Line User Details in configuration
   * 
   * @throws java.lang.InterruptedException
   */

  public String addUserFieldsValues(String dataType, String Orderval) throws InterruptedException {
    String userFieldName = "UserField-1_" + w.randomName();
    String userFieldDescription = "User Field Descrption For data type--" + dataType;
    String APIKey = "API-001" + w.randomName();
    Thread.sleep(2500);
    w.mouseOverOnElement(CssLocators.userFieldLinkKey);
    w.click(CssLocators.addUserFieldButtonKey);
    w.sendKeys(CssLocators.addUserFieldNameFieldKey, userFieldName);
    w.sendKeys(CssLocators.addUserFieldDescriptionFeildKey, userFieldDescription);
    w.sendKeys(CssLocators.addUserFieldDataTypeFieldKey, dataType);
    w.sendKeys(CssLocators.addUserFieldAPIFieldKey, APIKey);
    w.sendKeys(CssLocators.addUserFieldOrderFieldKey, Orderval);
    System.out.println("values are entered...");
    w.click(CssLocators.addUserFieldSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.addUserFieldButtonKey);
    return userFieldName;

  }

  /**
   * Method to delete Product Line User field details
   * 
   * @throws java.lang.InterruptedException
   */

  public String deleteUserFieldsValues(String productLineName) throws Exception {
    w.click(CssLocators.userFieldLinkKey);
    Thread.sleep(2000);
    String UserFields = w.getDriver().findElement(By.xpath("//*[@id=\"tblUserFields\"]/tbody/tr[1]/td[1]")).getText();
    w.clickOnPartialTextLink("Delete");
    w.click(CssLocators.deleteConfirmKey);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.addUserFieldButtonKey);

    return UserFields;

  }

  /**
   * Method to Configure Dynamic endpoint Attributes
   *
   * @return Attribute Name
   * @throws java.lang.InterruptedException
   */
  public String addConfigureDynamicEpAttributes(String productLineName, String DynamicEndPoint, String datatype, String id) throws InterruptedException {
    log.debug("add configureProductLineAttributes method invoked...");

    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.waitUntilElementVisible(CssLocators.selectDynamicEndPoint);
    w.sendKeys(CssLocators.selectDynamicEndPoint, DynamicEndPoint);
    w.click(CssLocators.ConfigureText);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, DynamicEndPoint);
    Thread.sleep(3000);
    String AttributeName = addAttribute(productLineName, datatype, id);
    w.waitUntilElementVisible(CssLocators.attributesLinkKey);
    // w.click(CssLocators.searchProductLineResultKey);
    return AttributeName;
  }

  /**
   * Method to add Configure Dynamic endpoint
   * 
   * @throws java.lang.InterruptedException
   */
  public String addConfigureDynamicEp(String productLineName, String DynamicEndPoint) throws InterruptedException {
    log.debug("add configureProductLineAttributes method invoked...");

    String mfgId = Constants.MFGID;
    Thread.sleep(2000);
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.mouseOverOnElement(CssLocators.configurationSettingButton);
    w.click(CssLocators.addEndpointButtonKey);
    w.getDriver().findElement(By.xpath("//button[text()=\"Create\"]")).click();
    w.sendKeys(CssLocators.addEndpointNameFieldKey, DynamicEndPoint);
    w.sendKeys(CssLocators.addEndpointMfgIdFieldKey, mfgId);
    w.sendKeys(CssLocators.addEndpointDescriptionFieldKey, "Test ep used for e2e autotest");
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);

    return DynamicEndPoint;

  }

  /**
   * Method to edit Configure Dynamic endpoint Attributes
   *
   * @return Attribute Name
   * @throws java.lang.InterruptedException
   */
  public String editConfigureDynamicEpAttributes(String productLineName, String DynamicEndPoint) throws InterruptedException {
    log.debug("Edit configureProductLine Attribute method invoked...");
    /*
     * selectProductLine(productLineName); w.waitForElementToBeClickable(CssLocators. configureProductLineButtonKey);
     * w.click(CssLocators.configureProductLineButtonKey); Thread.sleep(3000); w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
     * w.selectValueByIndex(CssLocators.selectDynamicEndPoint, 1); w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, DynamicEndPoint);
     */
    Thread.sleep(3000);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    String NewAttributeName = EditAttribute();

    return NewAttributeName;

  }

  /**
   * Method to Edit Attributes reusable
   * 
   * @return attributeName
   * @throws java.lang.InterruptedException
   */

  public String EditAttribute() throws InterruptedException {
    log.debug("Edit Attribute method invoked...");
    String attributeName = "Edited";
    w.click(CssLocators.editAttributeLinkKey);
    Thread.sleep(2000);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    return attributeName;
  }

  /**
   * Method to delete Configure Dynamic endpoint Attributes
   *
   * @return Attribute Name
   * @throws java.lang.InterruptedException
   */
  public void deleteEpConfiguration(String productLineName, String DynamicEndPointName) throws InterruptedException {
    selectEpConfiguration(productLineName, DynamicEndPointName);
    // deleteAttribute(attributeName);
    w.mouseOverOnElement(CssLocators.configurationSettingButton);
    w.click(CssLocators.deleteEpConfigButton);
    w.click(CssLocators.deleteConfirmationNotificationKey);

  }

  /**
   * MMethod to Edit Notification Alerts
   * 
   * @throws java.lang.InterruptedException
   */

  public String editNotification(String productLineName) throws Exception {
    String AlertMessage = "Alert-Message-- edited";
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.notificationsLinkKey);
    w.click(CssLocators.addNotificationsEditAlertlinkKey);
    w.click(CssLocators.addNotificationsNextButtonKey);
    w.clear(CssLocators.addNotificationsAlertMessageKey);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, AlertMessage);
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);
    String alertMessage_new = w.getText(CssLocators.addNotificationsAlertMessageKey);
    Thread.sleep(2000);
    return alertMessage_new;

  }

  /*
   * Click Configure button click Notifications click Add Notification Select Event Action Fill the Alert Details Click Save
   */

  public String configureProductLineNotifications(String productLineName, String EventAction) throws InterruptedException {
    log.debug("configureProductLineNotifications method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.notificationsLinkKey);
    w.click(CssLocators.addNotificationsButtonKey);
    w.sendKeys(CssLocators.addNotificationsEventActionKey, EventAction);
    w.click(CssLocators.addNotificationsNextButtonKey);
    return addNotificationEvent(EventAction);
  }

  /**
   * Method to Add Notification Alerts
   * 
   * @throws java.lang.InterruptedException
   */
  public String addNotificationEvent(String eventAction) throws InterruptedException {
    String alertName = "Alert- " + w.randomName() + " " + eventAction;
    // String alertMessage = "Message- " + eventAction;
    w.sendKeys(CssLocators.addNotificationsAlertNameKey, alertName);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, eventAction);
    w.selectValue(CssLocators.addNotificationsAlertRoleLevelKey, "Administrator");
    // w.sendKeys(CssLocators.addNotificationsAlertSeverityKey, "Medium");
    w.sendKeys(CssLocators.addNotificationsAlertUserKey, "Individual");
    if (eventAction == "User logged out") {
      w.selectValue(CssLocators.addNotificationsAlertDeliveryMethodKey, "Websocket");
    }
    w.selectValue(CssLocators.addNotificationsAlertDeliveryMethodKey, "WebHook");
    w.selectValue(CssLocators.addOutputType, "Default");
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);
    Thread.sleep(2000);
    if (w.isTextPresent("Event template already exist.")) {
      w.click(CssLocators.addNotificationsAlertCancelButtonKey);
    }
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(6000);
    w.mouseOverOnElement(CssLocators.addNotificationsButtonKey);

    return alertName;

  }

  /**
   * Method to addD uplicate Notification Alerts
   * 
   * @throws java.lang.InterruptedException
   */
  public String addDuplicateNotification(String eventAction) throws InterruptedException {
    String alertName = "Alert--" + w.randomName() + " " + eventAction;
    String alertMessage = "Alert-Message--" + w.randomName() + "" + eventAction;
    w.click(CssLocators.addNotificationsButtonKey);
    Thread.sleep(1000);
    w.selectValue(CssLocators.addNotificationsEventActionKey, eventAction);
    Thread.sleep(1000);
    w.sendKeys(CssLocators.addNotificationsAlertNameKey, alertName);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, alertMessage);
    w.selectValue(CssLocators.addNotificationsAlertRoleLevelKey, "User");
    // w.sendKeys(CssLocators.addNotificationsAlertSeverityKey, "Medium");
    w.sendKeys(CssLocators.addNotificationsAlertUserKey, "Individual");
    w.selectValue(CssLocators.addNotificationsAlertDeliveryMethodKey, "WebHook");
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);

    return alertName;

  }

  /**
   * Method to Delete Alert Notifications
   * 
   * @throws java.lang.InterruptedException
   */

  public String deleteNotificationsAlert(String productLineName) throws Exception {
    w.click(CssLocators.notificationsLinkKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    String alertName = w.getDriver().findElement(By.xpath("//table[@id=\"tblNotification\"]/tbody/tr[1]/td[1]")).getText();
    w.clickOnPartialTextLink("Delete");
    w.click(CssLocators.deleteConfirmKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.notificationsLinkKey);
    return alertName;

  }

  /**
   * Method to delete Configure Dynamic endpoint Command
   *
   * @return CommandName
   * @throws java.lang.InterruptedException
   */
  public String deleteConfigureDynamicEpCommand(String productLineName) throws InterruptedException {

    log.debug("Delete Configuaration Dynamic Endpoint Command method invoked...");
    Thread.sleep(4000);
    w.click(CssLocators.commandsLinkKey);
    Thread.sleep(2000);
    String CommandName = w.getDriver().findElement(By.xpath("//*[@id=\"tblCommands\"]/tbody/tr/td[1]")).getText();
    deleteCommand();
    Thread.sleep(4000);
    return CommandName;

  }

  /**
   * Method to Delete Setting reusable
   * 
   * @throws java.lang.InterruptedException
   */

  public void deleteCommand() throws InterruptedException {
    log.debug("Delete Command method invoked...");
    w.clickOnPartialTextLink("Delete");
    w.click(CssLocators.deleteConfirmKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
  }

  /**
   * Method to edit Configure Dynamic endpoint Command
   *
   * @return Command Name
   * @throws java.lang.InterruptedException
   */
  public String editConfigureDynamicEpCommands(String productLineName, String DynamicEndPoint) throws InterruptedException {
    log.debug("Edit configuration Dynamic EP Command method invoked...");
    Thread.sleep(2000);
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
    w.selectValueByIndex(CssLocators.selectDynamicEndPoint, 1);
    Thread.sleep(6000);
    w.click(CssLocators.commandsLinkKey);
    String Commandname = EditCommand();

    return Commandname;

  }

  /**
   * Method to Edit Command reusable
   * 
   * @return Command name
   * @throws java.lang.InterruptedException
   */

  public String EditCommand() throws InterruptedException {
    log.debug("Edit Command method invoked...");
    String CommandNameEdited = "Edited" + w.randomName();
    Thread.sleep(2000);
    w.click(CssLocators.commandEditLinkKey);
    Thread.sleep(2000);
    w.clear(CssLocators.addCommandNameFieldKey);
    w.sendKeys(CssLocators.addCommandNameFieldKey, CommandNameEdited);
    w.click(CssLocators.addCommandsSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.commandsLinkKey);
    return CommandNameEdited;
  }

  /**
   * Method to edit Configure Dynamic endpoint Attributes
   *
   * @return AttributeName
   * @throws java.lang.InterruptedException
   */
  public String editConfigureDynamicEpSetting(String productLineName, String DynamicEndPoint) throws InterruptedException {

    /*
     * selectProductLine(productLineName); w.waitForElementToBeClickable(CssLocators. configureProductLineButtonKey);
     * w.click(CssLocators.configureProductLineButtonKey); Thread.sleep(3000); w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
     * w.selectValueByIndex(CssLocators.selectDynamicEndPoint, 1); Thread.sleep(5000); w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint,
     * DynamicEndPoint); w.click(CssLocators.settingsLinkKey);
     */
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    String settingName = editSetting();
    return settingName;

  }

  public String editSetting() throws InterruptedException {
    log.debug("Edit Setting method invoked...");
    String settingName = "Edited" + w.randomName();
    w.click(CssLocators.editSettinglinkKey);
    // w.selectValue(CssLocators.addSettingsTypeSelectKey, type);
    Thread.sleep(5000);
    w.clear(CssLocators.addSettingNameFieldKey);
    w.sendKeys(CssLocators.addSettingNameFieldKey, settingName);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.settingsLinkKey);

    return settingName;
  }

  /**
   * Method to delete Configure Dynamic endpoint Setting
   *
   * @return Setting Name
   * @throws java.lang.InterruptedException
   */
  public void deleteConfigureDynamicEpSetting(String settingName) throws InterruptedException {
    // selectProductLine(ProductLineName);
    log.debug("configureProductLine  Dynamic EP Setting method invoked...");
    w.click(CssLocators.settingsLinkKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    deleteSetting(settingName);

  }

  /**
   * Method to Delete Setting reusable
   * 
   * @throws java.lang.InterruptedException
   */

  public void deleteSetting() throws InterruptedException {
    log.debug("Delete Setting method invoked...");
    w.clickOnPartialTextLink("Delete");
    w.click(CssLocators.deleteConfirmKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(6000);
    w.mouseOverOnElement(CssLocators.settingsLinkKey);

  }

  /**
   * Method to open the c Connected Dynamic Ep
   *
   * @throws java.lang.InterruptedException
   */
  public void ConnectedDynamicEP(String productLineName, String deviceId) throws InterruptedException {
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    Thread.sleep(4000);
    w.clear(CssLocators.searchBoxProductKey);
    w.sendKeys(CssLocators.searchBoxProductKey, deviceId);
    Thread.sleep(4000);
    w.click(CssLocators.dynamicPointButton);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.click(CssLocators.dynamicEndPointlinkKey);
    Thread.sleep(3000);
    Thread.sleep(3000);
  }

  public void clickOnEndpoints() throws InterruptedException {
    String path = ".//button[@id=\"btnEndpoint\"]";
    w.getDriver().findElement(By.xpath(path)).click();
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);

  }

  /**
   * Method to open the c Connected Dynamic Ep
   *
   * @throws java.lang.InterruptedException
   */
  public void connectedProduct(String productLineName, String deviceId) throws InterruptedException {
    selectProductLine(productLineName);
    Thread.sleep(2000);
    w.click(CssLocators.openProductLine);
    Thread.sleep(5000);
    w.clear(CssLocators.searchBoxProductKey);
    Thread.sleep(2000);
    w.sendKeys(CssLocators.searchBoxProductKey, deviceId);
    Thread.sleep(2000);
    w.click(CssLocators.viewButtonOfProductKey);
    Thread.sleep(4000);

  }

  /**
   * Method to open the c Connected Dynamic Ep
   *
   * @throws java.lang.InterruptedException
   */
  public void connectedProductSettings(String productLineName, String deviceID) throws InterruptedException {
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.clear(CssLocators.searchBoxProductKey);
    Thread.sleep(2000);
    w.sendKeys(CssLocators.searchBoxProductKey, deviceID);
    /*
     * String path= ".//button[@id=\"product-grid-data-row-static-endpoint-details-link\"]"; w.getDriver().findElement(By.xpath(path)).click();
     */
    w.click(CssLocators.viewButtonOfProductKey);
    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);
    w.click(CssLocators.productSettingLinkKey);
    Thread.sleep(3000);

  }

  /**
   * Method to verify Setting Va;lues sent from Agent
   *
   * @throws java.lang.InterruptedException
   */
  public String verifySettingValuesFromAgent(String settingName) throws InterruptedException {
    log.debug("verifySettingValuesFromAgent started..");
    Thread.sleep(5000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td//a[@value=\"History\"]")).click();
    Thread.sleep(2000);
    String currentValue = w.getText(CssLocators.getcurrentValHistory);
    System.out.println(currentValue);
    String value = currentValue.substring(15);
    w.click(CssLocators.CancelButton);
    Thread.sleep(2000);

    return value;

  }

  /**
   * Method to verify Setting Va;lues sent from Agent
   *
   * @throws java.lang.InterruptedException
   */
  public String verifyEmphemerSettingValue(String settingGrpName, String settingName) throws InterruptedException {

    log.debug("verifyEmphemerSettingValue started..");
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingGrpName + "')]")).click();
    Thread.sleep(4000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td//a[@value=\"History\"]")).click();
    Thread.sleep(6000);
    String currentValue = w.getText(CssLocators.getcurrentValHistory);
    System.out.println(currentValue);
    String value = currentValue.substring(15);
    w.click(CssLocators.CancelButton);
    Thread.sleep(3000);
    return value;

  }

  /**
   * Method to verify Setting Va;lues sent from Agent
   *
   * @throws java.lang.InterruptedException
   */
  public String verifySettingValue(String settingGrpName, String settingName) throws InterruptedException {

    log.debug("verifyEmphemerSettingValue started..");
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingGrpName + "')]")).click();
    Thread.sleep(4000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td//a[@value=\"History\"]")).click();
    Thread.sleep(6000);
    String currentValue = w.getText(CssLocators.getSettingValueInHistory);
    w.click(CssLocators.CancelButton);
    Thread.sleep(3000);
    return currentValue;

  }

  /**
   * Method to verify Setting Va;lues sent from Agent
   *
   * @throws java.lang.InterruptedException
   */
  public String verifyEndPointsSettingValuesFromAgent(String settingName) throws InterruptedException {

    log.debug("verifySettingValuesFromAgent started..");
    Thread.sleep(3000);
    System.out.println(settingName);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    Thread.sleep(4000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td//a[@value=\"History\"]")).click();
    Thread.sleep(3000);
    String currentValue = w.getText(CssLocators.getCurrentValueforPeripheralSetting);
    System.out.println(currentValue);

    w.click(CssLocators.endpointSettingHistoryCancelButton);
    Thread.sleep(3000);

    return currentValue;

  }

  public boolean verifyAttributeValue(String attrGroupName, String value) throws InterruptedException {

    log.debug("verifyAttributeValue started..");
    System.out.println("GroupNameAttriBute== " + attrGroupName);
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + attrGroupName + "')]")).click();
    Thread.sleep(3000);
    String currentval = w.getDriver()
        .findElement(By.xpath("//td[contains(text(),'" + attrGroupName + "')]//following-sibling::td[@data-label=\"Current Value\"]")).getText();
    System.out.println(attrGroupName + "---currentval---" + currentval);
    Thread.sleep(3000);
    if (currentval.contains(value)) {
      w.getDriver().findElement(By.xpath("//div[@id=\"divAttributesTab\"]//label[contains(text(),'" + attrGroupName + "')]")).click();
      return true;
    } else
      return false;

  }

  public String verifyJsonAttributeValue(String attrGroupName, String value) throws InterruptedException {
    log.debug("verifyJsonAttributeValue started..");
    System.out.println("GroupNameAttriBute== " + attrGroupName);
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + attrGroupName + "')]")).click();
    Thread.sleep(2000);
    w.click(CssLocators.clickViewJsonButton);
    String currentval = w.getText(CssLocators.getJsonValue);
    w.click(CssLocators.clickViewJsonDonebutton);
    System.out.println(attrGroupName + "---currentval---" + currentval.replace("\n", ""));
    Thread.sleep(2000);

    return currentval.replace("\n", "").replaceAll("\\s+", "");

  }

  public boolean verifyDEPAttributeValue(String attrGroupName, String value) throws InterruptedException {

    log.debug("verifyDEPAttributeValue started..");
    Thread.sleep(5000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + attrGroupName + "')]")).click();
    Thread.sleep(3000);
    String currentval = w.getDriver().findElement(By.xpath("//td[contains(text(),'" + attrGroupName + "')]//following-sibling::td[@data-label=\"Value\"]"))
        .getText();
    System.out.println(attrGroupName + "---currentval---" + currentval);
    Thread.sleep(3000);
    if (currentval.contains(value)) {
      w.getDriver().findElement(By.xpath("//label[contains(text(),'" + attrGroupName + "')]")).click();
      return true;
    } else
      return false;

  }

  /**
   * Method to get the Setting/command nme Name of Connected Dynamic Ep
   * 
   * @returns Setting Name/command name
   * @throws java.lang.InterruptedException
   */
  public void getConnectedDynamicEndPointconfigName(String productLineName, String deviceId, String config) throws InterruptedException {

    ConnectedDynamicEP(productLineName, deviceId);
    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);

    if (config == "Setting") {
      w.click(CssLocators.productSettingLinkKey);
      w.click(CssLocators.explandSetting);

      Thread.sleep(3000);
    } else if (config == "UserFields") {
      Thread.sleep(3000);
      w.click(CssLocators.userFieldLinkKey);

    } else {
      w.click(CssLocators.commandproductLink);
      Thread.sleep(3000);

    }

  }

  /**
   * Method to add firmware
   * 
   * @param firmwareName
   * @param descripton
   * @param swversion
   * @param hwversion
   * @return true if added else false
   * @throws InterruptedException
   * @throws ParseException
   * @throws IOException
   */
  public boolean addFirmware(String firmwareName, String descripton, String swversion, String hwversion)
      throws InterruptedException, ParseException, IOException {
    try {
      log.debug("addFirmware method invoked...");
      String deviceId = "e2e" + w.generateRandomNumber() + "d" + w.generateRandomNumber();
      String serialNumber = "30042_" + w.generateRandomNumber();
      Thread.sleep(2000);
      w.click(CssLocators.addPackageButtonKey);
      w.sendKeys(CssLocators.addPackageNameFieldKey, firmwareName);
      w.sendKeys(CssLocators.addPackageGroupDescriptionFieldKey, descripton);
      w.sendKeys(CssLocators.addSoftwareVersionFieldKey, swversion);
      w.sendKeys(CssLocators.addHardwareVersionFieldKey, hwversion);
      writeJsonFile(deviceId, serialNumber);
      Thread.sleep(5000);
      w.sendKeys(CssLocators.adduploadButtonFieldKey, absolutePath());
      w.click(CssLocators.addPackageSaveButtonFieldKey);
      Thread.sleep(2000);
      if (w.isDisplayed(CssLocators.addPackageButtonKey)) {

        return true;
      }
    } catch (Exception e) {
      return false;
    }
    return false;
  }

  /**
   * Method to handle the save dialog box during download
   * 
   * @throws AWTException
   * @throws InterruptedException
   */
  public void handleDownloadDialog() throws AWTException, InterruptedException {
    Thread.sleep(3000);
    Robot robot = new Robot();
    robot.keyPress(KeyEvent.VK_ALT);
    robot.keyPress(KeyEvent.VK_S);
    robot.keyRelease(KeyEvent.VK_S);
    robot.keyRelease(KeyEvent.VK_ALT);
    Thread.sleep(2000);
    robot.keyPress(KeyEvent.VK_ENTER);
    robot.keyRelease(KeyEvent.VK_ENTER);
  }

  /**
   * Method to change the company/tenent
   * 
   * @param companyName
   * @return true if changed successful
   */
  public boolean changeCompany(String companyName) {
    try {
      log.debug("changeCompany started");
      Thread.sleep(2000);
      w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
      w.click(CssLocators.changeCompanyKey);
      Thread.sleep(2000);

      w.sendKeys(CssLocators.changeCompanySelectKey, companyName);
      w.click(CssLocators.submitCompanyButtonKey);

      w.waitUntilElementVisible(CssLocators.productLineSearchboxKey);
      w.mouseOverOnElement(CssLocators.clickOnProductLink);
      Thread.sleep(4000);
      if (w.isDisplayed(CssLocators.productLineSearchboxKey)) {
        log.debug("Company changed successfully...");
        return true;
      } else {
        log.debug("failed to change the company");
        return false;
      }
    } catch (Exception e) {
      log.debug("failed to change the company");
      return false;
    }
  }

  /**
   * Method to productLine User field details
   */
  public String getProductLineUserDetail(String productLineName) throws InterruptedException {
    log.debug("productLineUserDetails method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(3000);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
    w.click(CssLocators.userFieldLinkKey);
    String useName = w.getText(CssLocators.userFieldNamePL);
    return useName;
  }

  /**
   * Method to staticEP User field details
   */
  public String productUserDetail(String productLineName, String deviceID) throws InterruptedException {
    log.debug("staticEPUserDetails method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    searchProduct(deviceID);
    w.click(CssLocators.viewButtonOfProductKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.click(CssLocators.userFieldLinkKey);

    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    String useName = w.getText(CssLocators.userFieldName);
    return useName;
  }

  /**
   * Method to Edit Product Line UserFieldsValues
   * 
   * @throws java.lang.InterruptedException
   */

  public String editUserFields() throws Exception {

    String userFieldDescription = "Edited-User Field Descrption ";

    w.click(CssLocators.addDynamicEPUserFieldEditButtonKey);
    Thread.sleep(2000);
    w.clear(CssLocators.addUserFieldDescriptionFeildKey);
    w.sendKeys(CssLocators.addUserFieldDescriptionFeildKey, userFieldDescription);
    String UserDetailsName = w.getText(CssLocators.addUserFieldNameFieldKey);
    w.click(CssLocators.addUserFieldSaveButtonKey);
    Thread.sleep(5000);
    // String UserDetailsName=
    // w.getText(CssLocators.addUserFieldNameFieldKey);
    return UserDetailsName;

  }

  /**
   * Method to delete User field details
   * 
   * @throws java.lang.InterruptedException
   */
  public String deleteDynamicEPUserFields() throws Exception {
    // w.click(CssLocators.configureProductLineButtonKey);
    // w.selectValueByIndex(CssLocators.selectDynamicEndPoint, 1);
    w.click(CssLocators.userFieldLinkKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    String UserFields = w.getDriver().findElement(By.xpath("//*[@id=\"tblUserFields\"]/tbody/tr[1]/td[1]")).getText();
    w.clickOnLink("Delete");
    w.click(CssLocators.deleteConfirmKey);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(6000);
    w.mouseOverOnElement(CssLocators.userFieldLinkKey);

    return UserFields;

  }
  /*
   * Click Configure button click Notifications click Add Notification Select Event Action Fill the Alert Details Click Save
   */

  public String addconfigureNotifications(String EventAction) throws InterruptedException {
    log.debug("configureProductLineNotifications method invoked...");
    Thread.sleep(4000);
    w.mouseOverOnElement(CssLocators.notificationsLinkKey);
    w.click(CssLocators.addNotificationsButtonKey);
    w.selectValue(CssLocators.addNotificationsEventActionKey, EventAction);
    String alertName = addNotificationEvent(EventAction);
    Thread.sleep(3500);
    return alertName;
  }

  /**
   * Search Settings
   * 
   * @param name
   * @return
   */

  public boolean searchNotification(String name) {
    try {
      w.clear(CssLocators.searchNotificationContainer);
      w.sendKeys(CssLocators.searchNotificationContainer, name);
      if (w.getDriver().findElement(By.xpath("//td[contains(text(),'" + name + "')][1]")).isDisplayed()) {
        log.debug("Notification found");
        w.clear(CssLocators.searchNotificationContainer);
        return true;
      } else {
        w.clear(CssLocators.searchNotificationContainer);
        return false;
      }
    } catch (Exception e) {
      w.clear(CssLocators.searchNotificationContainer);
      log.debug("Notification doesnt exists");
      e.printStackTrace();
      return false;
    }
  }

  /**
   * Method to Add Notification Alerts
   * 
   * @note Select Exisiting Setting for Setting name
   * @throws java.lang.InterruptedException
   */
  public String addNotificationAgentSettingEvent(String EventAction) throws InterruptedException {
    String AlertName = "Alert--" + w.randomName() + " " + EventAction;
    String AlertMessage = "Alert-Message--" + w.randomName() + "" + EventAction;
    w.click(CssLocators.addNotificationsButtonKey);
    w.sendKeys(CssLocators.addNotificationsEventActionKey, EventAction);

    w.sendKeys(CssLocators.addNotificationsAlertNameKey, AlertName);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, AlertMessage);
    w.selectValue(CssLocators.addNotificationsAlertRoleLevelKey, "User");
    // w.sendKeys(CssLocators.addNotificationsAlertSeverityKey, "Medium");
    w.sendKeys(CssLocators.addNotificationsAlertUserKey, "Individual");
    w.selectValue(CssLocators.addNotificationsAlertDeliveryMethodKey, "WebHook");
    Thread.sleep(3000);
    w.selectValueByIndex(CssLocators.addNotificationsAlertSettingName, 1);
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);
    Thread.sleep(2000);

    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return AlertName;

  }

  /**
   * Method to Add Notification Alerts
   * 
   * @throws java.lang.InterruptedException
   */
  public String addNotificationAttributeOPEvent(String EventAction) throws InterruptedException {
    String AlertName = "Alert--" + w.randomName() + " " + EventAction;
    String AlertMessage = "Alert-Message--" + w.randomName() + "" + EventAction;
    w.click(CssLocators.addNotificationsButtonKey);
    w.selectValue(CssLocators.addNotificationsEventActionKey, EventAction);

    w.sendKeys(CssLocators.addNotificationsAlertNameKey, AlertName);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, AlertMessage);
    w.selectValue(CssLocators.addNotificationsAlertRoleLevelKey, "User");
    // w.sendKeys(CssLocators.addNotificationsAlertSeverityKey, "Medium");
    w.sendKeys(CssLocators.addNotificationsAlertUserKey, "Individual");
    w.selectValue(CssLocators.addNotificationsAlertDeliveryMethodKey, "WebHook");
    Thread.sleep(1000);
    w.selectValueByIndex(CssLocators.addNotificationsAlertAttributeName, 1);
    Thread.sleep(3000);
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);
    Thread.sleep(2000);

    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(6000);
    w.mouseOverOnElement(CssLocators.addNotificationsButtonKey);
    return AlertName;

  }

  /**
   * Method to Delete Alert Notifications
   * 
   * @throws java.lang.InterruptedException
   */

  public String deleteNotificationsAlert() throws Exception {
    w.click(CssLocators.notificationsLinkKey);
    Thread.sleep(2000);
    String AlertName = w.getDriver().findElement(By.xpath("//*[@id=\"tblNotification\"]/tbody/tr[1]/td[1]")).getText();

    w.clickOnPartialTextLink("Delete");
    w.click(CssLocators.deleteConfirmKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.addNotificationsButtonKey);

    return AlertName;

  }

  /**
   * MMethod to Edit Notification Alerts
   * 
   * @throws java.lang.InterruptedException
   */

  public String editNotification() throws Exception {
    String AlertMessage = "Alert-MessageNew-- edited";

    w.click(CssLocators.addNotificationsEditAlertlinkKey);
    Thread.sleep(2000);
    w.clear(CssLocators.addNotificationsAlertMessageKey);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, AlertMessage);
    String AlertMessage_new = w.getText(CssLocators.addNotificationsAlertMessageKey);
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);
    Thread.sleep(2000);

    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.addNotificationsButtonKey);
    return AlertMessage_new;

  }

  /**
   * Method to select firmware from the list
   * 
   * @param name
   * @return
   * @throws InterruptedException
   */
  public boolean selectProductLineInFirmwareTab(String name) throws InterruptedException {
    log.debug("selectFirmware method invoked...");
    try {
      w.waitUntilElementVisible(CssLocators.fwProdLineSearchboxKey);
      w.clear(CssLocators.fwProdLineSearchboxKey);
      w.sendKeys(CssLocators.fwProdLineSearchboxKey, name);

      // Assert.assertEquals(w.getText(CssLocators.searchProductLineResultKey),
      // name);
      // w.click(CssLocators.searchProductLineResultKey);
      getElementFromList(name, CssLocators.searchFwProdLineResultKey).click();
      return true;
    } catch (Exception e) {
      return false;
    }

  }

  /**
   * Method to Search Firmware
   * 
   * @param id
   * @param column
   * @return
   * @throws InterruptedException
   */
  public boolean searchFirmware(String id) throws InterruptedException {
    log.debug("searchFirmware method invoked...");
    w.waitUntilElementVisible(CssLocators.searchFirmwareTextboxKey);
    w.clear(CssLocators.searchFirmwareTextboxKey);
    w.sendKeys(CssLocators.searchFirmwareTextboxKey, id);
    w.click(CssLocators.searchFirmwareGoButtonKey);
    Assert.assertEquals(w.getText(CssLocators.searchFirmwareResultKey), id);
    log.debug("Search Result is Displayed");
    return true;

  }

  /**
   * Method to verify format of MFG id
   * 
   * @throws java.lang.InterruptedException
   */
  public void VerifyConfigureDynamicEp(String productLineName, String DynamicEndPoint) throws InterruptedException {
    log.debug("add configureProductLineAttributes method invoked...");

    Thread.sleep(2000);
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.click(CssLocators.addEndpointButtonKey);
    w.sendKeys(CssLocators.addEndpointNameFieldKey, DynamicEndPoint);

    // add Space as MFGID
    w.sendKeys(CssLocators.addEndpointMfgIdFieldKey, "        ");
    w.sendKeys(CssLocators.addEndpointDescriptionFieldKey, "Test ep used for e2e autotest");
    w.click(CssLocators.clickSaveButtonKey);
    w.waitUntilCSSElementVisible(CssLocators.messageDisplayed);
    String errorMessage = w.getText(CssLocators.messageDisplayed);
    log.debug(errorMessage + "--ErrorMessage Displayed");
    Assert.assertEquals(errorMessage, "MfgId can not be empty.");
    w.mouseOverOnElement(CssLocators.clickOnProductLink);

    // Add Special Character as MFGID
    w.waitForElementToBeClickable(CssLocators.addEndpointButtonKey);
    w.click(CssLocators.addEndpointButtonKey);
    w.sendKeys(CssLocators.addEndpointNameFieldKey, DynamicEndPoint);
    w.sendKeys(CssLocators.addEndpointMfgIdFieldKey, "%");
    w.sendKeys(CssLocators.addEndpointDescriptionFieldKey, "Test ep used for e2e autotest");
    w.click(CssLocators.clickSaveButtonKey);
    w.waitUntilCSSElementVisible(CssLocators.messageDisplayed);
    String errorMessage1 = w.getText(CssLocators.messageDisplayed);
    log.debug(errorMessage1 + "--ErrorMessage Displayed");
    Assert.assertEquals(errorMessage1, "MFGId should contain alphanumeric only.");
    Thread.sleep(2000);

    /*
     * // Add Combination of Numeric+Character value w.sendKeys(CssLocators.addEndpointMfgIdFieldKey, "mg2d4s4ds"); w.click(CssLocators.clickSaveButtonKey);
     * Thread.sleep(8000); selectProductLine(productLineName);
     */

  }

  /**
   * Method to Add virtual Products
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addVirtualProducts(String productLineName) throws InterruptedException {
    log.debug("getProductStatus method invoked...");
    // searchProduct(id);
    String virtualProdName = "virtualprod";
    selectProductLine(productLineName);
    String user = w.getText(CssLocators.getuserNameDisplayed);
    w.click(CssLocators.virtualProductsLinkFieldKey);
    w.click(CssLocators.addVirtualProductsLinkFieldKey);
    Thread.sleep(4000);
    w.sendKeys(CssLocators.addVirtualProductsInputFieldKey, virtualProdName);
    w.sendKeys(CssLocators.addVirtualProductsSelectUserFieldKey, "Chandran Peethambaram");

    w.selectValueByIndex(CssLocators.addVirtualProductsSelectProductFieldKey, 1);
    w.click(CssLocators.addVirtualProductsSaveButtonFieldKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.virtualProductsLinkFieldKey);

    return virtualProdName;
  }

  /**
   * Method to Edit virtual Products
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public void editVirtualProducts(String productLineName) throws InterruptedException {
    log.debug("getProductStatus method invoked...");
    // searchProduct(id);
    selectProductLine(productLineName);
    w.click(CssLocators.virtualProductsLinkFieldKey);
    w.click(CssLocators.editVirtualProductsButtonFieldKey);
    Thread.sleep(3000);
    w.clear(CssLocators.addVirtualProductsInputFieldKey);
    w.sendKeys(CssLocators.addVirtualProductsInputFieldKey, "editvirtualprod");
    w.sendKeys(CssLocators.addVirtualProductsSelectUserFieldKey, "Chandran Peethambaram");
    w.click(CssLocators.editVirtualProductsUpdateButtonFieldKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.virtualProductsLinkFieldKey);
  }

  /**
   * Method to delete virtual Products
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public void deleteVirtualProducts(String productLineName) throws InterruptedException {
    log.debug("getProductStatus method invoked...");
    // searchProduct(id);
    selectProductLine(productLineName);
    w.click(CssLocators.virtualProductsLinkFieldKey);
    w.click(CssLocators.deleteVirtualProductsButtonFieldKey);
    w.click(CssLocators.deleteVirtualProductsNotificationButtonFieldKey);
  }

  /**
   * Method to delete virtual Products
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public void viewVirtualProducts(String productLineName, String deviceId, String virtualProdName) throws InterruptedException {
    log.debug("getProductStatus method invoked...");
    // searchProduct(id);
    selectProductLine(productLineName);
    String user = w.getText(CssLocators.getuserNameDisplayed);
    w.click(CssLocators.productLink);
    searchProduct(deviceId);
    w.click(CssLocators.viewProductsButtonFieldKey);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.waitForElementToBeClickable(CssLocators.viewProductBackButtonKey);
    String virtualProdId = w.getText(CssLocators.getVirtualProductsidValueKey);
    w.click(CssLocators.viewProductBackButtonKey);
    w.click(CssLocators.virtualProductsLinkFieldKey);
    w.sendKeys(CssLocators.searchBoxProductKey, virtualProdName);

    Assert.assertEquals(w.getText(CssLocators.viewProductVirtualproductIdKey), virtualProdId);

  }

  /**
   * Method to verify Products
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public boolean verifyProducts(String productLineName) throws InterruptedException {
    log.debug("getProductStatus method invoked...");
    // searchProduct(id);
    selectProductLine(productLineName);
    w.click(CssLocators.productLink);
    w.click(CssLocators.viewProductsButtonFieldKey);
    log.info("DeviceID:" + w.getText(CssLocators.getProductDeviceIdFromSummery));
    String var = w.getText(CssLocators.getProductDeviceIdFromSummery);
    log.info("Product ID:" + w.getText(CssLocators.getProductDeviceIdFromSummery));
    if (!(var.equals(" "))) {
      return true;
    }
    return false;
  }

  /**
   * Method to deactivateProduct
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public void deactivateProduct(String productLineName, String deviceId) throws InterruptedException {
    log.debug("getProductStatus method invoked...");
    connectedProduct(productLineName, deviceId);
    Thread.sleep(2500);
    w.click(CssLocators.productDeactivateButtonFieldKey);
    w.click(CssLocators.productDeactivateSuccessButtonKey);
    Thread.sleep(2500);

  }

  /**
   * Method to Search Product/Virtual Product/UserFields in ProductLine Page
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public boolean searchProduct(String id, String column) throws InterruptedException {
    log.debug("searchProduct method invoked...");
    try {
      w.waitUntilElementVisible(CssLocators.searchboxKey);
      w.clear(CssLocators.searchboxKey);
      w.sendKeys(CssLocators.searchboxKey, id);
      w.click(CssLocators.productSearchGoButtonKey);
      if (column == "Product") {
        Assert.assertEquals(w.getText(CssLocators.searchProductDeviceIDResultKey), id);
        log.debug("Search Result is Displayed");
        return true;
      } else if (column == "VirtualProduct") {
        Assert.assertEquals(w.getText(CssLocators.searchVirtualProductNameResultKey), id);
        log.debug("Search Result is Displayed");
        return true;

      } else {
        Assert.assertEquals(w.getText(CssLocators.searchUserFieldNameResultKey), id);
        log.debug("Search Result is Displayed");
        return true;

      }
    } catch (Exception e) {
      Assert.assertEquals(w.getText(CssLocators.pageCount), "Page 0 of 0");
      return false;
    }

  }

  /**
   * Method to edit Configure static endpoint settings
   *
   * @return settings name
   * @throws java.lang.InterruptedException
   */
  public String editConfigurestaticEpSetting(String productLineName) throws InterruptedException {
    // selectProductLine(productLineName);
    log.debug("Edit configureProductLine EP attributes method invoked...");
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(4000);
    // w.click(CssLocators.settingsLinkKey);

    String NewAttributeName = editSetting2();

    return NewAttributeName;

  }

  public boolean searchProductMasterUserField(String userFieldName) {
    try {
      w.sendKeys(CssLocators.searchProductMasetUserField, userFieldName);
      Thread.sleep(1200);
      if (w.getDriver().findElement(By.xpath("//td[contains(text(),'" + userFieldName + "')][1]")).isDisplayed()) {
        log.debug("UserField found");
        w.clear(CssLocators.searchProductMasetUserField);
        return true;
      } else {
        w.clear(CssLocators.searchProductMasetUserField);
        return false;
      }
    } catch (Exception e) {
      w.clear(CssLocators.searchProductMasetUserField);
      log.debug("UserField	 doesnt exists");
      e.printStackTrace();
      return false;
    }

  }

  /**
   * Method to edit Configure static endpoint Setting
   *
   * @return Setting Name
   * @throws java.lang.InterruptedException
   */
  public String editSetting2() throws InterruptedException {
    log.debug("Edit Setting method invoked...");
    String settingName = "Edited" + w.randomName();
    w.click(CssLocators.editSettinglinkKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(2000);

    w.clear(CssLocators.addSettingNameFieldKey);
    w.sendKeys(CssLocators.addSettingNameFieldKey, settingName);
    w.click(CssLocators.addSettingsSaveButtonKey);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.settingsLinkKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return settingName;

  }

  /**
   * Method to edit Configure static endpoint Setting
   *
   * @return Setting Name
   * @throws java.lang.InterruptedException
   */
  public void editSettingDefaltValue(String productLineName, String settingName) throws InterruptedException {
    log.debug(" editSettingDefaltValue method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(3000);
    w.click(CssLocators.settingsLinkKey);
    Thread.sleep(2000);
    searchSettings(settingName);
    Thread.sleep(2000);
    w.click(CssLocators.editSettinglinkKey);
    Thread.sleep(4000);
    w.clear(CssLocators.addSettingDefaultValueFieldKey);
    w.click(CssLocators.addSettingsSaveButtonKey);
    Thread.sleep(2000);

  }

  /**
   * Method to edit Configure static endpoint settings
   *
   * @return settings name
   * @throws java.lang.InterruptedException
   */
  public String editConfigurestaticAttr(String productLineName) throws InterruptedException {
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);

    String NewAttributeName = editAttribute();

    return NewAttributeName;

  }

  /**
   * Method to edit Configure static endpoint Attribute
   *
   * @return Setting Name
   * @throws java.lang.InterruptedException
   */
  public String editAttribute() throws InterruptedException {
    log.debug("Edit Attribute method invoked...");
    String attributeName = "AttriEdited";
    String description = "description_" + w.randomName();
    w.click(CssLocators.editAttributeLinkKey);
    Thread.sleep(3000);
    w.clear(CssLocators.editAttributeDescriptionKey);
    w.sendKeys(CssLocators.editAttributeDescriptionKey, description);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);
    return description;
  }

  /**
   * Method to delete Configure static endpoint Setting
   *
   * @return Setting Name
   * @throws java.lang.InterruptedException
   */
  public void deleteConfigureStaticEpSetting(String productLineName, String settingName) throws InterruptedException {
    selectProductLine(productLineName);
    log.debug("DeleteConfigureStaticEpSetting method invoked...");
    w.click(CssLocators.configureProductLineButtonKey);
    // w.click(CssLocators.ConfigureText);
    Thread.sleep(2000);
    w.click(CssLocators.settingsLinkKey);
    Thread.sleep(3000);

    deleteSetting(settingName);

  }

  /**
   * Method to Delete Setting reusable
   * 
   * @throws java.lang.InterruptedException
   */

  public void deleteSetting(String settingName) throws InterruptedException {
    log.debug("Delete Setting method invoked...");
    Thread.sleep(1000);
    searchSettings(settingName);
    w.click(CssLocators.deleteSettingsLinkKey);
    w.click(CssLocators.deleteConfirmKey);
    // w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(1000);
    w.getDriver().navigate().refresh();

  }

  /**
   * Method to delete Configure Dynamic endpoint attribute
   *
   * @return attribute Name
   * @throws java.lang.InterruptedException
   */
  public void deleteStaticEpAttribute(String productLineName, String staticAttributeName) throws InterruptedException {
    selectProductLine(productLineName);
    log.debug("DeleteStaticEpAttribute method invoked...");
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(2000);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(1500);
    deleteAttribute(staticAttributeName);

  }

  /**
   * Method to Delete static attribute reusable
   * 
   * @throws java.lang.InterruptedException
   */

  public void deleteAttribute(String attributeName) throws InterruptedException {
    log.debug("Delete attribute method invoked...");
    Thread.sleep(2000);
    searchAttribute(attributeName);
    Thread.sleep(2000);
    w.click(CssLocators.deleteStaticAttributeFieldKey);
    w.click(CssLocators.deleteConfirmKey);
    Thread.sleep(2000);
    w.getDriver().navigate().refresh();
  }

  /**
   * Method to Verify Upload Product using XML file
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  public void addProductUsingXMLFile(String productLineName, String path) throws java.text.ParseException, IOException, InterruptedException {
    log.debug("addProductUsingJsonFile method invoked...");
    // Assert.assertEquals(selectProductLine(productLineName), true);
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    w.click(CssLocators.addProductButtonKey);

    w.sendKeys(CssLocators.fileInputStringKey, path);
    w.waitForElementToBeClickable(CssLocators.fileUploadSubmitButtonKey);
    w.click(CssLocators.fileUploadSubmitButtonKey);

  }

  /**
   * Method to Verify Customize columns
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */

  public void VerifyProductsCustomizeColumns() throws InterruptedException {
    log.debug("VerifyProductsCustomizeColumns method invoked...");
    String statusLabel;
    String deviceIdLabel;
    String swVersion;
    w.click(CssLocators.customizeColumnDownArrow);
    if (w.isSelected(CssLocators.statusCheckBox)) {
      statusLabel = w.getText(CssLocators.getStatusText);
      w.click(CssLocators.customizeColumnDownArrow);
      Assert.assertTrue(w.isDisplayed(CssLocators.statusTextonProductlinePage));
      log.debug("Status Coulumn is visible in the Page");
      w.click(CssLocators.customizeColumnDownArrow);

    } else {
      w.click(CssLocators.customizeColumnDownArrow);
      Assert.assertFalse(w.isDisplayed(CssLocators.statusTextonProductlinePage));
      log.debug("Status Coulumn is Not visible in the Page");
      w.click(CssLocators.customizeColumnDownArrow);

    }

    if (w.isSelected(CssLocators.deviceIdCheckBox)) {

      deviceIdLabel = w.getText(CssLocators.getDeviceIDText);
      w.click(CssLocators.customizeColumnDownArrow);
      Assert.assertTrue(w.isDisplayed(CssLocators.devideIDTextonProductlinePage));
      log.debug("DevideID Coulumn is visible in the Page");
      w.click(CssLocators.customizeColumnDownArrow);
    } else {
      w.click(CssLocators.customizeColumnDownArrow);
      Assert.assertFalse(w.isDisplayed(CssLocators.devideIDTextonProductlinePage));
      log.debug("DeviceID Coulumn is Not visible in the Page");
      w.click(CssLocators.customizeColumnDownArrow);

    }

    if (w.isSelected(CssLocators.softWareVersionCheckBox)) {

      swVersion = w.getText(CssLocators.getSWVersionText);
      w.click(CssLocators.customizeColumnDownArrow);
      Assert.assertTrue(w.isDisplayed(CssLocators.softWareVersionTextonProductlinePage));
      log.debug("DevideID Coulumn is visible in the Page");
    }

  }

  /**
   * Method to Edit Configure Dynamic endpoint
   * 
   * @throws java.lang.InterruptedException
   */
  public String editDynamicEndpoint(String productLineName, String DynamicEndPoint) throws InterruptedException {
    log.debug("add configureProductLineAttributes method invoked...");
    String NewDynamicEPName = "DEP" + w.randomName() + "11New";
    selectEpConfiguration(productLineName, DynamicEndPoint);
    w.click(CssLocators.editDynamicEndPointButton);
    Thread.sleep(2000);
    w.clear(CssLocators.addEndpointNameFieldKey);
    w.sendKeys(CssLocators.addEndpointNameFieldKey, NewDynamicEPName);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return NewDynamicEPName;

  }

  /**
   * Method to Copy Configure Dynamic endpoint
   * 
   * @throws java.lang.InterruptedException
   */
  public String copyDynamicEp(String productLineName, String DynamicEndPoint) throws InterruptedException {
    log.debug("add configureProductLineAttributes method invoked...");
    Thread.sleep(6000);
    selectProductLine(productLineName);
    selectEpConfiguration(productLineName, DynamicEndPoint);
    w.click(CssLocators.copyDynamicEndPointButton);
    Thread.sleep(2000);
    w.click(CssLocators.selectTargetDynamicEp);
    w.sendKeys(CssLocators.selectTargetDynamicEp, "");
    String DestinationProductLine = w.getSelectedText(CssLocators.selectTargetDynamicEp);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.addEndpointButtonKey);

    return DestinationProductLine;

  }

  /**
   * Method to Create Product using Json file
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  public void addProductUsingJsonFile_50products(String productLineName) throws java.text.ParseException, IOException, InterruptedException {
    log.debug("addProductUsingJsonFile method invoked...");
    // Assert.assertEquals(selectProductLine(productLineName), true);
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    Thread.sleep(5000);
    w.click(CssLocators.addProductButtonKey);
    writeJsonFile_50products();
    Thread.sleep(5000);
    w.sendKeys(CssLocators.fileInputStringKey, absolutePath());
    w.waitForElementToBeClickable(CssLocators.fileUploadSubmitButtonKey);
    w.click(CssLocators.fileUploadSubmitButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);

    w.waitUntilCssElementVisible(CssLocators.fileUploadConfirmButtonKey);

    w.click(CssLocators.fileUploadConfirmButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.fileUploadConfirmButtonKey);
    // w.click(CssLocators.fileUploadOkButtonKey);

  }

  /**
   * Method to write Json file
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  @SuppressWarnings("unchecked")
  public void writeJsonFile_50products() throws java.text.ParseException, IOException, InterruptedException {
    log.debug("writeJsonFile method invoked...");
    JSONObject obj = new JSONObject();
    JSONArray list = new JSONArray();
    int count = 0;

    obj = new JSONObject();
    obj.put("RUNDATE", "2014-05-05T06:00:00.000Z");
    obj.put("SITE", "ATL");

    while (count < 50) {
      obj = new JSONObject();
      String deviceId = "e5e8" + w.generateRandomNumber() + "s" + w.generateRandomNumber();
      String serialNumber = "7021" + w.generateRandomNumber() + w.generateRandomNumber();
      JSONObject productPropetiesObj = new JSONObject();
      productPropetiesObj.put("Status", "0X10");
      productPropetiesObj.put("MAC", "00:00:00:00:00:00");
      productPropetiesObj.put("OS", "LINUX");
      productPropetiesObj.put("OSVersion", "1.0.0");
      productPropetiesObj.put("ApiVersion", "1.0");
      productPropetiesObj.put("HwVersion", "1.0.0");
      productPropetiesObj.put("HwName", "Amsterdam");
      productPropetiesObj.put("Builddate", "2014-05-05T06:00:00.000Z");
      productPropetiesObj.put("Batch", "100");
      productPropetiesObj.put("RMA", "123");
      productPropetiesObj.put("RmaDate", "2016-05-05T06:00:00.000Z");
      productPropetiesObj.put("SN", serialNumber);
      productPropetiesObj.put("SwVersion", "1.0.0");
      productPropetiesObj.put("SwName", "FocusLife" + w.randomName());
      productPropetiesObj.put("DeviceId", deviceId);

      list.add(productPropetiesObj);
      count++;
    }
    obj.put("PRODUCT", list);

    try {

      FileWriter file = new FileWriter(absolutePath());
      file.write(obj.toJSONString());
      file.flush();
      file.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.print(obj);
  }

  public void clickAddProductLineUserFieldSetting() {
    w.click(CssLocators.productMaster);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.mouseOverOnElement(CssLocators.productMasterSettingButton);
    w.click(CssLocators.userFieldDropdown);
  }

  /**
   * Method to Add Product Line User Details in ProductLine Settings
   * 
   * @throws java.lang.InterruptedException
   */

  public String addSettingsUserFieldsValues(String dataType, String Orderval) throws InterruptedException {
    String userFieldName = "UserField-1_" + w.randomName();
    String userFieldDescription = "User Field Descrption For data type--" + dataType;
    String APIKey = "API-001" + w.randomName();
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.click(CssLocators.addUserFieldsSettingButton);
    Thread.sleep(3000);
    w.sendKeys(CssLocators.addUserFieldNameFieldKey, userFieldName);
    w.sendKeys(CssLocators.addUserFieldDescriptionFeildKey, userFieldDescription);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.sendKeys(CssLocators.addUserFieldDataTypeFieldKey, dataType);
    w.sendKeys(CssLocators.addUserFieldAPIFieldKey, APIKey);
    w.sendKeys(CssLocators.addUserFieldOrderFieldKey, Orderval);
    System.out.println("values are entered...");
    w.click(CssLocators.addUserFieldSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return userFieldName;

  }

  /**
   * Method to Edit Setting Product Line UserFieldsValues
   * 
   * @throws java.lang.InterruptedException
   */

  public String editSettingsUserFields(String userField) throws Exception {
    String userFieldDescription = "Edited-User Field Descrption12";
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    String path = "//table[@id=\"tblUserField\"]/tbody//td[text()='" + userField + "']/following-sibling::td/a[@id=\"lnkEditUserfield\"]";
    w.getDriver().findElement(By.xpath(path)).click();
    Thread.sleep(3000);
    w.clear(CssLocators.addUserFieldDescriptionFeildKey);
    w.sendKeys(CssLocators.addUserFieldDescriptionFeildKey, userFieldDescription);
    w.click(CssLocators.addUserFieldSaveButtonKey);
    Thread.sleep(5000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return userFieldDescription;

  }

  /**
   * Method to delete User field details
   * 
   * @throws java.lang.InterruptedException
   */

  public void deleteSettingUserFields(String userField) throws Exception {
    w.sendKeys(CssLocators.seacrhProductMasterUserField, userField);
    w.click(CssLocators.deleteUserFieldButton);
    w.click(CssLocators.deleteConfirmKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
  }

  /**
   * Method to delete Configure Dynamic endpoint attribute
   *
   * @return attribute Name
   * @throws java.lang.InterruptedException
   */
  public String deleteConfigureStaticEpCommand(String ProductLineName) throws InterruptedException {
    // selectProductLine(ProductLineName);
    log.debug("configureProductLine  Static EP Command method invoked...");
    // w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(2000);
    w.click(CssLocators.commandsLinkKey);
    Thread.sleep(2000);
    String commandName = w.getDriver().findElement(By.xpath("//*[@id=\"tblCommands\"]/tbody/tr[1]/td[1]")).getText();

    deleteCommand();
    Thread.sleep(3000);
    return commandName;

  }

  /**
   * Method to Delete static attribute reusable
   * 
   * @throws java.lang.InterruptedException
   */

  public void DeleteStaticCommand() throws InterruptedException {
    log.debug("Delete Command method invoked...");
    w.click(CssLocators.deleteStaticCommandFieldKey);
    w.click(CssLocators.deleteConfirmKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.addCommandsButtonKey);

  }

  /**
   * This method method used to click on Notifications of Application
   *
   * @throws InterruptedException
   */
  public void notifications() throws Exception {
    log.debug("Click on Notificatons started");
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    w.clickOnLink("Notifications");
    log.debug("Click on Notificatons completed");
  }

  /**
   * This method used to subscribe the Notification
   * 
   * @throws Exception
   */
  public void subscribeNotifications(String DeliveryMethod) throws Exception {
    log.debug("subscribeNotifications started");
    w.click(CssLocators.clickOnProductLine);
    String AlertName = w.getText(CssLocators.getUnsubscribedNotificationName);
    w.click(CssLocators.subscribeAlert);
    if (DeliveryMethod == "WebHook") {
      w.click(CssLocators.clickOnWebHookCheckBox);
    } else if (DeliveryMethod == "Websocket") {
      w.click(CssLocators.clickOnWebSocketCheckBox);
    } else {
      w.click(CssLocators.clickOnEmailCheckBox);
    }
    w.click(CssLocators.clickSaveButtonAlert);
    w.waitUntilCssElementVisible(CssLocators.notificationHeader);

    Assert.assertTrue(
        w.getDriver().findElement(By.xpath("//div[@id='subscribed-notification-row-container']//p[contains(text(), '" + AlertName + "')]")).isDisplayed());
  }

  /**
   * MMethod to Edit Notification Alerts
   * 
   * @throws java.lang.InterruptedException
   */

  public String editDynamicEPNotification(String productLineName, String DynamicEndPointName) throws Exception {

    selectEpConfiguration(productLineName, DynamicEndPointName);
    w.click(CssLocators.notificationsLinkKey);
    w.click(CssLocators.addNotificationsEditAlertlinkKey);
    w.click(CssLocators.addNotificationsNextButtonKey);
    w.sendKeys(CssLocators.addNotificationsAlertNameKey, "DynamicEP");
    String AlertName = w.getText(CssLocators.addNotificationsAlertNameKey);
    w.sendKeys(CssLocators.addNotificationsAlertRoleLevelKey, "Administrator");
    w.sendKeys(CssLocators.addNotificationsAlertDeliveryMethodKey, "WebHook");
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);
    Thread.sleep(6000);
    return AlertName;

  }

  /**
   * MMethod to Edit Notification Alerts
   * 
   * @throws java.lang.InterruptedException
   */

  public void editNotificationAlert(String productLineName, String EventAction, String DeliveryMethod) throws Exception {

    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    w.click(CssLocators.notificationsLinkKey);
    w.getDriver().findElement(By.xpath("//table[@id=\"tblNotification\"]//td[contains(text(),'" + EventAction + "')][1]/following-sibling::td/a[1]")).click();

    w.clear(CssLocators.addNotificationsAlertMessageKey);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, EventAction);
    w.selectValue(CssLocators.addNotificationsAlertRoleLevelKey, "Administrator");
    w.selectValue(CssLocators.addNotificationsAlertDeliveryMethodKey, DeliveryMethod);

    w.click(CssLocators.addNotificationsAlertSaveButtonKey);

    Thread.sleep(5000);

  }

  /**
   * This method method used to verify the current date in GMT format to the notification time
   *
   * @throws InterruptedException
   */
  public void verifyNotificationAlert(String timestamp, String currentDate) throws Exception {
    DateFormat readFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aaa");
    DateFormat writeFormat = new SimpleDateFormat("MM/dd/yyyy hh ");
    Date date = null;
    date = readFormat.parse(timestamp);
    String formattedDate = "";
    if (date != null) {
      formattedDate = writeFormat.format(date);
    }

    Assert.assertEquals(currentDate, formattedDate);

  }

  /**
   * This method method used to click on Notifications of Application
   *
   * @throws InterruptedException
   */
  public void clickOnOptionsInUserIcon(String option) throws Exception {
    log.debug("Click on clickOnOptionsInUserIcon started");
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    w.clickOnLink(option);
    log.debug("Click on clickOnOptionsInUserIcon completed");
  }

  /**
   * This method method used toUpdate the company details
   *
   * @throws InterruptedException
   */
  public void companyDataUpdate(String CompanyName) throws Exception {

    clickOnOptionsInUserIcon("Change Company");
    Thread.sleep(2000);
    clickOnOptionsInUserIcon("Flex Admin User");
    w.getDriver().findElement(By.xpath("//a[@id=\"lnkDetailsPage\" and contains(text(),'" + CompanyName + "')]")).click();
    w.click(CssLocators.editCompany);
    w.click(CssLocators.updateCompanyButton);
    clickOnOptionsInUserIcon("Company selection");
    w.selectValue(CssLocators.signinCompanySelectKey, CompanyName);
    w.click(CssLocators.submitCompanyButtonKey);
    w.waitForElementToBeClickable(CssLocators.addProductLineLinkKey);

  }

  /**
   * This method method used to open new window for websocket
   *
   * @throws InterruptedException
   */
  public String openNewWindowForWebSocket(String username, String password) throws Exception {

    log.debug("loginAs " + username + " method invoked...");

    WebDriver driver = w.openBrowser(testNg.getBrowser());
    Set<String> windows = driver.getWindowHandles();
    String currentWindow = driver.getWindowHandle();
    ((org.openqa.selenium.JavascriptExecutor) driver).executeScript("window.open();");
    Set<String> newWindow = driver.getWindowHandles();
    newWindow.removeAll(windows);
    String newWindowTochkWebSocket = ((String) newWindow.toArray()[0]);
    driver.switchTo().window(newWindowTochkWebSocket);
    driver.get(testNg.getUrl());
    w.click(CssLocators.loginLinkKey);

    Thread.sleep(5000);
    w.waitForTitle("Select Company - Flex Connected Intelligence");
    w.selectValue(CssLocators.signinCompanySelectKey, "NEXTracker");
    w.click(CssLocators.submitCompanyButtonKey);
    w.waitForTitle("Product Line - Flex Connected Intelligence");
    w.waitUntilElementVisible(CssLocators.productLineSearchboxKey);
    clickOnOptionsInUserIcon("Notifications");
    w.click(CssLocators.websocketButtonKey);
    Thread.sleep(5000);
    driver.switchTo().window(currentWindow);
    return newWindowTochkWebSocket;

  }

  /**
   * This method method used to open new window for PutsReq
   *
   * @throws InterruptedException
   */
  public String openNewWindowForPutsReq(String url, String currentWindow) throws Exception {

    log.debug("openNewWindowForPutsReq method invoked...");
    Set<String> windows = w.getDriver().getWindowHandles();
    ((JavascriptExecutor) w.getDriver()).executeScript("window.open();");
    Set<String> newWindow = w.getDriver().getWindowHandles();
    newWindow.removeAll(windows);
    String newWindowPutsReq = ((String) newWindow.toArray()[0]);
    w.getDriver().switchTo().window(newWindowPutsReq);
    w.getDriver().get(url);
    w.getDriver().navigate().refresh();
    Thread.sleep(10000);
    w.getDriver().navigate().refresh();
    w.getDriver().switchTo().window(currentWindow);
    return newWindowPutsReq;

  }

  /**
   * Search Attribute
   * 
   * @param name
   * @return
   */
  public boolean searchAttribute(String name) {
    try {
      w.sendKeys(CssLocators.searchContainer, name);
      Thread.sleep(1200);
      if (w.getDriver().findElement(By.xpath("//td[contains(text(),'" + name + "')][1]")).isDisplayed()) {
        log.debug("Attibute found");
        w.clear(CssLocators.searchContainer);
        return true;
      } else {
        w.clear(CssLocators.searchContainer);
        return false;
      }
    } catch (Exception e) {
      w.clear(CssLocators.searchContainer);
      log.debug("Attibute doesnt exists");
      e.printStackTrace();
      return false;
    }
  }

  /**
   * Search Commands Methoids
   * 
   * @param name
   * @return
   */

  public boolean searchCommands(String name) {
    try {
      w.sendKeys(CssLocators.searchCommandsContainer, name);
      Thread.sleep(1200);
      if (w.getDriver().findElement(By.xpath("//td[contains(text(),'" + name + "')][1]")).isDisplayed()) {
        log.debug("Commands found");
        w.clear(CssLocators.searchCommandsContainer);
        return true;
      } else {
        w.clear(CssLocators.searchCommandsContainer);
        return false;
      }
    } catch (Exception e) {
      w.clear(CssLocators.searchCommandsContainer);
      log.debug("Command doesnt exists");
      e.printStackTrace();
      return false;
    }
  }

  /**
   * Search Settings
   * 
   * @param name
   * @return
   */

  public boolean searchSettings(String name) {
    try {
      w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
      Thread.sleep(1200);
      w.sendKeys(CssLocators.searchSettingsContainer, name);
      Thread.sleep(1200);
      if (w.getDriver().findElement(By.xpath("//td[contains(text(),'" + name + "')][1]")).isDisplayed()) {
        log.debug("Settings found");
        w.clear(CssLocators.searchSettingsContainer);
        return true;
      } else {
        w.clear(CssLocators.searchSettingsContainer);
        return false;
      }
    } catch (Exception e) {
      w.clear(CssLocators.searchSettingsContainer);
      log.debug("Setiings doesnt exists");
      e.printStackTrace();
      return false;
    }
  }

  /**
   * This method used to subscribe the Notification
   * 
   * @throws Exception
   */
  public boolean subscribeNotifications(String productMasterName, String eventAction, String DeliveryMethod) throws Exception {
    log.debug("subscribeNotifications started");
    Thread.sleep(2000);
    if (w.getDriver().findElement(By.xpath("//div[@id=\"divUnsubscribedAlert\"]//td[contains(text(),'" + eventAction + "')]")).isDisplayed()) {
      w.getDriver().findElement(By.xpath("//td[contains(text(),'" + eventAction + "')]//following-sibling::td/a[@id=\"lnkSubscribe\"]")).click();
      w.click(CssLocators.clickOnWebHookCheckBox);
      w.click(CssLocators.clickSaveButtonAlert);
      Thread.sleep(4000);
      if (w.getDriver().findElement(By.xpath("//div[@id=\"divSubscribedAlertData\"]//td[contains(text(),'" + eventAction + "')]")).isDisplayed()) {
        log.debug("Notification Subscribed");

        Thread.sleep(2000);
        return true;
      }
      log.debug("Notification is not subscribed");
      return false;

    }
    log.debug("Template is not found in unsucribed list");
    return false;

  }

  /**
   * This method used to Unsubscribe the Notification
   * 
   * @throws Exception
   */
  public String unsubscribeNotifications(String productLineName) throws Exception {
    log.debug("unsubscribeNotifications started");
    w.getDriver().findElement(By.xpath("//*[@id='list-group']/li//a[@title='" + productLineName + "']")).click();
    String AlertName = w.getText(CssLocators.getSubscribedNotificationName);
    w.click(CssLocators.unsubscribeAlert);
    if (w.isSelected(CssLocators.clickOnWebHookCheckBox)) {
      w.click(CssLocators.clickOnWebHookCheckBox);
    }
    if (w.isSelected(CssLocators.clickOnWebSocketCheckBox)) {
      w.click(CssLocators.clickOnWebSocketCheckBox);
    }
    /*
     * if (w.isSelected(CssLocators.clickOnEmailCheckBox)) { w.click(CssLocators.clickOnEmailCheckBox); }
     */
    w.click(CssLocators.clickSaveButtonAlert);
    w.waitForElementToBeClickable(CssLocators.backButtonKey);
    w.click(CssLocators.websocketButtonKey);
    w.getDriver().findElement(By.xpath("//div[@id=\"divItemListContainer\"]//a[@title='" + productLineName + "']")).click();
    w.waitForElementToBeClickable(CssLocators.backButtonKey);

    Assert.assertTrue(w.getDriver().findElement(By.xpath("//table[@id='tblUnsubscribedNotificationData']//td[text()='" + AlertName + "']")).isDisplayed());
    return AlertName;
  }

  /**
   * Method to Add User
   *
   * @return userId
   * @throws Exception
   * @throws IOException
   * @throws ParseException
   */
  public String verifyRegisteredUser(String userId, String emailId) throws Exception {
    log.debug("verifyRegisteredUser method invoked..." + emailId);
    w.checkGMail1(userId, emailId);
    return userId;
  }

  /**
   * Method to verifyProfile
   *
   * @return
   * @throws Exception
   */
  public String verifyProfile() throws Exception {
    log.debug("verifyProfile method invoked...");
    w.getText(CssLocators.editSecondaryEmail);
    String firstname = w.getText(CssLocators.editFirstName);
    w.getText(CssLocators.editLastName);
    return firstname;
  }

  /**
   * Method to verifyProfile
   *
   * @return
   * @throws Exception
   */
  public String verifyUserRoleAdmin() throws Exception {
    log.debug("verifyProfile method invoked...");
    // w.getText(CssLocators.editSecondaryEmail);
    w.getDriver().findElement(By.xpath("//*[@id=\"list-group\"]/li[1]")).click();
    String admin = w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[1]/td[1]")).getText();
    System.out.println("@@@@@@@@" + admin);
    return admin;
  }

  /**
   * Method to verifyProfile
   *
   * @return
   * @throws Exception
   */
  public String verifyUserRoleInstaller() throws Exception {
    log.debug("verifyUserRoleInstaller method invoked...");
    // w.getText(CssLocators.editSecondaryEmail);
    w.getDriver().findElement(By.xpath("//*[@id=\"list-group\"]/li[2]")).click();
    String installer = w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[1]/td[1]")).getText();
    System.out.println("@@@@@@@@" + installer);
    return installer;
  }

  /**
   * Method to verifyProfile
   *
   * @return
   * @throws Exception
   */
  public String verifyUserRoleManager() throws Exception {
    log.debug("verifyUserRoleManager method invoked...");
    // w.getText(CssLocators.editSecondaryEmail);
    w.getDriver().findElement(By.xpath("//*[@id=\"list-group\"]/li[3]")).click();
    String manager = w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[1]/td[1]")).getText();
    System.out.println("@@@@@@@@" + manager);
    return manager;
  }

  /**
   * Method to verifyProfile
   *
   * @return
   * @throws Exception
   */
  public String verifyUserRoleUser() throws Exception {
    log.debug("verifyUserRoleUser method invoked...");
    // w.getText(CssLocators.editSecondaryEmail);
    w.getDriver().findElement(By.xpath("//*[@id=\"list-group\"]/li[4]")).click();
    String user = w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[1]/td[1]")).getText();
    System.out.println("@@@@@@@@" + user);
    return user;
  }

  /**
   * Method to verifyProfile
   *
   * @return
   * @throws Exception
   */
  public String verifyUserRoleViewer() throws Exception {
    log.debug("verifyUserRoleViewer method invoked...");
    // w.getText(CssLocators.editSecondaryEmail);
    w.getDriver().findElement(By.xpath("//*[@id=\"list-group\"]/li[5]")).click();
    String viewer = w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[1]/td[1]")).getText();
    System.out.println("@@@@@@@@" + viewer);
    return viewer;
  }

  /**
   * Method to veriry admin can update another type of users details
   *
   * @return
   * @throws Exception
   */
  public String editInstaller(String emailId, String userRole) throws Exception {
    log.debug("editInstaller method invoked...");
    String fName = "editedFname";
    w.getDriver().findElement(By.xpath("//*[@id=\"list-group\"]/li[2]")).click();
    w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[1]/td[1]")).click();
    w.click(CssLocators.editUserKey);
    Thread.sleep(2000);
    w.clear(CssLocators.secondaryEmail);
    w.sendKeys(CssLocators.secondaryEmail, emailId);
    w.clear(CssLocators.firstName);
    w.sendKeys(CssLocators.firstName, fName);
    w.clear(CssLocators.lastName);
    w.sendKeys(CssLocators.lastName, "test user");
    w.clear(CssLocators.telephone);
    w.sendKeys(CssLocators.telephone, "1234567890");
    w.selectValue(CssLocators.userRole, userRole);
    // Thread.sleep(5000);
    w.click(CssLocators.userIdUpdatebuttonFieldKey);
    return fName;
  }

  /**
   * Method to Search Product Line
   *
   * @return
   * @throws java.lang.InterruptedException
   */
  public String deleteInstaller(String userRole) throws InterruptedException {
    log.debug("deleteInstaller method invoked...");
    w.getDriver().findElement(By.xpath("//*[@id=\"list-group\"]/li[2]")).click();
    String name = w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[1]/td[1]")).getText();
    w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr[2]/td[1]")).click();
    w.click(CssLocators.deleteUserKey);
    w.getDriver().findElement(By.xpath("//*[@id=\"btnDelete\"]")).click();
    Thread.sleep(1500);
    // w.waitUntilElementVisible(CssLocators.userIdSearchboxKey);
    // Assert.assertFalse(selectUser(name));
    return name;

  }

  /**
   * Method to Search Product Line
   *
   * @return
   * @throws Exception
   */
  public String verifyAlertPreferences() throws Exception {
    log.debug("verifyAlertPreferences method invoked...");
    addUserPreferences();
    // String
    // url=w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr/td[3]")).getText();
    w.getDriver().findElement(By.xpath("//*[@id=\"tblUsers\"]/tbody/tr/td[5]/p/a")).click();
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    Thread.sleep(2000);
    w.click(CssLocators.notificationsLinkKey);
    String newUrl = w.getDriver().findElement(By.xpath("//*[@id=\"divWebHookNotification\"]/div/h4")).getText();
    String formatStr = newUrl.substring(14, newUrl.length());
    System.out.println("OOOOOOOOOOOOOOO" + formatStr);
    Thread.sleep(1500);
    // w.waitUntilElementVisible(CssLocators.userIdSearchboxKey);
    // Assert.assertFalse(selectUser(name));
    return formatStr;

  }

  /**
   * Method to Configure Dynamic endpoint
   * 
   * @param je2
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public void endpointsImport(String reportingGroupName, String deviceId, String serialNumber, String MfgId, JavascriptExecutor je2)
      throws InterruptedException, ParseException, IOException {
    log.debug("endpointsImport method invoked...");
    w.click(CssLocators.groupLinkKey);
    Thread.sleep(4000);
    w.click(CssLocators.reportingGroupKey);

    WebElement element = w.getDriver().findElement(By.cssSelector(CssLocators.configureReportingGroupKey));

    je2.executeScript("arguments[0].scrollIntoView(true);", element);
    Thread.sleep(1000);
    je2.executeScript("scroll(250, 0)");
    Thread.sleep(2500);
    w.click(CssLocators.configureReportingGroupKey);
    w.click(CssLocators.configureReportingGroupMoverKey);
    w.click(CssLocators.reportingGroupSiteConfigKey);
    writeJsonFileSiteConfig(deviceId, serialNumber, reportingGroupName, MfgId);
    w.sendKeys(CssLocators.siteConfigUploadInputKey, absolutePath());
    w.click(CssLocators.siteConfigSubmitKey);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);

  }

  /**
   * Method to write Json file for site config
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  @SuppressWarnings("unchecked")
  public void writeJsonFileSiteConfig(String deviceId, String serialNumber, String reportingGroupName, String mfgID)
      throws java.text.ParseException, IOException, InterruptedException {
    log.debug("writeJsonFileSiteConfig method invoked...");

    JSONObject productObj = new JSONObject();
    productObj.put("name", "Product name" + w.randomName());
    productObj.put("sn", serialNumber);
    productObj.put("deviceid", deviceId);

    JSONObject endpointPropetiesObj1 = new JSONObject();
    endpointPropetiesObj1.put("name", "Ep " + w.randomName());
    endpointPropetiesObj1.put("mfgid", mfgID);
    endpointPropetiesObj1.put("serialNo", serialNumber + w.generateRandomNumber());
    JSONObject endpointPropetiesObj2 = new JSONObject();
    endpointPropetiesObj2.put("name", "Ep " + w.randomName());
    endpointPropetiesObj2.put("mfgid", mfgID);
    endpointPropetiesObj2.put("serialNo", serialNumber + w.generateRandomNumber());
    JSONObject endpointPropetiesObj3 = new JSONObject();
    endpointPropetiesObj3.put("name", "Ep " + w.randomName());
    endpointPropetiesObj3.put("mfgid", mfgID);
    endpointPropetiesObj3.put("serialNo", serialNumber + w.generateRandomNumber());

    JSONArray endpointList = new JSONArray();
    endpointList.add(endpointPropetiesObj1);
    endpointList.add(endpointPropetiesObj2);
    endpointList.add(endpointPropetiesObj3);

    productObj.put("endpoints", endpointList);

    JSONArray productList = new JSONArray();
    productList.add(productObj);

    JSONObject customFieldsObj = new JSONObject();
    customFieldsObj.put("key", "Field 1");
    customFieldsObj.put("value", "Test value");
    JSONArray customFieldsList = new JSONArray();
    customFieldsList.add(customFieldsObj);

    JSONObject reportingGroupObj = new JSONObject();
    reportingGroupObj.put("products", productList);
    reportingGroupObj.put("customfields", customFieldsList);
    reportingGroupObj.put("groupname", reportingGroupName);

    JSONObject finalObj = new JSONObject();
    finalObj.put("ReportingGroup", reportingGroupObj);

    try {

      FileWriter file = new FileWriter(absolutePath());
      file.write(finalObj.toJSONString());
      file.flush();
      file.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.print(reportingGroupObj);
  }

  /**
   * Method to write Json file for site config
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  @SuppressWarnings("unchecked")
  public void writeJsonSiteConfigMultipleEP(String deviceId, String serialNumber, String reportingGroupName, String mfgID, int epRequiredCount)
      throws java.text.ParseException, IOException, InterruptedException {
    log.debug("writeJsonFileSiteConfig method invoked...");

    JSONObject productObj = new JSONObject();
    JSONObject endpointPropetiesObj1 = new JSONObject();
    productObj.put("name", "Product name" + w.randomName());
    productObj.put("sn", serialNumber);
    productObj.put("deviceid", deviceId);
    int count = 0;
    JSONArray endpointList = new JSONArray();
    while (count < epRequiredCount) {
      endpointPropetiesObj1 = new JSONObject();
      endpointPropetiesObj1.put("name", "E19" + w.randomName());
      endpointPropetiesObj1.put("mfgid", mfgID);
      endpointPropetiesObj1.put("serialNo",
          serialNumber + w.generateRandomNumber() + w.generateRandomNumber() + w.generateRandomNumber() + w.generateRandomNumber());
      endpointList.add(endpointPropetiesObj1);
      count++;
    }

    productObj.put("endpoints", endpointList);

    JSONArray productList = new JSONArray();
    productList.add(productObj);

    JSONObject customFieldsObj = new JSONObject();
    customFieldsObj.put("key", "Field 1");
    customFieldsObj.put("value", "Test value");
    JSONArray customFieldsList = new JSONArray();
    customFieldsList.add(customFieldsObj);

    JSONObject reportingGroupObj = new JSONObject();
    reportingGroupObj.put("products", productList);
    reportingGroupObj.put("customfields", customFieldsList);
    reportingGroupObj.put("groupname", reportingGroupName);

    JSONObject finalObj = new JSONObject();
    finalObj.put("ReportingGroup", reportingGroupObj);

    try {

      FileWriter file = new FileWriter(absolutePath());
      file.write(finalObj.toJSONString());
      file.flush();
      file.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
    System.out.print(reportingGroupObj);
  }

  /**
   * Method to verify End points created by site config
   *
   * @throws java.lang.InterruptedException
   */
  public boolean verifySiteConfigEndPoints(String productLineName, String DeviceID, String mfgid) throws InterruptedException {
    log.debug("verifySiteConfigEndPoints method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    searchProduct(DeviceID);
    Thread.sleep(4000);
    w.click(CssLocators.dynamicPointButton);
    Thread.sleep(4000);
    Assert.assertTrue(w.isTextPresent(mfgid));
    w.click(CssLocators.dynamicEndPointlinkKey);
    if (w.isDisplayed(CssLocators.titleMessageDisplayed)) {
      if (w.getText(CssLocators.titleMessageDisplayed).equals("Error"))
        return false;
      else
        return true;
    }

    return true;

  }

  /**
   * Method to Configure Dynamic endpoint
   *
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public void endpointsImportWithHardCodeFile(String filePath) throws InterruptedException, ParseException, IOException {
    log.debug("endpointsImportWithHardCodeFile method invoked...");

    w.click(CssLocators.groupLinkKey);
    Thread.sleep(4000);
    w.click(CssLocators.reportingGroupKey);
    WebElement element = w.getDriver().findElement(By.cssSelector(CssLocators.configureReportingGroupKey));
    w.click(CssLocators.configureReportingGroupKey);
    w.click(CssLocators.configureReportingGroupMoverKey);
    w.click(CssLocators.reportingGroupSiteConfigKey);

    log.debug(filePath);
    w.sendKeys(CssLocators.siteConfigUploadInputKey, filePath);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.click(CssLocators.siteConfigSubmitKey);

  }

  public void ConnectedDynamicEndPointSetting(String productLineName, String deviceId) throws InterruptedException {
    selectProduct(productLineName, deviceId);
    w.click(CssLocators.dynamicPointButton);
    Thread.sleep(2000);
    w.click(CssLocators.dynamicEndPointlinkKey);
    Thread.sleep(4000);
    w.click(CssLocators.dynamicEpSettingLink);
    Thread.sleep(3000);
  }

  public boolean selectProduct(String productLineName, String product) throws InterruptedException {
    log.debug("selectProduct method invoked...");
    try {
      Thread.sleep(2000);
      selectProductLine(productLineName);
      w.click(CssLocators.openProductLine);
      Thread.sleep(4000);
      w.clear(CssLocators.searchBoxProductKey);
      w.sendKeys(CssLocators.searchBoxProductKey, product);
      Assert.assertEquals(w.getText(CssLocators.searchProductDeviceIDResultKey), product);
      return true;
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Method to Configure Dynamic endpoint
   *
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public void multipleEndPointImport(String reportingGroupName, String deviceId, String serialNumber, String MfgId, int count, JavascriptExecutor je2)
      throws InterruptedException, ParseException, IOException {
    log.debug("endpointsImport method invoked...");
    Thread.sleep(4000);
    w.click(CssLocators.groupLinkKey);
    Thread.sleep(4000);
    w.click(CssLocators.reportingGroupKey);
    WebElement element = w.getDriver().findElement(By.cssSelector(CssLocators.configureReportingGroupKey));

    je2.executeScript("arguments[0].scrollIntoView(true);", element);
    Thread.sleep(1000);
    je2.executeScript("scroll(250, 0)");
    Thread.sleep(2500);
    w.click(CssLocators.configureReportingGroupKey);
    w.click(CssLocators.configureReportingGroupMoverKey);
    w.click(CssLocators.reportingGroupSiteConfigKey);
    writeJsonSiteConfigMultipleEP(deviceId, serialNumber, reportingGroupName, MfgId, count);
    w.sendKeys(CssLocators.siteConfigUploadInputKey, absolutePath());
    w.click(CssLocators.siteConfigSubmitKey);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);

  }

  public void deleteProduct(String productLineName, String id) throws InterruptedException {
    log.debug("deleteProduct method invoked...");

    Thread.sleep(2000);
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    Thread.sleep(2000);
    searchProduct(id);
    w.click(CssLocators.deleteProductButton);
    w.click(CssLocators.deleteConfirmKey);

  }

  public void serachDynamicEP(String searchValue) throws InterruptedException {
    log.debug(" serachDynamicEP method invoked...");
    w.sendKeys(CssLocators.searchDynamicEpbox, searchValue);
    w.click(CssLocators.clickGoforSearchDEP);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(2000);
  }

  /**
   * Method toGet the Next date By using Current date
   */
  public String getNextDate(String curDate) {
    String nextDate = "";
    try {
      Calendar today = Calendar.getInstance();
      DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
      Date date = format.parse(curDate);
      today.setTime(date);
      today.add(Calendar.DAY_OF_YEAR, 1);
      nextDate = format.format(today.getTime());
    } catch (Exception e) {
      return nextDate;
    }
    return nextDate;
  }

  /**
   * Method to Create Product using Json file
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  public void addProductDuplicateUsingJsonFile(String productLineName, String deviceId, String serialNumber)
      throws java.text.ParseException, IOException, InterruptedException {
    log.debug("addProductUsingJsonFile method invoked...");
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    Thread.sleep(5000);
    w.click(CssLocators.addProductButtonKey);
    writeJsonFile(deviceId, serialNumber);
    Thread.sleep(5000);
    w.sendKeys(CssLocators.fileInputStringKey, absolutePath());
    w.click(CssLocators.fileUploadSubmitButtonKey);

  }

  /**
   * Method to Create Product using Json file
   *
   * @throws java.text.ParseException
   * @throws InterruptedException
   */
  public void verifyAddProductWithInvalidDetails(String deviceId, String serialNumber, String swVersion, String hwVersion)
      throws java.text.ParseException, IOException, InterruptedException {
    log.debug("addProductUsingJsonFile method invoked...");
    w.click(CssLocators.addProductButtonKey);
    writeProductJsonFile(deviceId, serialNumber, swVersion, hwVersion);
    Thread.sleep(5000);
    w.sendKeys(CssLocators.fileInputStringKey, absolutePath());
    w.click(CssLocators.fileUploadSubmitButtonKey);

  }

  /**
   * Method to test login failure due to wrong credentials in Azure
   * 
   * @param login
   * @param password
   */
  public void logInAzureWithWrongCredentials(String login, String password) {
    log.debug("logInWithWrongCredentials started");
    if (w.isDisplayed(CssLocators.useAnotherAccountKey)) {
      w.click(CssLocators.useAnotherAccountKey);
    }

    w.clear(CssLocators.inputUserIdKey);
    w.sendKeys(CssLocators.inputUserIdKey, login);
    w.clear(CssLocators.inputPasswordKey);
    w.sendKeys(CssLocators.inputPasswordKey, password);
    w.click(CssLocators.signInButtonKey);

  }

  /**
   * Method to check the forgotPassword
   * 
   * @param userName
   * @throws Exception
   */
  public void forgotPasswordAzure(String userName) throws Exception {
    // w.click(CssLocators.loginLinkKey);
    log.debug("forgotPassword started");
    w.click(CssLocators.forgotPasswordLinkAzure);
    Thread.sleep(5000);
    // w.sendKeys(CssLocators.forgrtPasswordAzureEmailKey, userName);
    Thread.sleep(3000);
    w.click(CssLocators.forgotPasswordCancel);

    log.debug("forgotPassword completed");
  }

  /**
   * Method to Add Package Details
   *
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public void addPackageDetails(String packageGroupName, String productline, String type) throws InterruptedException, ParseException, IOException {
    log.debug("addPackageDetails method invoked...");
    Thread.sleep(2000);
    String deviceId = "e2e" + w.generateRandomNumber() + "d" + w.generateRandomNumber();
    String serialNumber = "30042_" + w.generateRandomNumber();
    searchPackageGroup(packageGroupName, productline);
    w.click(CssLocators.searchPackageResultKey);
    Thread.sleep(1500);
    w.click(CssLocators.addpackageDetailsButtonKey);
    Thread.sleep(3000);
    w.sendKeys(CssLocators.selectPackagedetailKey, type);
    writeJsonFile(deviceId, serialNumber);
    w.sendKeys(CssLocators.choosePackagedetailKey, absolutePath());
    Thread.sleep(3000);
    w.click(CssLocators.uploadPackagedetailKey);
    Thread.sleep(2000);
  }

  /**
   * Method to Edit Package Details
   *
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public void editPackageDetails(String productLineName, String packageGroupName) throws InterruptedException, ParseException, IOException {
    log.debug("editPackageDetails method invoked...");
    Thread.sleep(2000);
    String deviceId = "e2e" + w.generateRandomNumber() + "d" + w.generateRandomNumber();
    String serialNumber = "30042_" + w.generateRandomNumber();
    searchPackageGroup(packageGroupName, productLineName);
    w.click(CssLocators.searchPackageResultKey);
    w.click(CssLocators.editpackageDetailsButtonKey);
    writeJsonFile(deviceId, serialNumber);
    Thread.sleep(2000);
    w.sendKeys(CssLocators.choosePackagedetailKey, absolutePath());
    Thread.sleep(5000);
    w.click(CssLocators.updateButtonKey);
  }

  /**
   * Method to Delete Package Details
   *
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   * @return packageName
   */
  public String deletePackageDetails(String productLineName, String packageGroupName) throws InterruptedException, ParseException, IOException {
    log.debug("editPackageDetails method invoked...");
    Thread.sleep(2000);
    searchPackageGroup(packageGroupName, productLineName);
    w.click(CssLocators.searchPackageResultKey);
    String packageName = w.getText(CssLocators.getPackgeName);
    w.click(CssLocators.deletePackageButton);
    w.click(CssLocators.deleteConfirmKey);
    return packageName;
  }

  /**
   * Method to Add APKPackage Details
   *
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public void addAPKPackageDetails(String productMaster, String packageGroupName, String path) throws InterruptedException, ParseException, IOException {
    log.debug("addPackageDetails method invoked...");
    String packageDescription = "APK-" + w.randomName();
    Thread.sleep(2000);
    searchPackageGroup(packageGroupName, productMaster);
    w.click(CssLocators.searchPackageResultKey);
    w.click(CssLocators.addpackageDetailsButtonKey);
    Thread.sleep(3000);
    w.sendKeys(CssLocators.selectPackagedetailKey, "APK");
    Thread.sleep(2000);
    w.sendKeys(CssLocators.choosePackagedetailKey, path);
    Thread.sleep(2000);
    w.click(CssLocators.uploadPackagedetailKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.sendKeys(CssLocators.addPackageGroupDescriptionFieldKey, packageDescription);
    Thread.sleep(2000);
    w.click(CssLocators.savePackageGroupButtonKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);

  }

  /**
   * Method to Add Package Details
   *
   * @throws java.lang.InterruptedException
   */
  public void downloadPackage(String packageName, String productline) throws InterruptedException {
    log.debug("addPackageDetails method invoked...");
    Thread.sleep(2000);
    selectProductLine(productline);
    // w.getDriver().findElement(By.xpath("//*[@id=\"btnViewPackages\"]/text()")).click();
    w.click(CssLocators.viewPackageButtonKey);
    w.click(CssLocators.addpackageDetailsButtonKey);
    w.selectValueByIndex(CssLocators.selectPackagedetailKey, 2);
    String path = "";
    w.sendKeys(CssLocators.choosePackagedetailKey, path);
    Thread.sleep(5000);
    w.click(CssLocators.uploadPackagedetailKey);
  }

  /**
   * Method to Delete Package
   * 
   * @throws java.lang.InterruptedException
   */
  public void deletePackage(String productLine, String packageName) throws InterruptedException {
    log.debug("deletePackage method invoked...");
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    Thread.sleep(2000);
    selectProductLine(productLine);
    Thread.sleep(2000);
    w.click(CssLocators.deletePackageFieldKey);
    w.click(CssLocators.deletePackageConfirmButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.packagesTabLinkKey);
  }

  /**
   * Method to Add Company
   *
   * @throws java.lang.InterruptedException
   */
  public String createTenant(String companyCuid, String companyName, String companyUrl, String companyLogoUrl, String companyEmail, String authenticationType)
      throws InterruptedException {
    log.debug("addCompany method invoked...");
    Thread.sleep(1000);
    // w.getDriver().findElement(By.xpath("//button[text()=\"Create New Tenant\"]")).click();
    w.click(CssLocators.flexAdminCreateCompanyKey);
    Thread.sleep(000);
    w.clear(CssLocators.addCompanyCuidKey);
    w.sendKeys(CssLocators.addCompanyCuidKey, companyCuid);
    w.clear(CssLocators.addCompanyNameKey);
    w.sendKeys(CssLocators.addCompanyNameKey, companyName);
    w.clear(CssLocators.addCompanyDescriptionKey);
    w.sendKeys(CssLocators.addCompanyDescriptionKey, "Company used for e2e autotest");
    w.clear(CssLocators.addCompanyUrlKey);
    w.sendKeys(CssLocators.addCompanyUrlKey, companyUrl);
    w.clear(CssLocators.addCompanyEnvKey);
    w.sendKeys(CssLocators.addCompanyEnvKey, "");
    w.clear(CssLocators.addCompanyLogoUrlKey);
    w.sendKeys(CssLocators.addCompanyLogoUrlKey, companyLogoUrl);
    w.clear(CssLocators.addCompanyEmailKey);
    w.sendKeys(CssLocators.addCompanyEmailKey, companyEmail);
    w.getDriver().findElement(By.xpath("//label[text()=\"Authentication Type\"]/following-sibling::select")).sendKeys(authenticationType);
    w.click(CssLocators.addCompanySaveKey);
    Thread.sleep(1000);
    return companyCuid;
  }

  /**
   * Method to Update Company
   *
   * @return companyName
   * @throws java.lang.InterruptedException
   */
  public String updateCompany(String companyName, String companyUrl, String companyEmail) throws InterruptedException {
    log.debug("updateCompany method invoked...");
    Date currentTime = new Date();
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    Thread.sleep(4000);
    // w.click(CssLocators.manageTenants);
    w.clickOnLink("Select Tenant");
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    Thread.sleep(2000);
    // w.click(CssLocators.manageTenants);
    w.clickOnLink("Manage Tenants");
    String currentDate = dateformat1.format(currentTime);
    searchCompany(companyName);
    Thread.sleep(3000);
    String companyNameNew = companyName + "_" + currentDate;
    Thread.sleep(2000);
    w.click(CssLocators.flexAdminEditKey);
    Thread.sleep(5000);
    w.clear(CssLocators.addCompanyNameKey);
    w.sendKeys(CssLocators.addCompanyNameKey, companyNameNew);
    w.clear(CssLocators.addCompanyDescriptionKey);
    w.sendKeys(CssLocators.addCompanyDescriptionKey, "Updated Company description");
    w.clear(CssLocators.addCompanyUrlKey);
    w.sendKeys(CssLocators.addCompanyUrlKey, companyUrl + "/go");
    w.clear(CssLocators.addCompanyLogoUrlKey);
    w.sendKeys(CssLocators.addCompanyLogoUrlKey, companyUrl + "/a.png");
    Thread.sleep(2000);
    w.click(CssLocators.companyUpdateKey);
    return companyNameNew;
  }

  /**
   * Method to Delete Company
   *
   * @throws java.lang.InterruptedException
   */
  public void deleteTenant(String companyName) throws InterruptedException {
    log.debug("deleteCompany method invoked...");

    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    w.click(CssLocators.changeCompanyKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    w.clickOnLink("Manage Tenants");
    Thread.sleep(4000);
    searchCompany(companyName);
    Thread.sleep(1000);
    w.click(CssLocators.companyDeleteKey);
    w.click(CssLocators.deleteConfirmKey);
  }

  public void sendCommandStaticEP(String productLineName, String deviceID, String commandName) throws InterruptedException {
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    searchProduct(deviceID);
    w.click(CssLocators.viewButtonOfProductKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);
    w.click(CssLocators.commandButton);
    Thread.sleep(1500);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + commandName + "')]")).click();
    Thread.sleep(4000);
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/input[2]")).click();
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/input[2]")).sendKeys(String.valueOf(500));
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/button[@id=\"btnSubmit\"]")).click();

  }

  public void sendCommandDynamicEP(String commandName) throws InterruptedException {
    Thread.sleep(3000);
    w.waitForElementToBeClickable(CssLocators.commandproductLink);
    w.click(CssLocators.commandproductLink);
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + commandName + "')]")).click();
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/input[2]")).click();
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/input[2]")).sendKeys(String.valueOf(1));
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/button[@id=\"btnSubmit\"]")).click();
    Thread.sleep(2000);

  }

  public void sendSysDateCommandStaticEP(String productLineName, String deviceID, String commandName) throws InterruptedException {
    selectProductLine(productLineName);
    w.click(CssLocators.openProductLine);
    searchProduct(deviceID);
    w.click(CssLocators.viewButtonOfProductKey);
    w.waitForElementToBeClickable(CssLocators.commandproductLink);
    Thread.sleep(2000);
    w.click(CssLocators.commandButton);
    Thread.sleep(1500);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + commandName + "')]")).click();
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/button[@id=\"btnSubmit\"]")).click();
    Thread.sleep(2000);

  }

  public void sendSysDateCommandDynamicEP(String commandName) throws InterruptedException {
    Thread.sleep(3000);
    w.waitForElementToBeClickable(CssLocators.commandproductLink);
    w.click(CssLocators.commandproductLink);
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + commandName + "')]")).click();
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//td[contains(text(),'" + commandName + "')]/following-sibling::td/button[@id=\"btnSubmit\"]")).click();
    Thread.sleep(2000);

  }

  public String productStatus(String productLineName, String deviceId) throws InterruptedException {
    selectProduct(productLineName, deviceId);
    String status = w.getDriver().findElement(By.xpath("//table[@id=\"tblProduct\"]//td[@data-label=\"State\"]")).getText().trim();
    return status;

  }

  /**
   * Method to Copy Configure Dynamic endpoint
   * 
   * @throws java.lang.InterruptedException
   */
  public String copyDynamicEp(String DestinationProductLine) throws InterruptedException {
    log.debug("add configureProductLineAttributes method invoked...");
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.configurationSettingButton);
    w.click(CssLocators.copyDynamicEndPointButton);
    Thread.sleep(2000);
    w.click(CssLocators.selectTargetDynamicEp);
    w.sendKeys(CssLocators.selectTargetDynamicEp, DestinationProductLine);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    return DestinationProductLine;

  }

  /**
   * Method to Copy Configure Dynamic endpoint to same productline
   * 
   * @throws java.lang.InterruptedException
   */
  public boolean copyDynamicEpToSameProductLine(String DestinationProductLine) throws InterruptedException {
    log.debug("add configureProductLineAttributes method invoked...");
    Thread.sleep(1500);
    w.mouseOverOnElement(CssLocators.configurationSettingButton);
    w.click(CssLocators.copyDynamicEndPointButton);
    Thread.sleep(2000);
    WebElement element = w.getDriver().findElement(By.cssSelector(CssLocators.selectTargetDynamicEp));
    Select select = new Select(element);
    List<WebElement> allOptions = select.getOptions();
    if (allOptions.contains(DestinationProductLine)) {
      w.click(CssLocators.CancelButton);
      return true;
    } else {
      w.click(CssLocators.CancelButton);
      return false;
    }
  }

  /**
   * Method to get Last connected time of Product and DynamicEp
   *
   * @throws java.lang.InterruptedException
   * @returns last connected time
   */
  public String lastConnectedTime(String productline, String deviceId, String type) throws InterruptedException {
    log.debug("lastConnectedTime method invoked...");

    selectProduct(productline, deviceId);
    if (type == "product") {
      w.click(CssLocators.viewButtonOfProductKey);
      w.waitForElementToBeClickable(CssLocators.backButtonProdKey);
    } else {
      clickOnEndpoints();
      w.click(CssLocators.dynamicEndPointlinkKey);
      w.waitForElementToBeClickable(CssLocators.backButtonProdKey);
    }
    String path = "//div[contains(text(),\"Last Active Time\")]/following-sibling::div";
    String LastConnectedTime = w.getDriver().findElement(By.xpath(path)).getText();
    return LastConnectedTime;
  }

  /**
   * - * Method to Add duplicate Product Line - * - * @return Product Line Name - * @throws java.lang.InterruptedException -
   */
  public void addProductLineDulpicateSKU(String productLineName, String productLineSku) throws InterruptedException {
    log.debug("addProductLine method invoked...");
    String productLineDescription = "Product Line definition used for e2e autotest";
    Thread.sleep(5000);
    w.click(CssLocators.addProductLineLinkKey);
    w.sendKeys(CssLocators.addProductLineNameFieldKey, productLineName);
    w.sendKeys(CssLocators.addProductLineSkuFieldKey, productLineSku);
    w.sendKeys(CssLocators.addProductLineDescriptionFieldKey, productLineDescription);
    w.click(CssLocators.addProductLineSaveButtonKey);
    w.waitUntilCssElementVisible(CssLocators.messageDisplayed);

  }

  /*
   * import DynamicEP configuration for project room location
   */

  public void exportEndPointConfiguration() throws java.text.ParseException, Exception {
    w.mouseOverOnElement(CssLocators.configurationSettingButton);
    ;
    w.click(CssLocators.endpointExportButton);
    handleDownloadDialog();
  }

  /*
   * import Product line configuration for project room location
   */

  public void exportProductLIneConfiguration(String productLineName) throws java.text.ParseException, Exception {
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(5000);
    w.waitUntilTextAppears(CssLocators.selectDynamicEndPoint, productLineName);
    w.mouseOverOnElement(CssLocators.configurationSettingButton);
    w.click(CssLocators.endpointExportButton);
    handleDownloadDialog();
  }

  /*
   * download DynamicEP configuration in a json file
   */
  public void importEndPointConfiguration(String productLineName, String path) throws java.text.ParseException, Exception {
    log.debug("uploadEndPointConfiguration method invoked...");
    Thread.sleep(1500);
    selectProductLine(productLineName);
    w.click(CssLocators.configureProductLineButtonKey);
    Thread.sleep(4000);
    w.mouseOverOnElement(CssLocators.configurationSettingButton);
    w.click(CssLocators.addEndpointButtonKey);
    w.getDriver().findElement(By.xpath("//button[text()=\"Import\"]")).click();
    w.sendKeys(CssLocators.importEndPoint, path);
    w.click(CssLocators.clickSaveButtonKey);
  }

  /*
   * Import ProductLine configuration
   */
  public void importProductLineConfiguration(String path) throws java.text.ParseException, Exception {
    log.debug("uploadEndPointConfiguration method invoked...");
    w.click(CssLocators.productMaster);
    w.mouseOverOnElement(CssLocators.productMasterSettingButton);
    w.click(CssLocators.importProductLine);
    w.sendKeys(CssLocators.importEndPoint, path);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(6000);
  }

  /**
   * - * Method to Verify the added element - * check in the first page if its not present click Next -
   */

  public boolean verifyAttributeAdded(String element) {
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    if (w.elementExistByText(element)) {
      log.debug(element + " is added successfully");
      return true;
    } else if (w.isEnabled(CssLocators.attributePaginationPrevious)) {
      w.getDriver().findElement(By.xpath("//ul[@role=\"navigation\"]//li[2]/a")).click();
      if (w.elementExistByText(element)) {
        log.debug(element + " is added successfully");
        return true;
      } else {
        while ((w.isEnabled(CssLocators.attributePaginationNext))) {
          w.click(CssLocators.attributePaginationNext);
          if (w.elementExistByText(element)) {
            log.debug(element + " is added successfully");
            return true;
          }
        }
      }
    } else if (w.isEnabled(CssLocators.attributePaginationNext)) {
      while ((w.isEnabled(CssLocators.attributePaginationNext))) {
        w.click(CssLocators.attributePaginationNext);
        if (w.elementExistByText(element)) {
          log.debug(element + " is added successfully");
          return true;
        }
      }
    } else {
      w.getDriver().findElement(By.xpath("//ul[@role=\"navigation\"]//li[2]/a")).click();
      Assert.assertTrue(w.elementExistByText(element));
      return true;
    }
    return false;
  }

  /**
   * - * Method to Verify Settings added element - * check in the first page if its not present click Next -
   * 
   * @throws Exception
   */
  public boolean verifySettingAdded(String element) throws Exception {
    w.mouseOverOnElement(CssLocators.clickOnProductLink);

    if (w.elementExistByText(element)) {
      log.debug(element + " is added successfully");
      return true;
    } else if (w.isEnabled(CssLocators.settingsPaginationPrevious)) {
      w.getDriver().findElement(By.xpath("//ul[@role=\"navigation\"]//li[2]/a")).click();
      if (w.elementExistByText(element)) {
        log.debug(element + " is added successfully");
        return true;
      } else {
        while ((w.isEnabled(CssLocators.settingsPaginationNext))) {
          w.click(CssLocators.settingsPaginationNext);
          if (w.elementExistByText(element)) {
            log.debug(element + " is added successfully");
            return true;
          }
        }
      }
    } else if (w.isEnabled(CssLocators.settingsPaginationNext)) {
      while ((w.isEnabled(CssLocators.settingsPaginationNext))) {
        w.click(CssLocators.settingsPaginationNext);
        Thread.sleep(3000);
        w.mouseOverOnElement(CssLocators.addSettingsButtonKey);
        if (w.elementExistByText(element)) {
          log.debug(element + " is added successfully");
          return true;
        }
      }
    } else {
      w.getDriver().findElement(By.xpath("//ul[@role=\"navigation\"]//li[2]/a")).click();
      Assert.assertTrue(w.elementExistByText(element));
      return true;
    }
    return false;
  }

  /*
   * Update UserFiled value
   */
  public String updateUserFiledInProductLine(String userfieldName) throws java.text.ParseException, Exception {
    log.debug("uploadEndPointConfiguration method invoked...");
    String value = "1" + w.generateRandomNumber();
    searchProductMasterUserField(userfieldName);
    w.click(CssLocators.userFieldEditButton);
    w.clear(CssLocators.userFieldValueKey);
    w.sendKeys(CssLocators.userFieldValueKey, value);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(3000);
    return value;

  }

  /*
   * Update UserFiled value
   */
  public String addUserFiledValue(String userfieldName) throws java.text.ParseException, Exception {
    log.debug("uploadEndPointConfiguration method invoked...");
    String value = "1" + w.generateRandomNumber();
    searchProductMasterUserField(userfieldName);
    w.click(CssLocators.userFieldEditButton);
    Thread.sleep(1000);
    w.sendKeys(CssLocators.userFieldValueKey, value);
    w.click(CssLocators.clickSaveButtonKey);
    Thread.sleep(2000);
    return value;

  }

  /*
   * verify History for Static EP
   */
  public void SettingsHistory(String settingName) throws java.text.ParseException, Exception {
    log.debug("verifySettingsHistory method invoked...");

    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);
    w.click(CssLocators.productSettingLinkKey);
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    Thread.sleep(4000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td//a[@value=\"History\"]")).click();
    Thread.sleep(3000);
  }

  /*
   * METHOD TO SET check the availability of Pending values
   */
  public boolean isPendingValuesAvailable(String settingName) throws java.text.ParseException, Exception {
    log.debug("Verify availability of Pending valuesinvoked...");
    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);
    w.click(CssLocators.productSettingLinkKey);
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    if (w.isDisplayed(CssLocators.pendingSettingKey)) {
      return true;
    }

    return false;

  }

  /*
   * verify History for Static EP
   */
  public boolean verifyCurrentVal(String settingName) throws java.text.ParseException, Exception {
    log.debug("verifycurrentVal method invoked...");

    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);
    w.click(CssLocators.productSettingLinkKey);
    Thread.sleep(3000);

    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    /*
     * Select dropdownValues = new Select(w.getDriver() .findElement(By.xpath("//label[text()='" + settingName + "']/../following-sibling::select"))); String
     * optionValue = dropdownValues.getFirstSelectedOption().getText(); System.out.println(optionValue);
     */

    String value = w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/input")).getText();
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td//a[@value=\"History\"]")).click();
    Thread.sleep(3000);
    String currentValue = w.getText(CssLocators.getcurrentValHistory);
    Thread.sleep(2000);
    w.click(CssLocators.CancelButton);
    if (currentValue.contains(value)) {
      return true;
    }

    return false;

  }

  /*
   * METHOD TO SET THE VALUES TO SETTING FROM PORTAL
   */
  public void sendSettingValuesFromPortal(String settingName, int val) throws java.text.ParseException, Exception {
    log.debug("sendSettingValuesFromPortal method invoked...");
    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);
    w.click(CssLocators.productSettingLinkKey);
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    Thread.sleep(3000);

    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/input")).click();
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/input")).clear();
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/input")).sendKeys(String.valueOf(val));
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/a[@id=\"btnSaveSetting\"]")).click();
    Thread.sleep(3000);

  }

  /*
   * METHOD TO SET THE VALUES TO SETTING FROM PORTAL
   */
  public String GetSettingPendingValues(String settingName) throws java.text.ParseException, Exception {
    log.debug("verifypendingValues method invoked...");
    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);
    w.click(CssLocators.productSettingLinkKey);
    Thread.sleep(2000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    Thread.sleep(4000);
    w.click(CssLocators.pendingSettingKey);
    Thread.sleep(2000);
    String value = w.getText(CssLocators.settingPendingvalue);
    return value;

  }

  // div[@id="divSettingsTab"]//span[text()="BigInt-Settingvvbxr"]
  /*
   * MWTHOD TO VARIFY THE TIME SETTING VALUE REQUESTED
   */
  public boolean verifySettingValRequestedTime(String settingName) throws java.text.ParseException, Exception {
    log.debug("verifySettingValRequestedTime method invoked...");
    Date currentTime = new Date();
    String currentDate = dateformat.format(currentTime);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + settingName + "')]")).click();
    Thread.sleep(7000);
    w.mouseOverOnElement(CssLocators.pendingSettingKey);
    w.click(CssLocators.pendingSettingKey);
    Thread.sleep(2000);
    String displayedTime = w.getText(CssLocators.displayedSettingTimestamp);
    if (displayedTime.substring(0, 10).equals(currentDate)) {
      return true;
    }
    return false;
  }

  /*
   * MWTHOD TO VARIFY THE Pending status of setting val
   */
  public String verifyPendingValueStatus(String settingName, int val) throws java.text.ParseException, Exception {
    log.debug("verifyPendingValueStatus method invoked...");
    SettingsHistory(settingName);
    Thread.sleep(3000);
    String status = w.getDriver().findElement(By.xpath("//td[contains(text(),'" + String.valueOf(val) + "')]/following-sibling::td[3]")).getText().trim();
    return status;

  }

  /*
   * MWTHOD TO VARIFY THE Pending status of setting val
   */
  public String verifyRequestedUserName(String settingName, int val) throws java.text.ParseException, Exception {
    log.debug("verifyPendingValueStatus method invoked...");
    SettingsHistory(settingName);
    Thread.sleep(3000);
    String userName = w.getDriver().findElement(By.xpath("//td[contains(text(),'" + String.valueOf(val) + "')]/following-sibling::td[4]")).getText().trim();

    Thread.sleep(2000);
    w.click(CssLocators.CancelButton);
    return userName;

  }

  /*
   * MWTHOD TO VARIFY THE Pending status of setting val
   */
  public boolean verifyRequestedTime(String settingName, int val) throws java.text.ParseException, Exception {
    log.debug("verifyPendingValueStatus method invoked...");

    Date currentTime = new Date();
    String currentDate = dateformat.format(currentTime);
    SettingsHistory(settingName);
    Thread.sleep(3000);
    String requestedTime = w.getDriver().findElement(By.xpath("//td[contains(text(),'" + String.valueOf(val) + "')]/following-sibling::td[1]")).getText()
        .trim();
    Thread.sleep(2000);
    System.out.println("requestedTime " + requestedTime);
    w.click(CssLocators.CancelButton);
    if (requestedTime.substring(0, 11).equals(currentDate)) {
      return true;
    }
    return false;

  }

  /*
   * Method to Search Reporting Group
   * @return
   * @throws java.lang.InterruptedException
   */
  public boolean searchCompany(String name) throws InterruptedException {
    log.debug("searchReportingGroup method invoked...");
    try {
      log.debug("searchReportingGroup method invoked...");

      Thread.sleep(3000);
      w.getDriver().findElement(By.xpath("//div[@id=\"divCompanyDetails\"]//div[2]/span")).sendKeys(name);
      Thread.sleep(3000);
      w.getDriver().findElement(By.xpath("//div[@class=\"row companySelect\"]//a")).click();
      return true;
    } catch (Exception e) {
      return false;
    }

  }

  public void selectProductMasterInPackage(String productMaster) throws InterruptedException {
    w.click(CssLocators.packagesTabLinkKey);
    Thread.sleep(4000);
    w.sendKeys(CssLocators.selectProductMaster, productMaster);

  }

  /**
   * Method to Add Package
   *
   * @throws java.lang.InterruptedException
   */
  public String addPackageGroup(String packageGroupName, String version, String hwVersion) throws InterruptedException {
    log.debug("addPackage method invoked...");
    String packageGroupDesc = "firmware definition used for e2e autotest";
    Thread.sleep(4000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    w.click(CssLocators.addPackageGroupButtonKey);
    Thread.sleep(2000);
    w.sendKeys(CssLocators.addPackageGroupNameInputFieldKey, packageGroupName);
    w.sendKeys(CssLocators.addPackageGroupDescriptionFieldKey, packageGroupDesc);
    w.sendKeys(CssLocators.addPackageGroupVersionFieldKey, version);
    w.sendKeys(CssLocators.addPackageGroupHardwareVersionFieldKey, hwVersion);
    w.click(CssLocators.clickSaveButtonKey);
    return packageGroupName;
  }

  /**
   * Method to Add Package
   *
   * @throws java.lang.InterruptedException
   */
  public String addPackageGroupInvalid(String packageGroupName, String SWversion, String HWversion) throws InterruptedException {
    log.debug("addPackage method invoked...");
    Thread.sleep(2000);
    w.click(CssLocators.addPackageGroupButtonKey);
    Thread.sleep(2000);
    w.clear(CssLocators.addPackageGroupNameInputFieldKey);
    w.sendKeys(CssLocators.addPackageGroupNameInputFieldKey, packageGroupName);
    w.clear(CssLocators.addPackageGroupDescriptionFieldKey);
    w.sendKeys(CssLocators.addPackageGroupDescriptionFieldKey, "package desc");
    w.clear(CssLocators.addPackageGroupVersionFieldKey);
    w.sendKeys(CssLocators.addPackageGroupVersionFieldKey, SWversion);
    w.clear(CssLocators.addPackageGroupHardwareVersionFieldKey);
    w.sendKeys(CssLocators.addPackageGroupHardwareVersionFieldKey, HWversion);
    w.click(CssLocators.clickSaveButtonKey);
    return packageGroupName;
  }

  /**
   * Method to Add Package Details
   *
   * @throws java.lang.InterruptedException
   * @throws IOException
   * @throws ParseException
   */
  public String addDetailsJsonPackage() throws InterruptedException, ParseException, IOException {
    log.debug("addPackageDetails method invoked...");
    Thread.sleep(4000);
    String packageName = "package-" + w.randomName();
    w.sendKeys(CssLocators.addPackageGroupNameInputFieldKey, packageName);
    w.sendKeys(CssLocators.addPacakgeDetailsVersion, "2.0.0");
    w.sendKeys(CssLocators.addPackageGroupDescriptionFieldKey, packageName);
    w.click(CssLocators.savePackageGroupButtonKey);
    Thread.sleep(4000);
    return packageName;

  }

  /**
   * Method to Search Firmware
   * 
   * @param id
   * @param column
   * @return
   * @throws InterruptedException
   */
  public boolean searchPackageGroup(String packageName, String productMaster) throws InterruptedException {
    log.debug("search Package group method invoked...");
    selectProductMasterInPackage(productMaster);
    Thread.sleep(3500);
    w.clear(CssLocators.searchPackageGroupFieldKey);
    Thread.sleep(1500);
    w.sendKeys(CssLocators.searchPackageGroupFieldKey, packageName);
    Thread.sleep(3000);
    Assert.assertEquals(w.getText(CssLocators.searchPackageResultKey), packageName);
    log.debug("Search Result is Displayed");
    return true;

  }

  /**
   * Method to verify the Filter for package Name
   */
  public boolean filerPackageGroupName(String packageGroupName) throws InterruptedException {
    Thread.sleep(1500);
    w.getDriver().findElement(By.xpath("//div[@id=\"divPackageGroupDetails\"]/div[3]/div[2]//select[@id=\"packageName\"]")).sendKeys(packageGroupName);
    Thread.sleep(1500);
    String filterContent = w.getText(CssLocators.searchPackageResultKey);
    if (filterContent.equals(packageGroupName)) {
      return true;
    }
    return false;
  }

  /**
   * Method to verify the Filter for package Version
   */
  public boolean filerPackageVersion(String version) throws InterruptedException {
    Thread.sleep(1500);
    w.getDriver().findElement(By.xpath("//div[@id=\"divPackageGroupDetails\"]/div[3]/div[2]//select[@id=\"softwareVersion\"]")).sendKeys(version);
    Thread.sleep(1500);
    String filterContent = w.getText(CssLocators.getPackageVersion);
    if (filterContent.equals(version)) {
      return true;

    }
    return false;
  }

  /**
   * Method to verify the Filter for package Version
   */
  public boolean filerPackageStatus(String status) throws InterruptedException {
    w.getDriver().findElement(By.xpath("//div[@id=\"divPackageGroupDetails\"]/div[3]/div[2]//select[@id=\"packageStatus\"]")).sendKeys(status);
    Thread.sleep(1500);
    String filterContent = w.getText(CssLocators.getPackageStatus);
    if (filterContent.equals(status)) {
      return true;
    }
    return false;
  }

  /**
   * Method to upload manifestfile
   * 
   * @param id
   * @param column
   * @return
   * @throws InterruptedException
   */
  public void uploadManifestFile(String fullpath) throws InterruptedException {
    log.debug("searchFirmware method invoked...");
    w.click(CssLocators.uploadManifestButtonFieldKey);
    log.debug(fullpath);
    w.sendKeys(CssLocators.choosemanifestFileLinkKey, fullpath);
    Thread.sleep(3000);
    w.click(CssLocators.submitManifestButtonFieldKey);
    w.waitUntilCSSElementInvisiblity(CssLocators.pageLoading);

  }

  /**
   * - * Method to Verify Notification added element - * check in the first page if its not present click Next -
   * 
   * @throws Exception
   */
  public boolean verifyNotificationAdded(String element) throws Exception {
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    if (w.isTextPresent(element)) {
      log.debug(element + " is added successfully");
      return true;

    } else if (w.isEnabled(CssLocators.notificationPaginationPrevious)) {
      w.getDriver().findElement(By.xpath("//ul[@role=\"navigation\"]//li[2]/a")).click();
      if (w.isTextPresent(element)) {
        log.debug(element + " is added successfully");
        return true;
      } else {
        while ((w.isEnabled(CssLocators.notificationPaginationNext))) {
          w.click(CssLocators.notificationPaginationNext);
          if (w.isTextPresent(element)) {
            log.debug(element + " is added successfully");
            return true;
          }
        }
      }
    } else if (w.isEnabled(CssLocators.notificationPaginationNext)) {
      while ((w.isEnabled(CssLocators.notificationPaginationNext))) {
        w.click(CssLocators.notificationPaginationNext);
        Thread.sleep(3000);
        w.mouseOverOnElement(CssLocators.addNotificationsButtonKey);
        if (w.isTextPresent(element)) {
          log.debug(element + " is added successfully");
          return true;
        }
      }
    } else {
      w.getDriver().findElement(By.xpath("//ul[@role=\"navigation\"]//li[2]/a")).click();
      Assert.assertTrue(w.isTextPresent(element));
      return true;
    }
    return false;
  }

  public String addLocationManagerUserFields(String name, String description, String aPIKey, String orderId) throws InterruptedException {
    Thread.sleep(2500);
    w.mouseOverOnElement(CssLocators.userFieldLinkKey);
    w.click(CssLocators.addUserFieldButtonKey);
    w.sendKeys(CssLocators.addUserFieldNameFieldKey, name);
    w.sendKeys(CssLocators.addUserFieldDescriptionFeildKey, description);
    w.sendKeys(CssLocators.addUserFieldDataTypeFieldKey, "String");
    w.sendKeys(CssLocators.addUserFieldAPIFieldKey, aPIKey);
    w.sendKeys(CssLocators.addUserFieldOrderFieldKey, orderId);
    System.out.println("values are entered...");
    w.click(CssLocators.addUserFieldSaveButtonKey);
    Thread.sleep(2000);
    w.mouseOverOnElement(CssLocators.clickOnProductLink);
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.addUserFieldButtonKey);
    return name;

  }

  public String addNotificationLocation(String eventAction, String name) throws InterruptedException {
    Thread.sleep(3000);
    w.click(CssLocators.addNotificationsButtonKey);
    w.selectValue(CssLocators.addNotificationsEventActionKey, eventAction);
    w.sendKeys(CssLocators.addNotificationsAlertNameKey, name);
    w.sendKeys(CssLocators.addNotificationsAlertMessageKey, name);
    w.selectValue(CssLocators.addNotificationsAlertRoleLevelKey, "User");
    // w.sendKeys(CssLocators.addNotificationsAlertSeverityKey, "Medium");
    w.sendKeys(CssLocators.addNotificationsAlertUserKey, "Individual");
    w.selectValue(CssLocators.addNotificationsAlertDeliveryMethodKey, "WebHook");
    Thread.sleep(3000);
    w.selectValue(CssLocators.addNotificationsAlertSettingName, name);
    w.click(CssLocators.addNotificationsAlertSaveButtonKey);

    Thread.sleep(2000);

    return name;

  }

  /**
   * Method to Add Attributes reusable
   * 
   * @return
   * @throws java.lang.InterruptedException
   */
  public String addAttributeForDTS(String attributeName, String groupName, String dataType, String id) throws InterruptedException {
    log.debug("addAttribute method invoked...");
    w.click(CssLocators.addAttributesButtonKey);
    Thread.sleep(2000);
    w.sendKeys(CssLocators.addAttributeNameFieldKey, attributeName);
    w.sendKeys(CssLocators.addAttributeGroupName, groupName);
    w.sendKeys(CssLocators.addAttributeDescriptionFieldKey, attributeName + " Object");
    w.selectValue(CssLocators.addAttributeDataTypeFieldKey, dataType);
    w.clear(CssLocators.addAttributeLocalIDFieldKey);
    w.sendKeys(CssLocators.addAttributeLocalIDFieldKey, id);
    w.click(CssLocators.clickSaveButtonKey);
    return attributeName;
  }

  /*
   * METHOD TO SET THE VALUES TO SETTING FROM PORTAL
   */
  public void sendEphemerisValuesUsingPortal(String groupName, String settingName, int val) throws java.text.ParseException, Exception {
    log.debug("sendSettingValuesFromPortal method invoked...");
    w.waitUntilCSSElementVisible(CssLocators.productSettingLinkKey);
    w.click(CssLocators.productSettingLinkKey);
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//label[contains(text(),'" + groupName + "')]")).click();
    Thread.sleep(3000);

    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/input")).click();
    Thread.sleep(3000);
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/input")).clear();
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/input")).sendKeys(String.valueOf(val));
    w.getDriver().findElement(By.xpath("//td[text()='" + settingName + "']/following-sibling::td/a[@id=\"btnSaveSetting\"]")).click();
    Thread.sleep(3000);

  }
}
