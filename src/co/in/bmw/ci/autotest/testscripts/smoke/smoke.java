package co.in.bmw.ci.autotest.testscripts.smoke;

import java.awt.AWTException;
import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import baseClass.ReadExcel;
import bmwModules.AllModels;
import bmwModules.Feedback;
import bmwModules.HomePage;
import bmwModules.Login;
import bmwModules.MyConfiguration;
import bmwModules.Myprofilebase;
import bmwModules.Payment;
import co.in.bmw.ci.autotest.utils.AppWebInterfaceWrapper;
import co.in.bmw.ci.autotest.utils.Constants;
import co.in.bmw.ci.autotest.utils.CssLocators;
import co.in.bmw.ci.autotest.utils.DriverInstance;
import co.in.bmw.ci.autotest.utils.ExtentManager;
import co.in.bmw.ci.autotest.utils.NorthBoundInterface;
import co.in.bmw.ci.autotest.utils.TestNgParameters;
import co.in.bmw.ci.autotest.utils.WebInterfaceWrapper;

public class smoke {

	private AppWebInterfaceWrapper aw;
	private WebInterfaceWrapper w;
	private static final Logger log = Logger.getLogger(smoke.class.getName());
	private WebDriver driver;
	private DriverInstance driverInstance;
	private TestNgParameters testNg;
	ExtentTest logger;
	ExtentReports extent;
	private String productLineName;
	private String dynamicEPName;
	String token;
	private NorthBoundInterface nbi;
	public static String cuid;
	public static String tenantName;
	private String companyUrl, logoUrl;
	private String userEmail;
	private String authenticationType;

	// LoginAs Administrator

	@BeforeClass
	public void setUp() throws Exception {
		extent = ExtentManager.getInstance();
		testNg = new TestNgParameters();
		driverInstance = new DriverInstance();
		driver = driverInstance.openBrowser(testNg.getBrowser());
		w = new WebInterfaceWrapper(driver);
		aw = new AppWebInterfaceWrapper(driver);
		cuid = Constants.COMPANY_CUID;
		tenantName = Constants.COMPANY_NAME;
		companyUrl = "www.smartnexus.io";
		logoUrl = "https://devcloudcoreuse001st.blob.core.windows.net/images/SmartNexus%20by%20Flex%20-%20PNG.png";
		userEmail = testNg.getUserName();
		nbi = new NorthBoundInterface();
		authenticationType = testNg.getAuthenticationType();
		nbi = new NorthBoundInterface();
		aw.login(authenticationType, testNg.getUserName(), testNg.getPassword());
		Thread.sleep(1000);
		//w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
		//w.click(CssLocators.manageTenants);
		// w.clickOnLink("Manage Tenants");
	}


	/*
	 * VERFIY WHETHER THE GO BUTTON IS ENABLE OR NOT TEST
	 * 
	 * @verify the Enability of Go button before entering any input
	 * 
	 * @verify the Enability of Go button after entering the Search Value
	 */

	@Test(priority = 1, enabled = true)
	public void testcase_001() throws IOException, InterruptedException {

		// public static void main (String args[]) throws IOException,
		// InterruptedException{

		// ExtentHtmlReporter htmlReporter = new
		// ExtentHtmlReporter("./Report/BMW_TS_002.html");
		// ExtentReports extent = new ExtentReports();
		// extent.attachReporter(htmlReporter);
		logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName())
				.assignCategory(this.getClass().getSimpleName());
		String retMessage;
		int scrnCnt = 0;

		ReadExcel rdxlTab = new ReadExcel();
		List<List<String>> listConf = rdxlTab.ReadExcelTab("BMW Design v0.3.xlsx", "Smoke_test_01", 5);

		AllModels AM = new AllModels();
		Login LG = new Login();
		MyConfiguration MY = new MyConfiguration();

		Payment py = new Payment();
		Myprofilebase Mb = new Myprofilebase();
		HomePage HP = new HomePage();
		Feedback FB = new Feedback();
		Myprofilebase Myp = new Myprofilebase();
		int index = 0;

		for (List<String> strTemp : listConf) {
			if (strTemp.get(0).equalsIgnoreCase("Yes")) {
				// ExtentTest logger =
				// extent.createTest("SmokeTest_"+strTemp.get(9));

				logger.log(LogStatus.INFO, "Launching the browser and site");
				retMessage = LG.BrowserLaunch(strTemp);
				scrnCnt = scrnCnt + 1;
				if (retMessage.equals("Launched Browser")) {
					logger.log(LogStatus.PASS, "Launching the browser and site sucessfully ");
				} else {
					logger.log(LogStatus.FAIL, "Unable to launch the browser and site");
				}

				if (retMessage.equals("Launched Browser")) {
					logger.log(LogStatus.INFO, "Home page: Validating headers");
					retMessage = HP.verifyHeaders_GU();
					scrnCnt = scrnCnt + 1;

					if (retMessage.equals("Home page: Sucessfully navigated to expected BMW home site")) {
						logger.log(LogStatus.PASS, retMessage);
					} else {
						logger.log(LogStatus.FAIL, retMessage);
					}
					if (retMessage.contains("Sucessfully navigated to expected BMW home site")) {
						logger.log(LogStatus.INFO, "Home page: Validating carousel banner images");
						retMessage = HP.clickCarouselImages();
						scrnCnt = scrnCnt + 1;

						if (retMessage.equals("Home page: Sucessfully clicked all carousel image banners")) {
							logger.log(LogStatus.PASS, retMessage);
						} else {
							logger.log(LogStatus.FAIL, retMessage);
						}

						if (retMessage.contains("Sucessfully clicked all carousel image banners")) {
							logger.log(LogStatus.INFO, "Home page: Clicking Login button");
							retMessage = LG.BMWLogin(strTemp);
							scrnCnt = scrnCnt + 1;

							if (retMessage.contains("Successfuly login to the system.")) {
								logger.log(LogStatus.PASS, retMessage);
							} else {
								logger.log(LogStatus.FAIL, retMessage);
							}

							if (retMessage.startsWith("Successfuly login to the system")) {
								retMessage = AM.carSelection(strTemp);
								scrnCnt = scrnCnt + 1;

								logger.log(LogStatus.INFO, "Selected a car series ");
								logger.log(LogStatus.INFO, "Selected the fuel type and variants");
								logger.log(LogStatus.INFO, "Selected the Exterior and Interior:" + retMessage);
								if (retMessage.contains("Sucessfully selected the Interior colour")) {
									logger.log(LogStatus.PASS, retMessage);
								} else {
									logger.log(LogStatus.FAIL, retMessage);
								}

								if (retMessage.contains("Sucessfully selected the Interior colour")) {
									logger.log(LogStatus.INFO, "Vaidate Zoom functionality: ");
									retMessage = AM.clickZoomImage(strTemp);
									scrnCnt = scrnCnt + 1;

									if (retMessage.equals("Zoom Window: Clicked on each images")) {
										logger.log(LogStatus.PASS, retMessage);
									} else {
										logger.log(LogStatus.FAIL, retMessage);
									}
									if (retMessage.equals("Zoom Window: Clicked on each images")) {
										logger.log(LogStatus.INFO, "Validate Calculate EMI:");
										retMessage = AM.calculateEMI();
										scrnCnt = scrnCnt + 1;

										if (retMessage
												.equals("Calculate EMI: Navigated to expected EMI calculator url")) {
											logger.log(LogStatus.PASS, retMessage);
										} else {
											logger.log(LogStatus.FAIL, retMessage);
										}

										if (retMessage
												.equals("Calculate EMI: Navigated to expected EMI calculator url")) {
											logger.log(LogStatus.INFO, "Validate Check availability:");
											retMessage = AM.checkavailablity(strTemp);
											scrnCnt = scrnCnt + 1;
											if (retMessage.contains("Clicking Confirm button in Variants page")) {
												logger.log(LogStatus.PASS, "Check availability:" + retMessage);
											} else {
												logger.log(LogStatus.FAIL, "Check availability:" + retMessage);
											}

											if (retMessage.contains("Clicking Confirm button in Variants page")) {
												logger.log(LogStatus.INFO, "Validate Download Summary:");
												retMessage = MY.downloadSummary();
												scrnCnt = scrnCnt + 1;

												if (retMessage.contains("Sucessfully download the summary")) {
													logger.log(LogStatus.PASS, retMessage);
												} else {
													logger.log(LogStatus.FAIL, retMessage);
												}
												if (retMessage.contains("Sucessfully download the summary")) {
													logger.log(LogStatus.INFO, "Validate Change Config:");
													retMessage = MY.changeConfig();
													scrnCnt = scrnCnt + 1;

													if (retMessage.equals(
															"Change Config: Navigated to expected change configuration url")) {
														logger.log(LogStatus.PASS, retMessage);
													} else {
														logger.log(LogStatus.FAIL, retMessage);
													}

													if (retMessage.contains(
															"Navigated to expected change configuration url")) {
														logger.log(LogStatus.INFO,
																"Validate calculate EMI in My Config:");
														retMessage = MY.calculateEMIMyConfig();
														scrnCnt = scrnCnt + 1;

														if (retMessage.equals(
																"Calculate EMI: Navigated to expected EMI calculator url")) {
															logger.log(LogStatus.PASS, "My Config:" + retMessage);
														} else {
															logger.log(LogStatus.FAIL, "My Config:" + retMessage);
														}
														if (retMessage
																.contains("Navigated to expected EMI calculator url")) {
															// logger.log(Status.INFO,"Validating
															// Remove
															// configuration in
															// My Config:");
															// retMessage =
															// AM.removeConfig();
															// scrnCnt = scrnCnt
															// + 1;
															//
															// if(retMessage.equals("Remove
															// Config:
															// Sucessfully
															// removed the
															// configuration"))
															// {
															// logger.log(Status.PASS,"My
															// Config:"
															// +retMessage);
															// }
															// else
															// {
															// logger.log(Status.FAIL,"My
															// Config:"
															// +retMessage);
															// }
															//
															// if
															// (retMessage.contains("Sucessfully
															// removed the
															// configuration"))
															// {
															logger.log(LogStatus.INFO, "Validating  Book Test Drive:");
															retMessage = MY.bookTestDrive();
															scrnCnt = scrnCnt + 1;

															if (retMessage.startsWith(
																	"Book Test Drive: Sucessfully Booked and the message captured is")) {
																logger.log(LogStatus.PASS, retMessage);
															} else {
																logger.log(LogStatus.FAIL, retMessage);
															}

															if (retMessage.startsWith(
																	"Book Test Drive: Sucessfully Booked and the message captured is")) {
																logger.log(LogStatus.INFO,
																		"Validating Dealer Selection");
																retMessage = MY.selectDealer(strTemp);
																scrnCnt = scrnCnt + 1;

																if (retMessage.contains("Dealer Selected")) {
																	logger.log(LogStatus.PASS, retMessage);
																} else {
																	logger.log(LogStatus.FAIL, retMessage);
																}

																if (retMessage.contains("Dealer Selected")) {
																	logger.log(LogStatus.INFO,
																			"Validating Request a Quote:");

																	retMessage = MY.RequestaQuote();
																	scrnCnt = scrnCnt + 1;

																	if (retMessage.contains(
																			"Sucessfully Booked a request a quote and the message captured is -")) {
																		logger.log(LogStatus.PASS, retMessage);
																	} else {
																		logger.log(LogStatus.FAIL, retMessage);
																	}

																}

																if (strTemp.get(26).equalsIgnoreCase("Yes")) {
																	logger.log(LogStatus.INFO,
																			"Moving to Start payment:");
																	retMessage = py.MakePayment(strTemp);
																	scrnCnt = scrnCnt + 1;

																	if (retMessage == "Payment Completed") {
																		logger.log(LogStatus.PASS,
																				"Payment completed : " + retMessage);
																	} else {
																		logger.log(LogStatus.FAIL,
																				"Payment not completd : " + retMessage);
																	}
																	if (retMessage.equals("Payment Completed")) {
																		logger.log(LogStatus.INFO,
																				"Validating view BMW");
																		retMessage = AM.viewBmw();
																		scrnCnt = scrnCnt + 1;

																		if (retMessage == "View BMW:Sucessfully clicked the view BMW button") {
																			logger.log(LogStatus.PASS, retMessage);
																		} else {
																			logger.log(LogStatus.FAIL, retMessage);
																		}
																		if (retMessage.contains(
																				"Sucessfully clicked the view BMW button")) {
																			logger.log(LogStatus.INFO,
																					"Validating My purchase");
																			// retMessage
																			// =
																			// mb.MyPurchaseVerifyVariantBookingIDAndDealer(strTemp);
																			scrnCnt = scrnCnt + 1;

																			if (retMessage == "My purchases: Carbooking status displayed") {
																				logger.log(LogStatus.PASS, retMessage);
																			} else {
																				logger.log(LogStatus.FAIL, retMessage);
																			}

																		}
																	}

																}
																switch (strTemp.get(9)) {

																case "IN":

																	Myp.MyprofileEditIN(strTemp);
																	break;
																case "TH":

																	Myp.MyprofileEditTH(strTemp);
																	break;

																default:

																	System.out.println("No data");

																	// Statements
																}

																if (strTemp.get(62).equalsIgnoreCase("Y")) {
																	FB.inputFeedback(strTemp);
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}

				}
			}

			listConf.set(index, strTemp);
			index = index + 1;
			extent.flush();

		}

		//String xx = rdxlTab.WriteExcelTab("BMW Design v0.3.xlsx", "Smoke_test_01", listConf, 5, 66);

		System.out.println("listConf :" + listConf);

		LG.BMWLogout();

		AM.tearDown();
		// loginModule.tearDown();
		// */

		System.out.println("Execution Complete");
	}

	/*
	 * VERFIFY FOR ANY DELETE, IT DISPLAYS POPUP TEST
	 * 
	 * @Step Search ProductLine and Select
	 * 
	 * @Step Click on delete for Product Line
	 * 
	 * @Verify Pop up is displayed for the Delete
	 */
	@Test(priority = 426, enabled = false)
	public void verifyForDeletePopupDisplyed() {
		log.debug("Executing verifyForDeletePopupDisplyed  test");
		logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName())
				.assignCategory(this.getClass().getSimpleName());
		w.click(CssLocators.deleteProductLineButtonKey);
		Assert.assertTrue(w.isDisplayed(CssLocators.deletePopUp));
		w.click(CssLocators.CancelButton);
		logger.log(LogStatus.PASS, "Verify Delete Pop up Test Completed");
		log.debug("Verify Delete Pop up Displayed Test Completed");
	}

	/*
	 * VERIFY FORMAT FOR MFGID TEST
	 * 
	 * @Step Select the Product line
	 * 
	 * @Step Click on Configurations and Click on add button for Dynamic Ep
	 * 
	 * @Step Fill the Dynamic EP name anf For MFG id fill the Space and click on
	 * submit
	 * 
	 * @Verify the error Message Displayed
	 * 
	 * @step Enter the Special charactes as the MFG ID and check for the Error
	 * Message
	 * 
	 * @step Clear and Enter the MFG id with Numeric and Character combination
	 * and Submit
	 */

	@Test(priority = 427, enabled = false)
	public void verifyDynamicEPMFGidFormat() throws Exception {
		log.debug("Executing verifyDynamicEPMFGidFormat  test");
		logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
		aw.VerifyConfigureDynamicEp(productLineName, dynamicEPName);
		logger.log(LogStatus.PASS, "Verify verifyDynamicEPMFGidFormat Test Completed");
		log.debug("Verify DverifyDynamicEPMFGidFormat Test Completed");

	}

	/*
	 * @AfterMethod public void writeResult(ITestResult result) { String
	 * methodName = result.getMethod().getMethodName(); try { if
	 * (result.getStatus() == ITestResult.SUCCESS) {
	 * nbi.changeJiraStatus(methodName, "Pass"); } else if (result.getStatus()
	 * == ITestResult.FAILURE) { nbi.changeJiraStatus(methodName, "Fail");
	 * w.captureScreenShot(logger, result.getThrowable(), result.getName()); }
	 * else if (result.getStatus() == ITestResult.SKIP) {
	 * nbi.changeJiraStatus(methodName, "Unknown"); } } catch (Exception e) {
	 * System.out.println("@AfterMethod: Exception caught");
	 * e.printStackTrace(); } extent.endTest(logger); }
	 */

	/*
	 * @AfterClass public void tearDown() throws Exception { aw.logOut();
	 * w.getDriver().quit(); log.debug("LookAndFeel Test completed"); }
	 */

}
