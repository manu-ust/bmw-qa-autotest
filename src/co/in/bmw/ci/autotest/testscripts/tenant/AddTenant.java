package co.in.bmw.ci.autotest.testscripts.tenant;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import co.in.bmw.ci.autotest.utils.AppWebInterfaceWrapper;
import co.in.bmw.ci.autotest.utils.Constants;
import co.in.bmw.ci.autotest.utils.CssLocators;
import co.in.bmw.ci.autotest.utils.DriverInstance;
import co.in.bmw.ci.autotest.utils.ExtentManager;
import co.in.bmw.ci.autotest.utils.NorthBoundInterface;
import co.in.bmw.ci.autotest.utils.TestNgParameters;
import co.in.bmw.ci.autotest.utils.WebInterfaceWrapper;

public class AddTenant {

  private AppWebInterfaceWrapper aw;
  private WebInterfaceWrapper w;
  private static final Logger log = Logger.getLogger(AddTenant.class.getName());
  public static WebDriver driver;
  private DriverInstance driverInstance;
  private TestNgParameters testNg;
  ExtentTest logger;
  ExtentReports extent;
  public static String cuid;
  public static String tenantName;
  private String companyUrl, logoUrl;
  private String userEmail;
  private NorthBoundInterface nbi;
  private String authenticationType;

  // LoginAs Administrator
  @BeforeClass

  public void setUp() throws Exception {
    extent = ExtentManager.getInstance();
    testNg = new TestNgParameters();
    driverInstance = new DriverInstance();
    driver = driverInstance.openBrowser(testNg.getBrowser());
    w = new WebInterfaceWrapper(driver);
    aw = new AppWebInterfaceWrapper(driver);
    cuid = Constants.COMPANY_CUID;
    tenantName = Constants.COMPANY_NAME;
    companyUrl = "www.smartnexus.io";
    logoUrl = "https://devcloudcoreuse001st.blob.core.windows.net/images/SmartNexus%20by%20Flex%20-%20PNG.png";
    userEmail = testNg.getUserName();
    nbi = new NorthBoundInterface();
    authenticationType = testNg.getAuthenticationType();
    nbi = new NorthBoundInterface();
    aw.login(authenticationType, testNg.getUserName(), testNg.getPassword());
    Thread.sleep(1000);
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    w.click(CssLocators.manageTenants);
    // w.clickOnLink("Manage Tenants");
  }

  /*
   * ADD COMPANY TEST
   * @Step Login to web portal as Flex Admin user
   * @Step Add Company by providing name, CUID, description, URL and email
   * @Step Search and verify created Company exist= true)
   */

  @Test(priority = 1, enabled = true)
  public void TEST_86_addTenant() throws IOException, InterruptedException {
    log.debug("Executing addCompany test");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    cuid = aw.createTenant(cuid, tenantName, companyUrl, logoUrl, userEmail, authenticationType);
    // Assert.assertEquals("Company Saved Successfully...", w.getText(CssLocators.successMessage));
    Assert.assertEquals("Allow 5-10 minutes to let information from this tenant to propagate before adding anything.", w.getText(CssLocators.successMessage));
    Thread.sleep(3000);
    w.mouseOverOnElement(CssLocators.sideNavigationOptionKey);
    logger.log(LogStatus.PASS, "addCompany test Completed");
    log.debug("addCompany test Completed");

  }

  /*
   * ADD COMPANY WITH INVALID CUID TEST
   * @Step Login to web portal as Flex Admin user
   * @Step Add Company by invalid CUID
   * @Step verify appropriate error message on portal UI
   */

  @Test(priority = 2, enabled = true)
  public void TEST_185_addCompanyInvalidCUID() throws IOException, InterruptedException {
    log.debug("Executing addCompanyInvalidCUID test");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    Thread.sleep(2000);
    aw.createTenant("ABC-01", tenantName, companyUrl, companyUrl, userEmail, authenticationType);
    Assert.assertEquals(w.getText(CssLocators.messageDisplayed), "CUID should contain alphanumeric only.");
    ;
    w.click(CssLocators.CancelButton);
    logger.log(LogStatus.PASS, "addCompanyInvalidCUID test Completed");
    log.debug("addCompanyInvalidCUID test Completed");

  }

  /*
   * ADD COMPANY WITH INVALID URL TEST
   * @Step Login to web portal as Flex Admin user
   * @Step Add Company by invalid URL
   * @Step verify appropriate error message on portal UI
   */

  @Test(priority = 3, enabled = true)
  public void TEST_186_addCompanyInvalidUrl() throws IOException, InterruptedException {
    log.debug("Executing addCompanyInvalidUrl test");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    aw.createTenant(cuid, tenantName, "flex", logoUrl, userEmail, authenticationType);
    Assert.assertEquals(w.getText(CssLocators.messageDisplayed), "Invalid url.");
    w.click(CssLocators.CancelButton);
    logger.log(LogStatus.PASS, "addCompanyInvalidUrl test Completed");
    log.debug("addCompanyInvalidUrl test Completed");

  }

  /*
   * ADD COMPANY WITH INVALID LogoURL TEST
   * @Step Login to web portal as Flex Admin user
   * @Step Add Company by invalid URL
   * @Step verify appropriate error message on portal UI
   */

  @Test(priority = 4, enabled = true)
  public void TEST_188_addCompanyInvalidLogoUrl() throws IOException, InterruptedException {
    log.debug("Executing addCompanyInvalidLogoUrl test");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    aw.createTenant(cuid, tenantName, companyUrl, "flex", userEmail, authenticationType);
    Assert.assertEquals(w.getText(CssLocators.messageDisplayed), "Invalid logoUrl.");
    w.click(CssLocators.CancelButton);
    logger.log(LogStatus.PASS, "addCompanyInvalidLogoUrl test Completed");
    log.debug("addCompanyInvalidLogoUrl test Completed");

  }

  /*
   * ADD COMPANY WITH INVALID EMAIL TEST
   * @Step Login to web portal as Flex Admin user
   * @Step Add Company by invalid EMAIL
   * @Step verify appropriate error message on portal UI
   */

  @Test(priority = 5, enabled = true)
  public void TEST_187_addCompanyInvalidEmail() throws IOException, InterruptedException {
    log.debug("Executing addCompanyInvalidEmail test");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    aw.createTenant(cuid, tenantName, companyUrl, logoUrl, "abc", authenticationType);
    // Assert.assertFalse(w.isDisplayed(CssLocators.flexAdminCreateCompanyKey));
    w.click(CssLocators.CancelButton);
    logger.log(LogStatus.PASS, "addCompanyInvalidEmail test Completed");
    log.debug("addCompanyInvaliautotestpoijpdEmail test Completed");

  }

  /*
   * UPDATE COMPANY TEST
   * @Step Login to web portal as Flex Admin user
   * @Step Update Company of tenant
   * @Step Verify Company updated on portal UI
   */

  @Test(priority = 6, enabled = true)
  public void TEST_87_UpdateCompany() throws IOException, InterruptedException {
    log.debug("Executing UpdateCompany test");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    tenantName = aw.updateCompany(tenantName, companyUrl, userEmail);
    Assert.assertEquals(w.getText(CssLocators.messageDisplayed), "Tenant updated successfully");
    logger.log(LogStatus.PASS, "UpdateCompany test Completed");
    log.debug("UpdateCompany test Completed");

  }

  @AfterMethod
  public void writeResult(ITestResult result) {
    String methodName = result.getMethod().getMethodName();
    try {
      if (result.getStatus() == ITestResult.SUCCESS) {
        nbi.changeJiraStatus(methodName, "Pass");
      } else if (result.getStatus() == ITestResult.FAILURE) {
        nbi.changeJiraStatus(methodName, "Fail");
        w.captureScreenShot(logger, result.getThrowable(), result.getName());
      } else if (result.getStatus() == ITestResult.SKIP) {
        nbi.changeJiraStatus(methodName, "Unknown");
      }
    } catch (Exception e) {
      System.out.println("@AfterMethod: Exception caught");
      e.printStackTrace();
    }
    extent.endTest(logger);
  }

  @AfterClass
  public void tearDown() {
    try {
      aw.logOut();
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } finally {
      w.getDriver().quit();
    }
    log.debug("Add Company Test Suite Completed");
  }

}
