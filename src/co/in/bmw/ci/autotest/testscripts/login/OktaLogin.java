package co.in.bmw.ci.autotest.testscripts.login;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import co.in.bmw.ci.autotest.testscripts.tenant.AddTenant;
import co.in.bmw.ci.autotest.utils.AppWebInterfaceWrapper;
import co.in.bmw.ci.autotest.utils.DriverInstance;
import co.in.bmw.ci.autotest.utils.ExtentManager;
import co.in.bmw.ci.autotest.utils.NorthBoundInterface;
import co.in.bmw.ci.autotest.utils.TestNgParameters;
import co.in.bmw.ci.autotest.utils.WebInterfaceWrapper;

public class OktaLogin {
	 private AppWebInterfaceWrapper aw;
	  private WebInterfaceWrapper w;
	  private static final Logger log = Logger.getLogger(OktaLogin.class.getName());
	  private WebDriver driver;
	  private DriverInstance driverInstance;
	  private TestNgParameters testNg;
	  private AddTenant ac;
	  ExtentTest logger;
	  ExtentReports extent;
	  private NorthBoundInterface nbi;
	  private String path;
	  String userName;
	  String passWord;
	  

	  // LoginAs Administrator
	  @BeforeClass
	  public void setUp() throws Exception {
	    extent = ExtentManager.getInstance();
	    testNg = new TestNgParameters();
	    driverInstance = new DriverInstance();
	    driver = driverInstance.openBrowser(testNg.getBrowser());
	    w = new WebInterfaceWrapper(driver);
	    aw = new AppWebInterfaceWrapper(driver);
	    nbi = new NorthBoundInterface();
	    userName = testNg.getUserName();
	    passWord = testNg.getPassword();
	  
	   
	  
	  }
	
	  
	  /*
	   * Azure Login TEST
	   */
	  @Test(priority = 1, enabled = true)
	  public void oktaLogin() throws Exception {
	    log.debug("Executing oktaLogin test");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    aw.loginAsOkta(userName, passWord);
	    logger.log(LogStatus.PASS, "oktaLogin test Completed");
	    log.debug("oktaLogin test Completed");

	  }
	

	

}

