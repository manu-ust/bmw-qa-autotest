package co.in.bmw.ci.autotest.testscripts.login;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import co.in.bmw.ci.autotest.testscripts.tenant.AddTenant;
import co.in.bmw.ci.autotest.utils.AppWebInterfaceWrapper;
import co.in.bmw.ci.autotest.utils.CssLocators;
import co.in.bmw.ci.autotest.utils.DriverInstance;
import co.in.bmw.ci.autotest.utils.ExtentManager;
import co.in.bmw.ci.autotest.utils.NorthBoundInterface;
import co.in.bmw.ci.autotest.utils.TestNgParameters;
import co.in.bmw.ci.autotest.utils.WebInterfaceWrapper;

public class LoginTestAzure {

	  private AppWebInterfaceWrapper aw;
	  private WebInterfaceWrapper w;
	  private static final Logger log = Logger.getLogger(LoginTestAzure.class.getName());
	  private WebDriver driver;
	  private DriverInstance driverInstance;
	  private TestNgParameters testNg;
	  ExtentTest logger;
	  ExtentReports extent;
	  String productLineName;
	  private AddTenant ac;
	  String token = null;
	  String wrongUsername;
	  String wrongPassword;
	  String companyName;

	  private NorthBoundInterface nbi;

	  // LoginAs Administrator
	  @BeforeClass
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void setUp(String authenticationType, String systemUserName, String systemUserPassword, String tenantName) throws Exception {
	    extent = ExtentManager.getInstance();
	    testNg = new TestNgParameters();
	    driverInstance = new DriverInstance();
	    driver = driverInstance.openBrowser(testNg.getBrowser());
	    w = new WebInterfaceWrapper(driver);
	    aw = new AppWebInterfaceWrapper(driver);
	    nbi = new NorthBoundInterface();
	    wrongUsername = "dummyTestUser@Flexiot.onmicrosoft.com";
	    wrongPassword = "wrongpassword";
	  
	    
	  }

	  /*
	   * Login To Portal TEST
	   * 
	   * @Step Login to web portal
	   * @Step Verify successful login
	   */
	  @Test(priority = 418, enabled = true)
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void loginToPortal(String authenticationType, String systemUserName, String systemUserPassword, String tenantName) throws Exception {
	    log.debug("loginToPortal test Started");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    aw.login(authenticationType, systemUserName, systemUserPassword);
	    aw.selectCompany(tenantName);
	    //Assert.assertNotNull(token, "Not null");
	    logger.log(LogStatus.PASS, "loginToPortal test Completed");
	    log.debug("loginToPortal test Completed");

	  }

	  /*
	   * USER MULTI TENANT LOGIN TO PORTAL
	   * 
	   * @Step Login to portal
	   * @Step Select one Tenent
	   * @Step Change the tenent
	   * @verify the successful login
	   */
	  @Test(priority = 419, enabled = true)
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void multiTenentloginToPortal(String tenantName) throws IOException, InterruptedException, java.text.ParseException {
	    log.debug("multiTenentloginToPortal test Started");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    Assert.assertTrue(aw.changeCompany(tenantName));
	    logger.log(LogStatus.PASS, "multiTenentloginToPortal test Completed");
	    log.debug("multiTenentloginToPortal test Completed");

	  }

	  /*
	   * LOGOUT FROM PORTAL TEST
	   * 
	   * @Step Login to web portal
	   * @Step Logout from the portal
	   * @Step Verify successful logut
	   */
	  @Test(priority =420, enabled = true)
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void logoutFromPortal(String systemUserName) throws Exception {
	    log.debug("logoutFromPortal test Started");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    aw.logOut();
	    Thread.sleep(3000);
	    String userName=systemUserName.toLowerCase();
	    w.getDriver().findElement(By.xpath("//a[@data-upn='"+userName+"']")).click();;
	    Assert.assertTrue(w.isDisplayed(CssLocators.azureLoginLinkKey));
	    logger.log(LogStatus.PASS, "logoutFromPortal test Completed");
	    log.debug("logoutFromPortal test Completed");

	  }

	  /*
	   * INVALIED USERNAME LOGIN TO PORTAL TEST
	   * 
	   * @Step Login to web portal using invalid username
	   * @Step Verify the error message
	   */

	  @Test(priority = 421, enabled = true)
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void invalidUsernameloginToPortal(String systemUserPassword) throws IOException, InterruptedException, java.text.ParseException {
	    log.debug("invalidUsernameloginToPortal test Started");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    w.click(CssLocators.azureLoginLinkKey);
	    aw.logInAzureWithWrongCredentials(wrongUsername, systemUserPassword);	 
	    Assert.assertEquals(w.getText(CssLocators.loginFailMessageAzure),"We don't recognize this user ID or password");
	    logger.log(LogStatus.PASS, "invalidUsernameloginToPortal test Completed");
	    log.debug("invalidUsernameloginToPortal test Completed");

	  }

	  /*
	   * INVALIED PASSWORD LOGIN TO PORTAL TEST
	   * 
	   * @Step Login to web portal using invalid password
	   * 
	   * @Step Verify the error message
	   * 
	   */

	  @Test(priority = 422, enabled = true)
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void invalidPasswordloginToPortal(String systemUserName) throws IOException, InterruptedException, java.text.ParseException {
	    log.debug("invalidPasswordloginToPortal test Started");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    aw.logInAzureWithWrongCredentials(systemUserName, wrongPassword);
	    
	    Assert.assertEquals(w.getText(CssLocators.loginFailMessageAzure),"We don't recognize this user ID or password");
	    logger.log(LogStatus.PASS, "invalidPasswordloginToPortal test Completed");
	    log.debug("invalidPasswordloginToPortal test Completed");

	  }

	  /*
	   * FORGOT PASSWORD ON LOGIN PAGE TEST
	   * 
	   * @Step Goto Login page
	   * @Step Goto Forgot Password
	   * @Step Enter the email. click send button
	   * @Step Verify the success message for email sent
	   * 
	   */
	  @Test(priority = 423, enabled = true)
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void forgotPasswordOnLoginPage(String systemUserName) throws Exception {
	    log.debug("forgotPasswordOnLoginPage test Started");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    aw.forgotPasswordAzure(systemUserName);	   
	    logger.log(LogStatus.PASS, "forgotPasswordOnLoginPage test Completed");
	    log.debug("forgotPasswordOnLoginPage test Completed");

	  }

	  

	  @AfterMethod
	  public void writeResult(ITestResult result) {
	    String methodName = result.getMethod().getMethodName();
	    try {
	      if (result.getStatus() == ITestResult.SUCCESS) {
	        nbi.changeJiraStatus(methodName, "Pass");
	      } else if (result.getStatus() == ITestResult.FAILURE) {
	        nbi.changeJiraStatus(methodName, "Fail");
	        w.captureScreenShot(logger, result.getThrowable(), result.getName());
	      } else if (result.getStatus() == ITestResult.SKIP) {
	        nbi.changeJiraStatus(methodName, "Unknown");
	      }
	    } catch (Exception e) {
	      System.out.println("@AfterMethod: Exception caught");
	      e.printStackTrace();
	    }
	    extent.endTest(logger);
	  }

	  @AfterClass
	  public void tearDown() throws Exception {
	  //  aw.logOut();
	    w.getDriver().quit();
	    log.debug("LoginTest suite completed");
	  }


}
