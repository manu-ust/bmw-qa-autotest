package co.in.bmw.ci.autotest.testscripts.login;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import co.in.bmw.ci.autotest.testscripts.tenant.AddTenant;
import co.in.bmw.ci.autotest.utils.AppWebInterfaceWrapper;
import co.in.bmw.ci.autotest.utils.CssLocators;
import co.in.bmw.ci.autotest.utils.DriverInstance;
import co.in.bmw.ci.autotest.utils.ExtentManager;
import co.in.bmw.ci.autotest.utils.NorthBoundInterface;
import co.in.bmw.ci.autotest.utils.TestNgParameters;
import co.in.bmw.ci.autotest.utils.WebInterfaceWrapper;

public class LoginTest {

  private AppWebInterfaceWrapper aw;
  private WebInterfaceWrapper w;
  private static final Logger log = Logger.getLogger(LoginTest.class.getName());
  private WebDriver driver;
  private DriverInstance driverInstance;
  private TestNgParameters testNg;
  ExtentTest logger;
  ExtentReports extent;
  String productLineName;
  private AddTenant ac;
  String token = null;
  String wrongUsername;
  String wrongPassword;
  String companyName;
  private NorthBoundInterface nbi;

  // LoginAs Administrator
  @BeforeClass
  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
  public void setUp(String authenticationType, String systemUserName, String systemUserPassword, String tenantName) throws Exception {
    extent = ExtentManager.getInstance();
    testNg = new TestNgParameters();
    driverInstance = new DriverInstance();
    driver = driverInstance.openBrowser(testNg.getBrowser());
    w = new WebInterfaceWrapper(driver);
    aw = new AppWebInterfaceWrapper(driver);
    nbi = new NorthBoundInterface();
    wrongUsername = "dummyTestUser@abc.com";
    wrongPassword = "wrongpassword";
    
    
  }

  /*
   * Login To Portal TEST
   * 
   * @Step Login to web portal
   * @Step Verify successful login
   */
  
  @Test(priority = 412, enabled = true)
  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
  public void TEST_215_loginToPortal(String authenticationType, String systemUserName, String systemUserPassword, String tenantName) throws Exception {
    log.debug("loginToPortal test Started");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    aw.login(authenticationType, systemUserName, systemUserPassword);
    aw.selectCompany(tenantName);
    //Assert.assertNotNull(token, "Not null");
    logger.log(LogStatus.PASS, "loginToPortal test Completed");
    log.debug("loginToPortal test Completed");

  }

  /*
   * USER MULTI TENANT LOGIN TO PORTAL
   * 
   * @Step Login to portal
   * @Step Select one Tenent
   * @Step Change the tenent
   * @verify the successful login
   */
  @Test(priority = 413, enabled =false)
  public void TEST_216_multiTenentloginToPortal() throws IOException, InterruptedException, java.text.ParseException {
    log.debug("multiTenentloginToPortal test Started");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    Assert.assertTrue(aw.changeCompany(companyName));
    logger.log(LogStatus.PASS, "multiTenentloginToPortal test Completed");
    log.debug("multiTenentloginToPortal test Completed");

  }

  /*
   * LOGOUT FROM PORTAL TEST
   * 
   * @Step Login to web portal
   * @Step Logout from the portal
   * @Step Verify successful logut
   */
  @Test(priority = 413, enabled = true)
  public void TEST_217_logoutFromPortal() throws Exception {
    log.debug("logoutFromPortal test Started");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    aw.logOut();
    Assert.assertTrue(w.isDisplayed(CssLocators.loginLinkKey));
    logger.log(LogStatus.PASS, "logoutFromPortal test Completed");
    log.debug("logoutFromPortal test Completed");

  }

  /*
   * INVALIED USERNAME LOGIN TO PORTAL TEST
   * 
   * @Step Login to web portal using invalid username
   * @Step Verify the error message
   */

  @Test(priority = 414, enabled = true)
  @Parameters ({"systemUserPassword"})
  public void TEST_218_invalidUsernameloginToPortal(String systemUserPassword) throws IOException, InterruptedException, java.text.ParseException {
    log.debug("invalidUsernameloginToPortal test Started");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    w.click(CssLocators.loginLinkKey);
    aw.logInWithWrongCredentials(wrongUsername, systemUserPassword);
    Assert.assertTrue(w.isDisplayed(CssLocators.loginFailedMessage));
    logger.log(LogStatus.PASS, "invalidUsernameloginToPortal test Completed");
    log.debug("invalidUsernameloginToPortal test Completed");

  }

  /*
   * INVALIED PASSWORD LOGIN TO PORTAL TEST
   * 
   * @Step Login to web portal using invalid password
   * 
   * @Step Verify the error message
   * 
   */

  @Test(priority = 415, enabled = true)
  @Parameters ({"systemUserName"})
  public void TEST_219_invalidPasswordloginToPortal( String systemUserName) throws IOException, InterruptedException, java.text.ParseException {
    log.debug("invalidPasswordloginToPortal test Started");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    aw.logInWithWrongCredentials(systemUserName, wrongPassword);
    Assert.assertTrue(w.isDisplayed(CssLocators.loginFailedMessage));
    logger.log(LogStatus.PASS, "invalidPasswordloginToPortal test Completed");
    log.debug("invalidPasswordloginToPortal test Completed");

  }

  /*
   * FORGOT PASSWORD ON LOGIN PAGE TEST
   * 
   * @Step Goto Login page
   * @Step Goto Forgot Password
   * @Step Enter the email. click send button
   * @Step Verify the success message for email sent
   * 
   */
  @Test(priority = 416, enabled = true)
  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
  public void TEST_220_forgotPasswordOnLoginPage(String authenticationType, String systemUserName, String systemUserPassword, String tenantName) throws IOException, InterruptedException, java.text.ParseException {
    log.debug("forgotPasswordOnLoginPage test Started");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    aw.forgotPassword(systemUserName);
    Assert.assertTrue(w.isDisplayed(CssLocators.emailSentContainerKey));
    logger.log(LogStatus.PASS, "forgotPasswordOnLoginPage test Completed");
    log.debug("forgotPasswordOnLoginPage test Completed");

  }

  /*
   * FLEX LOGO EXISTS IN LOGIN PAGE TEST
   * 
   * @Step Goto the Login page
   * @Step Verify the flex logo existance
   */
  @Test(priority = 417, enabled = true)
  public void TEST_221_FlexLogo() throws IOException, InterruptedException, java.text.ParseException {
    log.debug("FlexLogo test Started");
    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
    Assert.assertTrue(aw.isFlexLogoAvailable());
    w.getDriver().quit();
    logger.log(LogStatus.PASS, "FlexLogo test Completed");
    log.debug("FlexLogo test Completed");

  }

  @AfterMethod
  public void writeResult(ITestResult result) {
    String methodName = result.getMethod().getMethodName();
    try {
      if (result.getStatus() == ITestResult.SUCCESS) {
        nbi.changeJiraStatus(methodName, "Pass");
      } else if (result.getStatus() == ITestResult.FAILURE) {
        nbi.changeJiraStatus(methodName, "Fail");
        w.captureScreenShot(logger, result.getThrowable(), result.getName());
      } else if (result.getStatus() == ITestResult.SKIP) {
        nbi.changeJiraStatus(methodName, "Unknown");
      }
    } catch (Exception e) {
      System.out.println("@AfterMethod: Exception caught");
      e.printStackTrace();
    }
    extent.endTest(logger);
  }

  @AfterClass
  public void tearDown() throws Exception {
    //aw.logOut();
    w.getDriver().quit();
    log.debug("LoginTest suite completed");
  }

}
