package co.in.bmw.ci.autotest.testscripts.login;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import co.in.bmw.ci.autotest.testscripts.tenant.AddTenant;
import co.in.bmw.ci.autotest.utils.AppWebInterfaceWrapper;
import co.in.bmw.ci.autotest.utils.DriverInstance;
import co.in.bmw.ci.autotest.utils.ExtentManager;
import co.in.bmw.ci.autotest.utils.NorthBoundInterface;
import co.in.bmw.ci.autotest.utils.TestNgParameters;
import co.in.bmw.ci.autotest.utils.WebInterfaceWrapper;

public class AzureLogin {
	 private AppWebInterfaceWrapper aw;
	  private WebInterfaceWrapper w;
	  private static final Logger log = Logger.getLogger(AzureLogin.class.getName());
	  private WebDriver driver;
	  private DriverInstance driverInstance;
	  private TestNgParameters testNg;
	  private AddTenant ac;
	  ExtentTest logger;
	  ExtentReports extent;
	  private NorthBoundInterface nbi;
	  private String path;
	  String azureUserName;
	  String azurePassWord;

	  // LoginAs Administrator
	  @BeforeClass
	  @Parameters ({"authenticationType","systemUserName", "systemUserPassword","tenantName" })
	  public void setUp(String authenticationType, String systemUserName, String systemUserPassword, String tenantName) throws Exception {
	    extent = ExtentManager.getInstance();
	    testNg = new TestNgParameters();
	    driverInstance = new DriverInstance();
	    driver = driverInstance.openBrowser(testNg.getBrowser());
	    w = new WebInterfaceWrapper(driver);
	    aw = new AppWebInterfaceWrapper(driver);
	    nbi = new NorthBoundInterface();
	    azureUserName = testNg.azureUserName();
	    azurePassWord = testNg.azurePassword();
	  
	   
	  
	  }
	  
	  /*
	   * Azure Login TEST
	   * 
	   
	   */
	  @Test(priority = 1, enabled = true)
	  public void azureLogin() throws Exception {
	    log.debug("Executing azureLogin test");
	    logger = extent.startTest(Thread.currentThread().getStackTrace()[1].getMethodName()).assignCategory(this.getClass().getSimpleName());
	    aw.loginAsAzure(azureUserName, azurePassWord);
	    logger.log(LogStatus.PASS, "azureLogin test Completed");
	    log.debug("azureLogin test Completed");

	  }
	

	

}
