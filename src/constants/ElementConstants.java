package constants;

public class ElementConstants {
	
//Locators Constants
	public static final String ID = "id";
	public static final String XPATH = "xpath";
	public static final String NAME = "name";
	public static final String CLASS_NAME = "classname";
	public static final String CSS_NAME = "cssname";
	public static final String LINK_TEXT = "linkText";
	public static final String PARTIAL_LINK_TEXT = "PARTIAL_LINK_TEXT";	
	
//Home
    public static final String headerBMWLink_GU = "headerBMWLink_GU";
    public static final String carouselImage_GU = "carouselImage_GU";

//Login 	
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String LOGINBUTTON = "loginButton";
	public static final String LOGINBOTCHECK = "loginbotcheck";	
	public static final String DELZSCALAR = "delzscalar";
	public static final String BMWLOGINCLICK = "bmwLoginClick";
	public static final String LGNSUCCESSMSG = "LgnSuccessMsg";
	public static final String LGNFAILUREMSG = "LgnFailureMsg";
	public static final String LOGOUT = "Logout";
	
//Myprofile edit -IN 	
	public static final String MYPROFILE="Myprofile";
	public static final String MYPROFILEEDIT="Edit";
	public static final String TITLEFIELD="Titlefield";
	public static final String TITLE="Title";
	public static final String FIRSTNAMEFIELD="Firstnamefield";
	public static final String FIRSTNAME="FirstName";
	public static final String LASTNAMEFIELD="Lastnamefield";
	public static final String LASTNAME="LastName";
	public static final String PINCODEFIELD="Pincodefield";
	public static final String PINCODE="Pincode";
	public static final String STATEFIELD="Statefield";
	public static final String STATE="State";
	public static final String CITYFIELD="Cityfield";		
	public static final String CITY="City";
	public static final String NOTIFICATIONCHOSENFIELD="Notificationchosenfield";
	public static final String NOTIFICATIONEMAIL="Notificationemail";
	public static final String NOTIFICATIONSMS="Notificationsms";
	public static final String ADDRESS1FIELD="Address1field";
	public static final String ADDRESS1="Address1";
	public static final String ADDRESS2FIELD="Address2field";
	public static final String ADDRESS2="Address2";
	public static final String ADDRESS3FIELD="Address3field";
	public static final String ADDRESS3="Address3";
	public static final String BIRTHDAYFIELD="Birthdayfield";
	public static final String BIRTHDAY="Birthday";
	public static final String GENDERFIELD="Genderfield";
	public static final String GENDER = "Gender";
	public static final String WEDDINGANNIVERSARYDATEFIELD="WeddingAnniversaryDatefield";
	public static final String WEDDINGANNIVERSARYDATE="WeddingAnniversaryDate";
	public static final String OCCUPATIONFIELD="Occupationfield";
	public static final String OCCUPATION="Occupation";
	public static final String PROFESSIONFIELD="Professionfield";
	public static final String PROFESSION="Profession";
	public static final String YOURHOBBIESINTERETSVALUESFIELD="YourHobbies/Interestsvaluesfield";
	public static final String YOURHOBBIESINTERETSVALUES="YourHobbies/Interestsvalues";
	public static final String BRANDSCARSFIELD="Brands/Carsfield";
	public static final String BRANDSCARS="Brands/Cars";
	public static final String PRIMARYCARFIELD="PrimaryCarfield";
	public static final String PRIMARYCAR="PrimaryCar";
	public static final String BRAND1FIELD="Brand1field";
	public static final String BRAND1="Brand1";
	public static final String BRAND2FIELD="Brand2field";
	public static final String BRAND2="Brand2";
	public static final String MODEL1FIELD="Model1field";
	public static final String MODEL1="Model1";
	public static final String MODEL2FIELD="Model2field";
	public static final String MODEL2="Model2";
	public static final String UPDATE="Update";
	public static final String CANCEL="Cancel";
	public static final String CONFIRMATIONCANCEL="Confirmationcancel";
	public static final String CONFIRMATIONYES= "confirmationYes";
	public static final String UpdateOK="UpdateOK";	
	public static final String BMONTH="BMonth";
    public static final String BYEAR="Byear";       
    public static final String BDATE="Bdate";
    
//Myprofile edit -TH 
  	public static final String  AMPHURFIELD ="AmphurField";
  	public static final String	AMPHUR=	"Amphur";
  	public static final String	DISTRICTFIELD="DistrictField"; 
  	public static final String	DISTRICT="District";
  	public static final String	LINEIDFIELD="LineIDField";
  	public static final String	LINEID="LineID";
  	public static final String	NUMBEROFFAMILYMEMBERSFIELD="NumberoffamilymembersField";
  	public static final String	NUMBEROFFAMILYMEMBERS="Numberoffamilymembers";
  	public static final String	MARITALSTATUSFIELD="MaritalStatusField";
  	public static final String	MARITALSTATUS="MaritalStatus";
  	public static final String	PREVIOUSBRANDFIELD="PreviousBrandField";
  	public static final String	PREVIOUSBRAND="PreviousBrand";
  	public static final String	MOTIVEOFPURCHASEFIELD="MotiveOfPurchaseField";
  	public static final String	MOTIVEOFPURCHASE="MotiveOfPurchase";
  	public static final String  YOURHOBBIESINTERETSVALUESFIELD_TH ="YourHobbiesField_TH";
  	public static final String  YOURHOBBIESINTERETSVALUES_TH ="YourHobbies_TH";
	
//All Model page
  	public static final String Exteriornamedisplay="Exteriornamedisplay";
  	public static final String Interiornamedisplay="Interiornamedisplay";
  	public static final String CalcualteEMIMyconfig="CalcualteEMIMyconfig";
  	public static final String CarbookingStatus ="CarbookingStatus";	
	public static final String PriceRange = "PriceRange";
	public static final String CheckAvailabilityButton = "CheckAvailabilityButton";
    public static final String CheckAvailabilityButtonMessage = "CheckAvailabilityButtonMessage";
    public static final String CheckAvailabilityButtonMessageOK = "CheckAvailabilityButtonMessageOK";
    public static final String CheckAvailabilityconfirmButton = "CheckAvailabilityconfirmButton";         
    public static final String ZoomiconImage ="ZoomiconImage";
    public static final String Carvariantslist ="Carvariantslist";
    public static final String Carzoomlist ="Carzoomlist";
    public static final String Zoomwindowclose ="Zoomwindowclose";
    public static final String Checkavailabilitywindowexterior ="Checkavailabilitywindowexterior";
    public static final String Checkavailabilitywindowinterior ="Checkavailabilitywindowinterior";
    public static final String Carserieslist="Carserieslist";
    public static final String Carseriesclicktext="Carseriesclicktext";
    public static final String Carseriesclickimage="Carseriesclickimage";
    public static final String Carseriesclickbutton="Carseriesclickbutton";    
    public static final String Pricerangedropdown="Pricerangedropdown";
    public static final String Pricerangeoption="Pricerangeoption";    
    public static final String Fueltypeoption="Fueltypeoption";
    public static final String Varianttypeoption="Varianttypeoption";    
    public static final String Exteriortype="Exteriortype";
    public static final String Interiortype="Interiortype";
    
//MyConfiguraiton
	public static final String ConfigCarName = "ConfigCarName";
	public static final String ViewConfiguration= "ViewConfiguration";
	public static final String EnginePower = "EnginePower";
	public static final String ExteriorColor = "ExteriorColor";
	public static final String Upholstery = "Upholstery";
	public static final String ExShowroomPrice = "ExShowroomPrice";
	public static final String OffersMessage1 = "OffersMessage1";
	public static final String OffersMessage2 = "OffersMessage2";
	public static final String OffersMessage3 = "OffersMessage3";
	public static final String OffersMessage4 = "OffersMessage4";
	public static final String OffersMessage5 = "OffersMessage5";
	public static final String SelectaDealerFirst = "SelectaDealerFirst";
	public static final String InputPincode = "InputPincode";
	public static final String SelectCitydropdown = "SelectCitydropdown";
	public static final String SelectCitydropdown1 = "SelectCitydropdown1";
	public static final String SelectDealer = "SelectDealer";
	public static final String SelectDealer1 = "SelectDealer1";
	public static final String ContinueButton = "ContinueButton";
	public static final String RemoveConfiguration = "RemoveConfiguration";
	public static final String MyBMW = "MyBMW";
	public static final String RemoveConfigurationYes = "RemoveConfigurationYes";
	public static final String RemoveConfigurationNo = "RemoveConfigurationNo";
	public static final String RemoveConfigYesMsg = "RemoveConfigYesMsg";
	public static final String RemoveConfigYesMsgOK = "RemoveConfigYesMsgOK";
	public static final String Config1ofn = "Config1ofn";
	public static final String clickRight = "clickRight";
	public static final String ConfigCurCnt = "ConfigCurCnt";
	public static final String BuyOnlineButton = "BuyOnlineButton";
	public static final String ChangeConfiguration = "ChangeConfiguration";	
	public static final String DownloadSummary = "DownloadSummary";
    public static final String Scrn_Variants   = "Scrn_Variants";
    public static final String Scrn_BookingID  = "Scrn_BookingID";
    public static final String Scrn_DealerName = "Scrn_DealerName";
    public static final String MyPurchasesTab = "MyPurchasesTab";
    public static final String CaclulateEMI = "CaclulateEMI";
    public static final String BookTestDrive = "BookTestDrive";
    public static final String RequestaQuote = "RequestaQuote";
    public static final String RequestaQuoteSkip = "RequestaQuoteSkip";
    public static final String RequestaQuoteMsg = "RequestaQuoteMsg";        
    public static final String BookTestDriveConfirm = "BookTestDriveConfirm";
    public static final String BookTestDriveMsg = "BookTestDriveMsg";
    public static final String BookTestReconfirm="BookTestReconfirm";
	
// Payment
	public static final String MakePayment = "MakePayment";
	public static final String MakePaymentchkbox ="MakePaymentchkbox";
	public static final String NetbankingPopupClick = "NetbankingPopupClick";
	public static final String BMWNETBANKINGDEMOselectBank = "BMWNETBANKINGDEMOselectBank";
	public static final String BankTestBank	= "BankTestBank";
	public static final String BMWNetBankingsubmit = "BMWNetBankingsubmit";
	public static final String CustomerID = "CustomerID";
	public static final String Submit = "Submit";
	public static final String SubmitAmount	= "SubmitAmount";
	public static final String TransactionPassword = "TransactionPassword";
	public static final String PurchaseComplete = "PurchaseComplete";	
	public static final String ViewMyBMW="ViewMyBMW";	
    
// Feedback 	
	public static final String Feedback = "Feedback";
    public static final String Rating1 = "Rating1";
    public static final String Rating2 = "Rating2";
    public static final String Rating3 = "Rating3";
    public static final String Rating4 = "Rating4";
    public static final String Rating5 = "Rating5";
    public static final String Rating6 = "Rating6";
    public static final String Rating7 = "Rating7";
    public static final String Rating8 = "Rating8";
    public static final String Rating9 = "Rating9";
    public static final String Rating10 = "Rating10";
    public static final String Textbox = "Textbox";
    public static final String FeedbackSubmitButton = "FeedbackSubmitButton";
    public static final String FeedbackCloseButton = "FeedbackCloseButton";
    public static final String FeedbackClose = "FeedbackClose";
    public static final String FeedbackMessageAlert = "FeedbackMessageAlert";
    public static final String FeedbackMessageAlertOkButton = "FeedbackMessageAlertOkButton";
    
//Chat window     
    public static final String BMWOnlineGenius = "BMWOnlineGenius";
    public static final String MessageTextArea = "MessageTextArea";
    public static final String MessageSend = "MessageSend";
    public static final String ChatWindow = "ChatWindow";
    public static final String Chatwindowclose="Chatwindowclose";
    public static final String MessageRecive="MessageRecive";      
    public static final String Dummy = "Dummy";
    
    







}
