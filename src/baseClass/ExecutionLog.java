package baseClass;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;



public class ExecutionLog {
	public static String Currentfile;
    
	public static void createLog() throws IOException
	{
		
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss") ;
		File file = new File("./TestExecutionLog"+dateFormat.format(date) + ".txt") ;
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		Currentfile=file.getAbsolutePath();
		out.write("Logging Test Execution.......");
		out.close();
		
				
	}//end of create log method
	
	
	public static void printLog(String s) throws IOException
	{
		
	    System.out.println(Currentfile);
		FileWriter fstream = new FileWriter(Currentfile, true); //true tells to append data.
	    BufferedWriter out = new BufferedWriter(fstream);
	    out.write("\r\n");
	    out.write(s);
		out.close();
		
		 
	   
		
	}// end of Print log Method
		

}
