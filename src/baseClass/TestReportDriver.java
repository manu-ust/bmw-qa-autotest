package baseClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class TestReportDriver{

	public void createWord(String TestCaseName, String TestCaseNo, int scrnCnt) {
		try {	

			//creting a folder with currentdate
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy"); //-hh-mm
			String foldername = ft.format(dNow);

			String pathScreenShot = System.getProperty("user.dir") + "\\TestReport";
			String pathtestreport = System.getProperty("user.dir")
					+ "\\TestReport\\" + foldername;
			// creating a folder
			new File(pathtestreport).mkdir();

			try {
				XWPFDocument doc = new XWPFDocument();
				XWPFParagraph p = doc.createParagraph();
				XWPFRun xwpfRun = p.createRun();
				List<String> imgList = new ArrayList<String>();
				
				System.out.println("TRD scrnCnt :" + scrnCnt);
				for (int i = 1; i <= scrnCnt; i++){
					imgList.add(pathScreenShot + "\\" + TestCaseNo + "_" + i + ".png");					
				}
									
				xwpfRun.setText(TestCaseNo);
				xwpfRun.addBreak();
				for (String imgFile : imgList) {
					System.out.println("TRD imgFile2 :" + imgFile);		
					int format = XWPFDocument.PICTURE_TYPE_JPEG;
					//xwpfRun.setText(imgFile);
					xwpfRun.addBreak();
					xwpfRun.addBreak();
					xwpfRun.addPicture(new FileInputStream(imgFile), format,
							imgFile, Units.toEMU(500), Units.toEMU(250)); // 400x200//
																		  // W  x  H//
																		  // pixels
					// xwpfRun.addBreak(BreakType.PAGE);
				}
				FileOutputStream out = new FileOutputStream(pathtestreport+"\\"
						+ TestCaseName + "_" + TestCaseNo + ".docx");
				doc.write(out);
				out.close();			
				
			} catch (Exception e) {
				// TODO: handle exception
			}
				
		} catch (Exception e) {
			// TODO: handle exception
		}		
	}
}
