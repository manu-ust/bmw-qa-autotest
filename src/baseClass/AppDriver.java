package baseClass;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

//import org.testng.Assert;
//import org.testng.annotations.AfterClass;
//import org.testng.annotations.BeforeClass;
import bmwModules.Login;

public class AppDriver {
	public static BaseDriver base;
	static Properties prop;
	
	 /*Method used to:
     * Clearing the browser cookies*/	
	public void Clearcookies()
	{
		base.driver.manage().deleteAllCookies();
	}
	 /*Method used to:
     * apply explicit wait time */
	public void sleep(int value) {
		base.sleep(value);
	}
	
	 /*Method used to:
     * Switching to parent window*/
	public void switchbacktoMainWindow() throws InterruptedException {
		base.switchbacktoMainWindow();
	}
	
	/*Method used to:
     * Switching to next window*/
	public void switchwindow() throws InterruptedException {
		base.switchwindow();
	}
    
	public AppDriver(String propFileName) {
		prop = new Properties();
		setUp(propFileName);
	}
	
	/*Method used to:
     * loading the property file*/
	public void setUp(String propFileName) {
		try {
			prop.load(new FileInputStream(propFileName));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/*Method used to:
     * Retrieve the webelemets from property file*/
	public static String getElement(String element) {
		return prop.getProperty(element);
	}
	
	/*Method used to:
     * BrowserLaunch and Login to the application as per the input credential given in Excel sheet.*/
	public String LaunchBrowserLoginAndGetLoginList(List<List<String>> tmpLogin) throws IOException, InterruptedException {
	    for (List<String> login : tmpLogin)
	    {	   	
	    	if (login.get(0).equalsIgnoreCase("Yes")) 
	    	{
	    		String Login_ID = login.get(6);
	    		List<String> tmpLogin1 = SearchList(Login_ID, tmpLogin); 
	    		if (!tmpLogin1.get(0).equals("FAIL")) 	
	    		{
	    			String retMessage = BrowserLaunch(tmpLogin1);
	    			if (retMessage.equals("PASS"))
	    			{
	    				Login loginModule = new Login();
		    			retMessage = loginModule.BMWLogin(login);
		    			
		    			if (retMessage.substring(0,3).equals("Hi "))
		    			{ 
		    				System.out.println("PASS - LaunchBrowserLoginAndGetLoginList");
			    			return "PASS";
		    			}
	    			}	    			
	    		}else {
	    			System.out.println("FAIL - LaunchBrowserLoginAndGetLoginList");
	    		}
	    	}		    
	    }
	    
	    return "FAIL";
	}
	
	/*Method used to:
     * Login as a guest user*/
	public String launchBrowser_GU(List<List<String>> tmpLogin,  List<String> tmpAllModels) throws IOException, InterruptedException {
		//List<String> tmplog = new ArrayList<String>();	
	    for (List<String> login : tmpLogin)
	    {	   	
	    	if (login.get(0).equalsIgnoreCase("Yes")) 
	    	{
	    		String Login_ID = login.get(6);
	    		List<String> tmpLogin1 = SearchList(Login_ID, tmpLogin); 
	    		if (!tmpLogin1.get(0).equals("FAIL")) 	
	    		{
	    			String retMessage = BrowserLaunch(tmpLogin1);
	    	}		    
	       }
	    }
	    
	    return "FAIL";
	}
	
	/*Method used to:
     * Replacing the special character and whitespace for a string value*/
	public String ReplaceSplCharbyWhiteSpace(String strTemp)
	{
		strTemp = strTemp.replaceAll("\\s","");	
		strTemp = strTemp.replaceAll("[>]","");					
		strTemp = strTemp.replaceAll("[.]","");					
		strTemp = strTemp.replaceAll("[/]","");
		strTemp = strTemp.replaceAll("[']","");
		strTemp = strTemp.replaceAll("[(]","");
		strTemp = strTemp.replaceAll("[)]","");
		strTemp = strTemp.replaceAll("[|]","");
		strTemp = strTemp.replaceAll("[-]","");
		return strTemp;
	}	
	
	/*Method used to:
     * Searching a key value from the xpath excel sheet and clicking the element*/
	public String SearchHMandClick(Map<String, String> hmXpath, String strIdent) {
		System.out.println(strIdent);
		if (hmXpath.containsKey(strIdent)){ 
			sleep(5000);
			String identifier = hmXpath.get(strIdent); 
		    //System.out.println("value for key :" + strIdent + "-" + identifier); 
		    String xpath_ident = "xpath" + "," + identifier;
			click(xpath_ident);
			return "PASS";
		}
		System.out.println("Key Not Found");
		return "FAIL";		 
	}
	
	/*Method used to:
     * Take screenshot method*/
	public void ScreenCapture(String TestCaseNo, int scrnCnt) throws IOException {
		
		String screenshotName = TestCaseNo + "_" + scrnCnt;
		captureImage(screenshotName);
		System.out.println("AP screenshotName :" + screenshotName);
	}
	
	/*Method used to:
     * Take screenshot method*/
	public void captureImage(String screenshotName) throws IOException {
		base.captureImage(screenshotName);
	}
    
	/*Method used to:
     * Deleting a file from the path given */
	public void deleteFile(String Filepath) {
		base.deleteFile(Filepath);
	}
   
	/*Method used to:
     * Deleting all files from the folder path given*/
	public void deleteAllFilesFromFolder(String Filepath) {
		base.deleteAllFilesFromFolder(Filepath);
	}
    
	/*Method used to:
     * Creating a file from the path given*/
	public void creatingFile(String Filepath, String data) throws IOException {
		base.creatingFile(Filepath, data);
	}
	
	/*Method used to:
     * Searching  a key value from the xpath excel sheet and taking gettext value for that element */
	public String SearchHMGetText(Map<String, String> hmXpath, String strIdent) {
		System.out.println(strIdent);
		if (hmXpath.containsKey(strIdent)){ 
			sleep(500);
			String identifier = hmXpath.get(strIdent); 
		    //System.out.println("value for key :" + strIdent + "-" + identifier); 
		    String xpath_ident = "cssname" + "," + identifier;
			String strText =  getText(xpath_ident);
			return strText;
		}
		System.out.println("Key Not Found");
		return "FAIL";		 
	}
	
	/*Method used to:
     * Searching  a key value from the xpath excel sheet and checkign the element is exist or not*/
	public boolean SearchHMexistsElement(Map<String, String> hmXpath, String strIdent) {
		System.out.println(strIdent);
		if (hmXpath.containsKey(strIdent)){ 
			sleep(500);
			String identifier = hmXpath.get(strIdent); 
		    String xpath_ident = "cssname" + "," + identifier;
			return existsElement(xpath_ident);
		}
		System.out.println("Key Not Found");
		return false;		 
	}
	
	/*Method used to:
     * Searching  a key value from the xpath excel sheet and selecting a value from  the dropdwon box by using index*/
	public String SearchHMandSelDrop(Map<String, String> hmXpath, String strIdent, int indexval) {
		System.out.println(strIdent);
		if (hmXpath.containsKey(strIdent)){ 
			sleep(500);
			String identifier = hmXpath.get(strIdent); 
		    //System.out.println("value for key :" + strIdent + "-" + identifier); 
		    String xpath_ident = "cssname" + "," + identifier;
		    dropDownByIndex(xpath_ident,indexval);
			return "PASS";
		}
		System.out.println("Key Not Found");
		return "FAIL";		 
	}
	
	/*Method used to:
     * Fetching the loginID from the list string*/
	public List<String> SearchList(String Login_ID, List<List<String>> listLogin) {

		List<String> strLogin = new ArrayList<String>();
		System.out.println("Login_ID AM :" + Login_ID);
		
		for (List<String> strLogin1 : listLogin){
			if (strLogin1.get(6).equalsIgnoreCase(Login_ID)) {
				return strLogin1;
			}
		}
		strLogin.add("FAIL");
		return strLogin;
		 
	}
	
	/*Method used to:
     * Browser launch at the first time accept the security message and click the accept button and then entering username/password in the alertbox displayed.*/
	public String BrowserLaunch(List<String> strTemp) {
		try {
			base = new BaseDriver();
			String url  = strTemp.get(3);
			String username  = strTemp.get(4);
			String password  = strTemp.get(5);
			url(url);
			maximizeScreen();
			Thread.sleep(8000);	
			
/*			String urlz = getcurrenturl();

			
			System.out.println("AD - Check8 URL: " + urlz);
			if(urlz.substring(0,25).equals("https://login.zscaler.net")){
				System.out.println("AD Zcalar going to click - Check9");
				click(getElement(ElementConstants.DELZSCALAR));
				Thread.sleep(5000);							
			}*/
			Thread.sleep(5000);
			//WebDriverWait wait = new WebDriverWait(base.driver, 10);      
			//Alert alert = wait.until(ExpectedConditions.alertIsPresent());     
			//alert.authenticateUsing(new UserAndPassword(username, password));
			setTexttoAlert(username);
			Thread.sleep(500);
			setTexttoAlert(password);
			Thread.sleep(500);		
			return "Launched Browser";	
		
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block		
			e.printStackTrace();
			return "FAIL";
		}	
	}
	
	/*Method used to:
     * scrolling down in the active window*/
	public void scrollDown() {
		base.scrollDown();
	}
	public void scrollup()
	{
		base.scrollup();
	}
	
	public void setTexttoAlert(String enteredValue) {
		base.setTexttoAlert(enteredValue);
	}
			
	public void maximizeScreen() {
		base.maximizeScreen();
	}
	
	public void click(String identifier) {
		base.click(identifier.split(",")[0], identifier.split(",")[1]);
	}
	
	public void tabOut() {
		base.tabOut();
	}
	
	public boolean isEnabled(String identifier) {
		return base.isEnabled(identifier.split(",")[0],
				identifier.split(",")[1]);
	}
	
	public void doubleClick(String identifier) {
		base.doubleClick(identifier.split(",")[0], identifier.split(",")[1]);
	}
	
	public void setValue(String identifier, String enteredValue) {
		base.setValue(identifier.split(",")[0], identifier.split(",")[1],
				enteredValue);
	}
	
	public void dropDownByIndex(String identifier, int indexval) {
		base.dropDownByIndex(identifier.split(",")[0],
				identifier.split(",")[1], indexval);
	}
	
	public void dropDownByValue(String identifier, String value) {
		base.dropDownByValue(identifier.split(",")[0],
				identifier.split(",")[1], value);
	}
	
	public String getText(String identifier) {
		return base.getText(identifier.split(",")[0], identifier.split(",")[1]);
	}
	
	public void backspace(String identifier) {
		base.backspace(identifier.split(",")[0], identifier.split(",")[1]);
	}
	
	public void clear(String identifier) throws IOException {
		base.clear(identifier.split(",")[0], identifier.split(",")[1]);
	}
	
	public String getAttributeTitle(String identifier) {
		return base.getAttributeTitle(identifier.split(",")[0],
				identifier.split(",")[1]);
	}
	
	public boolean isVisible(String identifier) {
		return base.isVisible(identifier.split(",")[0],
				identifier.split(",")[1]);
	}
	
	public void hoveronElement(String identifier) throws InterruptedException {
		base.hoveronElement(identifier.split(",")[0], identifier.split(",")[1]);
	}
	
	public boolean isElementSelected(String identifier) {
		return base.isElementSelected(identifier.split(",")[0],
				identifier.split(",")[1]);
	}
	
	public String readFromInput(String identifier) {
		return base.readFromInput(identifier.split(",")[0],
				identifier.split(",")[1]);

	}
	
	public boolean existsElement(String identifier) {
		return base.existsElement(identifier.split(",")[0],
				identifier.split(",")[1]);
	}

	public void explicitWaitByID(String identifier) {
		base.explicitWaitByID(identifier.split(",")[0],
				identifier.split(",")[1]);
	}
		
	public String getcurrenturl() {
		return base.getcurrenturl();
	}
	
	public void url(String url) {
		base.url(url);
	}
	
	public void acceptAlert() {
		base.acceptAlert();
	}
	
	public void declineAlert() {
		base.declineAlert();
	}
	
	public void ifAlertCloseAlert() {
		base.ifAlertCloseAlert();
	}	
	
	public void explicitWaitByxpath(String identifier) {
		base.explicitWaitByxpath(identifier.split(",")[1]);
	}
	
	public void explicitWaitByCSS(String identifier) {
		base.explicitWaitByCSS(identifier.split(",")[1]);
	}
	
	public void sendEnter() {
		base.sendEnter();
	}
	
	public void sendDown(int n) {
		base.sendDown(n);
	}

	public void sendRight(int n) {
		base.sendRight(n);
	}	
	
	public void tearDown() {
		base.tearDown();
		System.out.println("******************************************************************");

	}
	
	public void changeToTab(String frameid) {
		base.changeToTab(frameid);
	}
	
	public void clickTableCheckBox(String item) {
		base.clickTableCheckBox(item);
	
	}
	
	public void closewindow() throws InterruptedException {
		base.closewindow();
	} 
	
	 /*Method used to
     * Select a car series from the list as per the input data given in excel*/	
   public String selectCarSeries(String element1, String element2, String value){		
	   String ret_Msg = "";
		  try
		   {
	        List<WebElement> getelements = base.driver.findElements(By.cssSelector(element1));
			List<WebElement> carseries = base.driver.findElements(By.cssSelector(element2));
				for(int i=0; i<carseries.size();i++){
					   if(carseries.get(i).getText().contains(value)){
		                 getelements.get(i).click();
		                 sleep(4000);
		                 ret_Msg ="All Models: Successfully clicked the car series";
		                 break;
				   }	
				}			
		   }catch (Exception e){
				ret_Msg = "All Models:Unable to click the car series" +e;
			}
			   return ret_Msg;  
		   }
   /*Method used to
    * Select a price range from drop down as per the input data given in excel*/
   public String selectPriceRange(String element, String value)
   {   
	   String ret_Msg= "";
	  try
	   {
		  List<WebElement> dropdownlist = base.driver.findElements(By.cssSelector(element));
	      for(int i=0; i<dropdownlist.size();i++){
		   if(dropdownlist.get(i).getText().contains(value)){
			   dropdownlist.get(i).click();
			   ret_Msg = "All Models:Sucessfully selected the price range";
			   sleep(2000);
			   break;		    
	    }	
	   }
	  }catch (Exception e){
		ret_Msg = "All Models:Unable to select the  price range" +e;
	}
	   return ret_Msg;  
   }
   
   /*Method used to
    * Select a Fuel type from drop down as per the input data given in excel*/
   public String selectFuelType(String element, String value)
   {   
	   String ret_Msg= "";
	 try
	   {
	   List<WebElement> dropdownlist = base.driver.findElements(By.cssSelector(element));
	   for(int i=0; i<dropdownlist.size();i++){
		   if(dropdownlist.get(i).getText().contains(value)){
			   dropdownlist.get(i).click();
			   sleep(1000);
			   ret_Msg = "All Models:Sucessfully selected the Fuel type";  
			   break;			   
	   }	
	}	
	   }catch (Exception e){
			ret_Msg = "All Models:Unable to select the Fuel type" +e;
		}
		   return ret_Msg;  
	   }
   
   /*Method used to
    * Select a variant type from drop down as per the input data given in excel*/
   public String selectVariantType(String element, String value)
   {   
	   String ret_Msg= "";
	try
      {
	   List<WebElement> dropdownlist = base.driver.findElements(By.cssSelector(element));
	   for(int i=0; i<dropdownlist.size();i++){
		   if(dropdownlist.get(i).getText().contains(value)){
			   dropdownlist.get(i).click();
			   ret_Msg = "All Models:Sucessfully selected the Variant type";
			   break;			   
	    }	
	   }	
      }catch (Exception e){
			ret_Msg = "All Models:Unable to select the Variant type" +e;
		}
		   return ret_Msg;  
	   }
   
   /*Method used to
    * Select a Exterior colour as per the input data given in excel*/
   public String clickExteriorColour(String element, String value){
	   String ret_Msg= "";
		try
	      {
	       List<WebElement> exteriorList = base.driver.findElements(By.cssSelector(element));
			for(int i=0; i<exteriorList.size();i++){
				   if(exteriorList.get(i).getAttribute("title").contains(value)){
					 exteriorList.get(i).click();
	                 sleep(2000);
	                 ret_Msg = "All Models:Sucessfully selected the Exterior colour";
	                 break;
			   }	
			}			
	       }catch (Exception e){
				ret_Msg = "All Models:Unable to select the  Exterior colour" +e;
			}
			   return ret_Msg;  
		   }
   /*Method used to
    * Select a Interior colour as per the input data given in excel*/	
   public String clickInteriorColour(String element, String value){
	   String ret_Msg= "";
		try
	      {
		   List<WebElement> interiorList = base.driver.findElements(By.cssSelector(element));
			for(int i=0; i<interiorList.size();i++){
				   if(interiorList.get(i).getAttribute("title").contains(value)){
					   interiorList.get(i).click();
	                 sleep(2000);
	                 ret_Msg = "All Models:Sucessfully selected the Interior colour";
	                 break;
			   }	
			}			
	      }catch (Exception e){
				ret_Msg = "All Models:Unable to select the Interior colour" +e;
			}
			   return ret_Msg;  
		   }
   
   /*Method used to
    * Pass a value to the text field as per the input data given in excel*/
   public String setValueProfileTextFields(String element1, String element2, String value) throws IOException{   
       WebElement fieldName = base.driver.findElement(By.cssSelector(element1));   
       String ret="";
           if(isVisible(element2)){
                 click(element2); 
                 try{
                      clear(element2);
                      setValue(element2,value);
                      sleep(2000);    
                      ret="Edit Profile- " + fieldName.getText() + "value entered sucessfully ";
             } catch (Exception e) {
                  System.out.println("element issue");
                  ret="Edit Profile- " + fieldName.getText() + "unable to clear or pass value to the text field" +e;
                  ExecutionLog.printLog("Edit Profile- " + fieldName.getText() + "unable to clear or pass value to the text field" +e); 
             }   
           } else {
             ExecutionLog.printLog("Edit Profile- " + fieldName.getText() + " is not visible to edit");   
             ret="Edit Profile- " + fieldName.getText() + " is not visible to edit";
           }                    
       return  ret;
 }


   /*Method used to
    * Sleect  a value in the dropdown box  as per the input data given in excel*/
   public String selectValueProfileDropdownFields(String element1, String element2, String value) throws IOException{   
	   WebElement fieldName = base.driver.findElement(By.cssSelector(element1)); 
	   String ret="";
	   if(isVisible(element2)){
	   try{
		   dropDownByValue(element2, value);
		   sleep(1000);
		   ret="Edit Profile- " + fieldName.getText() + "value selected sucessfully ";
	   } catch (Exception e) {
		  System.out.println("element issue");
		  ret="Edit Profile- " + fieldName.getText() + "unable to select the value" +e;
		  ExecutionLog.printLog("Edit Profile- " + fieldName.getText() + "unable to select the value " +e);
	   }
	   } else {
           ExecutionLog.printLog("Edit Profile- " + fieldName.getText() + " is not visible to edit");   
           ret="Edit Profile- " + fieldName.getText() + " is not visible to edit";
         }                    
     return  ret;
   }
   
   /*Method used to
    * Select  a value in the checkbox as per the input data given in excel*/
   public String selectCheckBoxNotification(String element1, String element2, String value) throws IOException{
	   String ret="";
	   WebElement chkbox1 = base.driver.findElement(By.cssSelector(element1));
	   WebElement chkbox2 =base.driver.findElement(By.cssSelector(element2));
	   
	   if (value.equalsIgnoreCase("Email & SMS")){
		   if (!chkbox1.isSelected() && !chkbox2.isSelected()){
			   ExecutionLog.printLog("Edit Profile- " + chkbox1.getText() + chkbox2.getText() + "are selected");
		   } 
	   }
       if(value.equalsIgnoreCase("Email")){
			   chkbox2.click();
			   sleep(500);
			   ret="Edit Profile- " + chkbox1.getText() + "got selected";
       }
        if(value.equalsIgnoreCase("SMS")){
			   chkbox1.click();
			   sleep(500);
			   ret="Edit Profile- " + chkbox2.getText() + "got selected";
	   }   
	   return ret;  
   }
   
   /*Method used to
    * Select a date in the calendar icon as per the input data given in excel*/
   public String selectDateCalender(String element1){
	   
	   String month = "June";
	   String year = "1992";
	   
	   String ret="";
			   
	   WebElement calenderIcon = base.driver.findElement(By.cssSelector(element1));
	   calenderIcon.click();
	   sleep(500);
	   WebElement calendermonthclick = base.driver.findElement(By.cssSelector("section > section.container.border.mt-5 > div > form > div:nth-child(2) > app-content-select > div:nth-child(2) > div:nth-child(4) > p-calendar > span > div > div > div.ui-datepicker-header.ui-widget-header.ui-helper-clearfix.ui-corner-all > div > select.ui-datepicker-month.ng-tns-c21-9.ng-star-inserted"));
	   WebElement calenderyearclick = base.driver.findElement(By.cssSelector(" form > div:nth-child(2) > app-content-select > div:nth-child(2) > div:nth-child(4) > p-calendar > span > button > span.ui-button-text.ui-clickable"));
	   List<WebElement> calendermonth = base.driver.findElements(By.cssSelector("section > section.container.border.mt-5 > div > form > div:nth-child(2) > app-content-select > div:nth-child(2) > div:nth-child(4) > p-calendar > span > div > div > div.ui-datepicker-header.ui-widget-header.ui-helper-clearfix.ui-corner-all > div > select.ui-datepicker-month.ng-tns-c21-9.ng-star-inserted >option"));
	   List<WebElement> calenderyear= base.driver.findElements(By.cssSelector("section > section.container.border.mt-5 > div > form > div:nth-child(2) > app-content-select > div:nth-child(2) > div:nth-child(4) > p-calendar > span > div > div > div.ui-datepicker-header.ui-widget-header.ui-helper-clearfix.ui-corner-all > div > select.ui-datepicker-year.ng-tns-c21-9.ng-star-inserted > option"));
	   WebElement dateselect = base.driver.findElement(By.cssSelector(" section > section.container.border.mt-5 > div > form > div:nth-child(2) > app-content-select > div:nth-child(2) > div:nth-child(4) > p-calendar > span > div > div > div.ui-datepicker-calendar-container.ng-tns-c21-9.ng-star-inserted > table > tbody > tr:nth-child(1) > td:nth-child(4) > a"));
	   	   
	   calendermonthclick.click();
	   sleep(500);
	   int monthsize = calendermonth.size();
	   sleep(1000);	   
	   for(int i=0; i<calendermonth.size();i++){
		   if(calendermonth.get(i).getText().contains(month)){
			   calendermonth.get(i).click();
			   break;			   
	   }	
	}	
	   sleep(1000);
	   try{
	   dateselect.click();
	   sleep(1000);
	   }catch(Exception e) {
		   ret="Edit Profile- Birthday field unable to select the value" +e; 	   
	   }
	   finally{
		   calenderIcon.click(); 
	   }
	   return "PASS"; 
   }
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
}
