package baseClass;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.io.FileUtils;
//import org.junit.Before;
//import org.junit.BeforeClass;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import constants.ElementConstants;

@SuppressWarnings("deprecation")
public class BaseDriver {

  public static WebDriver driver;

  static {
    System.out.println(System.getProperty("os.name"));

    String os = System.getProperty("os.name").toLowerCase();
    if (os.contains("win")) {
      System.setProperty("webdriver.chrome.driver", "src/resources/chromedriver.exe");
      driver = new ChromeDriver();
    } else if (os.contains("nux") || os.contains("nix")) {
      // System.setProperty("webdriver.chrome.driver", "src/resources/chromedriver");
      // ChromeOptions options = new ChromeOptions();
      // options.addArguments("--headless", "--disable-gpu", "--window-size=1920,1200","--ignore-certificate-errors");
      // driver = new ChromeDriver(options);
      //
      DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
      capabilities.setBrowserName("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20160101 Firefox/66.0");
      HtmlUnitDriver driver = new HtmlUnitDriver(capabilities);
      driver.setJavascriptEnabled(true);

    }

  }

  // static{
  // System.setProperty("webdriver.chrome.driver", "src/resources/chromedriver.exe");

  // WebDriverManager.chromedriver().setup();
  // WebDriverManager.firefoxdriver().setup();
  // }
  // static{
  /*
   * System.setProperty("webdriver.chrome.driver", "<chromedrier_path>"); // Add options to Google Chrome. The window-size is important for responsive sites
   * ChromeOptions options = new ChromeOptions(); options.addArguments("headless"); options.addArguments("window-size=1200x600"); public WebDriver driver = new
   * ChromeDriver(options);
   */

  // System.setProperty("webdriver.chrome.driver", "/user/bin/chromedriver");

  // public WebDriver driver = new ChromeDriver();
  // public WebDriver driver =new HtmlUnitDriver();

  // public WebDriver driver = new FirefoxDriver();
  // public WebDriver driver= new ChromeDriver();

  /*
   * public WebDriver driver;
   * @BeforeClass public void setupTest() { //WebDriverManager.chromedriver().setup(); ChromeOptions options = new ChromeOptions(); // Tested in Google Chrome
   * 59 on Linux. More info on: // https://developers.google.com/web/updates/2017/04/headless-chrome options.addArguments("--headless");
   * options.addArguments("--disable-gpu"); driver = new ChromeDriver(options); }
   */

  String mainWinID;
  String newAdwinID;

  public void captureImage(String screenshotName) throws IOException {
    File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    // // Now you can do whatever you need to do with it, for example copy
    // somewhere
    String path = System.getProperty("user.dir") + "\\TestReport\\";
    FileUtils.copyFile(scrFile, new File(path + screenshotName + ".png"));
  }

  public void deleteAllFilesFromFolder(String folderpath) {
    try {
      FileUtils.deleteDirectory(new File(folderpath));
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void creatingFile(String filepath, String data) throws IOException {
    sleep(2000);
    FileOutputStream out = new FileOutputStream(filepath);
    out.write(data.getBytes());
    out.close();
  }

  public void executeCmd(String testPlantPath) throws IOException, InterruptedException {
    System.out.println(testPlantPath);
    Process p;
    try {
      p = Runtime.getRuntime().exec(testPlantPath);
      System.out.println(p);

      p.waitFor();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void deleteFile(String filepath) {
    String fileName = filepath;
    File inputFile = new File(fileName);
    if (inputFile.exists()) {
      inputFile.delete();
    }
  }

  protected void switchbacktoMainWindow() throws InterruptedException {
    driver.switchTo().window(mainWinID);
  }

  protected void switchwindow() throws InterruptedException {
    Set<String> windowId = driver.getWindowHandles(); // get window id of

    Iterator itererator = (Iterator) windowId.iterator();
    mainWinID = (String) itererator.next();
    newAdwinID = (String) itererator.next();
    driver.switchTo().window(newAdwinID);

    System.out.println("Switch Window");
    Thread.sleep(2000);
  }

  public void setTexttoAlert(String value) {
    // Alert alert = driver.switchTo().alert();
    // alert.sendKeys(value);
    // alert.accept();
    driver.switchTo().alert().sendKeys(value);
    driver.switchTo().alert().accept();

  }

  protected void sleep(int value) {
    try {
      Thread.sleep(value);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
    }
  }

  public void maximizeScreen() {
    // TODO Auto-generated method stub
    driver.manage().window().maximize();
  }

  protected void click(String key, String value) {
    WebElement element = find(key, value);
    if (null != element) {
      element.click();
    }
  }

  protected void explicitWaitByxpath(String value) {
    WebDriverWait wait = new WebDriverWait(driver, 60);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value)));
  }

  protected void explicitWaitByCSS(String value) {
    WebDriverWait wait = new WebDriverWait(driver, 160);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(value)));
  }

  protected void tabOut() {
    driver.switchTo().activeElement().sendKeys(Keys.TAB);
  }

  protected boolean isEnabled(String key, String value) {

    WebElement element = find(key, value);
    boolean visible = false;
    if (null != element) {
      visible = element.isEnabled();
    }
    return visible;
  }

  protected void doubleClick(String key, String value) {
    WebElement element = find(key, value);
    if (null != element) {
      Actions action = new Actions(driver);
      action.doubleClick(element).perform();
    }
  }

  protected void setValue(String key, String value, String enteredValue) {
    WebElement element = find(key, value);
    if (null != element) {
      element.click();
      element.clear();
      element.sendKeys(enteredValue);
    }
  }

  public void dropDownByIndex(String key, String value, int indexval) {
    // TODO Auto-generated method stub
    WebElement element = find(key, value);
    System.out.println("check 1");
    JavascriptExecutor executor = (JavascriptExecutor) driver;
    executor.executeScript("arguments[0].click();", element);
    sleep(1000);
    Select dropdown = new Select(element);
    System.out.println("check 2");
    dropdown.selectByIndex(indexval);
    System.out.println("check 3");
  }

  public void dropDownByValue(String key, String value, String selectvalue) {
    // TODO Auto-generated method stub
    WebElement element = find(key, value);
    System.out.println("check 1");
    JavascriptExecutor executor = (JavascriptExecutor) driver;
    executor.executeScript("arguments[0].click();", element);
    Select dropdown = new Select(element);
    System.out.println("check 2");
    dropdown.selectByValue(selectvalue);
    System.out.println("check 3");
  }

  public void scrollDown() {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("window.scrollBy(0,500)", "");
  }

  public void scrollup() {
    JavascriptExecutor js = (JavascriptExecutor) driver;
    js.executeScript("window.scrollBy(500,0)", "");

  }

  public String getText(String key, String value) {
    // TODO Auto-generated method stub
    WebElement element = find(key, value);
    String text = null;
    if (null != element) {
      text = element.getText();
    }
    return text;
  }

  public void backspace(String key, String value) {
    // TODO Auto-generated method stub
    WebElement element = find(key, value);
    if (null != element) {
      element.sendKeys(Keys.BACK_SPACE);
    }
  }

  protected void clear(String key, String value) throws IOException {
    WebElement element = find(key, value);

    try {
      if (null != element) {
        element.clear();
      } else {
        System.out.println("Text box is already cleared");
      }
    } catch (Exception e) {
      ExecutionLog.printLog("Unable to clear the values");
    }

  }

  protected String getAttributeTitle(String key, String value) {
    WebElement element = find(key, value);
    if (null != element) {
      value = element.getAttribute("title");
    }
    return value;
  }

  protected boolean isVisible(String key, String value) {
    WebElement element = find(key, value);
    boolean visible = false;
    if (null != element) {
      visible = element.isDisplayed();
    }
    return visible;
  }

  public void hoveronElement(String key, String value) throws InterruptedException {
    WebElement element = find(key, value);
    if (null != element) {

      Actions action = new Actions(driver);
      action.moveToElement(element).build().perform();
      // Give wait for 2 seconds
      Thread.sleep(2000);
      /*
       * System.out.println("Hover 3"); // finally click on that element action.click(element).build().perform(); System.out.println("Hover 4");
       */
    }
  }

  public boolean isElementSelected(String key, String value) {
    WebElement element = find(key, value);
    if (element.isSelected()) {
      return true;
    }
    return false;
  }

  protected String readFromInput(String key, String value) {
    WebElement element = find(key, value);
    String returningvalue = null;
    if (null != element) {
      returningvalue = element.getAttribute("value");
    }
    return returningvalue;
  }

  public boolean existsElement(String key, String value) {
    WebElement element = null;
    try {
      element = find(key, value);
      if (element != null)
        return true;
    } catch (Exception e) {
      // TODO: handle exception
    }
    return false;
  }

  protected WebElement find(String key, String value) {

    if (ElementConstants.ID.equalsIgnoreCase(key)) {
      return driver.findElement(By.id(value));
    }
    if (ElementConstants.XPATH.equalsIgnoreCase(key)) {
      return driver.findElement(By.xpath(value));
    }
    if (ElementConstants.NAME.equalsIgnoreCase(key)) {
      return driver.findElement(By.name(value));
    }

    if (ElementConstants.CSS_NAME.equalsIgnoreCase(key)) {
      return driver.findElement(By.cssSelector(value));
    }
    if (ElementConstants.CLASS_NAME.equalsIgnoreCase(key)) {
      return driver.findElement(By.tagName(value));
    }
    if (ElementConstants.LINK_TEXT.equalsIgnoreCase(key)) {
      return driver.findElement(By.linkText(value));
    }
    if (ElementConstants.PARTIAL_LINK_TEXT.equalsIgnoreCase(key)) {
      return driver.findElement(By.partialLinkText(value));
    }
    System.out.println("BD  FIND -- IF not matchin");
    return null;

  }

  protected void explicitWaitByID(String key, String value) {
    WebDriverWait wait = new WebDriverWait(driver, 60);
    if (value.equals("xpath")) {
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(value)));
    } else {
      wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(value)));
    }
  }

  public String getcurrenturl() {
    return driver.getCurrentUrl();

  }

  public void url(String url) {
    // TODO Auto-generated method stub
    driver.get(url);
  }

  public void acceptAlert() {
    Alert alert = driver.switchTo().alert();
    alert.accept();
  }

  public void declineAlert() {
    Alert alert = driver.switchTo().alert();
    alert.dismiss();
  }

  public void ifAlertCloseAlert() {
    try {
      if (driver.switchTo().alert() != null) {
        Alert alert = driver.switchTo().alert();
        alert.accept();
      }
    } catch (Exception e) {
    }
  }

  public void waitByXPATH(String key, String value) {
    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(value)));
  }

  public void sendEnter() {
    driver.switchTo().activeElement().sendKeys(Keys.ENTER);
  }

  public void sendDown(int n) {
    for (int i = 0; i < n; i++) {
      driver.switchTo().activeElement().sendKeys(Keys.ARROW_DOWN);
    }

  }

  public void sendRight(int n) {
    for (int i = 0; i < n; i++) {
      driver.switchTo().activeElement().sendKeys(Keys.ARROW_RIGHT);
    }

  }

  public void tearDown() {
    driver.quit();
  }

  public void changeToTab(String frameid) {
    driver.switchTo().frame(frameid);
    // driver.switchTo().frame(driver.findElement(By.id("city")));
  }

  public void clickTableCheckBox(String item) {
    driver.findElement(By.xpath(item)).click();
  }

  protected void closewindow() throws InterruptedException {
    driver.close();
  }

}