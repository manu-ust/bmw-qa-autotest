package baseClass;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	
	public static String WriteExcelTab(String fileName, String sheetNo , List<List<String>> listConf, int rowCnt, int colCnt) throws IOException {
		String ret_Msg = "";
		
		FileInputStream fis = new FileInputStream(fileName);		
		Workbook workbook = null;
		if(fileName.toLowerCase().endsWith("xlsx")){
			workbook = new XSSFWorkbook(fis);
		}else if(fileName.toLowerCase().endsWith("xls")){
			workbook = new HSSFWorkbook(fis);
		}
		Sheet sheet = workbook.getSheet(sheetNo);
		
		//----
		rowCnt = rowCnt - 1; //Excel read starts with 0 , Substract 1 to counter this. 
		//colCnt -1 already factored while passing the value. 
		//colCnt = colCnt + 1;
		for (List<String> strTemp : listConf){
			Cell cell = null;
			
			String cellValue = strTemp.get(colCnt);
			//System.out.println("cellValue :" + cellValue + "|" + rowCnt + "|" + colCnt);
			cell = sheet.getRow(rowCnt).getCell(colCnt);
	        cell.setCellValue(cellValue);
	        
	        //Row row = sheet.getRow(rowCnt);
	        //row.createCell(6).setCellValue(cellValue); // To create row which doesnot exist.
	        
	        rowCnt = rowCnt + 1;			
		}		
        fis.close();

        FileOutputStream outFile =new FileOutputStream(fileName);
        workbook.write(outFile);
        outFile.close();	
        
/*
        //Update the value of cell
        cell = sheet.getRow(1).getCell(2);
        cell.setCellValue("TEST1");
        cell = sheet.getRow(2).getCell(2);
        cell.setCellValue("TEST2");
        Row row = sheet.getRow(0);
        row.createCell(3).setCellValue("Value 2");
*/
        ret_Msg = "Excel Updated";
		return ret_Msg;
	}
	    
	public static List<List<String>> ReadExcelTab(String fileName, String sheetNo , int rowCnt) {
        List<List<String>> xlLogin_List = new ArrayList<List<String>>();
        
		int xl_rowCnt = 0;
		int xl_colCnt = 0;
		try {
			//Create the input stream from the xlsx/xls file
			FileInputStream fis = new FileInputStream(fileName);
			
			//Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
			if(fileName.toLowerCase().endsWith("xlsx")){
				workbook = new XSSFWorkbook(fis);
			}else if(fileName.toLowerCase().endsWith("xls")){
				workbook = new HSSFWorkbook(fis);
			}
			//Sheet sheet = workbook.getSheetAt(sheetNo); //
			Sheet sheet = workbook.getSheet(sheetNo);
			xl_colCnt = 0;
			//every sheet has rows, iterate over them
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) 
	        {
				List<String> loginList = new ArrayList<String>();
				xl_rowCnt = xl_rowCnt + 1 ;
				//Get the row object
				Row row = rowIterator.next();
				//Every row has columns, get the column iterator and iterate over them
				Iterator<Cell> cellIterator = row.cellIterator();
				xl_colCnt = 0 ;
				if (xl_rowCnt >= rowCnt){ 
		            while (cellIterator.hasNext()) 
		            {
		            	xl_colCnt = xl_colCnt + 1;
		            	Cell cell = cellIterator.next();
		            	if ((xl_colCnt == 1) && (cell.getStringCellValue().trim().isEmpty())){
		            		break;			            	
		            	}
		            	loginList.add(cell.getStringCellValue().trim());
		            } //end of cell iterator
		            if (!loginList.isEmpty()){
		            	xlLogin_List.add(loginList);
		            }
				}
	        } //end of rows iterator
			
			//close file input stream
			fis.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		    
		return xlLogin_List;
	}
	
}
